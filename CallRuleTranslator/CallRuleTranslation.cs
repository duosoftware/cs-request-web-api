﻿using System;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace DuoSoftware.DC.CSRequestWebApi.CRTranslator
{
    public class CallRuleTranslation
    {
        private int _transID;
        private string _translationName;
        private readonly string _lAdd;
        private readonly string _rAdd;
        private readonly int _lRemove;
        private readonly int _rRemove;
        private readonly string _replace;
        private readonly StringBuilder _strBldr;

        log4net.ILog logService = log4net.LogManager.GetLogger("CallRuleValidationServiceLogger");

        public CallRuleTranslation(XElement transInfo)
        {

            var result = (from i in transInfo.Elements("Translation")
                         select new
                                    {
                                        id = i.Element("ID"),
                                        transName = i.Element("TransName"),
                                        lAdd = i.Element("LAdd"),
                                        rAdd = i.Element("RAdd"),
                                        lRemove = i.Element("LRemove"),
                                        rRemove = i.Element("RRemove"),
                                        replace = i.Element("Replace")
                                    }).FirstOrDefault();

            if(result != null)
            {
                logService.Info("Translation found - setting translation parameters");
                _transID = int.Parse(result.id.Value);
                _translationName = result.transName.Value;
                _lAdd = result.lAdd.Value;
                _rAdd = result.rAdd.Value;
                _lRemove = int.Parse(result.lRemove.Value);
                _rRemove = int.Parse(result.rRemove.Value);
                _replace = result.replace.Value;

            }
            else
            {
                logService.Warn("Translation not found");
            }

            
            _strBldr = new StringBuilder();
        }

        public string Translate(string tString)
        {
            try
            {
                if (tString != null)
                {
                    var splitArr = tString.Split(new string[] {"@"}, 2, StringSplitOptions.None);

                    _strBldr.Clear();
                    _strBldr.Append(splitArr[0]);
                    LeftRemove();
                    RightRemove();
                    LeftAdd();
                    RightAdd();
                    Replace();

                    if (splitArr.Length >= 2)
                    {
                        return _strBldr.Append("@" + splitArr[1]).ToString();
                    }
                    else
                    {
                        return _strBldr.ToString();
                    }
                }
                else
                {
                    return "";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LeftRemove()
        {
            try
            {
                if(_lRemove > 0)
                {
                    _strBldr.Remove(0, _lRemove);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RightRemove()
        {
            try
            {
                if (_rRemove > 0)
                {
                    if (_strBldr.Length > _rRemove)
                    {
                        int startIndex = _strBldr.Length - _rRemove - 1;

                        _strBldr.Remove(startIndex, _rRemove);
                    }
                    else
                    {
                        _strBldr.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LeftAdd()
        {
            try
            {
                if(!String.IsNullOrEmpty(_lAdd))
                {
                    _strBldr.Insert(0, _lAdd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RightAdd()
        {
            try
            {
                if (!String.IsNullOrEmpty(_rAdd))
                {
                    _strBldr.Append(_rAdd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Replace()
        {
            try
            {
                if (!String.IsNullOrEmpty(_replace))
                {
                    _strBldr.Clear();
                    _strBldr.Append(_replace);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }





    }
}

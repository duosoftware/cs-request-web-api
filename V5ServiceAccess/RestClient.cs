﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace DuoSoftware.DC.CSRequestWebApi.V5ServiceAccess
{
    public class RestClient
    {
        public static log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");
        public string Post(string url, string payload, string contentType)
        {
            try
            {
                var httpWReq = (HttpWebRequest)WebRequest.Create(url);

                var encoding = new ASCIIEncoding();

                byte[] data = encoding.GetBytes(payload);

                httpWReq.ContentType = contentType;
                httpWReq.Method = "POST";

                using (var stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                using (var response = (HttpWebResponse)httpWReq.GetResponse())
                {
                    string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    return responseString;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public string PostAsync<T>(string url, T payload, string contentType)
        {
            try
            {
                var client = new RestSharp.RestClient(url);

                var request = new RestRequest("DashboardEvent/Event/", Method.POST);

                request.AddHeader("Accept", contentType);

                request.AddJsonBody(payload);

                client.ExecuteAsync(request, i => logCSReq.Debug(String.Format("Dashboard reply -Status Code {0}, Error : {1}", i.StatusCode, i.ErrorException)));
                
                return "";

            }
            catch (Exception ex)
            {
                logCSReq.Error("Exception occurred - ", ex);
                return "";
            }
        }
    }
}

﻿using System;
using System.Linq;
using System.ServiceModel;
using DuoSoftware.DC.CSRequestWebApi.V5ServiceAccess.ProfileServiceRef;

namespace DuoSoftware.DC.CSRequestWebApi.V5ServiceAccess
{
    public static class ProfileServiceAccess
    {
        public static string GetDestinationForType(string guUserId, string epType, string securityToken)
        {
            try
            {

                var client = new ProfileServiceRef.RegistrationsClient();

                try
                {
                    var profInfo = client.searchProfile(RegistrationsHandlerProfileSearch.ProfileID, guUserId, 0, securityToken).FirstOrDefault();

                    if (profInfo != null)
                    {
                        client.Close();
                        return profInfo.Phonenumber;
                    }
                    else
                    {
                        throw new Exception("No profile found");
                    }

                }
                catch (Exception)
                {
                    if (client.State == CommunicationState.Opened)
                    {
                        client.Close();
                    }
                    else
                    {
                        client.Abort();
                    }

                    throw;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}

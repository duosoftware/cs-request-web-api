﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using DuoSoftware.DC.CSRequestWebApi.V5ServiceAccess.ResourceProxyServiceRef;

namespace DuoSoftware.DC.CSRequestWebApi.V5ServiceAccess
{
    public static class ResourceProxyServiceAccess
    {
        static log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceChanStateLogger");

        public static void ResourceModeChangeRequest(string guUserId, string requestType, int companyId, string callUuid, string otherUuid)
        {
            try
            {
                logCSReq.Info(String.Format("-Resource Mode Change Request - guUserId : {0}, reqType : {1}, callUuid : {2}, otherUuid : {3}, companyId : {4}", guUserId, requestType, callUuid, otherUuid, companyId));
                
                var client = new ResourceProxyServiceRef.ResourceProxyServicesClient();

                try
                {
                    switch (requestType)
                    {
                        case "Registered" :
                            {
                                
                                    client.ResourceRegistrationCompleted += (sender, args) =>
                                    {
                                        try
                                        {
                                            if (args.Error != null)
                                            {
                                                logCSReq.Error("RESOURCE PROXY ERROR : ", args.Error);
                                            }
                                            logCSReq.Info(String.Format("RESOURCE PROXY RESULT : {0}:{1}:{2}", args.Result.SessionID, args.Result.Result, args.Result.ResultString));
                                            client.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            if (client.State == CommunicationState.Opened)
                                            {
                                                client.Close();
                                            }
                                            else
                                            {
                                                client.Abort();
                                            }
                                            throw;
                                        }

                                    };

                                    client.ResourceRegistrationAsync("", "", "", "", CommunicationModes.WebService, guUserId, companyId.ToString());
                                
                                
                                break;
                            }
                        case "UnRegistered" :
                            {
                                client.ResourceForceLogoffCompleted += (sender, args) =>
                                    {
                                        try
                                        {
                                            if (args.Error != null)
                                            {
                                                logCSReq.Error("RESOURCE PROXY ERROR : ", args.Error);
                                            }

                                            logCSReq.Info(String.Format("RESOURCE PROXY RESULT : {0}:{1}:{2}", args.Result.SessionID, args.Result.Result, args.Result.ResultString));
                                            client.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            if (client.State == CommunicationState.Opened)
                                            {
                                                client.Close();
                                            }
                                            else
                                            {
                                                client.Abort();
                                            }
                                            throw;
                                        }
                                        
                                    };

                                client.ResourceForceLogoffAsync("", "", "", "", CommunicationModes.WebService, guUserId, companyId.ToString());

                                break;
                            }
                        case "Offline" :
                            {
                                client.SendModeChangeRequestOfflineCompleted += (sender, args) =>
                                    {
                                        try
                                        {
                                            if (args.Error != null)
                                            {
                                                logCSReq.Error("RESOURCE PROXY ERROR : ", args.Error);
                                            }

                                            logCSReq.Info(String.Format("RESOURCE PROXY RESULT : {0}:{1}:{2}", args.Result.SessionID, args.Result.Result, args.Result.ResultString));
                                            client.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            if (client.State == CommunicationState.Opened)
                                            {
                                                client.Close();
                                            }
                                            else
                                            {
                                                client.Abort();
                                            }
                                            throw;
                                        }
                                        
                                    };

                                client.SendModeChangeRequestOfflineAsync("", "", "", "", CommunicationModes.WebService, guUserId, companyId.ToString());

                                logCSReq.Debug(String.Format("[SendModeChangeRequestOfflineAsync] - Resource Proxy Message Sent - GuUserId : {0}, Company : {1}", guUserId, companyId));

                                break;
                            }
                        case "ACTIVE":
                            {
                                client.ResourceStatusChangeBusyCompleted += (sender, args) =>
                                    {
                                        try
                                        {
                                            if (args.Error != null)
                                            {
                                                logCSReq.Error("RESOURCE PROXY ERROR : ", args.Error);
                                            }

                                            logCSReq.Info(String.Format("RESOURCE PROXY RESULT : {0}:{1}:{2}", args.Result.SessionID, args.Result.Result, args.Result.ResultString));
                                            client.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            if (client.State == CommunicationState.Opened)
                                            {
                                                client.Close();
                                            }
                                            else
                                            {
                                                client.Abort();
                                            }
                                            throw;
                                        }
                                        
                                    };

                                var result = client.ResourceStatusChangeBusy("", "", "pabx", "", CommunicationModes.WebService, guUserId, String.Format("{0}#{1}", callUuid, otherUuid), companyId.ToString());

                                client.Close();

                                logCSReq.Info(String.Format("RESOURCE PROXY RESULT : {0}:{1}:{2} - CallUuid : {3}, State : {4}, OtherUuid : {5}", result.SessionID, result.Result, result.ResultString, callUuid, requestType, otherUuid));
                                //client.ResourceStatusChangeBusyAsync("", "", "pabx", "", CommunicationModes.WebService, guUserId, callUuid, companyId.ToString());

                                logCSReq.Debug(String.Format("[ResourceStatusChangeBusyAsync] - Resource Proxy Message Sent - GuUserId : {0}, Company : {1}, CallUuid : {2}, State : {3}, OtherUuid : {4}", guUserId, companyId, callUuid, requestType, otherUuid));

                                break;
                            }

                        case "HANGUP":
                            {
                                client.SendStatusChangeRequestIdelCompleted += (sender, args) =>
                                    {
                                        try
                                        {
                                            if (args.Error != null)
                                            {
                                                logCSReq.Error("RESOURCE PROXY ERROR : ", args.Error);
                                            }

                                            logCSReq.Info(String.Format("RESOURCE PROXY RESULT : {0}:{1}:{2}", args.Result.SessionID, args.Result.Result, args.Result.ResultString));
                                            client.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            if (client.State == CommunicationState.Opened)
                                            {
                                                client.Close();
                                            }
                                            else
                                            {
                                                client.Abort();
                                            }
                                            throw;
                                        }
                                        
                                    };

                                //client.SendStatusChangeRequestIdelAsync("", "", "pabx", "", CommunicationModes.WebService, guUserId, "", companyId.ToString(), callUuid);
                                var result = client.SendStatusChangeRequestIdel("", "", "pabx", "", CommunicationModes.WebService, guUserId, "", companyId.ToString(), String.Format("{0}#{1}", callUuid, otherUuid));

                                client.Close();

                                logCSReq.Info(String.Format("RESOURCE PROXY RESULT : {0}:{1}:{2} - CallUuid : {3}, State : {4}, OtherUuid : {5}", result.SessionID, result.Result, result.ResultString, callUuid, requestType, otherUuid));

                                logCSReq.Debug(String.Format("[SendStatusChangeRequestIdelAsync] - Resource Proxy Message Sent - GuUserId : {0}, Company : {1}, CallUuid : {2}, State : {3}, OtherUuid : {4}", guUserId, companyId, callUuid, requestType, otherUuid));

                                break;
                            }
                        default :
                            {
                                logCSReq.Warn(String.Format("Invalid Request Type : {0}", requestType));
                                break;
                            }
                    }

                }
                catch (Exception ex)
                {
                    logCSReq.Error(String.Format("Exception Occurred : CallId : {0}, RequestType : {1}", callUuid, requestType), ex);
                    if (client.State == CommunicationState.Opened)
                    {
                        client.Close();
                    }
                    else
                    {
                        client.Abort();
                    }

                    throw;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static void ResourceModeChangeRequestPost(string guUserId, string requestType, int companyId, string callUuid, string otherUuid, string securityToken)
        {
            try
            {
                logCSReq.Info(String.Format("-Resource Mode Change Request - guUserId : {0}, reqType : {1}, callUuid : {2}, otherUuid : {3}, companyId : {4}", guUserId, requestType, callUuid, otherUuid, companyId));

                var client = new ResourceProxyServiceRef.ResourceProxyServicesClient();

                try
                {
                    var rpUrl = ConfigurationManager.AppSettings.Get("ResourceProxyRestUrl");

                    if (!string.IsNullOrEmpty(rpUrl))
                    {
                        var restclient = new RestClient();

                        int sequencenumber = 0;

                        switch (requestType)
                        {
                            case "Registered":
                                {

                                    string url = string.Format("{0}login", rpUrl);

                                    string payload = "{\"_securityToken\":\"" + securityToken + "\",\"_interactionType\":\"pabx\",\"_deviceId\":\"\",\"_resourceId\":\"" + guUserId + "\"}";
                                    
                                    var response = restclient.Post(url, payload, "application/json");

                                    logCSReq.Debug(String.Format("RESOURCE PROXY RESULT : {0}", response));

                                    break;
                                }
                            case "UnRegistered":
                                {
                                    string url = string.Format("{0}logoffRequest", rpUrl);

                                    string payload = "{\"_securityToken\":\"" + securityToken + "\",\"_interactionType\":\"pabx\",\"_deviceId\":\"\",\"_resourceId\":\"" + guUserId + "\"}";

                                    var response = restclient.Post(url, payload, "application/json");

                                    logCSReq.Debug(String.Format("RESOURCE PROXY RESULT : {0}", response));

                                    break;
                                }
                            case "Offline":
                                {
                                    string url = string.Format("{0}modeChangeRequest", rpUrl);
                                                      
                                    string payload = "{\"_securityToken\":\"" + securityToken + "\",\"_interactionType\":\"pabx\",\"_deviceId\":\"\",\"_resourceId\":\"" + guUserId + "\",\"_requestMode\":\"offline\"}";

                                    var response = restclient.Post(url, payload, "application/json");

                                    logCSReq.Debug(String.Format("RESOURCE PROXY RESULT : {0}", response));
                                    
                                    break;
                                }
                            case "ACTIVE":
                                {
                                    string url = string.Format("{0}stateChangeRequest", rpUrl);

                                    string payload = "{\"_securityToken\":\"" + securityToken + "\",\"_interactionType\":\"pabx\",\"_deviceId\":\"\",\"sequenceNumber\":" + sequencenumber + ",\"_resourceId\":\"" + guUserId + "\",\"_requestState\":\"busy\",\"_otherData\":\"" + String.Format("{0}#{1}", callUuid, otherUuid) + "\"}";

                                    var response = restclient.Post(url, payload, "application/json");

                                    logCSReq.Debug(String.Format("RESOURCE PROXY RESULT : {0}", response));


                                    break;
                                }

                            case "HANGUP":
                                {

                                    string url = string.Format("{0}stateChangeRequest", rpUrl);
                                    
                                    string payload = "{\"_securityToken\":\"" + securityToken + "\",\"_interactionType\":\"pabx\",\"_deviceId\":\"\",\"sequenceNumber\":" + sequencenumber + ",\"_resourceId\":\"" + guUserId + "\",\"_requestState\":\"idle\",\"_otherData\":\"" + String.Format("{0}#{1}", callUuid, otherUuid) + "\"}";

                                    var response = restclient.Post(url, payload, "application/json");

                                    logCSReq.Debug(String.Format("RESOURCE PROXY RESULT : {0}", response));

                                    

                                    break;
                                }
                            default:
                                {
                                    logCSReq.Warn(String.Format("Invalid Request Type : {0}", requestType));
                                    break;
                                }
                        }
                    }
                    else
                    {
                        logCSReq.Error("RESOURCE PROXY URL NOT SET");
                    }

                }
                catch (Exception ex)
                {
                    logCSReq.Error(String.Format("Exception Occurred : CallId : {0}, RequestType : {1}", callUuid, requestType), ex);
                    if (client.State == CommunicationState.Opened)
                    {
                        client.Close();
                    }
                    else
                    {
                        client.Abort();
                    }

                    throw;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}

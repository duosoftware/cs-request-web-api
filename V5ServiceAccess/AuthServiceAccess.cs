﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using DuoSoftware.CommonTools.Security;

namespace DuoSoftware.DC.CSRequestWebApi.V5ServiceAccess
{
    public class AuthRealTimeData
    {
        public DateTime ExpireTime { get; set; }
        public string SecurityToken { get; set; }
    }

    public class AuthServiceAccess
    {
        private static AuthServiceAccess _instance;

        private static Dictionary<int, AuthRealTimeData> _authList;

        private static string _authUserName;
        private static string _authPassword;
        private static bool _checkTokenExpiration;

        static log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceChanStateLogger");

        private AuthServiceAccess()
        {
            _authList = new Dictionary<int, AuthRealTimeData>();
            _authUserName = ConfigurationManager.AppSettings.Get("AuthUser");
            _authPassword = ConfigurationManager.AppSettings.Get("AuthPwd");
            _checkTokenExpiration = bool.Parse(ConfigurationManager.AppSettings.Get("TokenExp"));

            FillUpTokenList();

            logCSReq.Debug(String.Format("Auth token list contains {0} elements", _authList.Count));
        }

        public static AuthServiceAccess Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AuthServiceAccess();
                }
                return _instance;
            }
        }

        private static void FillUpTokenList()
        {
            try
            {
                var client = new IauthClient();

                try
                {
                    lock (_authList)
                    {
                        if (_authList.Count == 0)
                        {
                            var authInfo = client.login(_authUserName, _authPassword, -2, "");

                            if (authInfo != null)
                            {
                                foreach (var comp in authInfo.CompanyIDs)
                                {
                                    try
                                    {
                                        var authInfoTemp = client.login(_authUserName, _authPassword, comp, "");

                                        var authData = new AuthRealTimeData
                                        {
                                            ExpireTime = authInfoTemp.TokenExpireOn,
                                            SecurityToken = authInfoTemp.SecurityToken
                                        };
                                        AuthRealTimeData tempAuthData;

                                        if (!_authList.TryGetValue(comp, out tempAuthData))
                                        {
                                            _authList.Add(comp, authData);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        logCSReq.Debug(String.Format("Expired - Exception: {0}", ex));
                                    }
                                }
                            }
                        }
                    }

                    client.Close();

                }
                catch (Exception ex)
                {
                    logCSReq.Debug(String.Format("Expired - Exception: {0}", ex));
                    if (client.State == CommunicationState.Opened)
                    {
                        client.Close();
                    }
                    else
                    {
                        client.Abort();
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public UserAuth GetAccess(string securityToken)
        {
            var authInfo = DuoSoftware.CommonTools.Common.GetAccess(securityToken, "");

            return authInfo;
        }

        public string GetSecurityToken(int companyId)
        {
            try
            {
                logCSReq.Debug(String.Format("GetSecurityToken : {0}", companyId));

                AuthRealTimeData authData;
                if (_authList.TryGetValue(companyId, out authData))
                {
                    logCSReq.Debug(String.Format("TryGetValue True: {0}", companyId));

                    if (_checkTokenExpiration)
                    {
                        if (authData.ExpireTime >= DateTime.Now.Subtract(new TimeSpan(0, 10, 0)))
                        {
                            logCSReq.Debug(String.Format("Expired : {0}", companyId));
                            var client = new IauthClient();

                            try
                            {
                                logCSReq.Debug(String.Format("Expired - Pre Login: {0}", companyId));
                                var authInfo = client.login(_authUserName, _authPassword, companyId, "");

                                logCSReq.Debug(String.Format("Expired - Post Login: {0}", companyId));

                                authData.ExpireTime = authInfo.TokenExpireOn;
                                authData.SecurityToken = authInfo.SecurityToken;

                                client.Close();

                            }
                            catch (Exception ex)
                            {
                                logCSReq.Debug(String.Format("Expired - Exception: {0}", ex));
                                if (client.State == CommunicationState.Opened)
                                {
                                    client.Close();
                                }
                                else
                                {
                                    client.Abort();
                                }

                                return authData.SecurityToken;
                            }
                        }

                        logCSReq.Debug(String.Format("Expired - New Token Returned : {0}", authData.SecurityToken));
                    }

                    return authData.SecurityToken;
                }
                else
                {
                    throw new Exception("Company ID Not found");
                }
                //else
                //{
                //    logCSReq.Debug(String.Format("TryGetValue False : {0}", companyId));
                //    var client = new AuthServiceRef.IauthClient();

                //    try
                //    {
                //        logCSReq.Debug(String.Format("Pre Login: {0}", companyId));
                //        var authInfo = client.login(_authUserName, _authPassword, companyId, "");

                //        logCSReq.Debug(String.Format("Post Login: {0}", companyId));

                //        authData = new AuthRealTimeData
                //            {
                //                ExpireTime = authInfo.TokenExpireOn,
                //                SecurityToken = authInfo.SecurityToken
                //            };

                //        logCSReq.Debug(String.Format("Set AuthRealTimeData : {0}", companyId));

                //        client.Close();

                //    }
                //    catch (Exception ex)
                //    {
                //        logCSReq.Debug(String.Format("Expired - Exception: {0}", ex));
                //        if (client.State == CommunicationState.Opened)
                //        {
                //            client.Close();
                //        }
                //        else
                //        {
                //            client.Abort();
                //        }

                //        return "";
                //    }

                //    logCSReq.Debug(String.Format("Pre - Add to Dictionary : {0}", companyId));

                //    AuthRealTimeData tempAuthObj;

                //    lock (_authList)
                //    {
                //        if (!_authList.TryGetValue(companyId, out tempAuthObj))
                //        {
                //            logCSReq.Debug(String.Format("Pre - Add to Dictionary - 2nd Try Get Fail : {0}", companyId));
                //            _authList.Add(companyId, authData);
                //        }
                //        else
                //        {
                //            logCSReq.Debug(String.Format("Pre - Add to Dictionary - 2nd Try Get Pass - Returning : {0}", companyId));
                //            return tempAuthObj.SecurityToken;
                //        }
                //    }

                //    logCSReq.Debug(String.Format("Post - Add to Dictionary : {0}", companyId));


                //    logCSReq.Debug(String.Format(" New Token Returned : {0}", authData.SecurityToken));

                //    return authData.SecurityToken;
                //}
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DuoSoftware.DC.CSRequestWebApi.V5ServiceAccess
{
    public static class ArdsComServiceAccess
    {
        static log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public static void ResourceModeChangeRequest(string securityToken, string requestType, string callUuid, string otherUuid)
        {
            try
            {

                var client = new ArdsServiceRef.RequestServerContractsClient();

                try
                {
                    switch (requestType)
                    {
                        case "ACTIVE":
                        case "HANGUP":
                            {
                                client.RemoveRequestCompleted += (sender, args) =>
                                {
                                    try
                                    {
                                        logCSReq.Info(String.Format("ARDS REMOVE REQ RESULT : {0}", args.Result));
                                        client.Close();
                                    }
                                    catch (Exception ex)
                                    {
                                        if (client.State == CommunicationState.Opened)
                                        {
                                            client.Close();
                                        }
                                        else
                                        {
                                            client.Abort();
                                        }
                                        throw;
                                    }

                                };

                                client.RemoveRequestAsync(securityToken, 0, String.Format("{0}#{1}", callUuid, otherUuid), "", "");

                                break;
                            }
                        default:
                            {
                                logCSReq.Warn(String.Format("Invalid Request Type : {0}", requestType));
                                break;
                            }
                    }

                }
                catch (Exception)
                {
                    if (client.State == CommunicationState.Opened)
                    {
                        client.Close();
                    }
                    else
                    {
                        client.Abort();
                    }

                    throw;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DuoSoftware.DC.CSRequestWebApi.V5ServiceAccess
{
    public static class ResourceMonitorServiceAccess
    {
        static log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public static int GetMaxConcurrencyForPabxAgent(string securityToken, string guUserId)
        {
            logCSReq.Debug(String.Format("Get max concurrency - securityToken : {0}, guUserID : {1} ", securityToken, guUserId));
            var client = new ResourceMonitorServiceRef.ResourceMonitoringClient();
            try
            {
                var agentDetail = client.GetOnlineAgentDetail(securityToken, guUserId);

                if (agentDetail != null && agentDetail.ConcurrencyInfo != null)
                {
                    var concForPabx = agentDetail.ConcurrencyInfo.FirstOrDefault(i => i.RequestType.ToUpper() == "PABX");

                    if (concForPabx != null)
                    {
                        client.Close();
                        return concForPabx.Max;
                    }
                    else
                    {
                        logCSReq.Warn("Concurrency info request type pabx not found");
                    }
                    
                }
                else
                {
                    logCSReq.Warn("Agent detail or concurrency info not found");
                }
                client.Close();

                return 0;
            }
            catch (Exception)
            {
                if (client.State == CommunicationState.Opened)
                {
                    client.Close();
                }
                else
                {
                    client.Abort();
                }

                throw;
            }
        }
    }
}

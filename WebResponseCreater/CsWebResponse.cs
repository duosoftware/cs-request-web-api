﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;

namespace DuoSoftware.DC.CSRequestWebApi.CSWebResponseCreater
{
    public static class CsWebResponse
    {
        public static log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public static XElement CreateDirectoryGroupProfile(GroupInfo grpInfo, string grpExt)
        {
            try
            {
                logCSReq.Info("Creating Directory Profile - User");

                var xElemUsrs = new XElement("users");
                var xElemUsersUp = new XElement("users");

                var xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "directory"),
                                                     new XElement("domain", new XAttribute("name", grpInfo.Domain), xElemUsersUp,
                                                                  new XElement("groups",
                                                                               new XElement("group", new XAttribute("name", grpExt), xElemUsrs))))); //grpInfo.GroupName

                foreach (var user in grpInfo.Users)
                {


                    xElemUsersUp.Add(new XElement("user", new XAttribute("id", user.Username), new XAttribute("cacheable", "false"), new XAttribute("number-alias", user.Extension),
                                                  new XElement("params",
                                                               new XElement("param", new XAttribute("name", "dial-string"), new XAttribute("value", "{sip_invite_domain=${domain_name},presence_id=${dialed_user}@${dialed_domain}}${sofia_contact(${dialed_user}@${dialed_domain})}")),
                                                               new XElement("param", new XAttribute("name", "password"), new XAttribute("value", user.Password))),
                                                  new XElement("variables",
                                                               new XElement("variable", new XAttribute("name", "domain"), new XAttribute("value", user.Domain)),
                                                               new XElement("variable", new XAttribute("name", "user_context"), new XAttribute("value", user.Context)),
                                                               new XElement("variable", new XAttribute("name", "user_id"), new XAttribute("value", user.Username)))));

                    xElemUsrs.Add(new XElement("user", new XAttribute("id", user.Extension), new XAttribute("type", "pointer"))); //user.Username

                }


                return xlem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static XElement CreateDirectoryUserProfile(string username, string password, string domain, string context, string extension, string email)
        {
            try
            {
                logCSReq.Info("Creating Directory Profile - User");

                var isCacheble = ConfigurationManager.AppSettings.Get("CacheDirectoryProfileInfo");

                if(string.IsNullOrEmpty(isCacheble))
                {
                    isCacheble = "false";
                }

                var xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "directory"),
                                                     new XElement("domain", new XAttribute("name", domain),
                                                                  new XElement("user", new XAttribute("id", username), new XAttribute("cacheable", isCacheble), new XAttribute("number-alias", extension),
                                                                                            new XElement("params",
                                                                                                new XElement("param", new XAttribute("name", "dial-string"), new XAttribute("value", "{sip_invite_domain=${domain_name},presence_id=${dialed_user}@${dialed_domain}}${sofia_contact(${dialed_user}@${dialed_domain})}")),
                                                                                                new XElement("param", new XAttribute("name", "password"), new XAttribute("value", password)),
                                                                                                new XElement("param", new XAttribute("name", "vm-email-all-messages"), new XAttribute("value", "true")),
                                                                                                new XElement("param", new XAttribute("name", "vm-attach-file"), new XAttribute("value", "true")),
                                                                                                new XElement("param", new XAttribute("name", "vm-mailto"), new XAttribute("value", email))),
                                                                                            new XElement("variables",
                                                                                                         new XElement("variable", new XAttribute("name", "domain"), new XAttribute("value", domain)),
                                                                                                         new XElement("variable", new XAttribute("name", "user_context"), new XAttribute("value", context)),
                                                                                                         new XElement("variable", new XAttribute("name", "user_id"), new XAttribute("value", username)))))));


                return xlem;
                

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static XElement CreateDirectorySipGateways(List<TrunkInfo> sipTrunkList)
        {
            try
            {
                logCSReq.Info("Creating Directory Profile - Gateways");

                var secXelem = new XElement("section", new XAttribute("name", "directory"));

                var gwXelem = new XElement("document", new XAttribute("type", "freeswitch/xml"), secXelem);

                foreach (var trunkInfo in sipTrunkList)
                {
                    string proxy = trunkInfo.IpUrl;

                    if (!String.IsNullOrEmpty(trunkInfo.SipServer))
                    {
                        proxy = trunkInfo.SipServer;
                    }

                    if (trunkInfo.PingSeconds > 0)
                    {
                        string registerReq = "";
                        

                        if (trunkInfo.RegDir == RegistrationDirection.None)
                        {
                            registerReq = "false";
                        }
                        else if (trunkInfo.RegDir == RegistrationDirection.RegisterOut)
                        {
                            registerReq = "true";
                        }
                        secXelem.Add(new XElement("domain", new XAttribute("name", trunkInfo.Domain),
                                                  new XElement("params",
                                                               new XElement("param", new XAttribute("name", "dial-string"), new XAttribute("value", "{presence_id=${dialed_user}${dialed_domain}}${sofia_contact(${dialed_user}${dialed_domain})}"))),
                                                  new XElement("variables",
                                                               new XElement("variable")),
                                                  new XElement("user", new XAttribute("id", trunkInfo.Username),
                                                               new XElement("gateways",
                                                                            new XElement("gateway", new XAttribute("name", trunkInfo.TrunkCode),
                                                                                         new XElement("param", new XAttribute("name", "username"), new XAttribute("value", trunkInfo.Username)),
                                                                                         new XElement("param", new XAttribute("name", "auth-username"), new XAttribute("value", trunkInfo.Username)),
                                                                                         new XElement("param", new XAttribute("name", "realm"), new XAttribute("value", trunkInfo.IpUrl)),
                                                                                         new XElement("param", new XAttribute("name", "proxy"), new XAttribute("value", proxy)),
                                                                                         new XElement("param", new XAttribute("name", "register-proxy"), new XAttribute("value", trunkInfo.IpUrl)),
                                                                                         new XElement("param", new XAttribute("name", "register-transport"), new XAttribute("value", "udp")),
                                                                                         new XElement("param", new XAttribute("name", "caller-id-in-from"), new XAttribute("value", "true")),
                                                                                         //new XElement("param", new XAttribute("name", "contact-params"), new XAttribute("value", trunkInfo.Username)),
                                                                                         //new XElement("param", new XAttribute("name", "contact-params"), new XAttribute("value", trunkInfo.IpUrl)),
                                                                                         //new XElement("param", new XAttribute("name", "contact-params"), new XAttribute("value", trunkInfo.Username + "@" + trunkInfo.IpUrl)),
                                                                                         new XElement("param", new XAttribute("name", "ping"), new XAttribute("value", trunkInfo.PingSeconds)),
                                                                                         new XElement("param", new XAttribute("name", "password"), new XAttribute("value", trunkInfo.Password)),
                                                                                         new XElement("param", new XAttribute("name", "from-user"), new XAttribute("value", trunkInfo.Username)),
                                                                                         new XElement("param", new XAttribute("name", "from-domain"), new XAttribute("value", trunkInfo.Domain)),
                                                                                         new XElement("param", new XAttribute("name", "expire-seconds"), new XAttribute("value", trunkInfo.ExpireSeconds)),
                                                                                         new XElement("param", new XAttribute("name", "retry-seconds"), new XAttribute("value", trunkInfo.RetrySeconds)),
                                                                                         new XElement("param", new XAttribute("name", "context"), new XAttribute("value", "public")),
                                                                                         new XElement("param", new XAttribute("name", "register"), new XAttribute("value", registerReq)),
                                                                                         new XElement("param", new XAttribute("name", "auth-calls"), new XAttribute("value", "false")),
                                                                                         new XElement("param", new XAttribute("name", "apply-register-acl"), new XAttribute("value", "provider")))),
                                                               new XElement("params",
                                                                            new XElement("param", new XAttribute("name", "password"), new XAttribute("value", trunkInfo.Password))))));
                    }
                    else
                    {
                        string registerReq = "";
                        if (trunkInfo.RegDir == RegistrationDirection.None)
                        {
                            registerReq = "false";
                        }
                        else if (trunkInfo.RegDir == RegistrationDirection.RegisterOut)
                        {
                            registerReq = "true";
                        }
                        secXelem.Add(new XElement("domain", new XAttribute("name", trunkInfo.Domain),
                                             new XElement("params",
                                                          new XElement("param", new XAttribute("name", "dial-string"), new XAttribute("value", "{presence_id=${dialed_user}${dialed_domain}}${sofia_contact(${dialed_user}${dialed_domain})}"))),
                                             new XElement("variables",
                                                          new XElement("variable")),
                                             new XElement("user", new XAttribute("id", trunkInfo.Username),
                                                 new XElement("gateways",
                                                     new XElement("gateway", new XAttribute("name", trunkInfo.TrunkCode),
                                                         new XElement("param", new XAttribute("name", "username"), new XAttribute("value", trunkInfo.Username)),
                                                         new XElement("param", new XAttribute("name", "auth-username"), new XAttribute("value", trunkInfo.Username)),
                                                         new XElement("param", new XAttribute("name", "realm"), new XAttribute("value", trunkInfo.IpUrl)),
                                                         new XElement("param", new XAttribute("name", "proxy"), new XAttribute("value", proxy)),
                                                         new XElement("param", new XAttribute("name", "register-proxy"), new XAttribute("value", trunkInfo.IpUrl)),
                                                         new XElement("param", new XAttribute("name", "register-transport"), new XAttribute("value", "udp")),
                                                         new XElement("param", new XAttribute("name", "caller-id-in-from"), new XAttribute("value", "true")),
                                                         //new XElement("param", new XAttribute("name", "contact-params"), new XAttribute("value", trunkInfo.Username)),
                                                         //new XElement("param", new XAttribute("name", "contact-params"), new XAttribute("value", trunkInfo.IpUrl)),
                                                         //new XElement("param", new XAttribute("name", "contact-params"), new XAttribute("value", trunkInfo.Username + "@" + trunkInfo.IpUrl)),
                                                         new XElement("param", new XAttribute("name", "password"), new XAttribute("value", trunkInfo.Password)),
                                                         new XElement("param", new XAttribute("name", "from-user"), new XAttribute("value", trunkInfo.Username)),
                                                         new XElement("param", new XAttribute("name", "from-domain"), new XAttribute("value", trunkInfo.Domain)),
                                                         new XElement("param", new XAttribute("name", "expire-seconds"), new XAttribute("value", trunkInfo.ExpireSeconds)),
                                                         new XElement("param", new XAttribute("name", "retry-seconds"), new XAttribute("value", trunkInfo.RetrySeconds)),
                                                         new XElement("param", new XAttribute("name", "context"), new XAttribute("value", "public")),
                                                         new XElement("param", new XAttribute("name", "register"), new XAttribute("value", registerReq)),
                                                         new XElement("param", new XAttribute("name", "auth-calls"), new XAttribute("value", "false")),
                                                         new XElement("param", new XAttribute("name", "apply-register-acl"), new XAttribute("value", "provider")))),
                                                         new XElement("params",
                                                             new XElement("param", new XAttribute("name", "password"), new XAttribute("value", trunkInfo.Password))))));
                    }

                }


                return gwXelem;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static XElement CreateHoldMusicConf(List<HoldusicInfo> hmList)
        {
            try
            {
                var hmXElem = new XElement("configuration", new XAttribute("name", "local_stream.conf"), new XAttribute("description", "stream files from local dir"));

                var xElem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "configuration"), hmXElem));

                foreach (var holdusicInfo in hmList)
                {
                    hmXElem.Add(new XElement("directory", new XAttribute("name", holdusicInfo.HoldMusicName), new XAttribute("path", holdusicInfo.HoldMusicPath),
                        new XElement("param", new XAttribute("name", "rate"), new XAttribute("value", holdusicInfo.HoldMusicName)),
                        new XElement("param", new XAttribute("name", "shuffle"), new XAttribute("value", "true")),
                        new XElement("param", new XAttribute("name", "channels"), new XAttribute("value", "1")),
                        new XElement("param", new XAttribute("name", "interval"), new XAttribute("value", "20")),
                        new XElement("param", new XAttribute("name", "timer-name"), new XAttribute("value", "soft"))));
                }

                return xElem;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static XElement CreateVoicemailConf(List<string> profile)
        {
            try
            {
                var str = new StringBuilder();

                str.Append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
                str.AppendLine("<document type=\"freeswitch/xml\">");
                str.AppendLine("<section name=\"configuration\">");
                str.AppendLine("<configuration name=\"voicemail.conf\" description=\"Voicemail\">");
                str.AppendLine("<profiles>");

                foreach (var profName in profile)
                {
                    str.AppendLine(String.Format("<profile name=\"{0}\">", profName));
                    str.AppendLine("<param name=\"file-extension\" value=\"wav\"/>");
                    str.AppendLine("<param name=\"terminator-key\" value=\"#\"/>");
                    str.AppendLine("<param name=\"max-login-attempts\" value=\"3\"/>");
                    str.AppendLine("<param name=\"digit-timeout\" value=\"10000\"/>");
                    str.AppendLine("<param name=\"min-record-len\" value=\"3\"/>");
                    str.AppendLine("<param name=\"max-record-len\" value=\"300\"/>");
                    str.AppendLine("<param name=\"max-retries\" value=\"3\"/>");
                    str.AppendLine("<param name=\"tone-spec\" value=\"%(1000, 0, 640)\"/>");
                    str.AppendLine("<param name=\"callback-dialplan\" value=\"XML\"/>");
                    str.AppendLine("<param name=\"callback-context\" value=\"default\"/>");
                    str.AppendLine("<param name=\"play-new-messages-key\" value=\"1\"/>");
                    str.AppendLine("<param name=\"play-saved-messages-key\" value=\"2\"/>");
                    str.AppendLine("<param name=\"login-keys\" value=\"0\"/>");
                    str.AppendLine("<param name=\"main-menu-key\" value=\"0\"/>");
                    str.AppendLine("<param name=\"config-menu-key\" value=\"5\"/>");
                    str.AppendLine("<param name=\"record-greeting-key\" value=\"1\"/>");
                    str.AppendLine("<param name=\"choose-greeting-key\" value=\"2\"/>");
                    str.AppendLine("<param name=\"change-pass-key\" value=\"6\"/>");
                    str.AppendLine("<param name=\"record-name-key\" value=\"3\"/>");
                    str.AppendLine("<param name=\"record-file-key\" value=\"3\"/>");
                    str.AppendLine("<param name=\"listen-file-key\" value=\"1\"/>");
                    str.AppendLine("<param name=\"save-file-key\" value=\"2\"/>");
                    str.AppendLine("<param name=\"delete-file-key\" value=\"7\"/>");
                    str.AppendLine("<param name=\"undelete-file-key\" value=\"8\"/>");
                    str.AppendLine("<param name=\"email-key\" value=\"4\"/>");
                    str.AppendLine("<param name=\"pause-key\" value=\"0\"/>");
                    str.AppendLine("<param name=\"restart-key\" value=\"1\"/>");
                    str.AppendLine("<param name=\"ff-key\" value=\"6\"/>");
                    str.AppendLine("<param name=\"rew-key\" value=\"4\"/>");
                    str.AppendLine("<param name=\"skip-greet-key\" value=\"#\"/>");
                    str.AppendLine("<param name=\"previous-message-key\" value=\"1\"/>");
                    str.AppendLine("<param name=\"next-message-key\" value=\"3\"/>");
                    str.AppendLine("<param name=\"skip-info-key\" value=\"*\"/>");
                    str.AppendLine("<param name=\"repeat-message-key\" value=\"0\"/>");
                    str.AppendLine("<param name=\"record-silence-threshold\" value=\"200\"/>");
                    str.AppendLine("<param name=\"record-silence-hits\" value=\"2\"/>");
                    str.AppendLine("<param name=\"web-template-file\" value=\"web-vm.tpl\"/>");
                    str.AppendLine("<param name=\"db-password-override\" value=\"false\"/>");
                    str.AppendLine("<param name=\"allow-empty-password-auth\" value=\"true\"/>");
                    str.AppendLine("<param name=\"operator-extension\" value=\"operator XML default\"/>");
                    str.AppendLine("<param name=\"operator-key\" value=\"9\"/>");
                    str.AppendLine("<param name=\"vmain-extension\" value=\"vmain XML default\"/>");
                    str.AppendLine("<param name=\"vmain-key\" value=\"*\"/>");
                    str.AppendLine("<email>");
                    str.AppendLine("<param name=\"template-file\" value=\"voicemail.tpl\"/>");
                    str.AppendLine("<param name=\"notify-template-file\" value=\"notify-voicemail.tpl\"/>");
                    str.AppendLine("<param name=\"date-fmt\" value=\"%A, %B %d %Y, %I %M %p\"/>");
                    str.AppendLine("<param name=\"email-from\" value=\"${voicemail_account}@${voicemail_domain}\"/>");
                    str.AppendLine("</email>");
                    str.AppendLine("</profile>");
                }
                
                str.AppendLine("</profiles>");
                str.AppendLine("</configuration>"); 
                str.AppendLine("</section>");
                str.AppendLine("</document>");

                var xl = XElement.Parse(str.ToString());

                return xl;
    
                //using (StreamReader sr = new StreamReader("C:\\Logs\\voicemail.conf.xml"))
                //{
                //    String line = sr.ReadToEnd();
                //    var xelem = XElement.Parse(line);


                //    return xelem;
                //}
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static XElement CreateNetworkList(List<TrunkInfo> sipTrunkList)
        {
            try
            {
                logCSReq.Info("Creating Directory Profile - Network-List");

                var listXElem = new XElement("list", new XAttribute("name", "provider"), new XAttribute("default", "deny"));

                var netLstXelem = new XElement("network-lists", listXElem);

                var secXelem = new XElement("section", new XAttribute("name", "directory"), netLstXelem);

                var gwXelem = new XElement("document", new XAttribute("type", "freeswitch/xml"), secXelem);

                foreach (var trunkInfo in sipTrunkList)
                {
                    listXElem.Add(new XElement("node", new XElement("param", new XAttribute("type", "allow"), new XAttribute("cidr", trunkInfo.IpUrl + "/32"))));
                }

                return gwXelem;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static XElement CreateDialplanToUrl(string destinationPattern, string context, int companyId, string phnNum, int maxCalls, string custVarString)
        {
            try
            {
                var url = ConfigurationManager.AppSettings.Get("VoiceServerUrl");

                var xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", context),
                               new XElement("extension",
                                   new XAttribute("name", "test"),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", destinationPattern),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", custVarString)),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "OperationType=IVR")),
                                       new XElement("action",
                                           new XAttribute("application", "limit"),
                                            new XAttribute("data", string.Format("hash {0} {1} {2} !USER BUSY", companyId, phnNum, maxCalls))),
                                       new XElement("action",
                                            new XAttribute("application", "answer")),
                                       new XElement("action",
                                           new XAttribute("application", "httapi"),
                                            new XAttribute("data", string.Format("{{url={0}}}", url))))
                                       ))));


                return xlem;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public static XElement CreateDialplanHangup(string destinationPattern, string context, int companyId, int tenantId, CallerCalleeInfo callerCalleeInfo, string fromGuUserId)
        {
            try
            {
                string tempCustCompStr = String.Format("CustomCompanyStr={0}_{1}", companyId, tenantId);
                string tempFromGuUser = String.Format("FromGuUserId={0}", fromGuUserId);

                string tempFromUser = String.Format("FromUser={0}", callerCalleeInfo.FromUser);
                string tempFromNumber = String.Format("FromNumber={0}", callerCalleeInfo.FromNumber);
                string tempToNumber = String.Format("ToNumber={0}", callerCalleeInfo.ToNumber);
                string tempToUser = String.Format("ToUser={0}", callerCalleeInfo.ToUser);

                
                var xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", context),
                               new XElement("extension",
                                   new XAttribute("name", "test"),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", destinationPattern),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=User")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromGuUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToUser)),
                                       new XElement("action",
                                           new XAttribute("application", "hangup"),
                                            new XAttribute("data", "USER_BUSY")))))));


                return xlem;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public static XElement CreateDialplanToUrl(string destinationPattern, string context)
        {
            try
            {
                var url = ConfigurationManager.AppSettings.Get("VoiceServerUrl");

                var xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", context),
                               new XElement("extension",
                                   new XAttribute("name", "test"),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", destinationPattern),
                                       new XElement("action",
                                            new XAttribute("application", "answer")),
                                       new XElement("action",
                                           new XAttribute("application", "httapi"),
                                            new XAttribute("data", string.Format("{{url={0}}}", url))))))));


                return xlem;

            }
            catch (Exception)
            {
                throw;
            }
        }

        //public static XElement CreateDialplan(string destinationPattern, string context, string tcpIp, string tcpPort, int companyId, string phnNum, int maxCalls)
        //{
        //    try
        //    {
        //        var xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
        //                                new XElement("section", new XAttribute("name", "dialplan"),
        //                   new XAttribute("description", "RE Dial Plan For FreeSwitch"),
        //                   new XElement("context",
        //                       new XAttribute("name", context),
        //                       new XElement("extension",
        //                           new XAttribute("name", "test"),
        //                           new XElement("condition",
        //                               new XAttribute("field", "destination_number"),
        //                               new XAttribute("expression", destinationPattern),
        //                               new XElement("action",
        //                                   new XAttribute("application", "limit"),
        //                                    new XAttribute("data", string.Format("hash {0} {1} {2} !USER BUSY", companyId, phnNum, maxCalls))),
        //                               new XElement("action",
        //                                    new XAttribute("application", "socket"),
        //                                    new XAttribute("data", string.Format("{0}:{1} async full", tcpIp, tcpPort)))
        //                                    )))));


        //        return xlem;

        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public static XElement CreateDialplan(string destinationPattern, string context, string tcpIp, string tcpPort)
        {
            try
            {
                var xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", context),
                               new XElement("extension",
                                   new XAttribute("name", "test"),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", destinationPattern),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "OperationType=CallController")),
                                       new XElement("action",
                                            new XAttribute("application", "socket"),
                                            new XAttribute("data", string.Format("{0}:{1} async full", tcpIp, tcpPort))))))));


                return xlem;

            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public static XElement CreatePBXFeatures(string hdNum, string pbxType, string domain)
        {
            try
            {
                var xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", "PBXFeatures"),
                               new XElement("extension",
                                   new XAttribute("name", hdNum),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", "^" + hdNum +"$"),
                                       new XElement("action",
                                            new XAttribute("application", "read"),
                                            new XAttribute("data", "3 4 'tone_stream://%(10000,0,350,440)' digits 30000 #")),
                                            new XElement("action",
                                            new XAttribute("application", "set"),
                                            new XAttribute("data", "origination_cancel_key=#")),
                                            new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "transfer_ringback=$${us-ring}")),
                                            //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "transfer_ringback=file_string://$${hold_music}")),
                                           // new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=local_stream://connecting")),
                                            //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                                            //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                                            //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "5 ab s execute_extension::att_xfer_speed_dial XML PBXFeatures")),
                                            //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                            new XElement("action",
                                            new XAttribute("application", "att_xfer"),
                                            new XAttribute("data", pbxType + "/${digits}@" + domain)))))));


                return xlem;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static XElement ConferenceTransfer(string hdNum, string domain, string context)
        {
            try
            {
                //XElement modeXElem = new XElement("action", new XAttribute("application", "conference"), new XAttribute("data", string.Format("${{digits}}")));

                XElement modeXElem = new XElement("action", new XAttribute("application", "transfer"), new XAttribute("data", string.Format("-bleg ${{digits}} XML {0}", context)));

                var xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", "PBXFeatures"),
                               new XElement("extension",
                                   new XAttribute("name", hdNum),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", "^" + hdNum + "$"),
                                       new XElement("action",
                                            new XAttribute("application", "read"),
                                            new XAttribute("data", "3 4 'tone_stream://%(10000,0,350,440)' digits 30000 #")),
                                            new XElement("action",
                                            new XAttribute("application", "set"),
                                            new XAttribute("data", "origination_cancel_key=#")),
                                            new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "transfer_ringback=$${us-ring}")),
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "5 ab s execute_extension::att_xfer_speed_dial XML PBXFeatures")),
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                            modeXElem)))));

                return xlem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static XElement CreatePBXFeaturesPark(string hdNum, string context)
        {
            try
            {
                var xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", "PBXFeatures"),
                               new XElement("extension",
                                   new XAttribute("name", hdNum),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", "^" + hdNum + "$"),
                                       new XElement("action",
                                            new XAttribute("application", "read"),
                                            new XAttribute("data", "3 4 'tone_stream://%(10000,0,350,440)' digits 30000 #")),
                                            new XElement("action",
                                            new XAttribute("application", "set"),
                                            new XAttribute("data", "origination_cancel_key=#")),
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "5 ab s execute_extension::att_xfer_speed_dial XML PBXFeatures")),
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                            new XElement("action", new XAttribute("application", "valet_park"), new XAttribute("data", string.Format("${0} ${{digits}}", context))))))));


                return xlem;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static XElement CreatePBXFeaturesFIFOQueue(string hdNum)
        {
            try
            {
                var xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", "PBXFIFO"),
                               new XElement("extension",
                                   new XAttribute("name", hdNum),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", @"[^\s]*"),
                                       new XElement("action", new XAttribute("application", "fifo"), new XAttribute("data", String.Format("{0}@${{domain_name}} in", hdNum))))))));


                return xlem;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static XElement CreatePBXFeaturesFIFO(string hdNum)
        {
            try
            {
                var xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", "PBXFeatures"),
                               new XElement("extension",
                                   new XAttribute("name", hdNum),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", "^" + hdNum + "$"),
                                       new XElement("action",
                                            new XAttribute("application", "read"),
                                            new XAttribute("data", "3 4 'tone_stream://%(10000,0,350,440)' digits 30000 #")),
                                            new XElement("action",
                                            new XAttribute("application", "set"),
                                            new XAttribute("data", "origination_cancel_key=#")),
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "5 ab s execute_extension::att_xfer_speed_dial XML PBXFeatures")),
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                            new XElement("action", new XAttribute("application", "fifo"), new XAttribute("data", "${digits}@${domain_name} in")))))));


                return xlem;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static XElement CreatePBXFeaturesOb(string hdNum, string pbxType)
        {
            try
            {
                var xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", "PBXFeatures"),
                               new XElement("extension",
                                   new XAttribute("name", hdNum),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", "^" + hdNum + "$"),
                                       new XElement("action",
                                            new XAttribute("application", "read"),
                                            new XAttribute("data", "9 10 'tone_stream://%(10000,0,350,440)' digits 30000 #")),
                                            new XElement("action",
                                            new XAttribute("application", "set"),
                                            new XAttribute("data", "origination_cancel_key=#")),
                                            new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "transfer_ringback=$${us-ring}")),
                                            new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                                            new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                                            new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s execute_extension::att_xfer_speed_dial XML PBXFeatures")),
                                            new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                            new XElement("action",
                                            new XAttribute("application", "att_xfer"),
                                            new XAttribute("data", pbxType + "/${digits}")))))));


                return xlem;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;

namespace DuoSoftware.DC.CSRequestWebApi.CCDialplanPicker
{
    public class PercentageBased : IDialplanPickAlgorithm
    {
        public CallControllerInfo PickCallController(List<CallControllerInfo> info, int companyId, int tenantId)
        {
            try
            {

                var specificCallController = (from i in info
                                              where i.EnableSpecificCompany && i.SpecificCompanyId == companyId && i.SpecificTenantId == tenantId
                                              orderby i.LastReqTimeStamp ascending
                                              select i).FirstOrDefault();

                if (specificCallController != null)
                {
                    return specificCallController;
                }

                var totalMax = Convert.ToDouble(info.Sum(callControllerInfo => callControllerInfo.MaxCapacity));
                var totalCurrent = Convert.ToDouble(info.Sum(callControllerInfo => callControllerInfo.CurrentCapacity));

                var min = -1.0;

                var cc = new CallControllerInfo();

                foreach (var callControllerInfo in info.Where(callControllerInfo => callControllerInfo.MaxCapacity > 0 || !(totalMax <= 0) || !(totalCurrent <= 0)))
                {
                    if(min < 0)
                    {
                        min = (callControllerInfo.CurrentCapacity/totalCurrent)/(callControllerInfo.MaxCapacity/totalMax);
                    }

                    var current = (callControllerInfo.CurrentCapacity/totalCurrent)/(callControllerInfo.MaxCapacity/totalMax);

                    if(current <= min)
                    {
                        min = current;
                        cc = callControllerInfo;
                    }
                }

                return cc;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}

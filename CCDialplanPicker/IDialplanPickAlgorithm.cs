﻿using System.Collections.Generic;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;

namespace DuoSoftware.DC.CSRequestWebApi.CCDialplanPicker
{
    public interface IDialplanPickAlgorithm
    {
        CallControllerInfo PickCallController(List<CallControllerInfo> info, int companyId, int tenantId);
    }
}

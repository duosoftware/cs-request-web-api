﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;

namespace DuoSoftware.DC.CSRequestWebApi.CCDialplanPicker
{
    public class RoundRobbing : IDialplanPickAlgorithm
    {
        public CallControllerInfo PickCallController(List<CallControllerInfo> info, int companyId, int tenantId)
        {
            try
            {
                var specificCallController = (from i in info
                                              where i.EnableSpecificCompany && i.SpecificCompanyId == companyId && i.SpecificTenantId == tenantId
                                              orderby i.LastReqTimeStamp ascending
                                              select i).FirstOrDefault();

                if (specificCallController != null)
                {
                    return specificCallController;
                }

                var callController = (from i in info
                                      where i.EnableSpecificCompany == false
                                     orderby i.LastReqTimeStamp ascending
                                     select i).FirstOrDefault();

                return callController;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

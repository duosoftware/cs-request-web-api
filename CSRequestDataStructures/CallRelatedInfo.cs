﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GenerateReportDataObjStore.Classes
{
    public class CallRelatedInfo
    {
        public string Class { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public string SessionId { get; set; }
        public string PhoneNumber { get; set; }
        public TimeSpan InitTime { get; set; }
        public TimeSpan IvrTime { get; set; }
        public TimeSpan QueueTime { get; set; }
        public TimeSpan RingTime { get; set; }
        public TimeSpan TalkTime { get; set; }
        public TimeSpan HoldTime { get; set; }
        public int HoldCount { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public TimeSpan AcwTime { get; set; }
        //sip user name
        public string Agent { get; set; }
        public string CallRingAgents { get; set; }
        public List<string> Skills { get; set; }
        public bool CallAnswered { get; set; }
        public bool CallQueued { get; set; }
        public string Direction { get; set; }
        public int Company { get; set; }
        public int Tenant { get; set; }
        public int ViewObjectId { get; set; }
        

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures
{
    public enum ExtType
    {
        IVR = 0,
        User = 1,
        Group = 2,
        PSTN = 3,
        ParkAsk = 4,
        Conference = 5,
        FIFO = 6

    }

    public enum ProfileType
    {
        InternalProfile = 1,
        ExternalProfile = 2
    }

    public enum CSStatus
    {
        Registered = 1,
        Activated = 2
    }

    public enum CcPickingAlgorithm
    {
        RoundRobbing = 1,
        PercentageBased = 2
    }

    public enum ClusterType
    {
        Cloud = 1,
        Private = 2
    }

    public enum RegistrationDirection
    {
        None = 0,
        RegisterIn = 1,
        RegisterOut = 2
    }

    public enum TrunkConnection
    {
        Cloud = 1,
        Private = 2
    }

    public enum PBXUsrStatus
    {
        DND = 1,
        Forward = 2,
        Available = 3
    }

    public enum ConferenceEndpointProtocol
    {
        Undefined = 0,
        User = 1,
        Phone = 2
    }

    public enum ConferenceJoinType
    {
        DialIn = 1,
        DialOut = 2
    }

    public enum ConferenceUserStatus
    {
        Undefined = 0,
        Validated = 2,
        Added = 1,
        Disconnected = 3,
        Kicked = 4,
        Tried = 5
    }

    public enum DisconnectReason
    {
        Unknown = 0,
        Busy = 1,
        NoAnswer = 2
    }

    public enum ConferenceUserAction
    {
        Mute = 1,
        UnMute = 2,
        Deaf = 3,
        UnDeaf = 4
    }

    public enum ConferenceUserLevel
    {
        Moderator = 1,
        User = 2
    }

    public enum PbxFeatureCodeType
    {
        Park = 1,
        Intercept = 2,
        PickUp = 3,
        Conference = 4,
        OutCallConf = 5,
        Voicemail = 6,
        Barge = 7,
        FIF0 = 8,
        ActDefFwd = 9,
        DeActDefFwd = 10,
        ActBSFwd = 11,
        DeActBSFwd = 12,
        ActNAFwd = 13,
        DeActNAFwd = 14
    }



    public enum ActApp
    {
        None = 0,
        Forwarding = 1,
        FollowMe = 2
    }

    public enum TrunkClass
    {
        SIP = 1,
        SS7 = 2,
        PSTN = 3,
        XMPP = 4
    }

    public enum ContextCat
    {
        Public = 0,
        Internal = 1,
        InternalPbx = 2
    }

    public enum Protocol
    {
        SIP=1,
        XMPP,
        PSTN,
        Unkown
    }

    public enum DnisType
    {
        User = 1,
        Gateway,
        Group,
        loopback,
        Unkown
    }

    public enum EndpointUserTy
    {
        Extension=1,
        NonExtension=2
    }

    public enum UnitOfMeasure
    {
        None = 0,
        Rings = 1,
        Seconds = 2
    }

    public enum CallType
    {
        Unknown = 0,
        Incoming = 1,
        Outgoing = 2,
        Both = 3,
        Fax = 4
    }
}

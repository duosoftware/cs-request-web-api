﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures
{
    public class EventData
    {
        public int Tenent { get; set; }
        public int Company { get; set; }
        public string EventClass { get; set; }
        public string EventType { get; set; }
        public string EventCategory { get; set; }
        public string SessionID { get; set; }
        public string TimeStamp { get; set; }
        public string Parameter1 { get; set; }
        public string Parameter2 { get; set; }
    }
}

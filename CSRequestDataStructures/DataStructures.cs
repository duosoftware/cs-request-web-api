﻿using System;
using System.Collections.Generic;

namespace DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures
{

    public class CallServerInfo
    {
        public int CsId { get; set; }
        public string CsName { get; set; }
    }

    public struct Endpoint
    {
        public Protocol Protocol { get; set; }
        public DnisType Type { get; set; }
        public string Profile { get; set; }
        public string Destination { get; set; }
        public string Origination { get; set; }
        public string Domain { get; set; }
        public int LegStartDelay { get; set; }
        public int LegTimeout { get; set; }
        public string IpUrl { get; set; }

    }

    public struct UserProfileInfo
    {

        private string _extension;
        public string Extension
        {
            get
            {
                return _extension;
            }
            set
            {
                _extension = value ?? "";
            }
        }

        private string _username;
        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value ?? "";
            }
        }

        private string _password;
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value ?? "";
            }
        }

        private string _domain;
        public string Domain
        {
            get
            {
                return _domain;
            }
            set 
            {
                _domain = value ?? "";
            }
        }

        private string _context;
        public string Context
        {
            get
            {
                return _context;
            }
            set 
            {
                _context = value ?? "";
            }
        }

        private string _emailAddr;
        public string EmailAddr
        {
            get
            {
                return _emailAddr;
            }
            set
            {
                _emailAddr = value ?? "";
            }
        }
    }

    public struct GroupInfo
    {
        private string _groupName;
        public string GroupName
        {
            get
            {
                return _groupName;
            }
            set 
            {
                _groupName = value ?? "";
            }
        }

        private string _domain;
        public string Domain
        {
            get
            {
                return _domain;
            }
            set 
            {
                _domain = value ?? "";
            }
        }

        public List<UserProfileInfo> Users;
    }

    public class CPProfiles
    {
        public int TenantID { get; set; }

        public ProfileType ProfileType { get; set; }

        public string ProfileName { get; set; }

        public string ProfileIntIp { get; set; }

        public string ProfileExtIp { get; set; }

        public int Port { get; set; }

        public int CPID { get; set; }

        public int CompanyID { get; set; }

        public int ClusterID { get; set; }

    }

    public struct TrunkInfo
    {
        private string _trunkCode;
        public string TrunkCode
        {
            get
            {
                return _trunkCode;
            }
            set
            {
                _trunkCode = value ?? "";
            }
        }

        private string _ipUrl;
        public string IpUrl
        {
            get
            {
                return _ipUrl;
            }
            set 
            {
                _ipUrl = value ?? "";
            }
        }

        private string _username;
        public string Username
        {
            get
            {
                return _username;
            }
            set 
            {
                _username = value ?? "";
            }
        }

        private string _password;
        public string Password
        {
            get
            {
                return _password;
            }
            set 
            {
                _password = value ?? "";
            }
        }

        private string _domain;
        public string Domain
        {
            get
            {
                return _domain;
            }
            set 
            {
                _domain = value ?? "";
            }
        }
        public RegistrationDirection RegDir;
        public int ExpireSeconds;
        public int PingSeconds;
        public int RetrySeconds;

        public TrunkClass TrClass;

        public string SipServer;
    }

    public class CallerCalleeInfo
    {
        public string FromUser { get; set; }
        public string FromNumber { get; set; }
        public string ToUser { get; set; }
        public string ToNumber { get; set; }
    }

    public class GroupDetails
    {
        public string GroupName { get; set; }

        public string GroupDescription { get; set; }

        public int TenantID { get; set; }

        public int CompanyID { get; set; }

        public string Domain { get; set; }
    }

    public class CallControllerInfo
    {
        public int CallControllerId;

        private string _tcpIp;
        public string TcpIp
        {
            get
            {
                return _tcpIp;
            }
            set 
            {
                _tcpIp = value ?? "";
            }
        }

        public int TcpPort;
        public int MaxCapacity;
        public int CurrentCapacity;
        public int CurrentState;
        public DateTime LastReqTimeStamp;
        public int SpecificCompanyId;
        public int SpecificTenantId;
        public bool EnableSpecificCompany;
    }

    public class HoldusicInfo
    {
        public string HoldMusicName { get; set; }

        public string HoldMusicPath { get; set; }
    }

    public class ConferenceInfo
    {
        public string ObjGuid { get; set; }
        public string RoomName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Pin { get; set; }
        public int MaxUsers { get; set; }
        public string ConferenceType { get; set; }
        public string ConferenceNumber { get; set; }
        public int TrunkId { get; set; }
        public string UUId { get; set; }
        public int CompanyId { get; set; }
        public int TenantId { get; set; }
        public string OperationalStatus { get; set; }
        public bool AllowAnonymousUser { get; set; }
        public bool LockedStatus { get; set; }
        public int ViewObjId { get; set; }
        public string Domain { get; set; }
    }

    public class ConferenceUser
    {
        public string GuUserId { get; set; }

        public string UserName { get; set; }
        
        public string RoomName { get; set; }

        public string Endpoint { get; set; }

        public ConferenceEndpointProtocol Protocol { get; set; }

        public ConferenceUserLevel UserLevel { get; set; }

        public bool InitMuteFlag { get; set; }

        public bool InitDeafFlag { get; set; }

        public bool InitModFlag { get; set; }

        public bool CurrentMuteFlag { get; set; }

        public bool CurrentDeafFlag { get; set; }

        public bool CurrentModFlag { get; set; }

        public ConferenceJoinType JoinType { get; set; }

        public string SessionId { get; set; }

        public string Crn { get; set; }

        public string Uuid { get; set; }

        public string Email { get; set; }

        public int CompanyId { get; set; }

        public int TenantId { get; set; }

        public string UserPin { get; set; }

        public ConferenceUserStatus Status { get; set; }

        public bool IsActiveTalker { get; set; }

        public int IsConnected { get; set; }


    }

    public class DevCallRuleData
    {
        public string Url { get; set; }
        public int CompanyId { get; set; }
        public int TenantId { get; set; }
        public int CallType { get; set; }
        public int MaxCalls { get; set; }
        public string PhoneNumber { get; set; }
        public string RuleParameter { get; set; }
    }

    public class ExtDetails
    {
        public string ExtName { get; set; }
        public ExtType ExtType { get; set; }
        public string Ext { get; set; }
        public int CompanyId { get; set; }
        public int TenantId { get; set; }
        public string Context { get; set; }
        public string GuUserId { get; set; }
        public string DidNumber { get; set; }
        public string DidEnabled { get; set; }
        public string DodNumber { get; set; }
        public string DodEnabled { get; set; }
        public string ExtParams { get; set; }
    }

    public class SessionData
    {
        public string path { get; set; }
        public string company { get; set; }
        public string tenent { get; set; }
        public string pbxcontext { get; set; }
        public string domain { get; set; }
        public string app { get; set; }
    }

    public class ProfileInfo
    {
        public int ClusterId { get; set; }
        public string InternalIp { get; set; }
        public string ExternalIp { get; set; }
        public string ProfileName { get; set; }
        public int CpId { get; set; }
        public int Port { get; set; }
        public ProfileType ProfileType { get; set; }
        public int CompanyId { get; set; }
        public int TenantId { get; set; }
    }

    public class ContextInfo
    {
        public string ContextName { get; set; }
        public ContextCat ContextCat { get; set; }
        public int CompanyId { get; set; }
        public int TenantId { get; set; }
        public int ViewObjId { get; set; }
        public bool QueueEnabled { get; set; }
        public bool ParkEnabled { get; set; }
        public bool MediaByPass { get; set; }
        public bool VoicemailEnabled { get; set; }
        public bool IgnoreEarlyMedia { get; set; }
    }

    public class PbxConfigurations
    {
        public string UserId { get; set; }
        public PBXUsrStatus UsrStatus { get; set; }
        public ActApp ActiveApp { get; set; }
        public string DefaultFwdEp { get; set; }
        public int CompanyId { get; set; }
        public int TenantId { get; set; }
        public int UOMVal { get; set; }
        public string FollowMeRingType { get; set; }
        public bool QueueEnabled { get; set; }
        public bool ParkEnabled { get; set; }
        public string ScheduleId { get; set; }
    }

    public class FollowMeConf
    {
        public string UsrExtension { get; set; }
        public string UsrProfile { get; set; }
        public string EpType { get; set; }
        public int CompanyId { get; set; }
        public int TenantId { get; set; }
        public int UomVal { get; set; }
        public int Priority { get; set; }

    }

    public class EndpointsConf
    {
        public string ObjGuid { get; set; }
        public Protocol EpProtocol { get; set; }
        public DnisType EpType { get; set; }
        public string Profile { get; set; }
        public string Domain { get; set; }
        public string Destination { get; set; }
        public int CompanyId { get; set; }
        public int TenantId { get; set; }
    }

    public class SubscriberDetails
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }
        public int CompanyId { get; set; }
        public int TenantId { get; set; }
        public bool VoicemailActive { get; set; }
        public string GuUserId { get; set; }
    }

    public class PbxFeatureCodes
    {
        public virtual string ObjGuid { get; set; }

        public virtual string FeatureCode { get; set; }

        public virtual int CompanyID { get; set; }

        public virtual int TenantID { get; set; }

        public virtual int ViewObjectID { get; set; }

        public virtual string RecAddUser { get; set; }

        public virtual string RecUpdateUser { get; set; }

        public virtual string RedAddTime { get; set; }

        public virtual string RecUpdateTime { get; set; }

        public virtual string ObjClass { get; set; }

        public virtual string ObjType { get; set; }

        public virtual string ObjCategory { get; set; }

        public string GuVersionId { get; set; }

        public PbxFeatureCodeType FeatureType { get; set; }
    }

    public class CallCdrInfo
    {
        public string CallId { get; set; }
        public DateTime CdrTime { get; set; }
        public string ToNumber { get; set; }
        public string FromNumber { get; set; }
        public string ToUser { get; set; }
        public string FromUser { get; set; }
        public string CallCdrObject { get; set; }
    }

    public class CallCdrInfoProcessed
    {
        public string CallId { get; set; }
        public string CallRefId { get; set; }
        public string FromUser { get; set; }
        public string FromNumber { get; set; }
        public string ToUser { get; set; }
        public string ToNumber { get; set; }
        public int CompanyID { get; set; }
        public int TenantID { get; set; }
        public int ViewObjectID { get; set; }
        public string ObjClass { get; set; }
        public string ObjType { get; set; }
        public string Leg { get; set; }
        public string FromGuUserId { get; set; }
        public string ToGuUserId { get; set; }

        private string _objCategory;

        public string ObjCategory
        {
            get
            {
                return _objCategory;
            }
            set
            {
                _objCategory = string.IsNullOrEmpty(value) ? "unspecified" : value;
            }
        }

        //private int _dialDuration;
        //private int _ringDuration;
        //private int _talkDuration;

        public string Direction { get; set; }
        public DateTime CallCreatedTime { get; set; }
        public DateTime RingStartedTime { get; set; }
        public DateTime CallAnsweredTime { get; set; }
        public DateTime CallHangupTime { get; set; }
        public string HangupCause { get; set; }
        public string CallerContext { get; set; }
        //public int DialDuration
        //{
        //    get
        //    {
        //        return _dialDuration;
        //    }
        //    set {
        //        _dialDuration = value < 0 ? 0 : value;
        //    }
        //}
        //public int RingDuration { get; set; }
        //public int TalkDuration { get; set; }
        public string ContextCategory { get; set; }

        public int Duration { get; set; }
        public int BillSec { get; set; }
        public int ProgressSec { get; set; }
        public int AnswerSec { get; set; }
        public int WaitSec { get; set; }
        public int ProgressMediaSec { get; set; }
        public int FlowBillSec { get; set; }

    }
}

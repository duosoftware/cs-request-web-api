﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DuoSoftware.DC.CallServerDBModelsOS;

namespace DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures
{
    public class ContextCached
    {
        public string ContextName { get; set; }
        public ContextCat ContextCategory { get; set; }
        public bool QueueEnabled { get; set; }
        public bool ParkEnabled { get; set; }
        public bool VoiceMailActive { get; set; }
        public bool BypassMedia { get; set; }
        public bool IgnoreEarlyMedia { get; set; }
        public int CompanyId { get; set; }
        public int TenantId { get; set; }
    }

    //RouteUser(string uuid, string extention, string context, string profile, string destinationPattern, Endpoint endpoint, string custVarString, string fromGuUserId, string toGuUserId, CallerCalleeInfo callerCalleeInfo, bool needRecord = false, bool isVoicemailEnabled = false, string group = "", bool bypassMed = true, bool ignoreEarlyMedia = false, string oprType = "User")
    public class ExtensionCached
    {
        public CSDB_UsrExtensions ExtConf { get; set; }
        public int SubConfValidity { get; set; }
        public CSDB_subscriber SubConf { get; set; }
        public int GrpConfValidity { get; set; }
        public CSDB_Groups GrpConf { get; set; }
        public CSDB_UsrGroup UsrGrpConf { get; set; }
        public CSDB_PBXConfiguration PbxConf { get; set; }
        public CSDB_FollowMeConf FollowMeConf { get; set; }
        public CSDB_Forwarding ForwardingConf { get; set; }
        public CSDB_ConferenceMaster ConfRoomConf { get; set; }
        public CSDB_ConferencePresence ConfPressConf { get; set; }

    }

    public class DidNumberCached
    {
        
    }
}

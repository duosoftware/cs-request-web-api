﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;
using Newtonsoft.Json.Linq;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost
{
    public class JsonMediaFormatter : MediaTypeFormatter
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");
        private readonly string txt = "application/json";

        public JsonMediaFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue(txt));
        }

        public override bool CanWriteType(Type type)
        {
            if (type == typeof(string))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool CanReadType(Type type)
        {
            if (type == typeof(JsonData))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override Task<object> ReadFromStreamAsync(Type type, Stream readStream, System.Net.Http.HttpContent content, IFormatterLogger formatterLogger)
        {
            logCSReq.Debug(String.Format("JsonMediaFormatter - Type : {0}, Content Type : {1}", type.Name, content.Headers.ContentType.MediaType));
            return Task.Factory.StartNew<object>(() =>
            {
                return new JsonData(content);
            });
        }

        public override Task WriteToStreamAsync(Type type, object value, Stream stream, System.Net.Http.HttpContent content, System.Net.TransportContext transportContext)
        {
            logCSReq.Info("Writing to stream async...");
            return Task.Factory.StartNew(() => BuildResponseString(value, stream, content.Headers.ContentType.MediaType));
        }

        private void BuildResponseString(object models, Stream stream, string contenttype)
        {
            logCSReq.Debug("Building response string..........................");

            //var header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\r\n";

            var resp = models;

            using (var writer = new StreamWriter(stream))
            {
                writer.Write(resp);
                logCSReq.Debug(String.Format("Response - {0}", resp));
            }
            stream.Close();
        }
    }

    public class JsonConfStatusData
    {
        public string ConferenceId { get; set; }
        public string RoomName { get; set; }
        public int CurRoomSize { get; set; }
        public string EvtAction { get; set; }
        public string Username { get; set; }
        public string Direction { get; set; }
        public string UserType { get; set; }


        public JsonConfStatusData(JToken contents)
        {
            ConferenceId = contents["ID"] == null ? "" : contents["ID"].ToString();
            RoomName = contents["name"] == null ? "" : contents["name"].ToString();
            CurRoomSize = contents["size"] == null ? 0 : int.Parse(contents["size"].ToString());
            EvtAction = contents["eventAction"] == null ? "" : contents["eventAction"].ToString();
            Username = contents["userName"] == null ? "" : contents["userName"].ToString();
            Direction = contents["direction"] == null ? "" : contents["direction"].ToString();
            UserType = contents["userType"] == null ? "" : contents["userType"].ToString();
        }
    }

    public class JsonDialerApiData
    {
        public string SecurityToken { get; set; }
        public string Dnis { get; set; }
        public string Filename { get; set; }

        public JsonDialerApiData(JToken contents)
        {
            SecurityToken = contents["SecurityToken"] == null ? "" : contents["SecurityToken"].ToString();
            Dnis = contents["DNIS"] == null ? "" : contents["DNIS"].ToString();
        }
    }

    public class JsonChannelData
    {
        public string Channel { get; set; }
        public string Direction { get; set; }
        public string CallId { get; set; }
        public string OtherCallId { get; set; }
        public string CallStatus { get; set; }
        public string Presence { get; set; }

        public JsonChannelData(JToken contents)
        {
            Channel = contents["channel"] == null ? "" : contents["channel"].ToString();
            Direction = contents["direction"] == null ? "" : contents["direction"].ToString();
            CallStatus = contents["callstate"] == null ? "" : contents["callstate"].ToString();
            CallId = contents["uuid"] == null ? "" : contents["uuid"].ToString();
            Presence = contents["presence"] == null ? "" : contents["presence"].ToString();
            OtherCallId = contents["otheruuid"] == null ? "" : contents["otheruuid"].ToString();
        }
    }

    public class TrunkData
    {
        public string TrunkCode { get; set; }
        public string Context { get; set; }
        public string TrunkNumber { get; set; }

        public TrunkData(JToken contents)
        {
            TrunkCode = contents["gateway"] == null ? "" : contents["gateway"].ToString();
            Context = contents["callercontext"] == null ? "" : contents["callercontext"].ToString();
            TrunkNumber = contents["caller"] == null ? "" : contents["caller"].ToString();
        }
    }

    public class JsonRegistrationData
    {
        public string Username { get; set; }
        public string Realm { get; set; }
        public string Profile { get; set; }
        public string RegStatus { get; set; }

        public JsonRegistrationData(JToken contents)
        {
            Username = contents["name"] == null ? "" : contents["name"].ToString();
            Realm = contents["realm"] == null ? "" : contents["realm"].ToString();
            RegStatus = contents["status"] == null ? "" : contents["status"].ToString();
            Profile = contents["profile"] == null ? "" : contents["profile"].ToString();
        }
    }

    public class JsonAddToQueueReqData
    {
        public string SessionId { get; set; }
        public int CompanyId { get; set; }
        public int TenantId { get; set; }
        public string UserExtension { get; set; }
        public string GuUserId { get; set; }

        public JsonAddToQueueReqData(JToken contents)
        {
            SessionId = contents["CallSessionId"] == null ? "" : contents["CallSessionId"].ToString();
            CompanyId = contents["CompanyId"] == null ? 0 : int.Parse(contents["CompanyId"].ToString());
            TenantId = contents["TenantId"] == null ? 0 : int.Parse(contents["TenantId"].ToString());
            UserExtension = contents["Extension"] == null ? "" : contents["Extension"].ToString();
            GuUserId = contents["GuUserId"] == null ? "" : contents["GuUserId"].ToString();
        }
    }

    public class JsonData
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public string RawJsonString { get; set; }
        public JToken OuterJson { get; set; }

        public JsonData(HttpContent contents)
        {
            try
            {
                var rawJsonString = contents.ReadAsStringAsync().Result;

                logCSReq.Debug("Read String Async - Body : " + rawJsonString);

                RawJsonString = rawJsonString;

                //var ss = RawJsonString.Split(new string[] { "," }, StringSplitOptions.None);

                //int i = 0;
                //string tempHangUpTime = "\"0\"";

                //foreach (var s in ss)
                //{
                //    if (s.StartsWith("\"hangup_time\""))
                //    {
                //        var time = ss[i].Split(new string[] { ":" }, StringSplitOptions.None);

                //        if (time[1] == "\"0\"")
                //        {
                //            RawJsonString = RawJsonString.Replace(ss[i], String.Format("\"hangup_time\":{0}", tempHangUpTime));
                //        }
                //        else
                //        {
                //            tempHangUpTime = time[1];
                //        }
                //    }
                //    i++;
                //}

                OuterJson = JToken.Parse(RawJsonString);
            }
            catch (Exception ex)
            {
                logCSReq.Error("Error - ", ex);
                throw;
            }
        }
    }

    public class JsonCdrData
    {
        private log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public string RawJsonString { get; set; }

        public string Uuid { get; set; }
        public string CallUuid { get; set; }
        public string BridgeUuid { get; set; }
        public string Ani { get; set; }
        public string CallerIdNumber { get; set; }
        public string CallerIdName { get; set; }
        public string DestinationNumber { get; set; }
        public string OperationType { get; set; }
        public string Direction { get; set; }
        public string Leg { get; set; }
        private JObject Originatee { get; set; }
        public string Origination { get; set; }
        public string OriginateDispossition { get; set; }
        public Dictionary<string, string> Apps = new Dictionary<string, string>();
        public string EntOriginateALegUuid { get; set; }
        public string Originator { get; set; }
        public string OriginatingLegUuid { get; set; }
        public string ConfName { get; set; }
        public string CurrApp { get; set; }
        public string FromGuUserId { get; set; }
        public string ToGuUserId { get; set; }
        public string FromUser { get; set; }
        public string FromNumber { get; set; }
        public string ToUser { get; set; }
        public string ToNumber { get; set; }
        public string FifoTarget { get; set; }
        public bool IsConnected { get; set; }
        public bool IsHungUp { get; set; }
        public bool IsCreated { get; set; }

        public int CompanyId { get; set; }
        public int TenantId { get; set; }

        private string _callRingTime;

        public string CallRingTime
        {
            get { return _callRingTime; }
            set
            {
                var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var floorVal = Math.Floor(double.Parse(value)/1000000);
                _callRingTime = dtDateTime.AddSeconds(floorVal).ToString("O");
            }
        }

        private string _callProgressMediaTime;

        public string CallProgressMediaTime
        {
            get { return _callProgressMediaTime; }
            set
            {
                var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var floorVal = Math.Floor(double.Parse(value)/1000000);
                _callProgressMediaTime = dtDateTime.AddSeconds(floorVal).ToString("O");
            }
        }

        private string _callCreatedTime;

        public string CallCreatedTime
        {
            get { return _callCreatedTime; }
            set
            {
                var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var floorVal = Math.Floor(double.Parse(value)/1000000);
                _callCreatedTime = dtDateTime.AddSeconds(floorVal).ToString("O");
            }
        }

        private string _callProgressTime;

        public string CallProgressTime
        {
            get { return _callProgressTime; }
            set
            {
                var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var floorVal = Math.Floor(double.Parse(value)/1000000);
                _callProgressTime = dtDateTime.AddSeconds(floorVal).ToString("O");
            }
        }

        private string _callAnsweredTime;

        public string CallAnsweredTime
        {
            get { return _callAnsweredTime; }
            set
            {
                var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var floorVal = Math.Floor(double.Parse(value)/1000000);
                _callAnsweredTime = dtDateTime.AddSeconds(floorVal).ToString("O");
            }
        }

        private string _callHangupTime;

        public string CallHangupTime
        {
            get { return _callHangupTime; }
            set
            {
                var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var floorVal = Math.Floor(double.Parse(value)/1000000);
                _callHangupTime = dtDateTime.AddSeconds(floorVal).ToString("O");
            }
        }

        public string HangupCause { get; set; }
        public string CallerContext { get; set; }
        public int DialDuration { get; set; }
        public int RingDuration { get; set; }
        public int TalkDuration { get; set; }

        public int HoldSec { get; set; }
        public int Duration { get; set; }
        public int BillSec { get; set; }
        public int ProgressSec { get; set; }
        public int AnswerSec { get; set; }
        public int WaitSec { get; set; }
        public int ProgressMediaSec { get; set; }
        public int FlowBillSec { get; set; }
        
        public string SipGatewayName { get; set; }
        public string SofiaProfileName { get; set; }


        public JsonCdrData(JToken outer)
        {
            try
            {
                var varSec = outer["variables"].Value<JObject>();
                var callFlowSec = outer["callflow"].Value<JObject>();
                var timesSec = callFlowSec["times"].Value<JObject>();
                var callerProfileSec = callFlowSec["caller_profile"].Value<JObject>();

                Originatee = callerProfileSec["originatee"] == null ? null : callerProfileSec["originatee"].Value<JObject>();

                var uuid = varSec["uuid"];
                var tempAppLogSec = outer["app_log"];

                if (tempAppLogSec != null)
                {
                    var appLogSec = tempAppLogSec.Value<JObject>();

                    var apps = appLogSec["applications"].Value<JArray>();

                    foreach (var app in apps)
                    {
                        var appName = app["app_name"];
                        var appData = app["app_data"];

                        if (appName != null)
                        {
                            try
                            {
                                Apps.Add(appName.ToString(), appData == null ? "" : appData.ToString());
                            }
                            catch
                            {

                            }
                        }
                    }
                }

                if (uuid != null)
                {
                    Uuid = uuid.ToString();
                    CallUuid = varSec["call_uuid"] == null ? "" : varSec["call_uuid"].ToString();
                    BridgeUuid = varSec["bridge_uuid"] == null ? "" : varSec["bridge_uuid"].ToString();
                    FifoTarget = varSec["fifo_target"] == null ? "" : varSec["fifo_target"].ToString();

                    FromGuUserId = varSec["FromGuUserId"] == null ? "" : varSec["FromGuUserId"].ToString();
                    ToGuUserId = varSec["ToGuUserId"] == null ? "" : varSec["ToGuUserId"].ToString();

                    FromUser = varSec["FromUser"] == null ? "" : varSec["FromUser"].ToString();
                    FromNumber = varSec["FromNumber"] == null ? "" : varSec["FromNumber"].ToString();
                    ToUser = varSec["ToUser"] == null ? "" : varSec["ToUser"].ToString();
                    ToNumber = varSec["ToNumber"] == null ? "" : varSec["ToNumber"].ToString();

                    Origination = varSec["origination"] == null ? "" : varSec["origination"].ToString();
                    OriginateDispossition = varSec["originate_disposition"] == null ? "" : varSec["originate_disposition"].ToString();

                    if (Originatee != null || !String.IsNullOrEmpty(OriginateDispossition))
                    {
                        Leg = Leg + "A";
                    }

                    Originator = varSec["originator"] == null ? "" : varSec["originator"].ToString();
                    OriginatingLegUuid = varSec["originating_leg_uuid"] == null ? "" : varSec["originating_leg_uuid"].ToString();

                    if (!String.IsNullOrEmpty(Originator) || !String.IsNullOrEmpty(OriginatingLegUuid))
                    {
                        Leg = Leg + "B";
                    }

                    IsConnected = timesSec["answered_time"] != null && timesSec["answered_time"].ToString() != "0";
                    //IsHungUp = timesSec["hangup_time"] != null && timesSec["hangup_time"].ToString() != "0";
                    IsHungUp = varSec["end_uepoch"] != null && varSec["end_uepoch"].ToString() != "0";
                    
                    IsCreated = timesSec["created_time"] != null && timesSec["created_time"].ToString() != "0";

                    ConfName = varSec["conference_name"] == null ? "" : varSec["conference_name"].ToString();
                    CurrApp = varSec["current_application"] == null ? "" : varSec["current_application"].ToString();
                    CallAnsweredTime = timesSec["answered_time"] == null ? "0" : timesSec["answered_time"].ToString();
                    CallCreatedTime = timesSec["created_time"] == null ? "0" : timesSec["created_time"].ToString();
                    //CallHangupTime = timesSec["hangup_time"] == null ? "0" : timesSec["hangup_time"].ToString();
                    CallHangupTime = varSec["end_uepoch"] == null ? "0" : varSec["end_uepoch"].ToString();
                    CallProgressTime = timesSec["progress_time"] == null ? "0" : timesSec["progress_time"].ToString();
                    
                    CallProgressMediaTime = timesSec["progress_media_time"] == null ? "0" : timesSec["progress_media_time"].ToString();
                    HangupCause = varSec["hangup_cause"] == null ? "" : varSec["hangup_cause"].ToString();
                    DialDuration = TimeSpan.Parse((DateTime.Parse(CallProgressTime) - DateTime.Parse(CallProgressMediaTime)).ToString()).Seconds;
                    //RingDuration = TimeSpan.Parse((DateTime.Parse(CallAnsweredTime) - DateTime.Parse(CallProgressTime)).ToString()).Seconds;

                    Duration = varSec["duration"] == null ? 0 : int.Parse(varSec["duration"].ToString());
                    BillSec = varSec["billsec"] == null ? 0 : int.Parse(varSec["billsec"].ToString());
                    HoldSec = varSec["hold_accum_seconds"] == null ? 0 : int.Parse(varSec["hold_accum_seconds"].ToString());
                    ProgressSec = varSec["progresssec"] == null ? 0 : int.Parse(varSec["progresssec"].ToString());
                    AnswerSec = varSec["answersec"] == null ? 0 : int.Parse(varSec["answersec"].ToString());
                    WaitSec = varSec["waitsec"] == null ? 0 : int.Parse(varSec["waitsec"].ToString());
                    ProgressMediaSec = varSec["progress_mediasec"] == null ? 0 : int.Parse(varSec["progress_mediasec"].ToString());
                    FlowBillSec = varSec["flow_billsec"] == null ? 0 : int.Parse(varSec["flow_billsec"].ToString());


                    if (IsConnected)
                    {
                        TalkDuration = TimeSpan.Parse((DateTime.Parse(CallHangupTime) - DateTime.Parse(CallAnsweredTime)).ToString()).Seconds;
                    }
                    else
                    {
                        TalkDuration = 0;
                    }

                    if (!IsConnected)
                    {
                        if (IsHungUp && IsCreated)
                        {
                            RingDuration = TimeSpan.Parse((DateTime.Parse(CallHangupTime) - DateTime.Parse(CallCreatedTime)).ToString()).Seconds;
                        }
                        else
                        {
                            RingDuration = 0;
                        }
                    }
                    else
                    {
                        if (IsConnected && IsCreated)
                        {
                            RingDuration = TimeSpan.Parse((DateTime.Parse(CallAnsweredTime) - DateTime.Parse(CallCreatedTime)).ToString()).Seconds; ;
                        }
                        else
                        {
                            RingDuration = 0;
                        }
                    }

                    Ani = callerProfileSec["ani"] == null ? "" : callerProfileSec["ani"].ToString();
                    CallerIdName = callerProfileSec["caller_id_name"] == null ? "" : callerProfileSec["caller_id_name"].ToString();
                    CallerIdNumber = callerProfileSec["caller_id_number"] == null ? "" : callerProfileSec["caller_id_number"].ToString();
                    DestinationNumber = callerProfileSec["destination_number"] == null ? "" : callerProfileSec["destination_number"].ToString();
                    OperationType = varSec["OperationType"] == null ? "" : varSec["OperationType"].ToString();
                    CallerContext = callerProfileSec["context"] == null ? "" : callerProfileSec["context"].ToString();
                    SipGatewayName = varSec["sip_gateway_name"] == null ? "" : varSec["sip_gateway_name"].ToString();
                    Direction = varSec["direction"] == null ? "" : varSec["direction"].ToString();
                    SofiaProfileName = varSec["sofia_profile_name"] == null ? "" : varSec["sofia_profile_name"].ToString();

                    var custCompStr = varSec["CustomCompanyStr"] == null ? "" : varSec["CustomCompanyStr"].ToString();

                    var splitArr = custCompStr.Split(new string[] {"_"}, 2, StringSplitOptions.None);

                    if (splitArr.Length == 2)
                    {
                        CompanyId = int.Parse(splitArr[0]);
                        TenantId = int.Parse(splitArr[1]);
                    }

                    //FromUser = 
                }
                else
                {
                    throw new Exception("Call Uuid is null");
                }
            }
            catch (Exception ex)
            {
                logCSReq.Error("Error - ", ex);
                throw;
            }

        }
    }
}
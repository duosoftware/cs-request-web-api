﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;
using DuoSoftware.DC.CSRequestWebApi.CRTranslator;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost
{
    public static  class LogicOperationsHandler
    {
        static log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");


        public static string FeatureCodeOperations(string destNum, ContextInfo contextInf, string context, string profileName, string fromGuUserId, CallerCalleeInfo callerCalleeInfo)
        {
            try
            {
                var xele = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "result"),
                                                     new XElement("result", new XAttribute("status", "not found"))));

                if (destNum.StartsWith("*") || destNum.StartsWith("#"))
                {
                    #region Feature Code Op

                    logCSReq.Info("Feature code detected");

                    var fcList = new List<PbxFeatureCodes>();

                    var featureCodeString = ConfigurationManager.AppSettings.Get("FeatureCodes");

                    var splitStr = featureCodeString.Split(new string[] { "," }, StringSplitOptions.None);

                    foreach (var s in splitStr)
                    {
                        var tempFcList = s.Split(new string[] { ":" }, 2, StringSplitOptions.None);

                        if (tempFcList.Length == 2)
                        {
                            var fc = new PbxFeatureCodes
                            {
                                FeatureCode = tempFcList[0],
                                FeatureType = (PbxFeatureCodeType)Enum.Parse(typeof(PbxFeatureCodeType), tempFcList[1])
                            };
                            fcList.Add(fc);
                        }

                    }

                    foreach (var fCode in fcList)
                    {
                        if (destNum.StartsWith(fCode.FeatureCode))
                        {
                            logCSReq.Info(String.Format("Feature code type : {0}", fCode.FeatureType));

                            var tempDestNum = destNum.Replace(fCode.FeatureCode, "");

                            logCSReq.Debug(String.Format("Getting extension for feature code : {0}, ext : {1}, company : {2}, tenant : {3}", fCode.FeatureCode, tempDestNum, contextInf.CompanyId, contextInf.TenantId));
                            var extDetails = AbstractBackendHandler.Instance.GetExtensionDetails(tempDestNum, contextInf.CompanyId, contextInf.TenantId);

                            switch (fCode.FeatureType)
                            {

                                case PbxFeatureCodeType.FIF0:
                                    {
                                        //Get queued call back from fifo queue by pressingh a digit
                                        return LitePBX.LitePBX.FIFOOut(tempDestNum, context, profileName, @"[^\s]*", tempDestNum).ToString();
                                    }
                                case PbxFeatureCodeType.Park:
                                    {
                                        return LitePBX.LitePBX.Park(tempDestNum, context, profileName, @"[^\s]*", tempDestNum, contextInf.CompanyId, contextInf.TenantId).ToString();
                                        //return LitePBX.LitePBX.Park(tempDestNum, context, profileName, string.Format("^\\{0}$", destNum), tempDestNum, contextInf.CompanyId, contextInf.TenantId).ToString();
                                    }
                                case PbxFeatureCodeType.PickUp:
                                    {
                                        if (extDetails != null)
                                        {
                                            return LitePBX.LitePBX.PickUP(extDetails.Ext, context, profileName, @"[^\s]*", extDetails.Ext, extDetails.CompanyId, extDetails.TenantId, fromGuUserId).ToString();
                                        }
                                        break;
                                    }
                                case PbxFeatureCodeType.Intercept:
                                    {
                                        if (extDetails != null)
                                        {
                                            return LitePBX.LitePBX.Intercept(extDetails.Ext, context, profileName, @"[^\s]*", String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), callerCalleeInfo, fromGuUserId).ToString();
                                        }
                                        break;
                                    }
                                case PbxFeatureCodeType.Voicemail:
                                    {
                                        if (extDetails != null)
                                        {
                                            //TODO: Need to check whether the requesting user is the same as the voicemail extension
                                            var sub = AbstractBackendHandler.Instance.GetSipSubDetails(extDetails.ExtName, extDetails.CompanyId, extDetails.TenantId);

                                            if (sub != null)
                                            {
                                                if (fromGuUserId.Equals(sub.GuUserId))
                                                {
                                                    return LitePBX.LitePBX.CheckVoicemail(extDetails.Ext, context, @"[^\s]*", profileName, sub.Domain).ToString();
                                                }
                                                else
                                                {
                                                    logCSReq.Error(String.Format("FromGuUserId : {0} and Voicemail Request GuUserId : {1} do not match", fromGuUserId, sub.GuUserId));

                                                    return xele.ToString();
                                                }
                                                
                                            }
                                            else
                                            {
                                                throw new Exception("User not found - voicemail");
                                            }
                                        }
                                        break;

                                    }
                                case PbxFeatureCodeType.Barge:
                                    {
                                        if (extDetails != null)
                                        {
                                            return LitePBX.LitePBX.Barge(extDetails.Ext, context, profileName, @"[^\s]*", String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), callerCalleeInfo, fromGuUserId).ToString();
                                        }
                                        break;
                                    }
                                case PbxFeatureCodeType.ActDefFwd:
                                case PbxFeatureCodeType.ActBSFwd:
                                case PbxFeatureCodeType.DeActBSFwd:
                                case PbxFeatureCodeType.ActNAFwd:
                                case PbxFeatureCodeType.DeActDefFwd:
                                case PbxFeatureCodeType.DeActNAFwd:
                                    {
                                        string fwdType = "Default";
                                        bool isActive = false;

                                        if (fCode.FeatureType == PbxFeatureCodeType.ActBSFwd)
                                        {
                                            fwdType = "Busy";
                                            isActive = true;
                                        }
                                        else if (fCode.FeatureType == PbxFeatureCodeType.ActDefFwd)
                                        {
                                            fwdType = "Default";
                                            isActive = true;
                                        }
                                        else if (fCode.FeatureType == PbxFeatureCodeType.ActNAFwd)
                                        {
                                            fwdType = "NoAnswer";
                                            isActive = true;
                                        }
                                        else if (fCode.FeatureType == PbxFeatureCodeType.DeActBSFwd)
                                        {
                                            fwdType = "Busy";
                                            isActive = false;
                                        }
                                        else if (fCode.FeatureType == PbxFeatureCodeType.DeActDefFwd)
                                        {
                                            fwdType = "Default";
                                            isActive = false;
                                        }
                                        else if (fCode.FeatureType == PbxFeatureCodeType.DeActNAFwd)
                                        {
                                            fwdType = "NoAnswer";
                                            isActive = false;
                                        }

                                        CSReqWebApiDataOperator.SetForwardOptions(fwdType, tempDestNum, isActive, fromGuUserId, contextInf.CompanyId, contextInf.TenantId);

                                        return xele.ToString();
                                    }
                            }

                        }

                    }

                    logCSReq.Warn("Feature code could not be matched");

                    
                    #endregion

                }

                return "";
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static string PbxOperations(string destNum, string varFromNumber, ExtDetails destExtDetails, ContextInfo contextInf, string context, string profileName, string varUserId, string huntDestNum, string callUuid, string fromGuUserId, CallerCalleeInfo callerCalleeInfo, bool dodEnable, string dodNumber, bool bypassMedia, CallType cType = CallType.Unknown, bool voicemailActive = false, string faxType = "", string extraParams = "")
        {
            try
            {
                logCSReq.Info("Context cat - InternalPbx");

                logCSReq.Debug(String.Format("Company Voicemail Status : {0}", voicemailActive));

                ExtDetails extensionDetails = destExtDetails;
                
                if (destExtDetails == null)
                {
                    extensionDetails = AbstractBackendHandler.Instance.GetExtensionDetails(destNum, contextInf.CompanyId, contextInf.TenantId);
                }
                else
                {
                    logCSReq.Debug("Extension Details Already Taken");
                }
                

                if (extensionDetails != null)
                {
                    if (extensionDetails.ExtParams != null && extensionDetails.ExtParams.StartsWith("CUSTOMXML"))
                    {
                        logCSReq.Debug("Trying - Custom XML Routing");

                        logCSReq.Info("Call rule type is custom xml for pbx");
                        var path = ConfigurationManager.AppSettings.Get("CustomXmlFilePath");

                        if (!String.IsNullOrEmpty(path))
                        {
                            var fileStr = extensionDetails.ExtParams.Split(new[]{"_"}, StringSplitOptions.None);

                            if (fileStr.Length == 2)
                            {
                                path = path + fileStr[1];
                                var xml = CSRequestDataLayer.FileHandler.ReadFileText(path);

                                return xml;
                            }
                            else
                            {
                                logCSReq.Warn("Ext Params split length is not 2");
                            }
                            
                        }
                        else
                        {
                            logCSReq.Warn("Custom Xml file path not found");
                        }
                    }
                    if (extensionDetails.ExtType == ExtType.ParkAsk)
                    {
                        logCSReq.Info("Extension type - Park");

                        #region Ext Type Park

                        return LitePBX.LitePBX.ParkAsk(extensionDetails.Ext, context, profileName, @"[^\s]*", contextInf.CompanyId, contextInf.TenantId).ToString();

                        #endregion
                    }

                    if (extensionDetails.ExtType == ExtType.FIFO)
                    {
                        logCSReq.Info("Extension type - FIFO");

                        #region Ext Type FIFO

                        return LitePBX.LitePBX.FIFOIn(extensionDetails.Ext, context, profileName, @"[^\s]*", extensionDetails.Ext).ToString();

                        #endregion
                    }

                        //TODO:Number Dialed can also be a trunk number mapped to a conference room

                    else if (extensionDetails.ExtType == ExtType.Conference)
                    {
                        logCSReq.Info("Extension Type - Conference");

                        var confRoomInf = AbstractBackendHandler.Instance.GetActiveConferenceRoomInfo(extensionDetails.ExtName, extensionDetails.CompanyId, extensionDetails.TenantId);

                        if (confRoomInf != null)
                        {
                            logCSReq.Debug(String.Format("Conference Room Found : {0}", confRoomInf.RoomName));
                            var mode = new StringBuilder();

                            if (confRoomInf.AllowAnonymousUser)
                            {
                                logCSReq.Debug("Anonymous User Allowed");

                                return LitePBX.LitePBX.Conference(extensionDetails.Ext, context, @"[^\s]*", confRoomInf.RoomName, confRoomInf.Domain, confRoomInf.Pin, mode.ToString(), confRoomInf.CompanyId, confRoomInf.TenantId).ToString();
                                //if (String.IsNullOrEmpty(varUserId))
                                //{
                                //    logCSReq.Debug("User is empty - call coming from outside");
                                //    return LitePBX.LitePBX.Conference(extensionDetails.Ext, context, @"[^\s]*", confRoomInf.RoomName, confRoomInf.Domain, confRoomInf.Pin, mode.ToString(), confRoomInf.CompanyId, confRoomInf.TenantId).ToString();
                                //}
                                //else
                                //{
                                //    var sub = AbstractBackendHandler.Instance.GetSipSubDetails(varUserId, extensionDetails.CompanyId, extensionDetails.TenantId);

                                //    if (sub != null)
                                //    {
                                //        logCSReq.Debug("Subscriber found for dial user");
                                //        return LitePBX.LitePBX.Conference(extensionDetails.Ext, context, @"[^\s]*", confRoomInf.RoomName, confRoomInf.Domain, confRoomInf.Pin, mode.ToString(), confRoomInf.CompanyId, confRoomInf.TenantId).ToString();
                                //    }
                                //    else
                                //    {
                                //        logCSReq.Debug("Subscriber not found - Conference dial for anonymous user");
                                        
                                //        //return LitePBX.LitePBX.Conference(extensionDetails.Ext, context, @"[^\s]*", confRoomInf.RoomName, confRoomInf.Domain, confRoomInf.Pin, mode.ToString(), confRoomInf.CompanyId, confRoomInf.TenantId).ToString();
                                //    }
                                //}
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(varUserId))
                                {
                                    throw new Exception("Anonymous users are not allowed to enter the room");
                                }
                                else
                                {
                                    var sub = AbstractBackendHandler.Instance.GetSipSubDetails(varUserId, extensionDetails.CompanyId, extensionDetails.TenantId);

                                    if (sub != null)
                                    {
                                        logCSReq.Debug("Subscriber found for dial user");

                                        var response = CallConferenceOp(sub, mode, confRoomInf, extensionDetails, varUserId, profileName, context, varFromNumber);

                                        if (!String.IsNullOrEmpty(response))
                                        {
                                            return response;
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception("Anonymous users are not allowed to enter the room");
                                    }
                                }
                            }
                        }

                    }

                    else if (extensionDetails.ExtType == ExtType.PSTN)
                    {
                        #region Gateway Extensions

                        logCSReq.Info("PSTN extension details found - trying direct gw call");

                        string tempTrunkNum;
                        string tempTrunkCode;
                        string tempTrunkDomain;
                        TrunkClass tempTrClass;
                        int timeout;
                        int transId;
                        int aniTransId;
                        string ipUrl = "";
                        int trId;
                        string targetScript = "";

                        string dest = extensionDetails.ExtName;

                        AbstractBackendHandler.Instance.GetGwFromRule(contextInf.CompanyId, contextInf.TenantId, dest, varFromNumber, out tempTrunkNum, out tempTrunkCode, out tempTrunkDomain, out tempTrClass, out timeout, out transId, out aniTransId, out ipUrl, out trId, out targetScript);

                        if (transId > 0)
                        {
                            var transDNIS = new CallRuleTranslation(AbstractBackendHandler.Instance.GetTranslationsFromDB(transId, contextInf.CompanyId, contextInf.TenantId));

                            dest = transDNIS.Translate(dest);
                        }

                        var ep = new Endpoint
                            {
                                Destination = dest,
                                LegStartDelay = 0,
                                Origination = tempTrunkNum,
                                Profile = tempTrunkCode,
                                Protocol = Protocol.SIP,
                                Type = DnisType.Gateway,
                                LegTimeout = timeout,
                                IpUrl = ipUrl
                            };

                        callerCalleeInfo.ToNumber = huntDestNum;
                        callerCalleeInfo.ToUser = huntDestNum;

                        if (dodEnable)
                        {
                            var dodNum = AbstractBackendHandler.Instance.GetActiveTrunkPhoneNumbers(dodNumber).FirstOrDefault();

                            if (dodNum != null)
                            {
                                ep.Origination = dodNumber;

                                if ((CallType)dodNum.CallType == CallType.Fax)
                                {
                                    return LitePBX.LitePBX.RouteFaxGateway(huntDestNum, context, profileName, @"[^\s]*", ep, fromGuUserId, contextInf.CompanyId, contextInf.TenantId, "GatewayFaxPassThrough", callerCalleeInfo, false, faxType, false, contextInf.IgnoreEarlyMedia).ToString();
                                }
                            }
                            else
                            {
                                throw new Exception("User dodnumber not configured on trunk numbers");
                            }
                        }

                        return LitePBX.LitePBX.RouteGateway(huntDestNum, context, profileName, @"[^\s]*", ep, fromGuUserId, contextInf.CompanyId, contextInf.TenantId, "Gateway", callerCalleeInfo, false, false, contextInf.IgnoreEarlyMedia).ToString();

                        #endregion

                    }
                    else
                    {
                        string tmpDest = extensionDetails.ExtName;

                        var pbxConf = AbstractBackendHandler.Instance.GetPbxConfigurations(extensionDetails.GuUserId, contextInf.CompanyId, contextInf.TenantId);

                        if (pbxConf != null)
                        {
                            if (!String.IsNullOrEmpty(pbxConf.ScheduleId))
                            {
                                var schedules = AbstractBackendHandler.Instance.GetAppointmentsForSchedule(pbxConf.ScheduleId);

                                if (schedules != null && schedules.Count > 0)
                                {
                                    var schController = new DuoSoftware.CSScheduleController.Scheduler();

                                    var appointment = schController.PickSchedule(schedules, DateTime.Now, pbxConf.CompanyId, pbxConf.TenantId);

                                    if (appointment != null)
                                    {
                                        if (appointment.EndpointType.Equals("Available"))
                                        {
                                            pbxConf.ActiveApp = ActApp.None;
                                            pbxConf.UsrStatus = PBXUsrStatus.Available;
                                        }
                                        else if (appointment.EndpointType.Equals("DefaultForward"))
                                        {
                                            pbxConf.ActiveApp = ActApp.None;
                                            pbxConf.UsrStatus = PBXUsrStatus.Forward;
                                        }
                                        else if (appointment.EndpointType.Equals("FollowMe"))
                                        {
                                            pbxConf.ActiveApp = ActApp.FollowMe;
                                            pbxConf.UsrStatus = PBXUsrStatus.Available;
                                        }
                                        else if (appointment.EndpointType.Equals("Forwarding"))
                                        {
                                            pbxConf.ActiveApp = ActApp.Forwarding;
                                            pbxConf.UsrStatus = PBXUsrStatus.Available;
                                        }
                                    }
                                }
                            }

                            if (pbxConf.UsrStatus == PBXUsrStatus.DND)
                            {
                                logCSReq.Info("PBXUsrStatus is DND");

                                callerCalleeInfo.ToUser = extensionDetails.ExtName;
                                callerCalleeInfo.ToNumber = extensionDetails.Ext;

                                //return CSWebResponseCreater.CsWebResponse.CreateDialplanHangup(@"[^\s]*", context, contextInf.CompanyId, contextInf.TenantId, callerCalleeInfo, fromGuUserId).ToString();
                                return CSWebResponseCreater.CsWebResponse.CreateDialplanHangup(@"[^\s]*", context, contextInf.CompanyId, contextInf.TenantId, callerCalleeInfo, fromGuUserId).ToString();
                                //var xele = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                //                        new XElement("section", new XAttribute("name", "result"),
                                //                                     new XElement("result", new XAttribute("status", "not found"))));

                                //return xele.ToString();
                            }
                            else if (pbxConf.UsrStatus == PBXUsrStatus.Available)
                            {
                                logCSReq.Info("PBXUsrStatus is Available");

                                if (pbxConf.ActiveApp == ActApp.FollowMe)
                                {
                                    #region FollowMe Operations

                                    logCSReq.Info("Active App is FollowMe");

                                    bool isParallel = true;
                                    isParallel = pbxConf.FollowMeRingType == "Ring all";

                                    logCSReq.Info(String.Format("Is Parallel : {0}", isParallel));

                                    //get follow me endpoints

                                    var fmEpList = new List<Endpoint>();

                                    var fmConf = AbstractBackendHandler.Instance.GetFollowMeConf(extensionDetails.GuUserId, extensionDetails.CompanyId, extensionDetails.TenantId);

                                    var tempsub = AbstractBackendHandler.Instance.GetSubscriberForGuUserId(extensionDetails.GuUserId, extensionDetails.CompanyId, extensionDetails.TenantId);

                                    string domain = "";
                                    if (tempsub != null)
                                    {
                                        domain = tempsub.Domain;
                                    }

                                    logCSReq.Info(String.Format("{0} Follow me configurations found", fmConf.Count));

                                    foreach (var followMeConf in fmConf)
                                    {

                                        //TODO:Call Profile service and get endpoint info

                                       // var secToken = V5ServiceAccess.AuthServiceAccess.Instance.GetSecurityToken(followMeConf.CompanyId);

                                       // var endpoint = V5ServiceAccess.ProfileServiceAccess.GetDestinationForType(extensionDetails.GuUserId, followMeConf.EpType, secToken);

                                        if (followMeConf.EpType == "Gateway")
                                        {
                                            logCSReq.Info(String.Format("FollowMe Endpoint Type is Non Extension - Endpoint : {0}, Priority : {1}", followMeConf.UsrProfile, followMeConf.Priority));
                                            //get data from endpoints
                                            string trunkNum = "";
                                            string trunkCode = "";
                                            string trunkDomain = "";
                                            TrunkClass tempTrClass;
                                            int timeout;
                                            int transId;
                                            int aniTransId;
                                            string ipUrl = "";
                                            int trId;
                                            string targetScript = "";

                                            try
                                            {
                                                AbstractBackendHandler.Instance.GetGwFromRule(followMeConf.CompanyId, followMeConf.TenantId, followMeConf.UsrProfile, varFromNumber, out trunkNum, out trunkCode, out trunkDomain, out tempTrClass, out timeout, out transId, out aniTransId, out ipUrl, out trId, out targetScript);
                                            }
                                            catch (Exception ex)
                                            {
                                                logCSReq.Warn(String.Format("Warn - {0}", ex.Message));
                                                continue;
                                            }

                                            string dest = followMeConf.UsrProfile;

                                            if (transId > 0)
                                            {
                                                var transDNIS = new CallRuleTranslation(AbstractBackendHandler.Instance.GetTranslationsFromDB(transId, contextInf.CompanyId, contextInf.TenantId));

                                                dest = transDNIS.Translate(followMeConf.UsrProfile);
                                            }


                                            logCSReq.Info(String.Format("Outbound trunk details : TrunkNumber : {0}, TrunkCode : {1}", trunkNum, trunkCode));

                                            var protocol = Protocol.SIP;
                                            var epType = DnisType.Gateway;
                                            var ep = new Endpoint
                                                {
                                                    Destination = dest,
                                                    LegStartDelay = 0,
                                                    Origination = trunkNum,
                                                    Profile = trunkCode,
                                                    Protocol = protocol,
                                                    Type = epType,
                                                    LegTimeout = followMeConf.UomVal,
                                                    IpUrl = ipUrl
                                                };

                                            fmEpList.Add(ep);

                                        }
                                        else
                                        {
                                            //get data from extensions
                                            logCSReq.Info(String.Format("FollowMe Endpoint Type is Extension - Endpoint : {0}, Priority : {1}", followMeConf.UsrProfile, followMeConf.Priority));

                                            var extCfg = AbstractBackendHandler.Instance.GetExtensionDetails(followMeConf.UsrProfile, followMeConf.CompanyId, followMeConf.TenantId);

                                            if (extCfg != null)
                                            {
                                                logCSReq.Info(String.Format("Extension : {0} found", extCfg.Ext));

                                                if (extCfg.ExtType == ExtType.User)
                                                {
                                                    var sub = AbstractBackendHandler.Instance.GetSipSubDetails(extCfg.ExtName, extCfg.CompanyId, extCfg.TenantId);

                                                    if (sub != null)
                                                    {
                                                        var ep = new Endpoint
                                                            {
                                                                Destination = extCfg.Ext,
                                                                Domain = sub.Domain,
                                                                LegStartDelay = 0,
                                                                Origination = varUserId,
                                                                Profile = profileName,
                                                                Protocol = Protocol.Unkown,
                                                                Type = DnisType.User,
                                                                LegTimeout = followMeConf.UomVal,
                                                                IpUrl = ""
                                                            };

                                                        fmEpList.Add(ep);
                                                    }
                                                    else
                                                    {
                                                        logCSReq.Warn("Subscriber not found for given extension");
                                                    }
                                                }
                                                else if (extCfg.ExtType == ExtType.Group)
                                                {
                                                    //get grp details and fill ep
                                                }

                                            }
                                            else
                                            {
                                                logCSReq.Warn("Extension not found");
                                            }
                                        }
                                    }

                                    #endregion

                                    string tempDomain = "";

                                    bool tempVoicemailActive = false;

                                    if (extensionDetails.ExtType == ExtType.User)
                                    {
                                        if (!voicemailActive)
                                        {
                                            var tempSub = AbstractBackendHandler.Instance.GetSipSubDetails(tmpDest, extensionDetails.CompanyId, extensionDetails.TenantId);

                                            if (tempSub != null)
                                            {
                                                tempVoicemailActive = tempSub.VoicemailActive;
                                                
                                            }
                                        }
                                        else
                                        {
                                            tempVoicemailActive = true;
                                        }
                                    }

                                    if (bypassMedia)
                                    {
                                        bypassMedia = contextInf.MediaByPass;
                                    }

                                    return LitePBX.LitePBX.FollowME(extensionDetails.Ext, domain, context, profileName, @"[^\s]*", fmEpList, extensionDetails.CompanyId, extensionDetails.TenantId, tempVoicemailActive, true, pbxConf.UOMVal, isParallel, bypassMedia, contextInf.IgnoreEarlyMedia).ToString();
                                }
                                else if (pbxConf.ActiveApp == ActApp.Forwarding)
                                {
                                    logCSReq.Info("Active App is Forwarding");
                                    var sub = AbstractBackendHandler.Instance.GetSipSubDetails(tmpDest, extensionDetails.CompanyId, extensionDetails.TenantId);

                                    if (sub != null)
                                    {
                                        callerCalleeInfo.ToUser = sub.Username;
                                        var ep = new Endpoint
                                        {
                                            Destination = extensionDetails.Ext,
                                            Domain = sub.Domain,
                                            Origination = varUserId,
                                            Profile = profileName,
                                            Protocol = Protocol.Unkown,
                                            Type = DnisType.User,
                                            LegTimeout = 60,
                                            LegStartDelay = 0
                                        };

                                        string grpExt = "";

                                        //string usr = sub.Username;
                                        //int comp = sub.CompanyId;
                                        //int tenant = sub.TenantId;

                                        //var grp = AbstractBackendHandler.Instance.GetGroupDetailsForUser(usr, comp, tenant);

                                        //if (grp != null)
                                        //{
                                        //    var extList = AbstractBackendHandler.Instance.GetExtensionDetailsForName(grp.GroupName, grp.CompanyID, grp.TenantID);

                                        //    var tempGrpExt = extList.FirstOrDefault(j => j.ExtType == ExtType.Group);

                                        //    if (tempGrpExt != null)
                                        //    {
                                        //        grpExt = tempGrpExt.Ext;
                                        //    }

                                        //}

                                        var tempVoicemailActive = voicemailActive;
                                        if (!voicemailActive)
                                        {
                                            tempVoicemailActive = sub.VoicemailActive;
                                        }

                                        //return LitePBX.LitePBX.RouteUser(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", ep, false, sub.VoicemailActive, grpExt).ToString();
                                        if (cType == CallType.Fax)
                                        {
                                            return LitePBX.LitePBX.RouteFax(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", ep, String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), fromGuUserId, sub.GuUserId, callerCalleeInfo, false, tempVoicemailActive, grpExt, faxType).ToString();
                                        }
                                        else
                                        {
                                            string luaParams = String.Format("CF.lua ${{originate_disposition}} '{0}' '{1}' '{2}' '{3}' '{4}' '{5}' '{6}' '{7}'", pbxConf.CompanyId, pbxConf.TenantId, sub.GuUserId, context, fromGuUserId, sub.Domain, callerCalleeInfo.FromUser, callerCalleeInfo.FromNumber);
                                            return LitePBX.LitePBX.RouteFwdUser(callUuid, extensionDetails.Ext, luaParams, context, profileName, @"[^\s]*", ep, String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), fromGuUserId, sub.GuUserId, callerCalleeInfo, false, tempVoicemailActive, grpExt, contextInf.MediaByPass).ToString();
                                        }

                                    }
                                    else
                                    {
                                        throw new Exception("User not found on subscriber");
                                    }
                                    
                                    //get forwarding endpoints
                                }
                                else if (pbxConf.ActiveApp == ActApp.None)
                                {
                                    logCSReq.Info("Active App is None");

                                    if (extensionDetails.ExtType == ExtType.User)
                                    {
                                        logCSReq.Info("----------- Pbx Normal User Dial -----------");

                                        callerCalleeInfo.ToNumber = extensionDetails.Ext;
                                        var sub = AbstractBackendHandler.Instance.GetSipSubDetails(tmpDest, extensionDetails.CompanyId, extensionDetails.TenantId);

                                        if (sub != null)
                                        {
                                            callerCalleeInfo.ToUser = sub.Username;
                                            var ep = new Endpoint
                                                {
                                                    Destination = extensionDetails.Ext,
                                                    Domain = sub.Domain,
                                                    Origination = varUserId,
                                                    Profile = profileName,
                                                    Protocol = Protocol.Unkown,
                                                    Type = DnisType.User,
                                                    LegTimeout = 20,
                                                    LegStartDelay = 0
                                                };

                                            string grpExt = "";

                                            var grp = AbstractBackendHandler.Instance.GetGroupDetailsForUser(sub.Username, sub.CompanyId, sub.TenantId);

                                            if (grp != null)
                                            {
                                                var extList = AbstractBackendHandler.Instance.GetExtensionDetailsForName(grp.GroupName, grp.CompanyID, grp.TenantID);

                                                var tempGrpExt = extList.FirstOrDefault(j => j.ExtType == ExtType.Group);

                                                if (tempGrpExt != null)
                                                {
                                                    grpExt = tempGrpExt.Ext;
                                                }

                                            }

                                            var tempVoicemailActive = voicemailActive;
                                            if (!voicemailActive)
                                            {
                                                tempVoicemailActive = sub.VoicemailActive;
                                            }

                                            //return LitePBX.LitePBX.RouteUser(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", ep, false, sub.VoicemailActive, grpExt).ToString();
                                            if (cType == CallType.Fax)
                                            {
                                                return LitePBX.LitePBX.RouteFax(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", ep, String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), fromGuUserId, sub.GuUserId, callerCalleeInfo, false, tempVoicemailActive, grpExt, faxType).ToString();
                                            }
                                            else
                                            {
                                                if (bypassMedia)
                                                {
                                                    bypassMedia = contextInf.MediaByPass;
                                                }
                                                return LitePBX.LitePBX.RouteUser(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", ep, String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), fromGuUserId, sub.GuUserId, callerCalleeInfo, false, tempVoicemailActive, grpExt, bypassMedia).ToString();
                                            }

                                        }
                                        else
                                        {
                                            throw new Exception("User not found on subscriber");
                                        }
                                    }
                                    else if (extensionDetails.ExtType == ExtType.Group)
                                    {
                                        logCSReq.Info("----------- Pbx Normal Group Dial -----------");

                                        var grp = AbstractBackendHandler.Instance.GetGroupDetailsForCompany(extensionDetails.ExtName, extensionDetails.CompanyId, extensionDetails.TenantId);

                                        if (grp != null)
                                        {
                                            var ep = new Endpoint
                                                {
                                                    Destination = extensionDetails.Ext,
                                                    Domain = grp.Domain,
                                                    Origination = varUserId,
                                                    Profile = profileName,
                                                    Protocol = Protocol.Unkown,
                                                    Type = DnisType.Group,
                                                    LegTimeout = 20,
                                                    LegStartDelay = 0
                                                };

                                            string grpExt = "";
                                            var extList = AbstractBackendHandler.Instance.GetExtensionDetailsForName(grp.GroupName, grp.CompanyID, grp.TenantID);

                                            var tempGrpExt = extList.FirstOrDefault(j => j.ExtType == ExtType.Group);

                                            if (tempGrpExt != null)
                                            {
                                                grpExt = tempGrpExt.Ext;
                                            }

                                            if (cType == CallType.Fax)
                                            {
                                                return LitePBX.LitePBX.RouteFax(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", ep, String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), fromGuUserId, "", callerCalleeInfo, false, false, grpExt, faxType).ToString();
                                            }
                                            else
                                            {
                                                if (bypassMedia)
                                                {
                                                    bypassMedia = contextInf.MediaByPass;
                                                }

                                                return LitePBX.LitePBX.RouteUser(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", ep, String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), fromGuUserId, "", callerCalleeInfo, false, false, grpExt, bypassMedia).ToString();
                                            }

                                        }
                                    }
                                }

                            }
                            else if (pbxConf.UsrStatus == PBXUsrStatus.Forward)
                            {
                                #region Forward Call - Mapping

                                logCSReq.Info(String.Format("PBXUsrStatus is Forward - Forward Endpoint : {0}", pbxConf.DefaultFwdEp));

                                string tempTrunkNum;
                                string tempTrunkCode;
                                string tempTrunkDomain;
                                TrunkClass tempTrClass;
                                int timeout;
                                int transId;
                                int aniTransId;
                                string ipUrl = "";
                                int trId;
                                string targetScript = "";

                                string dest = pbxConf.DefaultFwdEp;

                                var ext = AbstractBackendHandler.Instance.GetExtensionDetails(dest, pbxConf.CompanyId, pbxConf.TenantId);

                                if (ext != null)
                                {
                                    callerCalleeInfo.ToNumber = ext.Ext;
                                    var sub = AbstractBackendHandler.Instance.GetSipSubDetails(ext.ExtName, ext.CompanyId, ext.TenantId);

                                    if (sub != null)
                                    {
                                        callerCalleeInfo.ToUser = sub.Username;
                                        var enp = new Endpoint
                                        {
                                            Destination = ext.Ext,
                                            Domain = sub.Domain,
                                            Origination = varUserId,
                                            Profile = profileName,
                                            Protocol = Protocol.Unkown,
                                            Type = DnisType.User,
                                            LegTimeout = 20,
                                            LegStartDelay = 0
                                        };

                                        string grpExt = "";

                                        var grp = AbstractBackendHandler.Instance.GetGroupDetailsForUser(sub.Username, sub.CompanyId, sub.TenantId);

                                        if (grp != null)
                                        {
                                            var extList = AbstractBackendHandler.Instance.GetExtensionDetailsForName(grp.GroupName, grp.CompanyID, grp.TenantID);

                                            var tempGrpExt = extList.FirstOrDefault(j => j.ExtType == ExtType.Group);

                                            if (tempGrpExt != null)
                                            {
                                                grpExt = tempGrpExt.Ext;
                                            }

                                        }

                                        var tempVoicemailActive = voicemailActive;
                                        if (!voicemailActive)
                                        {
                                            tempVoicemailActive = sub.VoicemailActive;
                                        }

                                        //return LitePBX.LitePBX.RouteUser(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", ep, false, sub.VoicemailActive, grpExt).ToString();
                                        if (cType == CallType.Fax)
                                        {
                                            return LitePBX.LitePBX.RouteFax(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", enp, String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), fromGuUserId, sub.GuUserId, callerCalleeInfo, false, tempVoicemailActive, grpExt, faxType).ToString();
                                        }
                                        else
                                        {
                                            if (bypassMedia)
                                            {
                                                bypassMedia = contextInf.MediaByPass;
                                            }

                                            return LitePBX.LitePBX.RouteUser(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", enp, String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), fromGuUserId, sub.GuUserId, callerCalleeInfo, false, tempVoicemailActive, grpExt, bypassMedia).ToString();
                                        }

                                    }
                                    else
                                    {
                                        throw new Exception("User not found on subscriber");
                                    }
                                }
                                else
                                {
                                    AbstractBackendHandler.Instance.GetGwFromRule(contextInf.CompanyId, contextInf.TenantId, dest, varFromNumber, out tempTrunkNum, out tempTrunkCode, out tempTrunkDomain, out tempTrClass, out timeout, out transId, out aniTransId, out ipUrl, out trId, out targetScript);

                                    if (transId > 0)
                                    {
                                        var transDNIS = new CallRuleTranslation(AbstractBackendHandler.Instance.GetTranslationsFromDB(transId, contextInf.CompanyId, contextInf.TenantId));

                                        dest = transDNIS.Translate(dest);
                                    }
                                    var ep = new Endpoint
                                    {
                                        Destination = dest,
                                        LegStartDelay = 0,
                                        Origination = tempTrunkNum,
                                        Profile = tempTrunkCode,
                                        Protocol = Protocol.SIP,
                                        Type = DnisType.Gateway,
                                        LegTimeout = timeout,
                                        IpUrl = ipUrl
                                    };

                                    callerCalleeInfo.ToNumber = dest;
                                    callerCalleeInfo.ToUser = pbxConf.UserId;

                                    if (dodEnable)
                                    {
                                        var dodNum = AbstractBackendHandler.Instance.GetActiveTrunkPhoneNumbers(dodNumber).FirstOrDefault();

                                        if (dodNum != null)
                                        {
                                            ep.Origination = dodNumber;

                                            if ((CallType)dodNum.CallType == CallType.Fax)
                                            {
                                                return LitePBX.LitePBX.RouteFaxGateway(huntDestNum, context, profileName, @"[^\s]*", ep, fromGuUserId, contextInf.CompanyId, contextInf.TenantId, "GatewayFaxPassThrough", callerCalleeInfo, false, faxType, false, contextInf.IgnoreEarlyMedia).ToString();
                                            }
                                        }
                                        else
                                        {
                                            throw new Exception("User dodnumber not configured on trunk numbers");
                                        }
                                    }

                                    return LitePBX.LitePBX.RouteGateway(dest, context, profileName, @"[^\s]*", ep, fromGuUserId, contextInf.CompanyId, contextInf.TenantId, "GatewayForward", callerCalleeInfo, false, false, contextInf.IgnoreEarlyMedia).ToString();
                                }

                                

                                //var epConf = AbstractBackendHandler.Instance.GetEndpointConf(pbxConf.DefaultFwdEp, pbxConf.CompanyId, pbxConf.TenantId);

                                //if (epConf != null)
                                //{
                                //    if (epConf.EpType == DnisType.User)
                                //    {
                                //        #region Forward Map - User

                                //        //get user details
                                //        var extDetails = AbstractBackendHandler.Instance.GetExtensionDetails(epConf.Destination, epConf.CompanyId, epConf.TenantId);

                                //        if (extDetails != null)
                                //        {
                                //            var ep = new Endpoint
                                //                {
                                //                    Destination = epConf.Destination,
                                //                    Domain = epConf.Domain,
                                //                    Origination = varUserId,
                                //                    Profile = epConf.Profile,
                                //                    Protocol = epConf.EpProtocol,
                                //                    Type = epConf.EpType,
                                //                    LegTimeout = 20,
                                //                    LegStartDelay = 0
                                //                };

                                //            return LitePBX.LitePBX.RouteUser(callUuid, extDetails.Ext, context, profileName, @"[^\s]*", ep).ToString();
                                //        }
                                //        else
                                //        {
                                //            throw new Exception("No extension found for given destination on endpoints");
                                //        }

                                //        #endregion
                                //    }
                                //    else if (epConf.EpType == DnisType.Gateway)
                                //    {
                                //        #region Forward Map Gateway

                                //        //get trunk
                                //        string trunkNum = "";
                                //        string trunkCode = "";
                                //        string trunkDomain = "";
                                //        TrunkClass tempTrClass;

                                //        logCSReq.Info(String.Format("Endpoint : {0} found , Destination : {1}", epConf.ObjGuid, epConf.Destination));
                                //        AbstractBackendHandler.Instance.GetGwFromRule(epConf.CompanyId, epConf.TenantId, epConf.Destination, out trunkNum, out trunkCode, out trunkDomain, out tempTrClass);

                                //        logCSReq.Info(String.Format("Outbound trunk details : TrunkNumber : {0}, TrunkCode : {1}", trunkNum, trunkCode));

                                //        var ep = new Endpoint
                                //            {
                                //                Destination = epConf.Destination,
                                //                Origination = trunkNum,
                                //                Profile = trunkCode,
                                //                Protocol = epConf.EpProtocol,
                                //                Type = epConf.EpType,
                                //                LegTimeout = 20,
                                //                LegStartDelay = 0
                                //            };


                                //        return LitePBX.LitePBX.RouteGateway(destNum, context, profileName, @"[^\s]*", ep).ToString();

                                //        #endregion

                                //    }
                                //}
                                //else
                                //{
                                //    throw new Exception("Endpoint not found for defalut forward endpoint");
                                //}

                                #endregion
                            }
                        }
                        else
                        {
                            #region Normal User / Group Dial

                            logCSReq.Info("----------- Pbx Conf not found - Normal User / Group Dial -----------");

                            if (extensionDetails.ExtType == ExtType.User)
                            {
                                logCSReq.Info("----------- Normal User Dial -----------");

                                bool tempVoicemailActive = voicemailActive;

                                callerCalleeInfo.ToNumber = extensionDetails.Ext;

                                var sub = AbstractBackendHandler.Instance.GetSipSubDetails(tmpDest, extensionDetails.CompanyId, extensionDetails.TenantId);

                                if (sub != null)
                                {
                                    if (!voicemailActive)
                                    {
                                        tempVoicemailActive = sub.VoicemailActive;
                                    }

                                    callerCalleeInfo.ToUser = sub.Username;

                                    var ep = new Endpoint
                                        {
                                            Destination = extensionDetails.Ext,
                                            Domain = sub.Domain,
                                            Origination = varUserId,
                                            Profile = profileName,
                                            Protocol = Protocol.Unkown,
                                            Type = DnisType.User,
                                            LegTimeout = 20,
                                            LegStartDelay = 0
                                        };

                                    string grpExt = "";

                                    var grp = AbstractBackendHandler.Instance.GetGroupDetailsForUser(sub.Username, sub.CompanyId, sub.TenantId);

                                    if (grp != null)
                                    {
                                        var extList = AbstractBackendHandler.Instance.GetExtensionDetailsForName(grp.GroupName, grp.CompanyID, grp.TenantID);

                                        var tempGrpExt = extList.FirstOrDefault(j => j.ExtType == ExtType.Group);

                                        if (tempGrpExt != null)
                                        {
                                            grpExt = tempGrpExt.Ext;
                                        }
                                    }

                                    logCSReq.Debug(String.Format("Call Type : {0}", cType));

                                    if (cType == CallType.Fax)
                                    {
                                        return LitePBX.LitePBX.RouteFax(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", ep, String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), fromGuUserId, sub.GuUserId, callerCalleeInfo, false, tempVoicemailActive, grpExt, faxType).ToString();
                                    }
                                    else
                                    {
                                        if (bypassMedia)
                                        {
                                            bypassMedia = contextInf.MediaByPass;
                                        }


                                        return LitePBX.LitePBX.RouteUser(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", ep, String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), fromGuUserId, sub.GuUserId, callerCalleeInfo, false, tempVoicemailActive, grpExt, bypassMedia).ToString();
                                    }

                                }
                                else
                                {
                                    throw new Exception("User not found on subscriber");
                                }
                            }
                            else if (extensionDetails.ExtType == ExtType.Group)
                            {
                                logCSReq.Info("----------- Normal Group Dial -----------");

                                var grp = AbstractBackendHandler.Instance.GetGroupDetailsForCompany(extensionDetails.ExtName, extensionDetails.CompanyId, extensionDetails.TenantId);

                                if (grp != null)
                                {
                                    var ep = new Endpoint
                                        {
                                            Destination = extensionDetails.Ext,
                                            Domain = grp.Domain,
                                            Origination = varUserId,
                                            Profile = profileName,
                                            Protocol = Protocol.Unkown,
                                            Type = DnisType.Group,
                                            LegTimeout = 20,
                                            LegStartDelay = 0
                                        };

                                    //string grpExt = "";
                                    //var extList = AbstractBackendHandler.Instance.GetExtensionDetailsForName(grp.GroupName, grp.CompanyID, grp.TenantID);

                                    //var tempGrpExt = extList.FirstOrDefault(j => j.ExtType == ExtType.Group);

                                    //if (tempGrpExt != null)
                                    //{
                                    //    grpExt = tempGrpExt.Ext;
                                    //}

                                    if (cType == CallType.Fax)
                                    {
                                        return LitePBX.LitePBX.RouteFax(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", ep, String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), fromGuUserId, "", callerCalleeInfo, false, false, extensionDetails.Ext, faxType).ToString();
                                    }
                                    else
                                    {
                                        if (bypassMedia)
                                        {
                                            bypassMedia = contextInf.MediaByPass;
                                        }

                                        return LitePBX.LitePBX.RouteUser(callUuid, extensionDetails.Ext, context, profileName, @"[^\s]*", ep, String.Format("{0}_{1}", contextInf.CompanyId, contextInf.TenantId), fromGuUserId, "", callerCalleeInfo, false, false, extensionDetails.Ext, bypassMedia).ToString();
                                    }

                                }
                            }

                            #endregion
                        }
                    }
                }
                else
                {
                    #region Normal Gateway Call

                    logCSReq.Info("extension details not found - trying direct gw call");

                    //TODO:Check whether the calling party is a fax extension or not

                    string tempTrunkNum;
                    string tempTrunkCode;
                    string tempTrunkDomain;
                    string trunkIpUrl;
                    TrunkClass tempTrClass;
                    int timeout;
                    int transId;
                    int aniTransId;
                    string ipUrl = "";
                    int trId;
                    string targetScript = "";

                    AbstractBackendHandler.Instance.GetGwFromRule(contextInf.CompanyId, contextInf.TenantId, huntDestNum, varFromNumber, out tempTrunkNum, out tempTrunkCode, out tempTrunkDomain, out tempTrClass, out timeout, out transId, out aniTransId, out ipUrl, out trId, out targetScript);

                    string dest = huntDestNum;

                    if (transId > 0)
                    {
                        var transDNIS = new CallRuleTranslation(AbstractBackendHandler.Instance.GetTranslationsFromDB(transId, contextInf.CompanyId, contextInf.TenantId));

                        dest = transDNIS.Translate(huntDestNum);
                    }

                    if (!String.IsNullOrEmpty(targetScript) && targetScript.StartsWith("CUSTOMXML"))
                    {
                        logCSReq.Debug("Trying - Custom XML Routing");

                        logCSReq.Info("Call rule type is custom xml for pbx");
                        var path = ConfigurationManager.AppSettings.Get("CustomXmlFilePath");

                        if (!String.IsNullOrEmpty(path))
                        {
                            var fileStr = extensionDetails.ExtParams.Split(new[] { "_" }, StringSplitOptions.None);

                            if (fileStr.Length == 2)
                            {
                                path = path + fileStr[1];
                                var xml = CSRequestDataLayer.FileHandler.ReadFileText(path);

                                return xml;
                            }
                            else
                            {
                                logCSReq.Warn("Ext Params split length is not 2");
                            }

                        }
                        else
                        {
                            logCSReq.Warn("Custom Xml file path not found");
                        }
                    }

                    var ep = new Endpoint
                    {
                        Destination = dest,
                        LegStartDelay = 0,
                        Origination = tempTrunkNum,
                        Profile = tempTrunkCode,
                        Protocol = Protocol.SIP,
                        Type = DnisType.Gateway,
                        LegTimeout = timeout,
                        IpUrl = ipUrl
                    };

                    callerCalleeInfo.ToNumber = huntDestNum;
                    callerCalleeInfo.ToUser = huntDestNum;

                    //if (!String.IsNullOrEmpty(dodNumber))
                    //{
                    //    var dodNum = AbstractBackendHandler.Instance.GetActiveTrunkPhoneNumbers(dodNumber).FirstOrDefault();

                    //    if (dodNum != null)
                    //    {
                    //        if (dodEnable)
                    //        {
                    //            ep.Origination = dodNumber;
                    //        }

                    //        if ((CallType)dodNum.CallType == CallType.Fax)
                    //        {
                    //            return LitePBX.LitePBX.RouteFaxGateway(huntDestNum, context, profileName, @"[^\s]*", ep, fromGuUserId, contextInf.CompanyId, contextInf.TenantId, "GatewayFaxPassThrough", callerCalleeInfo, false, extraParams).ToString();
                    //        }
                    //    }
                    //}

                    if (dodEnable)
                    {
                        var dodNum = AbstractBackendHandler.Instance.GetActiveTrunkPhoneNumbers(dodNumber).FirstOrDefault();

                        if (dodNum != null)
                        {
                            ep.Origination = dodNumber;

                            if ((CallType) dodNum.CallType == CallType.Fax)
                            {
                                return LitePBX.LitePBX.RouteFaxGateway(huntDestNum, context, profileName, @"[^\s]*", ep, fromGuUserId, contextInf.CompanyId, contextInf.TenantId, "GatewayFaxPassThrough", callerCalleeInfo, false, faxType, false, contextInf.IgnoreEarlyMedia).ToString();
                            }
                        }
                        else
                        {
                            throw new Exception("User dodnumber not configured on trunk numbers");
                        }
                    }

                    return LitePBX.LitePBX.RouteGateway(huntDestNum, context, profileName, @"[^\s]*", ep, fromGuUserId, contextInf.CompanyId, contextInf.TenantId, "Gateway", callerCalleeInfo, false, false, contextInf.IgnoreEarlyMedia).ToString();

                    #endregion
                }

                
                

                return "";
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static string CallConferenceOp(SubscriberDetails sub, StringBuilder mode, ConferenceInfo confRoomInf, ExtDetails extensionDetails, string varUserId, string profileName, string context, string varFromNumber)
        {
            var confUsrInf = AbstractBackendHandler.Instance.GetConfUserInfo(confRoomInf.RoomName, confRoomInf.CompanyId, confRoomInf.TenantId);

            var chkUsrs = confUsrInf.FirstOrDefault(j => j.Status != ConferenceUserStatus.Validated && j.JoinType == ConferenceJoinType.DialOut && j.IsConnected != 1 && j.GuUserId != sub.GuUserId);

            if (chkUsrs == null)
            {
                #region Dial Conference In

                logCSReq.Info("There are validated users in the room waiting");

                var confUsr = confUsrInf.FirstOrDefault(j => j.GuUserId == sub.GuUserId && j.Status != ConferenceUserStatus.Validated);

                if (confUsr != null)
                {
                    if (confUsr.InitDeafFlag)
                    {
                        mode.Append("deaf|");
                    }
                    if (confUsr.InitModFlag)
                    {
                        mode.Append("moderator|");
                    }
                    if (confUsr.InitMuteFlag)
                    {
                        mode.Append("mute");
                    }

                    return LitePBX.LitePBX.Conference(extensionDetails.Ext, context, @"[^\s]*", confRoomInf.RoomName, confRoomInf.Domain, confRoomInf.Pin, mode.ToString(), confRoomInf.CompanyId, confRoomInf.TenantId).ToString();
                }

                #endregion
            }
            else
            {
                #region Dial Conference Out

                var confUsr = confUsrInf.FirstOrDefault(j => j.GuUserId == sub.GuUserId && j.Status != ConferenceUserStatus.Validated);

                if (confUsr != null)
                {
                    if (confUsr.InitDeafFlag)
                    {
                        mode.Append("deaf|");
                    }
                    if (confUsr.InitModFlag)
                    {
                        mode.Append("moderator|");
                    }
                    if (confUsr.InitMuteFlag)
                    {
                        mode.Append("mute");
                    }
                }

                var getAllOutbUsers = confUsrInf.Where(j => j.JoinType == ConferenceJoinType.DialOut && j.Status != ConferenceUserStatus.Validated);

                var epList = new List<Endpoint>();

                foreach (var conferenceUser in getAllOutbUsers)
                {
                    if (conferenceUser.Protocol == ConferenceEndpointProtocol.Phone)
                    {
                        #region Conf Out Gateway

                        //gateway call

                        string trunkNum;
                        string trunkCode;
                        string trunkDomain;
                        TrunkClass tempTrClass;
                        int timeout;
                        int transId;
                        int aniTransId;
                        string ipUrl = "";
                        int trId;
                        string targetScript = "";
                        //get rule
                        AbstractBackendHandler.Instance.GetGwFromRule(conferenceUser.CompanyId, conferenceUser.TenantId, conferenceUser.Endpoint, varFromNumber, out trunkNum, out trunkCode, out trunkDomain, out tempTrClass, out timeout, out transId, out aniTransId, out ipUrl, out trId, out targetScript);

                        string dest = conferenceUser.Endpoint;

                        if (transId > 0)
                        {
                            var transDNIS = new CallRuleTranslation(AbstractBackendHandler.Instance.GetTranslationsFromDB(transId, conferenceUser.CompanyId, conferenceUser.TenantId));

                            dest = transDNIS.Translate(conferenceUser.Endpoint);
                        }
                        var ep = new Endpoint
                        {
                            Destination = dest,
                            LegStartDelay = 0,
                            Origination = trunkNum,
                            Profile = trunkCode,
                            Protocol = Protocol.SIP,
                            Type = DnisType.Gateway,
                            LegTimeout = timeout,
                            IpUrl = ipUrl
                        };

                        epList.Add(ep);

                        #endregion
                    }
                    else if (conferenceUser.Protocol == ConferenceEndpointProtocol.User)
                    {
                        #region Conf Out User

                        var subscr = AbstractBackendHandler.Instance.GetSipSubDetails(conferenceUser.UserName, conferenceUser.CompanyId, conferenceUser.TenantId, conferenceUser.GuUserId);

                        if (subscr != null)
                        {
                            var ep = new Endpoint
                            {
                                Destination = conferenceUser.Endpoint,
                                Domain = subscr.Domain,
                                LegStartDelay = 0,
                                Origination = varUserId,
                                Profile = profileName,
                                Protocol = Protocol.Unkown,
                                Type = DnisType.User,
                                LegTimeout = 20
                            };

                            epList.Add(ep);

                        }
                        else
                        {
                            logCSReq.Warn("Subscriber not found for given extension");
                        }

                        #endregion
                    }

                }

                return LitePBX.LitePBX.OutCallConference(extensionDetails.Ext, context, @"[^\s]*", confRoomInf.RoomName, sub.Domain, epList, sub.CompanyId, sub.TenantId, confRoomInf.Pin, mode.ToString()).ToString();

                #endregion
            }

            return null;
        }
    }
}
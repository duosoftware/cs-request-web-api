﻿using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DuoSoftware.DC.CSRequestWebApi.WebHost.App_Start;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GlobalConfiguration.Configuration.Formatters.Insert(0, new JsonMediaFormatter());
            GlobalConfiguration.Configuration.Formatters.Insert(1, new FreeSwitchMediaFormatter());
            //GlobalConfiguration.Configuration.Formatters.Insert(1, new JsonMediaFormatter());

            //var jsonFormatter = new JsonNetFormatter();
            //jsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
            //GlobalConfiguration.Configuration.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
            //GlobalConfiguration.Configuration.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //GlobalConfiguration.Configuration.Formatters.Insert(1, new JsonMediaTypeFormatter());
        }
    }
}
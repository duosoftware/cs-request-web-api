﻿using System.Web.Mvc;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}

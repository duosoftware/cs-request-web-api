﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DialerAPI;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;


namespace DuoSoftware.DC.CSRequestWebApi.WebHost.Controllers
{
    public class DialerApiController : ApiController
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public string Post(JsonData jData)
        {
            try
            {
                var dialerApiData = new JsonDialerApiData(jData.OuterJson);

                var authRes = V5ServiceAccess.AuthServiceAccess.Instance.GetAccess(dialerApiData.SecurityToken);

                string trunkNum;
                string trunkCode;
                string trunkDomain;
                TrunkClass tempTrClass;
                int timeout;
                int transId;
                int aniTransId;
                string ipUrl = "";
                int trId;
                string targetScript = "";

                AbstractBackendHandler.Instance.GetGwFromRule(authRes.CompanyID, authRes.TenantID, dialerApiData.Dnis, "", out trunkNum, out trunkCode, out trunkDomain, out tempTrClass, out timeout, out transId, out aniTransId, out ipUrl, out trId, out targetScript);

                var result = CSReqWebApiDataOperator.GetCallServerForTrunk(trId);

                if (result != null)
                {
                    var dialerApi = new DialerAPI.Dialer();

                    string ani = String.Format("{0}@{1}", trunkNum, trunkDomain);
                    string mediaUrl = ConfigurationManager.AppSettings.Get("CurlFaxSendFileUrl");

                    var response = dialerApi.SendFax(result.CsId, result.CsName, 8080, dialerApiData.Dnis, trunkCode, Dialer.Faxtypes.T30, dialerApiData.Filename, ani, mediaUrl, dialerApiData.SecurityToken);

                    return response;
                }
                

                return "";
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}

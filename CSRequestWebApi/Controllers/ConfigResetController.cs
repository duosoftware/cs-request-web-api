﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost.Controllers
{
    public class ConfigResetController : ApiController
    {
        //
        // GET: /ConfigReset/

        public string Get()
        {
            try
            {
                var objStore = AbstractBackendHandler.Instance.ResetObjectStoreId();

                return objStore.ToString();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

    }
}

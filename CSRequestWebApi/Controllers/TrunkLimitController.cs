﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost.Controllers
{
    public class TrunkLimitController : ApiController
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public string Post(JsonData jData)
        {
            try
            {
                logCSReq.Debug("Post Message Hit - TrunkLimitController");

                var trData = new TrunkData(jData.OuterJson);

                //TODO:Check Status is Valid
                int companyId = 0;
                var trunkNumLimit = CSReqWebApiDataOperator.GetTrunkNumberLimit(trData.Context, trData.TrunkCode, trData.TrunkNumber, out companyId);

                if (trunkNumLimit >= 0)
                {
                    return String.Format("{{\"company\":\"{0}\", \"ani\":\"{1}\", \"concurrency\":\"{2}\"}}", companyId, trData.TrunkNumber, trunkNumLimit);
                }

                logCSReq.Error("Returning Default Xml");

                return "{}";


            }
            catch (Exception ex)
            {
                logCSReq.Error("ERROR on controller", ex);

                return "{}";
            }
        }

        public string Get(string ipUrl, string trunkNumber, string numType)
        {
            try
            {
                logCSReq.Debug("Get Message Hit - TrunkLimitController");

                logCSReq.Debug(String.Format("Params passed : ipUrl : {0}, trunkNumber : {1}, numType : {2}", ipUrl, trunkNumber, numType));

                var callType = (CallType)Enum.Parse(typeof(CallType), numType);

                var result = CSReqWebApiDataOperator.GetInboundTrDetailsForOpensips(trunkNumber, ipUrl, callType);

                return result;
            }
            catch (Exception ex)
            {
                logCSReq.Error("ERROR on controller", ex);

                return ",";
            }
        }
    }
}

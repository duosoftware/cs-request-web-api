﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost.Controllers
{
    public class ConferenceStatusController : ApiController
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public string Post(JsonData jData)
        {
            logCSReq.Debug("--------------------Conference Status Event-------------------------");

            var statusInfo = new JsonConfStatusData(jData.OuterJson);
            
            if (statusInfo.EvtAction.Equals("conference-create"))
            {
                //set conference uuid
                AbstractBackendHandler.Instance.UpdateConferenceRoom(statusInfo.RoomName, new Dictionary<string, object> {{"UUID", statusInfo.ConferenceId}});

            }
            else if (statusInfo.EvtAction.Equals("add-member"))
            {
                //set user state to Validated
                var roomInfo = AbstractBackendHandler.Instance.GetConferenceRoomInfoByGuid(statusInfo.ConferenceId);

                if (roomInfo != null)
                {
                    AbstractBackendHandler.Instance.UpdateConfPressence(roomInfo.RoomName, statusInfo.Username, roomInfo.CompanyId, roomInfo.TenantId, new Dictionary<string, object> { { "Status", ConferenceUserStatus.Validated }, { "IsConnected", 1 } });
                }
                
            }
            else if (statusInfo.EvtAction.Equals("start-talking"))
            {
                //set active talker

                var roomInfo = AbstractBackendHandler.Instance.GetConferenceRoomInfoByGuid(statusInfo.ConferenceId);

                if (roomInfo != null)
                {
                    AbstractBackendHandler.Instance.UpdateConfPressence(roomInfo.RoomName, statusInfo.Username, roomInfo.CompanyId, roomInfo.TenantId, new Dictionary<string, object> { { "ActiveTalker", "True" } });
                }
            }
            else if (statusInfo.EvtAction.Equals("del-member"))
            {
                var roomInfo = AbstractBackendHandler.Instance.GetConferenceRoomInfoByGuid(statusInfo.ConferenceId);

                if (roomInfo != null)
                {
                    AbstractBackendHandler.Instance.UpdateConfPressence(roomInfo.RoomName, statusInfo.Username, roomInfo.CompanyId, roomInfo.TenantId, new Dictionary<string, object> { { "Status", ConferenceUserStatus.Disconnected } });
                }
            }
            else if (statusInfo.EvtAction.Equals("conference-destroy"))
            {
                //add record to history and delete conference data
                var roomInfo = AbstractBackendHandler.Instance.GetConferenceRoomInfoByGuid(statusInfo.ConferenceId);

                if (roomInfo != null)
                {
                    var usrInf = AbstractBackendHandler.Instance.GetConfUserInfo(roomInfo.RoomName, roomInfo.CompanyId, roomInfo.TenantId);

                    var sbConnUsrs = new StringBuilder();
                    var sbNotConUsrs = new StringBuilder();

                    foreach (var usr in usrInf)
                    {
                        if (usr.IsConnected == 0)
                        {
                            sbNotConUsrs.Append(usr.UserName);
                            sbNotConUsrs.Append(",");
                        }
                        else
                        {
                            sbConnUsrs.Append(usr.UserName);
                            sbConnUsrs.Append(",");
                        }
                    }

                    AbstractBackendHandler.Instance.AddConferenceHistory(roomInfo.CompanyId, roomInfo.TenantId, roomInfo.StartTime.ToString(), roomInfo.EndTime.ToString(), roomInfo.RoomName, roomInfo.UUId, sbConnUsrs.ToString(), sbNotConUsrs.ToString(), roomInfo.ViewObjId);

                    AbstractBackendHandler.Instance.DeleteConferenceUsers(usrInf.Select(i => i.UserName).ToList());

                    AbstractBackendHandler.Instance.DeleteConferenceRoom(roomInfo.RoomName);
                }

                
            }




            return "";
        }
    }
}

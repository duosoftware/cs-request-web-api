﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Xml.Linq;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;
using DuoSoftware.DC.CSRequestWebApi.V5ServiceAccess;
using GenerateReportDataObjStore.Classes;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost.Controllers
{
    public class CdrController : ApiController
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceCdrLogger");

        public string Post(JsonData jData)
        {
            try
            {
                logCSReq.Debug("--------------------CDR-------------------------");

                var header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\r\n";
                var xele = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                    new XElement("section", new XAttribute("name", "result"),
                                                 new XElement("result", new XAttribute("status", "not found"))));

                string returnStr = header + xele.ToString();

                logCSReq.Debug(String.Format("CDR Data : \n{0}", jData.RawJsonString));

                var cdr = new JsonCdrData(jData.OuterJson);

                if (!cdr.OperationType.Equals("CallController"))
                {
                    var ctxt = AbstractBackendHandler.Instance.GetContextCategory(cdr.CallerContext);

                    string ctxtCat;
                    int companyId = 0;
                    int tenantId = 0;
                    int viewObjId = 0;

                    if (ctxt != null)
                    {
                        ctxtCat = ctxt.ContextCat.ToString();
                        companyId = ctxt.CompanyId;
                        tenantId = ctxt.TenantId;
                        viewObjId = ctxt.ViewObjId;
                    }
                    else
                    {
                        ctxtCat = "Public";
                    }

                    if (cdr.CompanyId != 0 && cdr.TenantId != 0)
                    {
                        companyId = cdr.CompanyId;
                        tenantId = cdr.TenantId;
                        logCSReq.Info(String.Format("Company - Tenant Set on Cdr Info - Overriding - CompanyId : {0}, Tenant : {1}, Uuid : {2}", companyId, tenantId, cdr.Uuid));

                    }

                    if (!string.IsNullOrEmpty(cdr.Direction))
                    {
                        string refId;
                        string trans;
                        string opType;

                        if (cdr.CurrApp.Equals("conference") && !string.IsNullOrEmpty(cdr.ConfName))
                        {
                            //generate conference cdr

                            logCSReq.Debug(String.Format("Cdr Current App is conference - uuid : {0}", cdr.Uuid));

                            var roomInf = AbstractBackendHandler.Instance.GetConferenceRoomInfoByGuid(cdr.ConfName);

                            if (roomInf != null)
                            {
                                logCSReq.Debug(String.Format("Cdr Conference room found - uuid : {0}", cdr.Uuid));
                                if (!string.IsNullOrEmpty(roomInf.UUId))
                                {
                                    refId = roomInf.UUId;
                                }
                                else
                                {
                                    refId = cdr.CallUuid;
                                }

                                opType = "conference";
                            }
                            else
                            {
                                throw new Exception(String.Format("Cdr Conference room not found - uuid : {0}", cdr.Uuid));
                            }
                        }
                        else if (cdr.CurrApp == "fifo")
                        {
                            logCSReq.Debug(String.Format("Cdr Current App is fifo - uuid : {0}", cdr.Uuid));
                            if (cdr.Leg == "A")
                            {
                                refId = cdr.Uuid;
                            }
                            else
                            {
                                refId = string.IsNullOrEmpty(cdr.BridgeUuid) ? cdr.FifoTarget : cdr.BridgeUuid;
                            }

                            opType = "fifo";
                        }
                        else
                        {
                            logCSReq.Debug(String.Format("Cdr Current App is Other - uuid : {0}", cdr.Uuid));
                            if (cdr.Direction == "outbound")
                            {
                                refId = cdr.OriginatingLegUuid;

                                if (cdr.Apps.TryGetValue("att_xfer", out trans))
                                {
                                    opType = "att_xfer";
                                }
                                else
                                {
                                    opType = cdr.OperationType;
                                }
                            }
                            else
                            {
                                string bargeAppData;
                                logCSReq.Debug(String.Format("Cdr OperationType : {0} - uuid : {1}", cdr.OperationType, cdr.Uuid));
                                if (cdr.OperationType == "Barge" && cdr.Apps.TryGetValue("eavesdrop", out bargeAppData))
                                {
                                    refId = bargeAppData;
                                }
                                else if (cdr.OperationType == "PickUp" || cdr.OperationType == "Intercept")
                                {
                                    refId = string.IsNullOrEmpty(cdr.BridgeUuid) ? cdr.CallUuid : cdr.BridgeUuid;
                                }
                                else
                                {
                                    logCSReq.Debug(String.Format("Cdr Normal Scenario - uuid : {0}", cdr.Uuid));
                                    refId = cdr.Uuid;
                                }

                                opType = cdr.OperationType;

                            }


                        }

                        string callType = "";// = string.IsNullOrEmpty(cdr.SipGatewayName) ? "user" : "gateway";

                        if (ctxtCat == "InternalPbx" || ctxtCat == "Internal")
                        {
                            callType = "outbound";
                        }
                        else
                        {
                            callType = "inbound";
                        }

                        var processedCdr = new CallCdrInfoProcessed
                        {
                            CallId = cdr.Uuid,
                            CallRefId = refId,
                            CallAnsweredTime = DateTime.Parse(cdr.CallAnsweredTime),
                            CallCreatedTime = DateTime.Parse(cdr.CallCreatedTime),
                            CallHangupTime = DateTime.Parse(cdr.CallHangupTime),
                            CallerContext = cdr.CallerContext,
                            //DialDuration = cdr.DialDuration < 0 ? 0 : cdr.DialDuration,
                            Direction = cdr.Direction,
                            ObjCategory = opType,
                            ObjType = "PBX",
                            ObjClass = "CallServer",
                            Leg = cdr.Leg,
                            //TalkDuration = cdr.TalkDuration < 0 ? 0 : cdr.TalkDuration,
                            RingStartedTime = DateTime.Parse(cdr.CallProgressTime),
                            HangupCause = cdr.HangupCause,
                            FromNumber = string.IsNullOrEmpty(cdr.FromNumber) ? cdr.CallerIdNumber : cdr.FromNumber,
                            FromUser = string.IsNullOrEmpty(cdr.FromUser) ? cdr.CallerIdName : cdr.FromUser,
                            ToNumber = string.IsNullOrEmpty(cdr.ToNumber) ? cdr.DestinationNumber : cdr.ToNumber,
                            ToUser = string.IsNullOrEmpty(cdr.ToUser) ? cdr.DestinationNumber : cdr.ToUser,
                            CompanyID = companyId,
                            TenantID = tenantId,
                            ViewObjectID = viewObjId,
                            ContextCategory = ctxtCat,
                            FromGuUserId = cdr.FromGuUserId,
                            ToGuUserId = cdr.ToGuUserId,
                            //RingDuration = cdr.RingDuration < 0 ? 0 : cdr.RingDuration,
                            BillSec = cdr.BillSec,
                            AnswerSec = cdr.AnswerSec,
                            Duration = cdr.Duration,
                            FlowBillSec = cdr.FlowBillSec,
                            ProgressSec = cdr.ProgressSec,
                            WaitSec = cdr.WaitSec,
                            ProgressMediaSec = cdr.WaitSec
                        };

                        //try
                        //{
                        //    //if (cdr.CallAnsweredTime == "0")
                        //    //{
                        //    //    if (cdr.CallHangupTime != "0" && cdr.CallCreatedTime != "0")
                        //    //    {
                        //    //        var ringTimeSpan = DateTime.Parse(cdr.CallHangupTime) - DateTime.Parse(cdr.CallCreatedTime);

                        //    //        processedCdr.RingDuration = ringTimeSpan.Seconds < 0 ? 0 : ringTimeSpan.Seconds;
                        //    //    }
                        //    //    else
                        //    //    {
                        //    //        processedCdr.RingDuration = 0;
                        //    //    }
                        //    //}
                        //    //else
                        //    //{
                        //    //    if (cdr.CallAnsweredTime != "0" && cdr.CallCreatedTime != "0")
                        //    //    {
                        //    //        var ringTimeSpan = DateTime.Parse(cdr.CallAnsweredTime) - DateTime.Parse(cdr.CallCreatedTime);

                        //    //        processedCdr.RingDuration = ringTimeSpan.Seconds < 0 ? 0 : ringTimeSpan.Seconds;
                        //    //    }
                        //    //    else
                        //    //    {
                        //    //        processedCdr.RingDuration = 0;
                        //    //    }
                        //    //}
                        //}
                        //catch (Exception)
                        //{
                        //    processedCdr.RingDuration = 0;
                        //}

                        var result1 = AbstractBackendHandler.Instance.SaveRawCallCdrInfo(new CallCdrInfo { CallId = cdr.Uuid, CdrTime = DateTime.Now, FromNumber = cdr.FromNumber, FromUser = cdr.FromUser, ToNumber = cdr.ToNumber, ToUser = cdr.ToUser,CallCdrObject = jData.RawJsonString });

                        if (result1)
                        {
                            logCSReq.Debug(String.Format("Raw cdr info saved successfully : uuid : {0}", cdr.Uuid));
                        }
                        else
                        {
                            logCSReq.Debug(String.Format("Raw cdr info not saved successfully : uuid : {0}", cdr.Uuid));
                        }

                        bool reportCdr = false;
                        bool.TryParse(ConfigurationManager.AppSettings.Get("SaveCdrForReports"), out reportCdr);

                        if (!reportCdr)
                        {
                            var result2 = AbstractBackendHandler.Instance.SaveProcessedCdrInfo(processedCdr);

                            if (result2)
                            {
                                logCSReq.Debug(String.Format("Raw cdr info saved successfully : uuid : {0}", cdr.Uuid));
                            }
                            else
                            {
                                logCSReq.Debug(String.Format("Raw cdr info not saved successfully : uuid : {0}", cdr.Uuid));
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(processedCdr.ObjCategory) || processedCdr.ObjCategory.Equals("CallController") || processedCdr.ObjCategory.Equals("unspecified"))
                            {
                                var result2 = AbstractBackendHandler.Instance.SaveProcessedCdrInfo(processedCdr);

                                return returnStr;
                            }

                            if (!String.IsNullOrEmpty(processedCdr.Leg) && processedCdr.Leg.Equals("B"))
                            {
                                string dir = "";
                                if (ctxtCat.Equals("Internal") || ctxtCat.Equals("InternalPbx"))
                                {
                                    dir = "OUTBOUND";
                                }
                                else
                                {
                                    dir = "INBOUND";
                                }

                                var timeList = new List<DateTime> { processedCdr.CallCreatedTime, processedCdr.CallAnsweredTime, processedCdr.RingStartedTime, processedCdr.CallHangupTime };

                                var endTime = timeList.OrderBy(i => i.Date).LastOrDefault();

                                var reportData = new CallRelatedInfo
                                {
                                    AcwTime = new TimeSpan(0, 0, 0, 0),
                                    CallAnswered = cdr.IsConnected,
                                    Agent = processedCdr.ToNumber,
                                    CallQueued = false,
                                    Category = processedCdr.ObjCategory,
                                    Class = "CALLSERVER",
                                    Company = processedCdr.CompanyID,
                                    Direction = dir,
                                    EndTime = endTime,
                                    HoldCount = 0,
                                    Tenant = processedCdr.TenantID,
                                    PhoneNumber = processedCdr.FromNumber,
                                    Type = "PBX",
                                    SessionId = processedCdr.CallId,
                                    StartTime = processedCdr.CallCreatedTime,
                                    TalkTime = new TimeSpan(0, 0, 0, processedCdr.BillSec),
                                    RingTime = new TimeSpan(0, 0, 0, processedCdr.ProgressMediaSec),
                                    ViewObjectId = 0,
                                    InitTime = new TimeSpan(0, 0, 0, processedCdr.AnswerSec),
                                    HoldTime = new TimeSpan(0, 0, 0, cdr.HoldSec)
                                };

                                AbstractBackendHandler.Instance.SaveReportCdrInfo(reportData);

                                
                            }
                            
                            if (processedCdr.Leg.Contains("A") && processedCdr.CompanyID != 0 && processedCdr.TenantID != 0)
                            {
                                string evtCat = "ANSWERED";

                                if (cdr.IsConnected && processedCdr.HangupCause.Equals("NORMAL_CLEARING"))
                                {
                                    evtCat = "ANSWERED";
                                }
                                else if (processedCdr.HangupCause.Equals("ORIGINATOR_CANCEL"))
                                {
                                    evtCat = "DROPPED";
                                }
                                else
                                {
                                    evtCat = "MISSED";
                                }
                                
                                var evtData = new EventData
                                {
                                    Company = processedCdr.CompanyID,
                                    EventCategory = evtCat,
                                    EventClass = "CALLSERVER",
                                    EventType = "PABX",
                                    SessionID = processedCdr.CallId,
                                    Tenent = processedCdr.TenantID,
                                    TimeStamp = DateTime.Now.ToString()
                                };

                                

                                var dashboardUrl = ConfigurationManager.AppSettings.Get("DashboardUrl");

                                if (dashboardUrl != null)
                                {
                                    var client = new RestClient();
                                    client.PostAsync(dashboardUrl, evtData, "application/json");
                                }
                                else
                                {
                                    logCSReq.Warn("No dashboard url found on config");
                                }

                            }
                            
                        }
                        
                    }
                    else
                    {
                        logCSReq.Warn("Cdr direction is null or empty - Not writing cdr");
                    }
                }
                else
                {
                    logCSReq.Info("Call Center Cdr - Skipped");
                }


                return returnStr;

            }
            catch (Exception ex)
            {
                logCSReq.Error("Exception on CDR Controller", ex);
                var xele = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                    new XElement("section", new XAttribute("name", "result"),
                                                 new XElement("result", new XAttribute("status", "not found"))));

                return xele.ToString();
            }
            
        }

    }
}

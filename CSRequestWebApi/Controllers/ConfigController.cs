﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Xml.Linq;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer;
using DuoSoftware.DC.CSRequestWebApi.CSWebResponseCreater;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost.Controllers
{
    public class ConfigController : ApiController
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public string Post(MultiFormKeyValueModel val)
        {
            try
            {
                logCSReq.Debug("Post Message Hit - ConfigController");
                string section;
                string tagName;
                string hostname;

                string defaultXml = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                                 new XElement("section", new XAttribute("name", "result"),
                                                              new XElement("result", new XAttribute("status", "not found")))).ToString();

                val.MsgData.TryGetValue("section", out section);
                val.MsgData.TryGetValue("tag_name", out tagName);
                val.MsgData.TryGetValue("hostname", out hostname);

                if (!string.IsNullOrEmpty(hostname) && !string.IsNullOrEmpty(section) && !string.IsNullOrEmpty(tagName))
                {
                    logCSReq.Debug(String.Format("MultiFormKeyValData - hostname : {0}, section : {1}, tag_name : {2}", hostname, section, tagName));

                    if (section.Equals("configuration") && tagName.Equals("configuration"))
                    {
                        int callServerId;
                        if (int.TryParse(hostname, out callServerId))
                        {
                            var profList = AbstractBackendHandler.Instance.GetCpProfileForCallServer(callServerId);

                            var profNameLst = profList.Select(i => i.ProfileName).ToList();

                            return CsWebResponse.CreateVoicemailConf(profNameLst).ToString();


                            //var result = AbstractBackendHandler.Instance.GetHoldMusicConfig(callServerId);

                            //var xmlResponse = CsWebResponse.CreateHoldMusicConf(result);

                            //return xmlResponse.ToString();

                        }
                    }
                        
                }

                logCSReq.Error("Response body parameters are invalid for config controller");

                return defaultXml;

            }
            catch (Exception ex)
            {
                logCSReq.Error("ERROR on controller", ex);

                var xele = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                    new XElement("section", new XAttribute("name", "result"),
                                                 new XElement("result", new XAttribute("status", "not found"))));

                return xele.ToString();
            }
        }
    }
}
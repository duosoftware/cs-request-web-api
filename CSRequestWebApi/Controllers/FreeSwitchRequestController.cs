﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer;
using LitePBX;
using Newtonsoft.Json.Linq;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost.Controllers
{
    public class FreeSwitchRequestController : ApiController
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public string Post(JsonData jData)
        {
            var header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\r\n";
            try
            {
                logCSReq.Debug("Post Message Hit - FreeSwitchRequestController");

                var qReqData = new JsonAddToQueueReqData(jData.OuterJson);

                var hash = RedisHandler.Instance.GetHashValues(qReqData.SessionId);

                if (hash != null)
                {
                    string jsonStr = "";

                    if (hash.TryGetValue("data", out jsonStr))
                    {
                        var jObj = JObject.Parse(jsonStr);

                        JToken fsIp;

                        if (jObj.TryGetValue("FreeSWITCH-IPv4", out fsIp))
                        {
                            var sub = CSReqWebApiDataOperator.GetSubscriberDetailsForGuUserId(qReqData.GuUserId, qReqData.CompanyId, qReqData.TenantId);

                            if (sub != null)
                            {

                                var fsApi = new FSApi(fsIp.ToString());

                                var result = fsApi.AddToFIFO(qReqData.SessionId, qReqData.UserExtension, sub.Domain);

                                if (result)
                                {
                                    logCSReq.Info(String.Format("Add to FreeSwitch Fifo Queue Successfull - SessionId : {0}", qReqData.SessionId));
                                    return header + "OK";
                                }
                                else
                                {
                                    logCSReq.Error(String.Format("Add to FreeSwitch Fifo Queue Failed - SessionId : {0}", qReqData.SessionId));
                                    return header + "FAIL";
                                }
                            }
                            else
                            {
                                logCSReq.Error(String.Format("No subscriber record found - SessionId : {0}", qReqData.SessionId));
                            }
                        }
                        else
                        {
                            logCSReq.Warn(String.Format("FreeSWITCH-IPv4 - not found on JObject - SessionId : {0}", qReqData.SessionId));
                        }
                    }
                    else
                    {
                        logCSReq.Error(String.Format("Key Data - Not found on hash - SessionId : {0}", qReqData.SessionId));
                    }

                }
                else
                {
                    logCSReq.Error(String.Format("Hash result from redis is null - FAIL - SessionId : {0}", qReqData.SessionId));
                }

                

                return header + "FAIL";
                

            }
            catch (Exception ex)
            {
                logCSReq.Error("ERROR on controller", ex);

                return header + "FAIL";
            }
        }
    }
}

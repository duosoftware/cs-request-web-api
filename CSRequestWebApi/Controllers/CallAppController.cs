﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Xml.Linq;
using DuoSoftware.DC.CSRequestWebApi.CCDialplanPicker;
using DuoSoftware.DC.CSRequestWebApi.CRTranslator;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;
using DuoSoftware.DC.CSRequestWebApi.CSWebResponseCreater;
using Newtonsoft.Json;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost.Controllers
{
    public class CallAppController : ApiController
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public string Post(MultiFormKeyValueModel val)
        {
            try
            {
                                
                logCSReq.Debug("Post Message Hit - CallAppController");
                string cdnum;
                string context;
                string hostname;
                string huntContext;
                string huntDestNum;
                string varDomain;
                string varUserId;
                string profileName;
                string callUuid;
                string varSipFromUri;
                string varSipToUri;
                string varUsrContext;
                string varFromNumber;

                string defaultXml = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                                 new XElement("section", new XAttribute("name", "result"),
                                                              new XElement("result", new XAttribute("status", "not found")))).ToString();

                val.MsgData.TryGetValue("Caller-Destination-Number", out cdnum);
                val.MsgData.TryGetValue("Caller-Context", out context);
                val.MsgData.TryGetValue("hostname", out hostname);
                val.MsgData.TryGetValue("Hunt-Destination-Number", out huntDestNum);
                val.MsgData.TryGetValue("Hunt-Context", out huntContext);
                val.MsgData.TryGetValue("variable_domain", out varDomain);
                val.MsgData.TryGetValue("variable_user_id", out varUserId);
                val.MsgData.TryGetValue("variable_sofia_profile_name", out profileName);
                val.MsgData.TryGetValue("variable_uuid", out callUuid);
                val.MsgData.TryGetValue("variable_sip_from_uri", out varSipFromUri);
                val.MsgData.TryGetValue("variable_sip_to_uri", out varSipToUri);
                val.MsgData.TryGetValue("variable_user_context", out varUsrContext);
                val.MsgData.TryGetValue("variable_FromNumber", out varFromNumber);

                logCSReq.Debug(String.Format("MultiFormKeyValData - hostname : {0}, Hunt-Destination-Number : {1}, Hunt-Context : {2}, Caller-Destination-Number : {3}, Caller-Context : {4}, variable_domain : {5}, variable_user_id : {6}, variable_sofia_profile_name : {7}, variable_uuid : {8}, variable_user_context : {9}, variable_FromNumber : {10}", hostname, huntDestNum, huntContext, cdnum, context, varDomain, varUserId, profileName, callUuid, varUsrContext, varFromNumber));

                var callerCalleeInfo = new CallerCalleeInfo();

                string dodNumber = "";
                bool dodEnabled = false;

                if (!string.IsNullOrEmpty(hostname) && !string.IsNullOrEmpty(huntDestNum) && !string.IsNullOrEmpty(huntContext))
                {
                    #region Attendant Transfer

                    if (huntContext == "PBXFIFO")
                    {
                        logCSReq.Info("-------------Pbx Fifo Direct Queue-------------");

                        var decodeUrl = HttpUtility.UrlDecode(huntDestNum);

                        var strArr = decodeUrl.Split(new string[] {"@"}, 2, StringSplitOptions.None);

                        var xmlResponse = CsWebResponse.CreatePBXFeaturesFIFOQueue(strArr[0]);

                        return xmlResponse.ToString();
                    }
                    else if (huntContext == "PBXFeatures" && huntDestNum == "att_xfer")
                    {
                        logCSReq.Info("----------- Attendant Transfer : User -----------");

                        var xmlResponse = CsWebResponse.CreatePBXFeatures(huntDestNum, "user", varDomain);

                        return xmlResponse.ToString();
                    }
                    else if (huntContext == "PBXFeatures" && huntDestNum == "att_xfer_group")
                    {
                        logCSReq.Info("----------- Attendant Transfer : Group -----------");

                        var xmlResponse = CsWebResponse.CreatePBXFeatures(huntDestNum, "group", varDomain);

                        return xmlResponse.ToString();
                    }
                    else if (huntContext == "PBXFeatures" && huntDestNum == "att_xfer_park")
                    {
                        logCSReq.Info("----------- Attendant Transfer : Park -----------");

                        var xmlResponse = CsWebResponse.CreatePBXFeaturesPark(huntDestNum, context);

                        return xmlResponse.ToString();
                    }
                    else if (huntContext == "PBXFeatures" && huntDestNum == "att_xfer_conference")
                    {
                        logCSReq.Info("----------- Attendant Transfer : Conference -----------");

                        var xmlResponse = CsWebResponse.ConferenceTransfer(huntDestNum, varDomain, varUsrContext);

                        return xmlResponse.ToString();
                    }

                    else if (huntContext == "PBXFeatures" && huntDestNum == "fifo")
                    {
                        logCSReq.Info("----------- Attendant Transfer : Fifo -----------");

                        var xmlResponse = CsWebResponse.CreatePBXFeaturesFIFO(huntDestNum);

                        return xmlResponse.ToString();
                    }


                    else if (huntContext == "PBXFeatures" && huntDestNum == "att_xfer_outbound")
                    {
                        logCSReq.Info("----------- Attendant Transfer : Outbound GW -----------");

                        if (!String.IsNullOrEmpty(varUsrContext))
                        {
                            var ctxt = AbstractBackendHandler.Instance.GetContextCategory(varUsrContext);

                            if (ctxt != null)
                            {
                                logCSReq.Debug(String.Format("Get Company for Context - Company : {0}, Tenant : {1}", ctxt.CompanyId, ctxt.TenantId));

                                string tempTrCode;
                                string tempTrNumber;
                                string tempDomain;
                                TrunkClass tempTrClass;
                                int timeout;
                                int transId;
                                int aniTransId;
                                string ipUrl = "";
                                int trId;
                                string targetScript = "";

                                AbstractBackendHandler.Instance.GetGwFromRule(ctxt.CompanyId, ctxt.TenantId, "6", varFromNumber, out tempTrNumber, out tempTrCode, out tempDomain, out tempTrClass, out timeout, out transId, out aniTransId, out ipUrl, out trId, out targetScript);

                                var xmlResponse = CsWebResponse.CreatePBXFeaturesOb(huntDestNum, String.Format("{{origination_caller_id_number={0}}}sofia/gateway/{1}", tempTrNumber, tempTrCode));

                                return xmlResponse.ToString();
                            }
                            else
                            {
                                logCSReq.Error("Cannot find an internal context");

                                return defaultXml;
                            }
                            //AbstractBackendHandler.Instance.GetCompanyForUserDomain(varUserId, varDomain, out companyId, out tenantId);

                            
                        }
                        else
                        {
                            logCSReq.Error("varUsrContext is null");

                            return defaultXml;
                        }

                    }

                    #endregion
                }

                if (!string.IsNullOrEmpty(cdnum) && !string.IsNullOrEmpty(context) && !string.IsNullOrEmpty(hostname))
                {
                    #region Dialplan

                    logCSReq.Info("----------- Dialplan -----------");

                    #region Get Context Cat

                    var destNum = "";

                    var tempContextCat = ContextCat.Public;

                    destNum = !string.IsNullOrEmpty(huntDestNum) ? huntDestNum : cdnum;
                    //CF/Company/Tenant/GuUserId/DisconReason/Context/FromGuUserId/Domain
                    if (Regex.IsMatch(destNum, "CF/"))
                    {
                        logCSReq.Info(String.Format("Call Forwarding - Handler - Destination Uri : {0}", destNum));

                        var splitStr = destNum.Split(new char[] { '/' });

                        if (splitStr.Length == 10)
                        {
                            string dest = "";
                            context = splitStr[5];
                            string domain = splitStr[7];
                            int companyId = int.Parse(splitStr[1]);
                            int tenantId = int.Parse(splitStr[2]);
                            string fromGuUserId = splitStr[6];
                            string fwdGuUserId = splitStr[3];

                            callerCalleeInfo.FromUser = splitStr[8];
                            callerCalleeInfo.FromNumber = splitStr[9];
                            
                            var disconReason = DisconnectReason.Unknown;

                            if (String.IsNullOrEmpty(splitStr[4]))
                            {
                                logCSReq.Error("Disconnection Reason is empty");

                                return defaultXml;
                            }

                            if (splitStr[4].Equals("USER_BUSY"))
                            {
                                disconReason = DisconnectReason.Busy;
                            }
                            else if (splitStr[4].Equals("NO_ANSWER"))
                            {
                                disconReason = DisconnectReason.NoAnswer;
                            }

                            var ctxtInf = AbstractBackendHandler.Instance.GetContextCategory(context);

                            bool bypassMedia = false;
                            if (ctxtInf != null && ctxtInf.ContextCat == ContextCat.InternalPbx)
                            {
                                bypassMedia = ctxtInf.MediaByPass;
                            }

                            var fwdConf = AbstractBackendHandler.Instance.GetFwdRule(disconReason, fwdGuUserId);

                            if (fwdConf != null)
                            {
                                if (bool.Parse(fwdConf.IsActive))
                                {
                                    dest = fwdConf.UserProfile;
                                    var extension = AbstractBackendHandler.Instance.GetExtensionDetails(dest, companyId, tenantId);

                                    if (extension != null)
                                    {
                                        var ep = new Endpoint
                                        {
                                            Destination = dest,
                                            Domain = domain,
                                            Origination = varUserId,
                                            Profile = profileName,
                                            Protocol = Protocol.Unkown,
                                            Type = DnisType.User,
                                            LegTimeout = 60,
                                            LegStartDelay = 0
                                        };

                                        callerCalleeInfo.ToNumber = extension.Ext;
                                        callerCalleeInfo.ToUser = extension.ExtName;

                                        return LitePBX.LitePBX.RouteUser(callUuid, dest, context, profileName, @"[^\s]*", ep, String.Format("{0}_{1}", companyId, tenantId), fromGuUserId, extension.GuUserId, callerCalleeInfo, false, false, "", bypassMedia, false, "CondFwdUser").ToString();
                                    }
                                    else
                                    {
                                        string tempTrunkNum;
                                        string tempTrunkCode;
                                        string tempTrunkDomain;
                                        string trunkIpUrl;
                                        TrunkClass tempTrClass;
                                        int timeout;
                                        int transId;
                                        int aniTransId;
                                        string ipUrl = "";
                                        int trId;
                                        string targetScript = "";

                                        AbstractBackendHandler.Instance.GetGwFromRule(companyId, tenantId, dest, varFromNumber, out tempTrunkNum, out tempTrunkCode, out tempTrunkDomain, out tempTrClass, out timeout, out transId, out aniTransId, out ipUrl, out trId, out targetScript);

                                        if (transId > 0)
                                        {
                                            var transDNIS = new CallRuleTranslation(AbstractBackendHandler.Instance.GetTranslationsFromDB(transId, companyId, companyId));

                                            dest = transDNIS.Translate(huntDestNum);
                                        }

                                        var ep = new Endpoint
                                        {
                                            Destination = dest,
                                            LegStartDelay = 0,
                                            Origination = tempTrunkNum,
                                            Profile = tempTrunkCode,
                                            Protocol = Protocol.SIP,
                                            Type = DnisType.Gateway,
                                            LegTimeout = timeout,
                                            IpUrl = ipUrl
                                        };

                                        callerCalleeInfo.ToNumber = huntDestNum;
                                        callerCalleeInfo.ToUser = huntDestNum;

                                        return LitePBX.LitePBX.RouteGateway(dest, context, profileName, @"[^\s]*", ep, fromGuUserId, companyId, tenantId, "CondFwdGateway", callerCalleeInfo, false, false).ToString();
                                    }


                                }
                                else
                                {
                                    logCSReq.Warn("Forwarding rule not active");
                                    return defaultXml;
                                }
                            }
                            else
                            {
                                logCSReq.Warn("No call forwarding rules found");
                                return defaultXml;
                            }
                        }
                        //Get Forwarding Rules and find the endpoint to call
                        
                        
                    }

                    var contextInf = AbstractBackendHandler.Instance.GetContextCategory(context);

                    if (contextInf != null)
                    {
                        tempContextCat = contextInf.ContextCat;
                    }

                    #endregion
                    

                    if (contextInf != null && tempContextCat == ContextCat.InternalPbx)
                    {
                        #region Internal PBX
                        string fromGuUserId = "";
                        string faxType = "";
                        if (!string.IsNullOrEmpty(varUserId))
                        {
                            int tempCompId;
                            tempCompId = contextInf.CompanyId;
                            int tempTenantId;
                            tempTenantId = contextInf.TenantId;
                            

                            var sub = AbstractBackendHandler.Instance.GetSipSubDetails(varUserId, tempCompId, tempTenantId);

                            if (sub != null)
                            {
                                fromGuUserId = sub.GuUserId;
                                callerCalleeInfo.FromUser = sub.Username;

                                var ext = CSReqWebApiDataOperator.GetExtensionDetailsForCompanyGuUserId(sub.GuUserId, sub.CompanyId, sub.TenantId);

                                if (ext != null)
                                {
                                    callerCalleeInfo.FromNumber = ext.Ext;
                                    dodEnabled = ext.DodEnabled == "True";
                                    dodNumber = ext.DodNumber;
                                    faxType = ext.ExtParams ?? "";
                                }
                            }

                        }
                        
                        logCSReq.Info("Context category is Internal PBX");
                        var fResult = LogicOperationsHandler.FeatureCodeOperations(destNum, contextInf, context, profileName, fromGuUserId, callerCalleeInfo);

                        if (!string.IsNullOrEmpty(fResult))
                        {
                            return fResult;
                        }

                        var result = LogicOperationsHandler.PbxOperations(destNum, varFromNumber, null, contextInf, context, profileName, varUserId, huntDestNum, callUuid, fromGuUserId, callerCalleeInfo, dodEnabled, dodNumber, contextInf.MediaByPass, CallType.Unknown, contextInf.VoicemailEnabled, faxType);

                        if (result != null)
                        {
                            return result;
                        }
                        else
                        {
                            logCSReq.Warn("No suitable path found for pbx user context");
                        }

                        #endregion
                    }
                    else if(tempContextCat == ContextCat.Public)
                    {
                        #region ARDS User Dialplan

                        logCSReq.Debug(String.Format("MultiFormKeyValData - hostname : {0}, cdnum : {1}, context : {2}", hostname, cdnum, "Public"));

                        logCSReq.Info("Context cat - Public");

                        #region PBX Call with context public

                        if (destNum.StartsWith("pbx"))
                        {
                            logCSReq.Info(String.Format("Destination number contains string pbx - Dest Num : {0}", destNum));
                            var splitStr = destNum.Split(new string[] {"/"}, 3, StringSplitOptions.None);

                            if (splitStr.Length == 3)
                            {
                                var ctxtInf = AbstractBackendHandler.Instance.GetContextCategory(splitStr[1]);

                                if (ctxtInf != null && ctxtInf.ContextCat == ContextCat.InternalPbx)
                                {
                                    int tempCompId = ctxtInf.CompanyId;
                                    int tempTenantId = ctxtInf.TenantId;
                                    string fromGuUserId = "";

                                    if (!string.IsNullOrEmpty(varUserId))
                                    {
                                        var sub = AbstractBackendHandler.Instance.GetSipSubDetails(varUserId, tempCompId, tempTenantId);

                                        if (sub != null)
                                        {
                                            fromGuUserId = sub.GuUserId;
                                            callerCalleeInfo.FromUser = sub.Username;

                                            var ext = CSReqWebApiDataOperator.GetExtensionDetailsForCompanyGuUserId(sub.GuUserId, sub.CompanyId, sub.TenantId);

                                            if (ext != null)
                                            {
                                                callerCalleeInfo.FromNumber = ext.Ext;
                                            }
                                        }
                                    }

                                    var fResult = LogicOperationsHandler.FeatureCodeOperations(destNum, contextInf, context, profileName, fromGuUserId, callerCalleeInfo);

                                    if (!string.IsNullOrEmpty(fResult))
                                    {
                                        return fResult;
                                    }

                                    var result = LogicOperationsHandler.PbxOperations(splitStr[2], varFromNumber, null, ctxtInf, context, profileName, varUserId, splitStr[2], callUuid, fromGuUserId, callerCalleeInfo, dodEnabled, dodNumber, false, CallType.Unknown, ctxtInf.VoicemailEnabled);

                                    if (result != null)
                                    {
                                        return result;
                                    }
                                    else
                                    {
                                        logCSReq.Warn("No suitable path found for pbx user context");
                                    }
                                }
                                else
                                {
                                    logCSReq.Warn("Context extracted from hunt dest num is not Internal PBX or does not exsist");
                                }
                                
                            }
                            else
                            {
                                logCSReq.Warn("Destination number does not contain 3 split elements");
                            }
                        }
                        #endregion

                        else
                        {
                            //TODO:Get Inbound Call Rule

                            var decodedSipFromUri = HttpUtility.UrlDecode(varSipFromUri);
                            var decodedSipToUri = HttpUtility.UrlDecode(varSipToUri);

                            int callServerId;
                            if (int.TryParse(hostname, out callServerId))
                            {
                                if (Regex.IsMatch(destNum, "CCID/"))
                                {
                                    #region Direct CC Transfer

                                    logCSReq.Info("Direct CC Transfer - with CCID given specifically");
                                    var splitStr = destNum.Split(new char[] {'/'});

                                    if (splitStr.Length == 2)
                                    {
                                        var cc = AbstractBackendHandler.Instance.GetSpecificCallController(int.Parse(splitStr[1]));

                                        if (cc == null)
                                        {
                                            throw new Exception("Cannot find a active call controller with the given id when getting specific CC");
                                        }

                                        logCSReq.Debug(String.Format("Call Controller Picked - CCID : {0}", cc.CallControllerId));

                                        var xmlResponse = CsWebResponse.CreateDialplan(@"[^\s]*", context, cc.TcpIp, cc.TcpPort.ToString(CultureInfo.InvariantCulture));

                                        AbstractBackendHandler.Instance.UpdateCcReqTimeStamp(cc.CallControllerId);

                                        return xmlResponse.ToString();
                                    }
                                    else
                                    {
                                        logCSReq.Error("Invalid CCID Pattern");
                                    }

                                    #endregion
                                }
                                else
                                {
                                    int companyId;
                                    int tenantId;
                                    int trunkNumLimit;
                                    CallType cType = CallType.Unknown;
                                    string scheduleId;

                                    logCSReq.Info(String.Format("Getting Inbound rule - csId : {0}, ani : {1}, dnis : {2}", callServerId, decodedSipFromUri, decodedSipToUri));

                                    var splitArrANI = decodedSipFromUri.Split(new string[] { "@" }, 2, StringSplitOptions.None);

                                    string domain = "";
                                    string aniNum = "";

                                    if (splitArrANI.Length == 2)
                                    {
                                        domain = splitArrANI[1];

                                        var domAndPort = domain.Split(new string[] { ":" }, 2, StringSplitOptions.None);

                                        if (domAndPort.Length == 2)
                                        {
                                            domain = domAndPort[0];
                                        }

                                        aniNum = splitArrANI[0];
                                    }

                                    var splitArrDNIS = decodedSipToUri.Split(new string[] { "@" }, 2, StringSplitOptions.None);

                                    string number = splitArrDNIS[0];

                                    var tempNum = number.Split(new char[] { '-' });

                                    number = tempNum[0];

                                    var trId = CSReqWebApiDataOperator.FindIncomingTrunkId(callServerId, number, domain, out trunkNumLimit, out companyId, out tenantId, out cType, out scheduleId);

                                    logCSReq.Debug(String.Format("Public call - Checking for DID Extension - Number : {0}, CompanyID : {1}, TenantID : {2}, CallType : {3}", number, companyId, tenantId, cType.ToString()));

                                    var extForDid = AbstractBackendHandler.Instance.GetExtnsionDetailsForDid(number, companyId, tenantId);

                                    if (extForDid != null)
                                    {
                                        logCSReq.Info("DID Enabled");
                                        var ctxtInf = AbstractBackendHandler.Instance.GetContextCategory(extForDid.Context);

                                        if (ctxtInf != null && ctxtInf.ContextCat == ContextCat.InternalPbx)
                                        {

                                            logCSReq.Debug(String.Format("Context : {0}, Voicemail Status : {1}", ctxtInf.ContextName, ctxtInf.VoicemailEnabled));
                                            var result = LogicOperationsHandler.PbxOperations(extForDid.Ext, varFromNumber, extForDid, ctxtInf, context, profileName, varUserId, huntDestNum, callUuid, "", new CallerCalleeInfo(), false, "", false, cType, ctxtInf.VoicemailEnabled, extForDid.ExtParams);

                                            return result;
                                        }
                                    }

                                    ////

                                    else
                                    {
                                        logCSReq.Debug(String.Format("Trying Schedule Id : {0}", scheduleId));
                                        if (!String.IsNullOrEmpty(scheduleId))
                                        {
                                            var schedules = AbstractBackendHandler.Instance.GetAppointmentsForSchedule(scheduleId);

                                            if (schedules != null && schedules.Count > 0)
                                            {
                                                var schController = new DuoSoftware.CSScheduleController.Scheduler();

                                                logCSReq.Debug(String.Format("Picking Schedule Current Date : {0}", DateTime.Now));
                                                var appointment = schController.PickSchedule(schedules, DateTime.Now, companyId, tenantId);

                                                if (appointment != null)
                                                {
                                                    if (appointment.EndpointType.Equals("Extension"))
                                                    {
                                                        var ext = AbstractBackendHandler.Instance.GetExtensionDetails(appointment.Endpoint, companyId, tenantId);

                                                        if (ext != null)
                                                        {
                                                            var ctxtInf = AbstractBackendHandler.Instance.GetContextCategory(ext.Context);

                                                            if (ctxtInf != null && ctxtInf.ContextCat == ContextCat.InternalPbx)
                                                            {

                                                                logCSReq.Debug(String.Format("Context : {0}, Voicemail Status : {1}", ctxtInf.ContextName, ctxtInf.VoicemailEnabled));
                                                                var result = LogicOperationsHandler.PbxOperations(ext.Ext, varFromNumber, ext, ctxtInf, context, profileName, varUserId, huntDestNum, callUuid, "", new CallerCalleeInfo(), false, "", false, cType, ctxtInf.VoicemailEnabled, ext.ExtParams);

                                                                return result;
                                                            }
                                                            else
                                                            {
                                                                logCSReq.Debug("Context not found for scheduling");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            logCSReq.Debug(String.Format("Extension not found for endpoint : {0}", appointment.Endpoint));
                                                        }

                                                    }
                                                    else if (appointment.EndpointType.Equals("Ivr"))
                                                    {
                                                        var sessionData = new SessionData
                                                        {
                                                            path = appointment.Endpoint,
                                                            company = companyId.ToString(),
                                                            tenent = tenantId.ToString(),
                                                            app = ""
                                                        };

                                                        var profile = CSReqWebApiDataOperator.GetMyProfileDetailsDb(companyId, tenantId);

                                                        if (profile != null)
                                                        {
                                                            var prof = profile.FirstOrDefault(i => (ProfileType)i.ProfileType == ProfileType.InternalProfile && i.CpId == int.Parse(hostname));

                                                            if (prof != null)
                                                            {
                                                                sessionData.domain = prof.InternalIp;
                                                            }
                                                            else
                                                            {
                                                                logCSReq.Debug(String.Format("No internal profiles found for Callserver : {0}", hostname));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            logCSReq.Debug(String.Format("No profiles found for Company : {0} , Tenant : {1}", companyId, tenantId));
                                                        }

                                                        var ctxtInf = CSReqWebApiDataOperator.GetInternalPbxContextConf(companyId, tenantId);

                                                        if (ctxtInf != null)
                                                        {
                                                            sessionData.pbxcontext = ctxtInf.ContextName;
                                                        }
                                                        else
                                                        {
                                                            logCSReq.Debug(String.Format("No internal pbx context found for Company : {0} , Tenant : {1}", companyId, tenantId));
                                                        }

                                                        var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(sessionData, Formatting.Indented);
                                                        logCSReq.Info(String.Format("Redis object added successfully for dev inbound rule - Key : {0}_data, Value : {1}", callUuid, jsonString));
                                                        //var jsonString = String.Format("{{\n\"path\":\"{0}\"\n,\"company\":\"{1}\"\n,\"tenent\":\"{2}\"\n}}", rule.Url, rule.CompanyId, rule.TenantId);

                                                        var response = RedisHandler.Instance.AddToRedisCache(callUuid + "_data", sessionData, new TimeSpan(0,1,0,0));

                                                        if (response)
                                                        {
                                                            var xmlResponse = CsWebResponse.CreateDialplanToUrl(@"[^\s]*", context, companyId, number, trunkNumLimit, String.Format("CustomCompanyStr={0}_{1}", companyId, tenantId));

                                                            return xmlResponse.ToString();
                                                        }
                                                        else
                                                        {
                                                            logCSReq.Info("Redis object add failed for dev inbound rule");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        logCSReq.Debug("Endpoint Type is not extension");
                                                    }
                                                }
                                                else
                                                {
                                                    logCSReq.Debug("Appointments not picked with the current configuration");
                                                }
                                            }
                                            else
                                            {
                                                logCSReq.Warn("No appointments found for schedule");
                                            }
                                        }
                                        else
                                        {
                                            logCSReq.Warn("Trying Schedule Id is null");
                                        }
                                    }

                                    ////

                                    if (cType == CallType.Fax)
                                    {
                                        logCSReq.Info("Call type is Fax");
                                        //get security token

                                        var token = V5ServiceAccess.AuthServiceAccess.Instance.GetSecurityToken(companyId);

                                        if (token != null)
                                        {
                                            return LitePBX.LitePBX.ReciveFax(huntDestNum, context, @"[^\s]*", "", companyId, tenantId, token).ToString();
                                        }
                                        else
                                        {
                                            throw new Exception(String.Format("Compnay : {0} not found on token list", companyId));
                                        }
                                    }

                                    

                                    var rule = CSReqWebApiDataOperator.GetInboundRule(number, aniNum, trId, companyId, tenantId);
                                    //var rule = CSReqWebApiDataOperator.CheckAndGetInboundRule(callServerId, decodedSipFromUri, decodedSipToUri);

                                    if (rule != null)
                                    {
                                        //TODO: Set Call Limit
                                        if (rule.CallType == 4)
                                        {
                                            logCSReq.Info("Call type - DevInbound");
                                            //add object to redis cache

                                            var sessionData = new SessionData
                                            {
                                                path = rule.Url,
                                                company = companyId.ToString(),
                                                tenent = tenantId.ToString(),
                                                app = rule.RuleParameter

                                            };

                                            var profile = CSReqWebApiDataOperator.GetMyProfileDetailsDb(companyId, tenantId);

                                            if (profile != null)
                                            {
                                                var prof = profile.FirstOrDefault(i => (ProfileType)i.ProfileType == ProfileType.InternalProfile && i.CpId == int.Parse(hostname));

                                                if (prof != null)
                                                {
                                                    sessionData.domain = prof.InternalIp;
                                                }
                                                else
                                                {
                                                    logCSReq.Debug(String.Format("No internal profiles found for Callserver : {0}", hostname));
                                                }
                                            }
                                            else
                                            {
                                                logCSReq.Debug(String.Format("No profiles found for Company : {0} , Tenant : {1}", companyId, tenantId));
                                            }

                                            var ctxtInf = CSReqWebApiDataOperator.GetInternalPbxContextConf(companyId, tenantId);

                                            if (ctxtInf != null)
                                            {
                                                sessionData.pbxcontext = ctxtInf.ContextName;
                                            }
                                            else
                                            {
                                                logCSReq.Debug(String.Format("No internal pbx context found for Company : {0} , Tenant : {1}", companyId, tenantId));
                                            }

                                            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(sessionData, Formatting.Indented);
                                            logCSReq.Info(String.Format("Redis object added successfully for dev inbound rule - Key : {0}_data, Value : {1}", callUuid, jsonString));
                                            //var jsonString = String.Format("{{\n\"path\":\"{0}\"\n,\"company\":\"{1}\"\n,\"tenent\":\"{2}\"\n}}", rule.Url, rule.CompanyId, rule.TenantId);

                                            var response = RedisHandler.Instance.AddToRedisCache(callUuid + "_data", sessionData, new TimeSpan(0,1,0,0));

                                            if (response)
                                            {
                                                var xmlResponse = CsWebResponse.CreateDialplanToUrl(@"[^\s]*", context, companyId, number, trunkNumLimit, String.Format("CustomCompanyStr={0}_{1}", companyId, tenantId));

                                                return xmlResponse.ToString();
                                            }
                                            else
                                            {
                                                logCSReq.Info("Redis object add failed for dev inbound rule");
                                            }
                                        }
                                        else if (rule.CallType == 5)
                                        {
                                            //load file
                                            logCSReq.Info("Call rule type is custom xml for pbx");
                                            var path = ConfigurationManager.AppSettings.Get("CustomXmlFilePath");

                                            if (!String.IsNullOrEmpty(path))
                                            {
                                                path = path + rule.Url;
                                                var xml = CSRequestDataLayer.FileHandler.ReadFileText(path);

                                                return xml;
                                            }
                                            else
                                            {
                                                logCSReq.Warn("Custom Xml file path not found");
                                            }
                                            
                                        }
                                        //TODO : have to check rule consists of a direct xml from file
                                        else
                                        {
                                            logCSReq.Info("Call type - Other");

                                            #region Round Robbing CC Transfer

                                            var availControllers = AbstractBackendHandler.Instance.GetAvailableCallControllers(callServerId);

                                            if (availControllers.Count > 0)
                                            {
                                                logCSReq.Debug(String.Format("Available controllers found - Count : {0}", availControllers.Count));

                                                int algorithm = 1;
                                                int.TryParse(ConfigurationManager.AppSettings.Get("DialplanPickAlgorithm"), out algorithm);

                                                IDialplanPickAlgorithm algo = new RoundRobbing();

                                                if ((CcPickingAlgorithm)algorithm == CcPickingAlgorithm.PercentageBased)
                                                {
                                                    algo = new PercentageBased();
                                                }
                                                else if ((CcPickingAlgorithm)algorithm == CcPickingAlgorithm.RoundRobbing)
                                                {
                                                    algo = new RoundRobbing();
                                                }

                                                var cc = algo.PickCallController(availControllers, rule.CompanyId, rule.TenantId);

                                                logCSReq.Debug(String.Format("Call Controller Picked - CCID : {0}", cc.CallControllerId));

                                                var xmlResponse = CsWebResponse.CreateDialplan(@"[^\s]*", context, cc.TcpIp, cc.TcpPort.ToString(CultureInfo.InvariantCulture));

                                                AbstractBackendHandler.Instance.UpdateCcReqTimeStamp(cc.CallControllerId);

                                                return xmlResponse.ToString();
                                            }
                                            else
                                            {
                                                logCSReq.Warn("No available call controllers found");
                                            }

                                            #endregion

                                        }

                                        //call litepbx web response string
                                    }
                                    else
                                    {
                                        logCSReq.Error("No rule returned");
                                    }
                                }

                            }
                            else
                            {
                                logCSReq.Error("Call server id is not an integer value");
                            }
                        }
                        
                        

                        #endregion
                    }
                    else if (contextInf != null && tempContextCat == ContextCat.Internal)
                    {
                        logCSReq.Debug(String.Format("MultiFormKeyValData - hostname : {0}, cdnum : {1}, context : {2}", hostname, cdnum, contextInf));


                        //TODO:Get Inbound Call Rule
                        
                        int callServerId;
                        if (int.TryParse(hostname, out callServerId))
                        {

                            //TODO: Set Call Limit

                            if (Regex.IsMatch(destNum, "CCID/"))
                            {
                                #region Direct CC Transfer

                                var splitStr = destNum.Split(new char[] {'/'});

                                if (splitStr.Length == 2)
                                {
                                    var cc = AbstractBackendHandler.Instance.GetSpecificCallController(int.Parse(splitStr[1]));

                                    if (cc == null)
                                    {
                                        throw new Exception("Cannot find a active call controller with the given id when getting specific CC");
                                    }

                                    logCSReq.Debug(String.Format("Call Controller Picked - CCID : {0}", cc.CallControllerId));

                                    var xmlResponse = CsWebResponse.CreateDialplan(@"[^\s]*", context, cc.TcpIp, cc.TcpPort.ToString(CultureInfo.InvariantCulture));

                                    AbstractBackendHandler.Instance.UpdateCcReqTimeStamp(cc.CallControllerId);

                                    return xmlResponse.ToString();
                                }
                                else
                                {
                                    logCSReq.Error("Invalid CCID Pattern");
                                }

                                #endregion
                            }
                            else
                            {
                                #region Round Robbing CC Transfer

                                var availControllers = AbstractBackendHandler.Instance.GetAvailableCallControllers(callServerId);

                                if (availControllers.Count > 0)
                                {
                                    logCSReq.Debug(String.Format("Available controllers found - Count : {0}", availControllers.Count));

                                    int algorithm = 1;
                                    int.TryParse(ConfigurationManager.AppSettings.Get("DialplanPickAlgorithm"), out algorithm);

                                    IDialplanPickAlgorithm algo = new RoundRobbing();

                                    if ((CcPickingAlgorithm) algorithm == CcPickingAlgorithm.PercentageBased)
                                    {
                                        algo = new PercentageBased();
                                    }
                                    else if ((CcPickingAlgorithm) algorithm == CcPickingAlgorithm.RoundRobbing)
                                    {
                                        algo = new RoundRobbing();
                                    }

                                    var cc = algo.PickCallController(availControllers, contextInf.CompanyId, contextInf.TenantId);

                                    logCSReq.Debug(String.Format("Call Controller Picked - CCID : {0}", cc.CallControllerId));

                                    var xmlResponse = CsWebResponse.CreateDialplan(@"[^\s]*", context, cc.TcpIp, cc.TcpPort.ToString(CultureInfo.InvariantCulture));

                                    AbstractBackendHandler.Instance.UpdateCcReqTimeStamp(cc.CallControllerId);

                                    return xmlResponse.ToString();
                                }
                                else
                                {
                                    logCSReq.Warn("No available call controllers found");
                                }

                                #endregion
                            }

                        }
                        else
                        {
                            logCSReq.Error("Call server id is not an integer value");
                        }
                    }


                }

              #endregion


                logCSReq.Error("Returning Default Xml");

                return defaultXml;



            }
            catch (Exception ex)
            {
                logCSReq.Error("ERROR on controller", ex);

                var xele = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "result"),
                                                     new XElement("result", new XAttribute("status", "not found"))));

                return xele.ToString();
            }
        }
    }
}

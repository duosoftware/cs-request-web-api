﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;
using DuoSoftware.DC.CSRequestWebApi.V5ServiceAccess;
using LitePBX;
using Newtonsoft.Json.Linq;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost.Controllers
{
    public class RegistrationStatusController : ApiController
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public string Post(JsonData jData)
        {
            var header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\r\n";

            try
            {
                logCSReq.Debug("Post Message Hit - RegistrationStatusController");

                var reg = new JsonRegistrationData(jData.OuterJson);

                //TODO:Check Status is Valid
                bool chkDomain = reg.RegStatus != "Unregistered";

                var subDetails = CSReqWebApiDataOperator.GetSubscriberDetailsForDomain(reg.Username, reg.Realm, chkDomain);

                if (subDetails != null)
                {
                    var extDetails = CSReqWebApiDataOperator.GetExtensionDetailsForCompanyGuUserId(subDetails.GuUserId, subDetails.CompanyId, subDetails.TenantId);

                    if (extDetails != null)
                    {
                        var ctxt = AbstractBackendHandler.Instance.GetContextCategory(extDetails.Context);

                        if (ctxt != null)
                        {
                            if (ctxt.ContextCat == ContextCat.InternalPbx)
                            {
                                var hashId = String.Format("{0}@{1}", reg.Username, reg.Realm);

                                var token = AuthServiceAccess.Instance.GetSecurityToken(subDetails.CompanyId);

                                ResourceProxyServiceAccess.ResourceModeChangeRequest(subDetails.GuUserId, reg.RegStatus, subDetails.CompanyId, "", "");

                                if (reg.RegStatus == "Registered")
                                {
                                    var setHashGuidResult = RedisHandler.Instance.AddToHash(hashId, "guuserid", subDetails.GuUserId);
                                    var setHashCompanyResult = RedisHandler.Instance.AddToHash(hashId, "companyid", subDetails.CompanyId.ToString());

                                    if (!setHashGuidResult)
                                    {
                                        logCSReq.Error(String.Format("Add to hashid : {0}, failed for user : {1}, domain : {2}, GuUserId : {3}", hashId, reg.Username, reg.Realm, subDetails.GuUserId));
                                    }

                                    if (!setHashCompanyResult)
                                    {
                                        logCSReq.Error(String.Format("Add to hashid : {0}, failed for user : {1}, domain : {2}, CompanyId : {3}", hashId, reg.Username, reg.Realm, subDetails.CompanyId));
                                    }

                                    if (!String.IsNullOrEmpty(token))
                                    {
                                        logCSReq.Info(String.Format("GetMaxConcurrencyForPabxAgent hashId : {0}, for user : {1}, domain : {2}, CompanyId : {3}", hashId, reg.Username, reg.Realm, subDetails.CompanyId));
                                        var maxConc = ResourceMonitorServiceAccess.GetMaxConcurrencyForPabxAgent(token, subDetails.GuUserId);

                                        logCSReq.Info(String.Format("GetMaxConcurrencyForPabxAgent - Done - MaxConc : {0}", maxConc));

                                        var hash = RedisHandler.Instance.GetHashValues(hashId);

                                        if (hash != null)
                                        {
                                            string jsonStr = "";

                                            if (hash.TryGetValue("Data", out jsonStr))
                                            {
                                                var jObj = JObject.Parse(jsonStr);

                                                JToken fsIp;

                                                if (jObj.TryGetValue("FreeSWITCH-IPv4", out fsIp))
                                                {
                                                    var ext = CSReqWebApiDataOperator.GetExtensionDetailsForCompanyGuUserId(subDetails.GuUserId, subDetails.CompanyId, subDetails.TenantId);

                                                    if (ext != null)
                                                    {
                                                        var fsApi = new FSApi(fsIp.ToString());

                                                        var result = fsApi.AddAgentToFIFO(ext.Ext, ext.Ext, reg.Realm, maxConc, subDetails.GuUserId);

                                                        if (result)
                                                        {
                                                            logCSReq.Info("Add to FreeSwitch Fifo Agent Queue Successfull");
                                                        }
                                                        else
                                                        {
                                                            logCSReq.Error("Add to FreeSwitch Fifo Agent Queue Failed");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        logCSReq.Error("No extension record found");
                                                    }
                                                }
                                                else
                                                {
                                                    logCSReq.Warn("FreeSWITCH-IPv4 - not found on JObject");
                                                }
                                            }
                                            else
                                            {
                                                logCSReq.Error("Key Data - Not found on hash");
                                            }

                                        }
                                        else
                                        {
                                            logCSReq.Error("Hash result from redis is null - FAIL");
                                        }
                                    }
                                    else
                                    {
                                        logCSReq.Error(String.Format("SecurityToken for company : {0} returned null or empty", subDetails.CompanyId));
                                    }
                                    //var fsApi = new FSApi();
                                }
                            }
                            else
                            {
                                logCSReq.Error("Context is not internal pbx");
                            }
                        }
                        else
                        {
                            logCSReq.Error("No context found");
                        }
                    }

                    

                    //ResourceProxyServiceAccess.ResourceModeChangeRequest(subDetails.GuUserId, reg.RegStatus, subDetails.CompanyId, "", "");
                }
                else
                {
                    throw new Exception("User not found for given username and domain");
                }
                

                string defaultXml = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                                 new XElement("section", new XAttribute("name", "result"),
                                                              new XElement("result", new XAttribute("status", "not found")))).ToString();




                logCSReq.Error("Returning Default Xml");

                return header + defaultXml;



            }
            catch (Exception ex)
            {
                logCSReq.Error("ERROR on controller", ex);

                var xele = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "result"),
                                                     new XElement("result", new XAttribute("status", "not found"))));

                return header + xele.ToString();
            }
        }
    }
}

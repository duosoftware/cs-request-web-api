﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer;
using DuoSoftware.DC.CSRequestWebApi.V5ServiceAccess;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost.Controllers
{
    public class ChannelStateController : ApiController
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceChanStateLogger");

        public string Post(JsonData jData)
        {
            var header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\r\n";

            try
            {
                logCSReq.Debug("-----------------------Post Message Hit - ChannelStateController---------------------------------");

                var chanData = new JsonChannelData(jData.OuterJson);

                logCSReq.Debug(String.Format("Channel Data : \n{0}",jData.RawJsonString));

                //TODO:Check Status is Valid

                var splitStr = chanData.Presence.Split(new string[] { "@" }, 2, StringSplitOptions.None);

                logCSReq.Debug(String.Format("String Splited User : {0}", splitStr[0]));

                string domain = "";
                string user = "";

                if (splitStr.Length == 2)
                {
                    domain = splitStr[1];
                    user = splitStr[0];
                }

                if (chanData.CallStatus.Equals("ACTIVE") || chanData.CallStatus.Equals("HANGUP"))
                {
                    logCSReq.Debug(String.Format("[GetSubscriberDetailsForDomain] User : {0}, domain : {1}, call id : {2}, status : {3}", user, domain, chanData.CallId, chanData.CallStatus));

                    //var subDetails = CSReqWebApiDataOperator.GetSubscriberDetailsForDomain(user, domain);

                    //logCSReq.Debug(String.Format("[GetSubscriberDetailsForDomain - Done] User : {0}, domain : {1}, call id : {2}, status : {3}", user, domain, chanData.CallId, chanData.CallStatus));

                    string hashObjId = String.Format("{0}@{1}", user, domain);
                    var hashObj = RedisHandler.Instance.GetHashValues(hashObjId);

                    string guUserId = "";
                    string companyId = "0";
                    if (hashObj != null)
                    {
                        if (hashObj.TryGetValue("guuserid", out guUserId))
                        {
                            logCSReq.Debug(String.Format("GuUserID Found on Redis - User : {0}, GuUserId : {1}", user, guUserId));

                            if (hashObj.TryGetValue("companyid", out companyId))
                            {
                                logCSReq.Debug(String.Format("Company Id Found on Redis - User : {0}, Company : {1}", user, companyId));

                                var tempCompanyId = int.Parse(companyId);

                                logCSReq.Debug(String.Format("[GetSecurityToken] - User : {0}", user));
                                var token = AuthServiceAccess.Instance.GetSecurityToken(tempCompanyId);

                                logCSReq.Debug(String.Format("[GetSecurityToken - Done] - User : {0}", user));


                                var hash = RedisHandler.Instance.GetHashValues(chanData.CallId);
                                string otherLegId = "";

                                if (hash != null)
                                {
                                    string cType = "";

                                    if (hash.TryGetValue("Call-Type", out cType))
                                    {
                                        if (cType == "FIFO")
                                        {
                                            if (hash.TryGetValue("Other-Leg-Unique-ID", out otherLegId))
                                            {
                                                logCSReq.Info(String.Format("Other leg unique id FOUND on hash - callUuid : {0}, State : {1}, User : {2}", chanData.CallId, chanData.CallStatus, user));
                                            }
                                            else
                                            {
                                                logCSReq.Info(String.Format("Other leg unique id NOT FOUND on hash - callUuid : {0}, State : {1}, User : {2}", chanData.CallId, chanData.CallStatus, user));
                                            }
                                        }
                                        else
                                        {
                                            logCSReq.Info(String.Format("Call-Type is NOT FIFO on hash - callUuid : {0}, State : {1}, User : {2}", chanData.CallId, chanData.CallStatus, user));
                                        }
                                    }
                                    else
                                    {
                                        logCSReq.Info(String.Format("Call-Type Not found on hash - callUuid : {0}, State : {1}, User : {2}", chanData.CallId, chanData.CallStatus, user));
                                    }
                                }
                                else
                                {
                                    logCSReq.Info(String.Format("Call object hash returned null - callUuid : {0}, State : {1}, User : {2}", chanData.CallId, chanData.CallStatus, user));
                                }

                                logCSReq.Debug(String.Format("[ResourceModeChangeRequest] - User : {0}", user));

                                var result = ConfigurationManager.AppSettings.Get("UseRestClient");

                                if (!string.IsNullOrEmpty(result))
                                {
                                    if (bool.Parse(result))
                                    {
                                        ResourceProxyServiceAccess.ResourceModeChangeRequestPost(guUserId, chanData.CallStatus, tempCompanyId, chanData.CallId, otherLegId, token);
                                        logCSReq.Debug(String.Format("[ResourceModeChangeRequestPost - Done] - User : {0}", user));
                                    }
                                    else
                                    {
                                        ResourceProxyServiceAccess.ResourceModeChangeRequest(guUserId, chanData.CallStatus, tempCompanyId, chanData.CallId, otherLegId);

                                        logCSReq.Debug(String.Format("[ResourceModeChangeRequest - Done] - User : {0}", user));

                                        ArdsComServiceAccess.ResourceModeChangeRequest(token, chanData.CallStatus, chanData.CallId, otherLegId);
                                    }
                                }
                                else
                                {
                                    ResourceProxyServiceAccess.ResourceModeChangeRequest(guUserId, chanData.CallStatus, tempCompanyId, chanData.CallId, otherLegId);

                                    logCSReq.Debug(String.Format("[ResourceModeChangeRequest - Done] - User : {0}", user));

                                    ArdsComServiceAccess.ResourceModeChangeRequest(token, chanData.CallStatus, chanData.CallId, otherLegId);
                                }
                                
                                
                            }
                            else
                            {
                                logCSReq.Error(String.Format("CompanyID not found on hash for hashId : {0}, calluuid : {1}", hashObjId, chanData.CallId));
                            }
                        }
                        else
                        {
                            logCSReq.Error(String.Format("GuUserID not found on hash for hashId : {0}, calluuid : {1}", hashObjId, chanData.CallId));
                        }
                    }
                    else
                    {
                        logCSReq.Error(String.Format("Hash object is null hashId : {0}, calluuid : {1}", hashObjId, chanData.CallId));
                    }

                    //if (subDetails != null)
                    //{
                    //    logCSReq.Debug(String.Format("[GetSubscriberDetailsForDomain] - Is Not Null - User : {0}", user));

                    //    logCSReq.Debug(String.Format("[GetSecurityToken] - User : {0}", user));
                    //    var token = AuthServiceAccess.Instance.GetSecurityToken(subDetails.CompanyId);

                    //    logCSReq.Debug(String.Format("[GetSecurityToken - Done] - User : {0}", user));

                    //    ArdsComServiceAccess.ResourceModeChangeRequest(token, chanData.CallStatus, chanData.CallId);

                    //    logCSReq.Debug(String.Format("[ResourceModeChangeRequest - Done] - User : {0}", user));


                    //    logCSReq.Debug(String.Format("[ResourceModeChangeRequest] - User : {0}", user));
                    //    ResourceProxyServiceAccess.ResourceModeChangeRequest(subDetails.GuUserId, chanData.CallStatus, subDetails.CompanyId, chanData.CallId);
                    //}
                    //else
                    //{
                    //    throw new Exception(String.Format("User not found for given username : {0} and domain : {1}, callUuid : {2}", user, domain, chanData.CallId));
                    //}
                }

                string defaultXml = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                                 new XElement("section", new XAttribute("name", "result"),
                                                              new XElement("result", new XAttribute("status", "not found")))).ToString();

                


                logCSReq.Error("Returning Default Xml");

                return header + defaultXml;



            }
            catch (Exception ex)
            {
                logCSReq.Error("ERROR on controller", ex);

                var xele = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "result"),
                                                     new XElement("result", new XAttribute("status", "not found"))));

                return header + xele.ToString();
            }
        }
    }
}

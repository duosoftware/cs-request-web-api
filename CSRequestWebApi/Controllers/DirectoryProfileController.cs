﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;
using DuoSoftware.DC.CSRequestWebApi.CSWebResponseCreater;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost.Controllers
{
    public class DirectoryProfileController : ApiController
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");
        
        public string Post(MultiFormKeyValueModel val)
        {
            try
            {
                logCSReq.Debug("Post Message Hit - DirectoryProfileController");
                string csId;
                string user;
                string domain;
                string action;
                string purpose;
                string group;
                string sipAuthRealm;
                string profile;

                val.MsgData.TryGetValue("hostname", out csId);
                val.MsgData.TryGetValue("user", out user);
                val.MsgData.TryGetValue("domain", out domain);
                val.MsgData.TryGetValue("action", out action);
                val.MsgData.TryGetValue("purpose", out purpose);
                val.MsgData.TryGetValue("group", out group);
                val.MsgData.TryGetValue("sip_auth_realm", out sipAuthRealm);
                val.MsgData.TryGetValue("profile", out profile);

                if (!string.IsNullOrEmpty(action) && !string.IsNullOrEmpty(group) && !string.IsNullOrEmpty(csId) && !string.IsNullOrEmpty(domain) && action.Equals("group_call"))
                {
                    logCSReq.Debug(String.Format("MultiFormKeyValData - Action : {4}, hostname : {0}, group : {1}, domain : {2}, sip_auth_realm : {3}", csId, group, domain, sipAuthRealm, action));

                    string tempAuthRealm = domain;
                    if (!string.IsNullOrEmpty(sipAuthRealm))
                    {
                        tempAuthRealm = sipAuthRealm;
                    }

                    int callServerId;
                    if (int.TryParse(csId, out callServerId))
                    {
                        ExtDetails extensionDetails = null;

                        if (bool.Parse(ConfigurationManager.AppSettings.Get("UseDomain")))
                        {
                            var subList = AbstractBackendHandler.Instance.GetCompanyForDomain(tempAuthRealm);

                            foreach (var sub in subList)
                            {
                                int company = sub.CompanyID.GetValueOrDefault();
                                int tenant = sub.TenantID.GetValueOrDefault();
                                
                                var tempExt = AbstractBackendHandler.Instance.GetExtensionDetails(group, company, tenant);

                                if (tempExt != null)
                                {
                                    extensionDetails = tempExt;

                                    break;
                                }
                            }
                            
                        }
                        else
                        {
                            extensionDetails = AbstractBackendHandler.Instance.GetExtensionDetails(group);
                        }

                        if (extensionDetails == null)
                        {
                            throw new Exception("Extension not found");
                        }

                        if((ExtType) extensionDetails.ExtType == ExtType.Group)
                        {
                            logCSReq.Info("Extension type is Group");

                            var result = AbstractBackendHandler.Instance.GetGroupDetails(callServerId, extensionDetails.ExtName, tempAuthRealm);

                            var xmlResponse = CsWebResponse.CreateDirectoryGroupProfile(result, extensionDetails.Ext);

                            return xmlResponse.ToString();
                        }
                        else
                        {
                            throw new Exception("Invalid Extension Type");
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(action) && !string.IsNullOrEmpty(user) && !string.IsNullOrEmpty(csId) && !string.IsNullOrEmpty(domain) && (action.Equals("sip_auth") || action.Equals("message-count")))
                {
                    logCSReq.Debug(String.Format("MultiFormKeyValData - Action : {4}, csId : {0}, user : {1}, domain : {2}, sip_auth_realm : {3}", csId, user, domain, sipAuthRealm, action));

                    string tempAuthRealm = domain;
                    if(!string.IsNullOrEmpty(sipAuthRealm))
                    {
                        tempAuthRealm = sipAuthRealm;
                    }

                    int callServerId;
                    if (int.TryParse(csId, out callServerId))
                    {
                        var result = AbstractBackendHandler.Instance.GetDirectoryProfileDetails(callServerId, user, tempAuthRealm);

                        var xmlResponse = CsWebResponse.CreateDirectoryUserProfile(result.Username, result.Password, result.Domain, result.Context, result.Extension, result.EmailAddr);

                        return xmlResponse.ToString();
                    }
                }
                else if (!string.IsNullOrEmpty(action) && !string.IsNullOrEmpty(user) && !string.IsNullOrEmpty(csId) && !string.IsNullOrEmpty(domain) && (action.Equals("user_call") || action.Equals("voicemail-lookup")))
                {
                    logCSReq.Debug(String.Format("MultiFormKeyValData - Action : {4}, csId : {0}, user : {1}, domain : {2}, sip_auth_realm : {3}", csId, user, domain, sipAuthRealm, action));

                    string tempAuthRealm = domain;
                    if (!string.IsNullOrEmpty(sipAuthRealm))
                    {
                        tempAuthRealm = sipAuthRealm;
                    }

                    int callServerId;
                    if (int.TryParse(csId, out callServerId))
                    {
                        ExtDetails extensionDetails = null;

                        if (bool.Parse(ConfigurationManager.AppSettings.Get("UseDomain")))
                        {
                            var extList = AbstractBackendHandler.Instance.GetExtensionDetailsList(user);

                            foreach (var ext in extList)
                            {
                                int company = ext.CompanyId;
                                int tenant = ext.TenantId;
                                string usrname = ext.ExtName;

                                var tempSub = AbstractBackendHandler.Instance.GetSubDetailsForDomainCompany(usrname, tempAuthRealm, company, tenant);
                                
                                if (tempSub != null)
                                {
                                    extensionDetails = ext;

                                    break;
                                }
                            }

                            //var subList = AbstractBackendHandler.Instance.GetCompanyForDomain(tempAuthRealm);

                            //foreach (var sub in subList)
                            //{
                            //    int company = sub.CompanyID.GetValueOrDefault();
                            //    int tenant = sub.TenantID.GetValueOrDefault();

                            //    var tempExt = AbstractBackendHandler.Instance.GetExtensionDetails(user, company, tenant);

                                

                            //    if (tempExt != null)
                            //    {
                            //        extensionDetails = tempExt;

                            //        break;
                            //    }
                            //}
                        }
                        else
                        {
                            extensionDetails = AbstractBackendHandler.Instance.GetExtensionDetails(user);
                        }

                        if (extensionDetails == null)
                        {
                            throw new Exception("Extension not found");
                        }

                        if ((ExtType) extensionDetails.ExtType == ExtType.User)
                        {
                            logCSReq.Info("Extension type is User");

                            var result = AbstractBackendHandler.Instance.GetDirectoryProfileDetails(callServerId, extensionDetails.ExtName, tempAuthRealm);

                            var xmlResponse = CsWebResponse.CreateDirectoryUserProfile(result.Username, result.Password, result.Domain, result.Context, result.Extension, result.EmailAddr);

                            return xmlResponse.ToString();
                        }
                        else
                        {
                            throw new Exception("Invalid Extension Type");
                        }

                    }
                }

                else if (!string.IsNullOrEmpty(purpose) && !string.IsNullOrEmpty(csId) && !string.IsNullOrEmpty(profile) && purpose.Equals("gateways"))
                {
                    logCSReq.Debug(String.Format("MultiFormKeyValData - Purpose : gateways, csId : {0}, profile : {1}", csId, profile));
                    int callServerId;

                    if (int.TryParse(csId, out callServerId))
                    {
                        var result = AbstractBackendHandler.Instance.GetSipGatewayDetailsForCallServer(callServerId, profile);

                        var xmlResponse = CsWebResponse.CreateDirectorySipGateways(result);

                        return xmlResponse.ToString();
                    }

                    logCSReq.Error("Call server id is not an integer value");

                }
                else if (!string.IsNullOrEmpty(purpose) && !string.IsNullOrEmpty(csId) && purpose.Equals("network-list"))
                {
                    logCSReq.Debug(String.Format("MultiFormKeyValData - Purpose : network-list, csId : {0}", csId));

                    int callServerId;

                    if (int.TryParse(csId, out callServerId))
                    {
                        var result = AbstractBackendHandler.Instance.GetSipGatewayDetailsForCallServer(callServerId);

                        var xmlResponse = CsWebResponse.CreateNetworkList(result);

                        return xmlResponse.ToString();
                    }

                    logCSReq.Error("Call server id is not an integer value");
                }

                logCSReq.Error("Response body parameters are invalid for directory profile");

                var xele = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "result"),
                                                     new XElement("result", new XAttribute("status", "not found"))));

                return xele.ToString();


            }
            catch (Exception ex)
            {
                logCSReq.Error("Exception thrown : ", ex);


                var xele = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                    new XElement("section", new XAttribute("name", "result"),
                                                 new XElement("result", new XAttribute("status", "not found"))));

                return xele.ToString();
            }
            
        }
    }
}

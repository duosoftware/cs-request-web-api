﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost
{
    public class FreeSwitchMediaFormatter : MediaTypeFormatter
    {
        public log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        private readonly string txt = "application/x-www-form-urlencoded";
        
        public FreeSwitchMediaFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue(txt));
        }

        public override bool CanWriteType(Type type)
        {
            if (type == typeof(string))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool CanReadType(Type type)
        {
            return true;
        }

        public override Task<object> ReadFromStreamAsync(Type type, Stream readStream, System.Net.Http.HttpContent content, IFormatterLogger formatterLogger)
        {
            logCSReq.Debug(String.Format("FreeSwitchMediaFormatter - Type : {0}, Content Type : {1}", type.Name, content.Headers.ContentType.MediaType));
            return Task.Factory.StartNew<object>(() =>
            {
                return new MultiFormKeyValueModel(content);
                
            });
        }

        public override Task WriteToStreamAsync(Type type, object value, Stream stream, System.Net.Http.HttpContent content, System.Net.TransportContext transportContext)
        {
            logCSReq.Info("Writing to stream async...");
            return Task.Factory.StartNew(() => BuildResponseString(value, stream, content.Headers.ContentType.MediaType));
        }

        private void BuildResponseString(object models, Stream stream, string contenttype)
        {
            logCSReq.Debug("Building response string..........");

            logCSReq.Debug(String.Format("Content Type : {0}", contenttype));

            var header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\r\n";

            var resp = header + models;

            using (var writer = new StreamWriter(stream))
            {
                writer.Write(resp);
                logCSReq.Debug(String.Format("Response - {0}", resp));
            }
            stream.Close();
        }
    }

    public class MultiFormKeyValueModel
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");
        
        HttpContent _contents;

        public Dictionary<string, string> MsgData;

        public MultiFormKeyValueModel(HttpContent contents)
        {
            try
            {
                _contents = contents;
                MsgData = new Dictionary<string, string>();
                
                string data = _contents.ReadAsStringAsync().Result;

                logCSReq.Debug("Read String Async - Body : " + data);

                string[] strList = data.Split('&');
                foreach (string strx in strList)
                {
                    string[] line = strx.Split('=');
                    if (line.Length > 1)
                    {
                        string key = line[0].Trim();
                        string value = line[1].Trim();
                        if (!MsgData.ContainsKey(key))
                            MsgData.Add(key, value);
                        else
                            MsgData[key] = value;
                    }
                }
            }
            catch (Exception ex)
            {
                logCSReq.Error("----ERROR-----", ex);
                throw;
            }
            
        }


        //public IEnumerable<string> Keys
        //{
        //    get
        //    {
        //        try
        //        {
        //            logCSReq.Debug("get keys");

        //            if (_contents == null)
        //            {
        //                logCSReq.Error("Content is null");
        //            }

        //            string data = _contents.ReadAsStringAsync().Result;

        //            logCSReq.Debug("Read String Async - Body : " + data);

        //            string[] strList = data.Split('&');
        //            foreach (string strx in strList)
        //            {
        //                string[] line = strx.Split('=');
        //                if (line.Length > 1)
        //                {
        //                    string key = line[0].Trim();
        //                    string value = line[1].Trim();
        //                    if (!MsgData.ContainsKey(key))
        //                        MsgData.Add(key, value);
        //                    else
        //                        MsgData[key] = value;
        //                }
        //            }

        //            return MsgData.Keys;
        //        }
        //        catch (Exception ex)
        //        {
        //            logCSReq.Error("Exception occurred : " + ex);
        //            throw;
        //        }

        //    }
        //}
    }
}
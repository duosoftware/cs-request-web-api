﻿using System.Web.Mvc;

namespace DuoSoftware.DC.CSRequestWebApi.WebHost.App_Start
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
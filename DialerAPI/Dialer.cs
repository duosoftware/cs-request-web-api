﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DialerAPI
{
    public class Dialer
    {


        public enum Faxtypes
        {
            T30 = 0,
            T38
        }

        public string DownloadFileToServer(string serverName,int port, string url)
        {
            string fileName = string.Empty;
           
            HttpStatusCode code;
            fileName = MakeRequestRAW(string.Format("http://{0}:{1}/api/http_get? {2}", serverName, port, url), "GET", "", out code);
            
            return fileName;
        }

        public string SendFax(int callServerID, string callserverName, int port, string DNIS, string profile, Faxtypes type, string fileName, string ANI, string mediaURL, string token)
        {
            string sessionID = string.Empty;

            string extraOption = string.Empty;

            //<action application="set" data="fax_enable_t38_request=true"/>
            //<action application="set" data="fax_enable_t38=true"/>

            if (type == Faxtypes.T38)
            {
                extraOption = string.Format("fax_enable_t38_request=true,fax_enable_t38=true");
            }


            string bypassMedia = "false";
            string destinationGroup = string.Format("gateway/{0}", profile);
         

            string protocoal = "sofia";
            string dnis = DNIS;
            string calling = string.Format("{0}/{1}/{2}", protocoal, destinationGroup, dnis);
           
            string fileToSend = DownloadFileToServer(callserverName, port, string.Format("{0}?URL={1}&SecurityToken={2}", mediaURL, fileName, token));
            if (string.IsNullOrEmpty(fileToSend))
            {
                fileToSend = fileName;
            }
            sessionID = MakeCall(callserverName,port, ANI, calling, 30, string.Format("&txfax({0})", fileToSend), extraOption);

            return sessionID;
        }

        public string MakeCall(string serverName,int port, string anix, string dnisx, int timeout, string data, string extraOptions)
        {

            string sessionID = string.Empty;
            string crn = string.Empty;
            try
            {
                HttpStatusCode code;
              
                crn = MakeRequestRAW(string.Format("http://{0}:{1}/api/create_uuid", serverName, port), "GET", "", out code);
                if (!string.IsNullOrEmpty(crn))
                {

                  
                    string options = string.Format("{{return_ring_ready=false,origination_uuid={0},origination_caller_id_number={1}}}", crn, anix);

                    if (!string.IsNullOrEmpty(extraOptions))
                    {
                        options = string.Format("{{ignore_early_media=false,return_ring_ready=false,origination_uuid={0},{1}}}", crn, extraOptions);

                    }

                    string serviceResponce = MakeRequestRAW(string.Format("http://{0}:{1}/api/originate? {2}{3} {4}", serverName, port, options, dnisx, data), "GET", "", out code);
                    if (serviceResponce != null && code == HttpStatusCode.OK)
                    {
                        sessionID = crn;
                    }
                }
            }
            catch (System.Exception ex)
            {
                
            }
            return sessionID;
        }

        string MakeRequestRAW(string url, string method, string requestBody, out HttpStatusCode statusCode)
        {
            // System.Net.ServicePointManager.Expect100Continue = false;
            // ResponseTextBox.Text = "Please wait...";

            //var url = UrlTextBox.Text;
            // var method = VerbComboBox.Text;
            // var requestBody = RequestBodyTextBox.Text;
            string reponseAsString = "";
            statusCode = HttpStatusCode.InternalServerError;

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = method;
                //request.ContentType = "application/xml";
                // request.Accept = "application/xml";
                //request.SendChunked = true;
                request.KeepAlive = false;
                //request.UserAgent = "C# 4.0";

                try
                {
                    SetBody(request, requestBody);
                }
                catch (System.Exception ex)
                {


                }


                var response = (HttpWebResponse)request.GetResponse();
                statusCode = response.StatusCode;
                reponseAsString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                response.Close();



            }
            catch (Exception ex)
            {

            }
            return reponseAsString;

        }

        void SetBody(HttpWebRequest request, string requestBody)
        {
            if (requestBody.Length > 0)
            {
                using (Stream requestStream = request.GetRequestStream())
                using (StreamWriter writer = new StreamWriter(requestStream))
                {
                    writer.Write(requestBody);
                }
            }
        }


    }
}

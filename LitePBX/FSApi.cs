﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml.Linq;

namespace LitePBX
{

    public struct ParkCallInfo
    {
        public string uuid { get; set; }
        public string slotid { get; set; }
    }

    public struct Caller
    {
        public string ID { get; set; }
        public string status { get; set; }
        public string number { get; set; }
        public string position { get; set; }
        public string slot { get; set; }
    }

    public struct BridgeCaller
    {
        public string ID { get; set; }
        public string number { get; set; }
    }

    public struct Consumer
    {
        public string uuid { get; set; }
        public string outgoinguuid { get; set; }
    }

    public struct Bridge
    {
        public BridgeCaller caller { get; set; }
        public Consumer consumer { get; set; }

    }

    public struct QueueInfo
    {
        public List<string> members { get; set; }
        public List<Caller> callers { get; set; }
        public List<Bridge> bridges { get; set; }
    }

    public class FSApi
    {
        static string fsIP = "localhost";


        public string FSIP
        {
            get
            {
                return fsIP;
            }
        }

        public FSApi(string fsip)
        {

            fsIP = fsip;
        }

        public List<ParkCallInfo> GetParkInfo(string context)
        {
            List<ParkCallInfo> parkcalls = new List<ParkCallInfo>();

            try
            {
                string url = string.Format("http://{0}:8080/api/valet_info? {1}", fsIP, context);
                HttpStatusCode code;

                string data = MakeRequestRAW(url, "GET", string.Empty, out code);

                if (code == HttpStatusCode.OK)
                {
                    if (!string.IsNullOrEmpty(data))
                    {
                        XElement d = XElement.Parse(data);


                        var calls = from p in d.Descendants("lot").Descendants("extension")
                                    select new ParkCallInfo
                                    {
                                        uuid = p.Attribute("uuid").Value,
                                        slotid = p.Value
                                    };


                        parkcalls.AddRange(calls);
                    }

                }

            }
            catch (Exception ex)
            {
            }


            return parkcalls;
        }


        public QueueInfo GetQueueInfo(string queue, string domain)
        {

            QueueInfo info = new QueueInfo();
            info.bridges = new List<Bridge>();
            info.callers = new List<Caller>();
            info.members = new List<string>();

            try
            {
                string url = string.Format("http://{0}:8080/api/fifo? list {1}@{2}", fsIP, queue, domain);
                HttpStatusCode code;

                string data = MakeRequestRAW(url, "GET", string.Empty, out code);

                if (code == HttpStatusCode.OK)
                {
                    if (!string.IsNullOrEmpty(data))
                    {
                        XElement d = XElement.Parse(data);





                        var members = from member in d.Descendants("fifo").Descendants("outbound") select member;
                        var callers = from caller in d.Descendants("fifo").Descendants("callers") select caller;
                        var bridges = from bridge in d.Descendants("fifo").Descendants("bridges") select bridge;




                        foreach (var item in members.Descendants("member"))
                        {
                            info.members.Add(item.Value);


                        }

                        foreach (var item in callers.Descendants("caller"))
                        {
                            //<caller uuid="fc6de456-4119-4d84-8a2e-f4fb6dc36f87" status="WAITING" caller_id_name="1000" caller_id_number="1000" timestamp="2014-05-28 14:41:07" position="1" slot="0"/>

                            info.callers.Add(new Caller() { ID = item.Attribute("uuid").ToString(), number = item.Attribute("caller_id_number").ToString(), position = item.Attribute("position").ToString(), slot = item.Attribute("slot").ToString(), status = item.Attribute("status").ToString() });

                        }


                        foreach (var item in bridges.Descendants("bridge"))
                        {
                            Bridge bridge = new Bridge();

                            bridge.caller = new BridgeCaller() { ID = item.Element("caller").Attribute("uuid").ToString(), number = item.Element("caller").Attribute("caller_id_number").ToString() };
                            bridge.consumer = new Consumer() { uuid = item.Element("consumer").Element("uuid").Value, outgoinguuid = item.Element("consumer").Element("outgoing_uuid").Value };

                        }

                    }

                }

            }
            catch (Exception ex)
            {
            }


            return info;

        }

        //http://localhost:8080/api/fifo_member?%20add%20myq%20user/1000



        public bool RouteAgentToFIFO(string queue, string user, string domain)
        {
            try
            {
                //originate sofia/example/300@foo.com &bridge(sofia/example/400@bar.com)

                string url = string.Format("http://{0}:8080/api/originate? user/{2}@{3} {1} XML PBXQueue", fsIP, queue, user, domain);
                HttpStatusCode code;

                string data = MakeRequestRAW(url, "GET", string.Empty, out code);

                if (code == HttpStatusCode.OK)
                {
                }
            }
            catch (Exception ex)
            {
                return false;
            }



            return true;
        }


        public bool AddAgentToFIFO(string queue, string user, string domain, int conc,string guid)
        {
            try
            {
                string url = string.Format("http://{0}:8080/api/fifo_member? add {1}@{3} {{ToGuUserId={5}}}user/{2}@{3} {4}", fsIP, queue, user, domain, conc, guid);
                HttpStatusCode code;

                string data = MakeRequestRAW(url, "GET", string.Empty, out code);

                if (code == HttpStatusCode.OK)
                {
                }
            }
            catch (Exception ex)
            {
                return false;
            }



            return true;
        }

        public bool RemoveAgentFromFIFO(string queue, string user, string domain)
        {
            try
            {
                string url = string.Format("http://{0}:8080/api/fifo_member? del {1}@{3} user/{2}@{3}", fsIP, queue, user, domain);
                HttpStatusCode code;

                string data = MakeRequestRAW(url, "GET", string.Empty, out code);

                if (code == HttpStatusCode.OK)
                {
                }
            }
            catch (Exception ex)
            {
                return false;
            }



            return true;
        }

        public bool AddToFIFO(string uuid, string queue, string domain)
        {

            try
            {
                string url = string.Format("http://{0}:8080/api/uuid_transfer? {1} {2}@{3} XML PBXFIFO", fsIP, uuid, queue, domain);
                HttpStatusCode code;

                string data = MakeRequestRAW(url, "GET", string.Empty, out code);

                if (code == HttpStatusCode.OK)
                {
                }
            }
            catch (Exception ex)
            {
                return false;
            }



            return true;
        }

        string MakeRequestRAW(string url, string method, string requestBody, out HttpStatusCode statusCode)
        {
            // System.Net.ServicePointManager.Expect100Continue = false;
            // ResponseTextBox.Text = "Please wait...";

            //var url = UrlTextBox.Text;
            // var method = VerbComboBox.Text;
            // var requestBody = RequestBodyTextBox.Text;
            string reponseAsString = "";
            statusCode = HttpStatusCode.InternalServerError;

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = method;
                //request.ContentType = "application/xml";
                // request.Accept = "application/xml";
                //request.SendChunked = true;
                request.KeepAlive = false;
                //request.UserAgent = "C# 4.0";

                try
                {
                    SetBody(request, requestBody);
                }
                catch (System.Exception ex)
                {


                }


                var response = (HttpWebResponse)request.GetResponse();
                statusCode = response.StatusCode;
                reponseAsString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                response.Close();



            }
            catch (Exception ex)
            {

            }
            return reponseAsString;

        }

        void SetBody(HttpWebRequest request, string requestBody)
        {
            if (requestBody.Length > 0)
            {
                using (Stream requestStream = request.GetRequestStream())
                using (StreamWriter writer = new StreamWriter(requestStream))
                {
                    writer.Write(requestBody);
                }
            }
        }
    }
}

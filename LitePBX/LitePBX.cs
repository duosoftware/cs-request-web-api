﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;
using ServiceStack.Redis;

namespace LitePBX
{
    public class LitePBX
    {
        public static log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");


        public static XElement RouteGateway(string extention, string context, string profile, string destinationPattern, Endpoint endpoint, string fromGuUserId, int companyId, int tenantId, string opType, CallerCalleeInfo callerCalleeInfo, bool needRecord = false, bool bypassMed = false, bool ignoreEarlyMedia = false)
        {
            try
            {
                logCSReq.Info("Creating dialplan PBX - gateway");

                var bypassMedia = "bypass_media=true";
                if (!bypassMed)
                {
                    bypassMedia = "bypass_media=false";
                }

                var ignoreEarlyM = "ignore_early_media=false";
                if (ignoreEarlyMedia)
                {
                    ignoreEarlyM = "ignore_early_media=true";
                }

                //<action application="bridge" data="user/1000@mydomain.com,[leg_delay_start=5]user/1001@mydomain.com,[leg_delay_start=15,leg_timeout=25]sofia/gateway/flowroute/12345678901" />
                string bridge = string.Empty;
               

                string destinationGroup = "external";
                switch (endpoint.Type)
                {
                    case DnisType.Gateway:
                        destinationGroup = string.Format("gateway/{0}", endpoint.Profile);

                        break;

                    case DnisType.User:
                        destinationGroup = "user";
                        //bypassMedia = "true";
                        break;

                    case DnisType.Group:
                        destinationGroup = "group";
                        //bypassMedia = "true";
                        break;

                    case DnisType.loopback:
                        destinationGroup = "loopback";
                        break;

                    default:
                        destinationGroup = string.Format("{0}", endpoint.Profile);
                        break;
                }

                string option = string.Empty;


                if (endpoint.LegStartDelay > 0)
                    option = string.Format("[leg_delay_start={0},leg_timeout={1},origination_caller_id_number={3},sip_h_X-Gateway={4}]", endpoint.LegStartDelay, endpoint.LegTimeout, bypassMedia, endpoint.Origination, endpoint.IpUrl);
                else
                    option = string.Format("[leg_timeout={0},origination_caller_id_number={2},sip_h_X-Gateway={3}]", endpoint.LegTimeout, bypassMedia, endpoint.Origination, endpoint.IpUrl);


                string protocoal = "sofia";
                string calling = string.Empty;
                string dnis = endpoint.Destination;


                //context.GetExtension<IActions>().GetSystemVariables(SystemVariables.Domain);
                if (!string.IsNullOrEmpty(endpoint.Domain))
                {
                    dnis = string.Format("{0}@{1}", dnis, endpoint.Domain);
                }

                switch (endpoint.Protocol)
                {
                    case Protocol.SIP:
                        calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    case Protocol.XMPP:
                        protocoal = "dingaling";
                        calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    case Protocol.PSTN:
                        protocoal = "freetdm";
                        calling = string.Format("{0}{1}/{2}/a/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    default:
                        calling = string.Format("{0}{1}/{2}", option, destinationGroup, dnis);
                        break;

                }

                string tempCustCompStr = String.Format("CustomCompanyStr={0}_{1}", companyId, tenantId);
                string tempFromGuUser = String.Format("FromGuUserId={0}", fromGuUserId);

                string tempFromUser = String.Format("FromUser={0}", callerCalleeInfo.FromUser);
                string tempFromNumber = String.Format("FromNumber={0}", callerCalleeInfo.FromNumber);
                string tempToNumber = String.Format("nolocal:ToNumber={0}", callerCalleeInfo.ToNumber);
                string tempToUser = String.Format("nolocal:ToUser={0}", callerCalleeInfo.ToUser);
                string tempOpType = String.Format("OperationType={0}", opType);

                XElement xlem = null;
                if (!needRecord)
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", context),
                               new XElement("extension",
                                   new XAttribute("name", extention),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", destinationPattern),
                                       //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempOpType)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromGuUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToUser)),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=false")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", ignoreEarlyM)),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", bypassMedia)),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                        //new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s valet_park::valet_lot auto in 8001 8999")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s execute_extension::att_xfer_conference XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", calling)),
                                       new XElement("action", new XAttribute("application", "hangup")))))));
                }
                else
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", context),
                               new XElement("extension",
                                   new XAttribute("name", extention),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", destinationPattern),
                                       //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=Gateway")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromGuUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToUser)),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=false")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", ignoreEarlyM)),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", bypassMedia)),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                        // new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s valet_park::valet_lot auto in 8001 8999")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s execute_extension::att_xfer_conference XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "execute_on_answer=record_session $${base_dir}/recordings/${uuid}.mp3")),
                                       new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", calling)),
                                       new XElement("action", new XAttribute("application", "hangup")))))));

                    //<action application="set" data="dtmf_type=info"/>
                    // <action application="export" data="execute_on_answer=record_session $${base_dir}/recordings/${strftime(%Y%m%d%H%M%S)}_${caller_id_number}.wav"/>
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "record_post_process_exec_api=true")),
                    //string options = string.Format("curl_sendfile:{0} file=$${{base_dir}}/recordings/{1}.{6} class=CALLSERVER&type=CALL&category=CONVERSATION&company={2}&tenent={3}&server={4}&callID={1}&securitytoken={5} event {1}", url, this.CallID, company, tenent, id, V5Utils.V5Utility.GetSecurityToken(company, tenent, GetChannelData("AuthName"), GetChannelData("PassWord")), recordType);
                    //SetChannelData("record_post_process_exec_api", options);

                }

                return xlem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static XElement RouteFaxGateway(string extention, string context, string profile, string destinationPattern, Endpoint endpoint, string fromGuUserId, int companyId, int tenantId, string opType, CallerCalleeInfo callerCalleeInfo, bool needRecord = false, string faxType = "", bool bypassMed = false, bool ignoreEarlyMedia = false)
        {
            try
            {
                logCSReq.Info(String.Format("Creating dialplan PBX - gateway fax - Type : {0}", faxType));

                XElement faxEnable = new XElement("Error");
                XElement faxSetProperty = new XElement("Error");

                if (faxType.Equals("AUDIO_T38"))
                {
                    //faxEnable = new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "sip_execute_on_image=t38_gateway self nocng"));
                    //faxSetProperty = new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "refuse_t38=true"));
                    faxEnable = new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "fax_enable_t38=true"));
                    faxSetProperty = new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "sip_execute_on_image=t38_gateway self nocng"));
                }
                else if (faxType.Equals("T38_AUDIO"))
                {
                    faxEnable = new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "fax_enable_t38=true"));
                    faxSetProperty = new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "sip_execute_on_image=t38_gateway self nocng"));
                }
                else if (faxType.Equals("T38_T30AUDIO"))
                {
                    faxEnable = new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "fax_enable_t38=true"));
                    faxSetProperty = new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "sip_execute_on_image=t38_gateway peer nocng"));
                }
                else if (faxType.Equals("T38_T38"))
                {
                    faxEnable = new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "t38_passthru=true"));
                    faxSetProperty = new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "refuse_t38=true"));
                }
                else if (faxType.Equals("T38PassThru"))
                {
                    faxEnable = null;
                    faxSetProperty = new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "t38_passthru=true")); ;
                }
                else
                {
                    logCSReq.Debug("Default Fax Type");
                    faxEnable = new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "fax_enable_t38=true"));
                    faxSetProperty = new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "sip_execute_on_image=t38_gateway self nocng"));
                }

                //<action application="bridge" data="user/1000@mydomain.com,[leg_delay_start=5]user/1001@mydomain.com,[leg_delay_start=15,leg_timeout=25]sofia/gateway/flowroute/12345678901" />
                string bridge = string.Empty;

                string bypassMedia = "bypass_media=false";

                if (bypassMed)
                {
                    bypassMedia = "bypass_media=true";
                }

                var ignoreEarlyM = "ignore_early_media=false";
                if (ignoreEarlyMedia)
                {
                    ignoreEarlyM = "ignore_early_media=true";
                }
                

                string destinationGroup = "external";
                switch (endpoint.Type)
                {
                    case DnisType.Gateway:
                        destinationGroup = string.Format("gateway/{0}", endpoint.Profile);

                        break;

                    case DnisType.User:
                        destinationGroup = "user";
                        bypassMedia = "true";
                        break;

                    case DnisType.Group:
                        destinationGroup = "group";
                        bypassMedia = "true";
                        break;

                    case DnisType.loopback:
                        destinationGroup = "loopback";
                        break;

                    default:
                        destinationGroup = string.Format("{0}", endpoint.Profile);
                        break;
                }

                string option = string.Empty;

                endpoint.LegTimeout = 60;

                if (endpoint.LegStartDelay > 0)
                    option = string.Format("[leg_delay_start={0},leg_timeout={1},origination_caller_id_number={3},sip_h_X-Gateway={4}]", endpoint.LegStartDelay, endpoint.LegTimeout, bypassMedia, endpoint.Origination, endpoint.IpUrl);
                else
                    option = string.Format("[leg_timeout={0},origination_caller_id_number={2},sip_h_X-Gateway={3}]", endpoint.LegTimeout, bypassMedia, endpoint.Origination, endpoint.IpUrl);


                string protocoal = "sofia";
                string calling = string.Empty;
                string dnis = endpoint.Destination;


                //context.GetExtension<IActions>().GetSystemVariables(SystemVariables.Domain);
                if (!string.IsNullOrEmpty(endpoint.Domain))
                {
                    dnis = string.Format("{0}@{1}", dnis, endpoint.Domain);
                }

                switch (endpoint.Protocol)
                {
                    case Protocol.SIP:
                        calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    case Protocol.XMPP:
                        protocoal = "dingaling";
                        calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    case Protocol.PSTN:
                        protocoal = "freetdm";
                        calling = string.Format("{0}{1}/{2}/a/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    default:
                        calling = string.Format("{0}{1}/{2}", option, destinationGroup, dnis);
                        break;

                }

                string tempCustCompStr = String.Format("CustomCompanyStr={0}_{1}", companyId, tenantId);
                string tempFromGuUser = String.Format("FromGuUserId={0}", fromGuUserId);

                string tempFromUser = String.Format("FromUser={0}", callerCalleeInfo.FromUser);
                string tempFromNumber = String.Format("FromNumber={0}", callerCalleeInfo.FromNumber);
                string tempToNumber = String.Format("nolocal:ToNumber={0}", callerCalleeInfo.ToNumber);
                string tempToUser = String.Format("nolocal:ToUser={0}", callerCalleeInfo.ToUser);
                string tempOpType = String.Format("OperationType={0}", opType);

                XElement xlem = null;
                if (!needRecord)
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", context),
                               new XElement("extension",
                                   new XAttribute("name", extention),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", destinationPattern),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempOpType)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromGuUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToUser)),
                                       faxEnable,
                                       faxSetProperty,
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=false")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", ignoreEarlyM)),
                                       //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", bypassMedia)),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                        //new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s valet_park::valet_lot auto in 8001 8999")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s execute_extension::att_xfer_conference XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", calling)),
                                       new XElement("action", new XAttribute("application", "hangup")))))));
                }
                else
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", context),
                               new XElement("extension",
                                   new XAttribute("name", extention),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", destinationPattern),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=Gateway")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromGuUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToUser)),
                                       faxEnable,
                                       faxSetProperty,
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=false")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", ignoreEarlyM)),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", bypassMedia)),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                        // new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s valet_park::valet_lot auto in 8001 8999")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s execute_extension::att_xfer_conference XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "execute_on_answer=record_session $${base_dir}/recordings/${uuid}.mp3")),
                                       new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", calling)),
                                       new XElement("action", new XAttribute("application", "hangup")))))));

                    //<action application="set" data="dtmf_type=info"/>
                    // <action application="export" data="execute_on_answer=record_session $${base_dir}/recordings/${strftime(%Y%m%d%H%M%S)}_${caller_id_number}.wav"/>
                    //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "record_post_process_exec_api=true")),
                    //string options = string.Format("curl_sendfile:{0} file=$${{base_dir}}/recordings/{1}.{6} class=CALLSERVER&type=CALL&category=CONVERSATION&company={2}&tenent={3}&server={4}&callID={1}&securitytoken={5} event {1}", url, this.CallID, company, tenent, id, V5Utils.V5Utility.GetSecurityToken(company, tenent, GetChannelData("AuthName"), GetChannelData("PassWord")), recordType);
                    //SetChannelData("record_post_process_exec_api", options);

                }

                return xlem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public static XElement Intercept(string extention, string context, string profile, string destinationPattern, string custVarString, CallerCalleeInfo callerCalleeInfo, string fromGuUserId)
        {
            //<action application="set" data="intercept_unanswered_only=true"/>
            //<action application="intercept" data="myUUID"/>

            try
            {
                string uuid = string.Empty;

                try
                {
                    using (RedisClient redisClient = new RedisClient(ConfigurationManager.AppSettings.Get("redisip")))
                    {
                        //string key = string.Format("{0}:{1}:{2}", boardID, skill, section);
                        //Console.WriteLine(key);
                        if (redisClient.ContainsKey(String.Format("{0}_{1}", custVarString, extention)))
                        {
                            //redisClient.Remove(extention);
                            uuid = redisClient.Get<string>(String.Format("{0}_{1}", custVarString, extention));
                        }
                    }
                }
                catch (Exception ex)
                {

                }

                XElement xlem = null;

                string tempCustCompStr = String.Format("CustomCompanyStr={0}", custVarString);
                string tempFromGuUser = String.Format("FromGuUserId={0}", fromGuUserId);
                
                string tempFromUser = String.Format("FromUser={0}", callerCalleeInfo.FromUser);
                string tempFromNumber = String.Format("FromNumber={0}", callerCalleeInfo.FromNumber);
                string tempToNumber = String.Format("nolocal:ToNumber={0}", callerCalleeInfo.ToNumber);
                string tempToUser = String.Format("nolocal:ToUser={0}", callerCalleeInfo.ToUser);

                if (!string.IsNullOrEmpty(uuid))
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                            new XElement("section", new XAttribute("name", "dialplan"),
                               new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                               new XElement("context",
                                   new XAttribute("name", context),
                                   new XElement("extension",
                                       new XAttribute("name", "test"),
                                       new XElement("condition",
                                           new XAttribute("field", "destination_number"),
                                           new XElement("action", new XAttribute("application", "answer")),
                                           new XAttribute("expression", destinationPattern),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=Intercept")),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromGuUser)),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromUser)),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromNumber)),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToNumber)),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToUser)),
                                           new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "intercept_unanswered_only=true")),
                                           new XElement("action", new XAttribute("application", "intercept"), new XAttribute("data", uuid)))))));
                }
                else
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                           new XElement("section", new XAttribute("name", "dialplan"),
                              new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                              new XElement("context",
                                  new XAttribute("name", context),
                                  new XElement("extension",
                                      new XAttribute("name", "test"),
                                      new XElement("condition",
                                          new XAttribute("field", "destination_number"),
                                          new XAttribute("expression", destinationPattern),
                                          new XElement("action", new XAttribute("application", "hangup")))))));

                }

                return xlem;

            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public static XElement Barge(string extention, string context, string profile, string destinationPattern, string custVarString, CallerCalleeInfo callerCalleeInfo, string fromGuUserId, bool caller = true, string opType = "")
        {
            //<action application="set" data="intercept_unanswered_only=true"/>
            //<action application="intercept" data="myUUID"/>

            try
            {
                string uuid = string.Empty;

                try
                {
                    using (RedisClient redisClient = new RedisClient(ConfigurationManager.AppSettings.Get("redisip")))
                    {
                        //string key = string.Format("{0}:{1}:{2}", boardID, skill, section);
                        //Console.WriteLine(key);
                        if (redisClient.ContainsKey(String.Format("{0}_{1}", custVarString, extention)))
                        {
                            //redisClient.Remove(extention);
                            uuid = redisClient.Get<string>(String.Format("{0}_{1}", custVarString, extention));
                        }
                    }
                }
                catch (Exception ex)
                {

                }

                XElement xlem = null;


                string dtmfString = "w1@500";
                if (!caller)
                {
                    dtmfString = "w2@500";
                }

                string tempCustCompStr = String.Format("CustomCompanyStr={0}", custVarString);
                string tempFromGuUser = String.Format("FromGuUserId={0}", fromGuUserId);
                
                string tempFromUser = String.Format("FromUser={0}", callerCalleeInfo.FromUser);
                string tempFromNumber = String.Format("FromNumber={0}", callerCalleeInfo.FromNumber);
                string tempToNumber = String.Format("nolocal:ToNumber={0}", callerCalleeInfo.ToNumber);
                string tempToUser = String.Format("nolocal:ToUser={0}", callerCalleeInfo.ToUser);


                if (!string.IsNullOrEmpty(uuid))
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                            new XElement("section", new XAttribute("name", "dialplan"),
                               new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                               new XElement("context",
                                   new XAttribute("name", context),
                                   new XElement("extension",
                                       new XAttribute("name", "test"),
                                       new XElement("condition",
                                           new XAttribute("field", "destination_number"),
                                           new XAttribute("expression", destinationPattern),
                                           new XElement("action", new XAttribute("application", "answer")),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=Barge")),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromGuUser)),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromUser)),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromNumber)),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToNumber)),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToUser)),
                                           new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "eavesdrop_enable_dtmf=true")),
                                           new XElement("action", new XAttribute("application", "queue_dtmf"), new XAttribute("data", dtmfString)),
                                           new XElement("action", new XAttribute("application", "eavesdrop"), new XAttribute("data", uuid)))))));
                }
                else
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                           new XElement("section", new XAttribute("name", "dialplan"),
                              new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                              new XElement("context",
                                  new XAttribute("name", context),
                                  new XElement("extension",
                                      new XAttribute("name", "test"),
                                      new XElement("condition",
                                          new XAttribute("field", "destination_number"),
                                          new XAttribute("expression", destinationPattern),
                                          new XElement("action", new XAttribute("application", "hangup")))))));

                }

                return xlem;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public static XElement PickUP(string extention, string context, string profile, string destinationPattern, string pickupExtention, int companyId, int tenantId, string fromGuUserId)
        {
            //<action application="set" data="intercept_unanswered_only=true"/>
            //<action application="intercept" data="myUUID"/>

            string tempCustCompStr = String.Format("CustomCompanyStr={0}_{1}", companyId, tenantId);
            string tempFromGuUser = String.Format("FromGuUserId={0}", fromGuUserId);

            
            try
            {
                XElement xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                            new XElement("section", new XAttribute("name", "dialplan"),
                               new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                               new XElement("context",
                                   new XAttribute("name", context),
                                   new XElement("extension",
                                       new XAttribute("name", "test"),
                                       new XElement("condition",
                                           new XAttribute("field", "destination_number"),
                                           new XAttribute("expression", destinationPattern),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=PickUp")),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromGuUser)),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                           new XElement("action", new XAttribute("application", "pickup"), new XAttribute("data", pickupExtention)))))));

                return xlem;

            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public static XElement OutCallConference(string extention, string context, string destinationPattern, string confID, string domain, List<Endpoint> endpoints, int companyId, int tenantId, string pin, string mode)
        {
            try
            {
                logCSReq.Info("Creating dialplan PBX - OutCallConference");

                string tempCustCompStr = String.Format("CustomCompanyStr={0}_{1}", companyId, tenantId);
                //<action application="bridge" data="user/1000@mydomain.com,[leg_delay_start=5]user/1001@mydomain.com,[leg_delay_start=15,leg_timeout=25]sofia/gateway/flowroute/12345678901" />
                string bridge = string.Empty;
                List<string> legs = new List<string>();
                string bypassMedia = "false";

                foreach (var endpoint in endpoints)
                {

                    string destinationGroup = "external";
                    switch (endpoint.Type)
                    {
                        case DnisType.Gateway:
                            destinationGroup = string.Format("gateway/{0}", endpoint.Profile);

                            break;

                        case DnisType.User:
                            destinationGroup = "user";
                            bypassMedia = "true";
                            break;

                        case DnisType.Group:
                            destinationGroup = "group";
                            bypassMedia = "true";
                            break;

                        case DnisType.loopback:
                            destinationGroup = "loopback";
                            break;

                        default:
                            destinationGroup = string.Format("{0}", endpoint.Profile);
                            break;
                    }

                    string option = string.Empty;


                    //if (endpoint.LegStartDelay > 0)
                    //    option = string.Format("[leg_delay_start={0},leg_timeout={1},bypass_media={2},origination_caller_id_number={3}]", endpoint.LegStartDelay, endpoint.LegTimeout, bypassMedia, endpoint.Origination);
                    //else
                    //    
                    option = string.Format("[origination_caller_id_number={0}]", endpoint.Origination);


                    string protocoal = "sofia";
                    string calling = string.Empty;
                    string dnis = endpoint.Destination;


                    //context.GetExtension<IActions>().GetSystemVariables(SystemVariables.Domain);
                    if (!string.IsNullOrEmpty(endpoint.Domain))
                    {
                        dnis = string.Format("{0}@{1}", dnis, endpoint.Domain);
                    }

                    switch (endpoint.Protocol)
                    {
                        case Protocol.SIP:
                            calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                            break;

                        case Protocol.XMPP:
                            protocoal = "dingaling";
                            calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                            break;

                        case Protocol.PSTN:
                            protocoal = "freetdm";
                            calling = string.Format("{0}{1}/{2}/a/{3}", option, protocoal, destinationGroup, dnis);
                            break;

                        default:
                            calling = string.Format("{0}{1}/{2}", option, destinationGroup, dnis);
                            break;

                    }

                    legs.Add(calling);

                }
                List<XElement> elements = new List<XElement>();


                foreach (string leg in legs)
                {
                    //<action application="conference_set_auto_outcall" data="user/1000@$${domain}"/>
                    XElement dialElement = new XElement("action", new XAttribute("application", "conference_set_auto_outcall"), new XAttribute("data", leg));
                    elements.Add(dialElement);
                }

                XElement modeXElem = new XElement("action", new XAttribute("application", "conference"), new XAttribute("data", string.Format("{0}@{1}+{2}+flags{{{3}}}", confID, domain, pin, mode)));

                if (String.IsNullOrEmpty(mode))
                {
                    modeXElem = new XElement("action", new XAttribute("application", "conference"), new XAttribute("data", string.Format("{0}@{1}+{2}", confID, domain, pin)));
                }

                XElement xlem = null;


                xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                    new XElement("section", new XAttribute("name", "dialplan"),
                       new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                       new XElement("context",
                           new XAttribute("name", context),
                           new XElement("extension",
                               new XAttribute("name", extention),
                               new XElement("condition",
                                   new XAttribute("field", "destination_number"),
                                   new XAttribute("expression", destinationPattern),
                                   new XElement("action", new XAttribute("application", "answer")),
                                   new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                   new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=ConferenceOut")),
                                   new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                   new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "conference_auto_outcall_timeout=20")),
                                   new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "conference_auto_outcall_flags=none")),
                                   //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "conference_auto_outcall_caller_id_name=$${effective_caller_id_name}")),
                                   //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "conference_auto_outcall_caller_id_number=$${effective_caller_id_number}")),
                                   new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "conference_auto_outcall_profile=default")),
                                   elements,
                                   modeXElem)))));



                return xlem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public static XElement CheckVoicemail(string extention, string context, string destinationPattern, string profile, string domain)
        {
            try
            {
                logCSReq.Info("Creating dialplan PBX - CheckVoicemail");


                //<action application="bridge" data="user/1000@mydomain.com,[leg_delay_start=5]user/1001@mydomain.com,[leg_delay_start=15,leg_timeout=25]sofia/gateway/flowroute/12345678901" />
                string bridge = string.Empty;
                List<string> legs = new List<string>();

                XElement xlem = null;
                xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                    new XElement("section", new XAttribute("name", "dialplan"),
                       new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                       new XElement("context",
                           new XAttribute("name", context),
                           new XElement("extension",
                               new XAttribute("name", extention),
                               new XElement("condition",
                                   new XAttribute("field", "destination_number"),
                                   new XAttribute("expression", destinationPattern),
                                   new XElement("action", new XAttribute("application", "answer")),
                                   new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "voicemail_authorized=${sip_authorized}")),
                                   new XElement("action", new XAttribute("application", "voicemail"), new XAttribute("data", string.Format("check auth default {0} {1}", domain, extention))))))));
                                   //new XElement("action", new XAttribute("application", "voicemail"), new XAttribute("data", string.Format("check auth {0} $${{domain}} {1}", profile, extention))))))));

                //TODO: pass the domain inside and put default domain
                //
                //<action application="set" data="voicemail_authorized=${sip_authorized}"/>
                //<action application="voicemail" data="check auth $${voicemail_profile} $${domain} $1"/>
                //

                return xlem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public static XElement ReciveFax(string extention, string context, string destinationPattern, string domain, int companyId, int tenantId, string securityToken)
        {
            try
            {
                logCSReq.Info("Creating dialplan PBX - ReciveFax");

                var url = ConfigurationManager.AppSettings.Get("CurlFaxSendFileUrl");
                string options = string.Format("curl_sendfile:{0} file={1} class=CALLSERVER&type=FAX&category=RECIVEDFAX&company={2}&tenent={3}&server={4}&callID={1}&securitytoken={5} event {1}", url, "$${{base_dir}}/fax/inbox/${uuid}.tif", companyId, tenantId, "${uuid}", securityToken);
                
                //<action application="set" data="execute_on_fax_success=lua process_fax.lua"/>
                //<action application="bridge" data="user/1000@mydomain.com,[leg_delay_start=5]user/1001@mydomain.com,[leg_delay_start=15,leg_timeout=25]sofia/gateway/flowroute/12345678901" />
                string bridge = string.Empty;
                List<string> legs = new List<string>();

                XElement xlem = null;
                xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                    new XElement("section", new XAttribute("name", "dialplan"),
                       new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                       new XElement("context",
                           new XAttribute("name", context),
                           new XElement("extension",
                               new XAttribute("name", extention),
                               new XElement("condition",
                                   new XAttribute("field", "destination_number"),
                                   new XAttribute("expression", destinationPattern),
                                   new XElement("action", new XAttribute("application", "answer")),
                                   new XElement("action", new XAttribute("application", "playback"), new XAttribute("data", "silence_stream://2000")),
                                   new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "fax_enable_t38_request=true")),
                                   new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "fax_enable_t38=true")),
                                   new XElement("action", new XAttribute("application", "set"), new XAttribute("data", String.Format("execute_on_fax_success={0}", options))),
                                   new XElement("action", new XAttribute("application", "rxfax"), new XAttribute("data", "$${base_dir}/fax/inbox/${uuid}.tif")),
                                   new XElement("action", new XAttribute("application", "hangup")))))));

                //
                //<extension name="fax_receive">
                //<condition field="destination_number" expression="^9978$">
                //  <action application="answer" />
                //  <action application="playback" data="silence_stream://2000"/>
                //  <action application="set" data="fax_enable_t38_request=true"/>
                //  <action application="set" data="fax_enable_t38=true"/>
                //  <action application="rxfax" data="/tmp/FAX-${uuid}.tif"/>
                //  <action application="hangup"/>
                // </condition>
                // </extension>
                //

                return xlem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        public static XElement Conference(string extention, string context, string destinationPattern, string confID, string domain, string pin, string mode, int companyId, int tenantId)
        {
            try
            {
                logCSReq.Info("Creating dialplan PBX - CallConference - IN");

                string tempCustCompStr = String.Format("CustomCompanyStr={0}_{1}", companyId, tenantId);

                XElement modeXElem = new XElement("action", new XAttribute("application", "conference"), new XAttribute("data", string.Format("{0}@{1}+{2}+flags{{{3}}}", confID, domain, pin, mode)));

                if (String.IsNullOrEmpty(mode))
                {
                    modeXElem = new XElement("action", new XAttribute("application", "conference"), new XAttribute("data", string.Format("{0}+{2}", confID, domain, pin)));
                }

                //<action application="bridge" data="user/1000@mydomain.com,[leg_delay_start=5]user/1001@mydomain.com,[leg_delay_start=15,leg_timeout=25]sofia/gateway/flowroute/12345678901" />
                string bridge = string.Empty;
                List<string> legs = new List<string>();

                XElement xlem = null;
                xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                    new XElement("section", new XAttribute("name", "dialplan"),
                       new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                       new XElement("context",
                           new XAttribute("name", context),
                           new XElement("extension",
                               new XAttribute("name", extention),
                               new XElement("condition",
                                   new XAttribute("field", "destination_number"),
                                   new XAttribute("expression", destinationPattern),
                                   new XElement("action", new XAttribute("application", "answer")),
                                   new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "OperationType=Conference")),
                                   new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                   new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                   modeXElem)))));
                                   



                return xlem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //     <extension name="Demonstrate conference_set_auto_outcall">
        //  <condition field="destination_number" expression="^(12345)$">

        //    <action application="answer"/>

        //    <action application="set" data="conference_auto_outcall_timeout=5"/>
        //    <action application="set" data="conference_auto_outcall_flags=none"/>
        //    <action application="set" data="conference_auto_outcall_caller_id_name=$${effective_caller_id_name}"/>
        //    <action application="set" data="conference_auto_outcall_caller_id_number=$${effective_caller_id_number}"/>
        //    <action application="set" data="conference_auto_outcall_profile=default"/>

        //    <action application="conference_set_auto_outcall" data="user/1000@$${domain}"/>
        //    <action application="conference_set_auto_outcall" data="user/1001@$${domain}"/>

        //    <action application="conference" data="$1@default"/>

        //  </condition>
        //</extension>




        //        <extension name="valet_parking_dest_ext">
        //  <condition field="destination_number" expression="^(6000)$">
        //    <action application="answer"/>
        //    <action application="valet_park" data="valet_lot ask 1 11 10000 ivr/ivr-enter_ext_pound.wav"/>
        //  </condition>
        //</extension>

        public static XElement ParkAsk(string extention, string context, string profile, string destinationPattern, int companyId, int tenantId)
        {
            //<action application="set" data="intercept_unanswered_only=true"/>
            //<action application="intercept" data="myUUID"/>

            try
            {
                string tempCustCompStr = String.Format("CustomCompanyStr={0}_{1}", companyId, tenantId);

                XElement xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                            new XElement("section", new XAttribute("name", "dialplan"),
                               new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                               new XElement("context",
                                   new XAttribute("name", context),
                                   new XElement("extension",
                                       new XAttribute("name", extention),
                                       new XElement("condition",
                                           new XAttribute("field", "destination_number"),
                                           new XAttribute("expression", destinationPattern),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=ParkAsk")),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                           new XElement("action", new XAttribute("application", "Answer")),
                                           new XElement("action", new XAttribute("application", "valet_park"), new XAttribute("data", string.Format("{0} ask 1 11 10000 ivr/ivr-enter_ext_pound.wav", context))))))));

                return xlem;

            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public static XElement Park(string extention, string context, string profile, string destinationPattern, string parkID, int companyId, int tenantId)
        {
            //<action application="set" data="intercept_unanswered_only=true"/>
            //<action application="intercept" data="myUUID"/>

            try
            {
                string tempCustCompStr = String.Format("CustomCompanyStr={0}_{1}", companyId, tenantId);

                XElement xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                            new XElement("section", new XAttribute("name", "dialplan"),
                               new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                               new XElement("context",
                                   new XAttribute("name", context),
                                   new XElement("extension",
                                       new XAttribute("name", extention),
                                       new XElement("condition",
                                           new XAttribute("field", "destination_number"),
                                           new XAttribute("expression", destinationPattern),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=Park")),
                                           new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                           new XElement("action", new XAttribute("application", "Answer")),
                                           new XElement("action", new XAttribute("application", "valet_park"), new XAttribute("data", string.Format("{0} {1}", context, parkID))))))));

                return xlem;

            }
            catch (Exception)
            {
                throw;
            }
        }


        public static XElement FIFOIn(string extention, string context, string profile, string destinationPattern, string id)
        {
            //<action application="set" data="intercept_unanswered_only=true"/>
            //<action application="intercept" data="myUUID"/>

            try
            {
                
                XElement xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                            new XElement("section", new XAttribute("name", "dialplan"),
                               new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                               new XElement("context",
                                   new XAttribute("name", context),
                                   new XElement("extension",
                                       new XAttribute("name", extention),
                                       new XElement("condition",
                                           new XAttribute("field", "destination_number"),
                                           new XAttribute("expression", destinationPattern),
                                           new XElement("action", new XAttribute("application", "fifo"), new XAttribute("data", string.Format("{0}@${{domain_name}} in", id))))))));

                return xlem;

            }
            catch (Exception)
            {
                throw;
            }
        }


        public static XElement FIFOOut(string extention, string context, string profile, string destinationPattern, string id)
        {
            //<action application="set" data="intercept_unanswered_only=true"/>
            //<action application="intercept" data="myUUID"/>

            try
            {
                XElement xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                            new XElement("section", new XAttribute("name", "dialplan"),
                               new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                               new XElement("context",
                                   new XAttribute("name", context),
                                   new XElement("extension",
                                       new XAttribute("name", extention),
                                       new XElement("condition",
                                           new XAttribute("field", "destination_number"),
                                           new XAttribute("expression", destinationPattern),
                                           new XElement("action", new XAttribute("application", "Answer")),
                                           new XElement("action", new XAttribute("application", "fifo"), new XAttribute("data", string.Format("{0}@${{domain_name}} out nowait", id))))))));

                return xlem;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public static XElement RouteFwdUser(string uuid, string extention, string luaParams, string context, string profile, string destinationPattern, Endpoint endpoint, string custVarString, string fromGuUserId, string toGuUserId, CallerCalleeInfo callerCalleeInfo, bool needRecord = false, bool isVoicemailEnabled = false, string group = "", bool bypassMed = true, bool ignoreEarlyMedia = false)
        {

            try
            {
                using (RedisClient redisClient = new RedisClient(ConfigurationManager.AppSettings.Get("redisip")))
                {
                    //string key = string.Format("{0}:{1}:{2}", boardID, skill, section);
                    //Console.WriteLine(key);
                    if (redisClient.ContainsKey(String.Format("{0}_{1}", custVarString, extention)))
                    {
                        redisClient.Remove(String.Format("{0}_{1}", custVarString, extention));
                    }
                    redisClient.Add<string>(String.Format("{0}_{1}", custVarString, extention), uuid, new TimeSpan(0, 55, 0));
                }
            }
            catch (Exception ex)
            { }

            try
            {
                logCSReq.Info("Creating dialplan PBX - Conditional Forward");

                var bypassMedia = "bypass_media=true";
                if (!bypassMed)
                {
                    bypassMedia = "bypass_media=false";
                }

                var ignoreEarlyM = "ignore_early_media=false";
                if (ignoreEarlyMedia)
                {
                    ignoreEarlyM = "ignore_early_media=true";
                }

                string tempCustCompStr = String.Format("CustomCompanyStr={0}", custVarString);

                //<action application="bridge" data="user/1000@mydomain.com,[leg_delay_start=5]user/1001@mydomain.com,[leg_delay_start=15,leg_timeout=25]sofia/gateway/flowroute/12345678901" />
                string bridge = string.Empty;

                string destinationGroup = "external";
                switch (endpoint.Type)
                {
                    case DnisType.Gateway:
                        destinationGroup = string.Format("gateway/{0}", endpoint.Profile);

                        break;

                    case DnisType.User:
                        destinationGroup = "user";
                        //bypassMedia = "true";
                        break;

                    case DnisType.Group:
                        destinationGroup = "group";
                        //bypassMedia = "true";
                        break;

                    case DnisType.loopback:
                        destinationGroup = "loopback";
                        break;

                    default:
                        destinationGroup = string.Format("{0}", endpoint.Profile);
                        break;
                }

                string option = string.Empty;

                endpoint.LegTimeout = 60;
                //if (endpoint.LegStartDelay > 0)
                //    option = string.Format("[leg_delay_start={0},leg_timeout={1},bypass_media={2},origination_caller_id_number={3}]", endpoint.LegStartDelay, endpoint.LegTimeout, bypassMedia, endpoint.Origination);
                //else
                //    option = string.Format("[leg_timeout={0},bypass_media={1},origination_caller_id_number={2}]", endpoint.LegTimeout, bypassMedia, endpoint.Origination);

                if (endpoint.LegStartDelay > 0)
                    option = string.Format("[leg_delay_start={0},leg_timeout={1},origination_caller_id_name={3},origination_caller_id_number={4}]", endpoint.LegStartDelay, endpoint.LegTimeout, bypassMedia, endpoint.Origination, callerCalleeInfo.FromNumber);
                else
                    option = string.Format("[leg_timeout={0},origination_caller_id_name={2},origination_caller_id_number={3}]", endpoint.LegTimeout, bypassMedia, endpoint.Origination, callerCalleeInfo.FromNumber);


                string protocoal = "sofia";
                string calling = string.Empty;
                string dnis = endpoint.Destination;


                //context.GetExtension<IActions>().GetSystemVariables(SystemVariables.Domain);


                if (!string.IsNullOrEmpty(endpoint.Domain))
                {
                    dnis = string.Format("{0}@{1}", dnis, endpoint.Domain);
                }

                switch (endpoint.Protocol)
                {
                    case Protocol.SIP:
                        calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    case Protocol.XMPP:
                        protocoal = "dingaling";
                        calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    case Protocol.PSTN:
                        protocoal = "freetdm";
                        calling = string.Format("{0}{1}/{2}/a/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    default:
                        calling = string.Format("{0}{1}/{2}", option, destinationGroup, dnis);
                        break;

                }

                if (endpoint.Type == DnisType.User)
                {
                    if (!string.IsNullOrEmpty(group))
                    {
                        calling = string.Format("{0},pickup/{1}", calling, group);
                        //pickup/mygroup
                    }
                }

                string tempFromGuUser = String.Format("FromGuUserId={0}", fromGuUserId);
                string tempToGuUser = String.Format("ToGuUserId={0}", toGuUserId);

                string tempFromUser = String.Format("FromUser={0}", callerCalleeInfo.FromUser);
                string tempFromNumber = String.Format("FromNumber={0}", callerCalleeInfo.FromNumber);
                string tempToNumber = String.Format("nolocal:ToNumber={0}", callerCalleeInfo.ToNumber);
                string tempToUser = String.Format("nolocal:ToUser={0}", callerCalleeInfo.ToUser);

                XElement xlem = null;

                if (!isVoicemailEnabled)
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", context),
                               new XElement("extension",
                                   new XAttribute("name", extention),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", destinationPattern),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=User")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromGuUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToGuUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToUser)),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", ignoreEarlyM)),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", bypassMedia)),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                        //new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s valet_park::valet_lot auto in 8001 8999")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s execute_extension::att_xfer_conference XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", calling)),
                                       new XElement("action", new XAttribute("application", "lua"), new XAttribute("data", luaParams)),
                                       new XElement("action", new XAttribute("application", "hangup")))))));
                }
                else
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                       new XElement("section", new XAttribute("name", "dialplan"),
                          new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                          new XElement("context",
                              new XAttribute("name", context),
                              new XElement("extension",
                                  new XAttribute("name", extention),
                                  new XElement("condition",
                                      new XAttribute("field", "destination_number"),
                                      new XAttribute("expression", destinationPattern),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=User")),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromGuUser)),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToGuUser)),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromUser)),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromNumber)),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToNumber)),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToUser)),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=true")),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", ignoreEarlyM)),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", bypassMedia)),
                                      new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                                      new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                        //new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s valet_park::valet_lot auto in 8001 8999")),
                                      new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                      new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s execute_extension::att_xfer_conference XML PBXFeatures")),
                                      new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", calling)),
                        //new XElement("action", new XAttribute("application", "voicemail"), new XAttribute("data", string.Format("default $${{domain}} {1}", profile, extention))),
                                      new XElement("action", new XAttribute("application", "answer")),
                                      new XElement("action", new XAttribute("application", "voicemail"), new XAttribute("data", string.Format("default {0} {1}", endpoint.Domain, extention)))
                                      )))));

                    //TODO: pass the domain inside and put default domain

                }

                return xlem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static XElement RouteUser(string uuid, string extention, string context, string profile, string destinationPattern, Endpoint endpoint, string custVarString, string fromGuUserId, string toGuUserId, CallerCalleeInfo callerCalleeInfo, bool needRecord = false, bool isVoicemailEnabled = false, string group = "", bool bypassMed = true, bool ignoreEarlyMedia = false, string oprType = "User")
        {
            try
            {
                using (RedisClient redisClient = new RedisClient(ConfigurationManager.AppSettings.Get("redisip")))
                {
                    //string key = string.Format("{0}:{1}:{2}", boardID, skill, section);
                    //Console.WriteLine(key);
                    if (redisClient.ContainsKey(String.Format("{0}_{1}", custVarString, extention)))
                    {
                        redisClient.Remove(String.Format("{0}_{1}", custVarString, extention));
                    }
                    redisClient.Add<string>(String.Format("{0}_{1}", custVarString, extention), uuid, new TimeSpan(0, 55, 0));
                }
            }
            catch (Exception ex)
            { }

            try
            {
                logCSReq.Info("Creating dialplan PBX - user");

                var bypassMedia = "bypass_media=true";
                if (!bypassMed)
                {
                    bypassMedia = "bypass_media=false";
                }

                var ignoreEarlyM = "ignore_early_media=false";
                if (ignoreEarlyMedia)
                {
                    ignoreEarlyM = "ignore_early_media=true";
                }

                string tempCustCompStr = String.Format("CustomCompanyStr={0}", custVarString);

                //<action application="bridge" data="user/1000@mydomain.com,[leg_delay_start=5]user/1001@mydomain.com,[leg_delay_start=15,leg_timeout=25]sofia/gateway/flowroute/12345678901" />
                string bridge = string.Empty;

                string destinationGroup = "external";
                switch (endpoint.Type)
                {
                    case DnisType.Gateway:
                        destinationGroup = string.Format("gateway/{0}", endpoint.Profile);

                        break;

                    case DnisType.User:
                        destinationGroup = "user";
                        //bypassMedia = "true";
                        break;

                    case DnisType.Group:
                        destinationGroup = "group";
                        //bypassMedia = "true";
                        break;

                    case DnisType.loopback:
                        destinationGroup = "loopback";
                        break;

                    default:
                        destinationGroup = string.Format("{0}", endpoint.Profile);
                        break;
                }

                string option = string.Empty;

                endpoint.LegTimeout = 60;
                
                if (endpoint.LegStartDelay > 0)
                    option = string.Format("[leg_delay_start={0},leg_timeout={1},origination_caller_id_name={3},origination_caller_id_number={4}]", endpoint.LegStartDelay, endpoint.LegTimeout, bypassMedia, endpoint.Origination, callerCalleeInfo.FromNumber);
                else
                    option = string.Format("[leg_timeout={0},origination_caller_id_name={2},origination_caller_id_number={3}]", endpoint.LegTimeout, bypassMedia, endpoint.Origination, callerCalleeInfo.FromNumber);


                string protocoal = "sofia";
                string calling = string.Empty;
                string dnis = endpoint.Destination;


                //context.GetExtension<IActions>().GetSystemVariables(SystemVariables.Domain);


                if (!string.IsNullOrEmpty(endpoint.Domain))
                {
                    dnis = string.Format("{0}@{1}", dnis, endpoint.Domain);
                }

                switch (endpoint.Protocol)
                {
                    case Protocol.SIP:
                        calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    case Protocol.XMPP:
                        protocoal = "dingaling";
                        calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    case Protocol.PSTN:
                        protocoal = "freetdm";
                        calling = string.Format("{0}{1}/{2}/a/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    default:
                        calling = string.Format("{0}{1}/{2}", option, destinationGroup, dnis);
                        break;

                }

                if (endpoint.Type == DnisType.User)
                {
                    if (!string.IsNullOrEmpty(group))
                    {
                        calling = string.Format("{0},pickup/{1}", calling, group);
                        //pickup/mygroup
                    }
                }

                string tempFromGuUser = String.Format("FromGuUserId={0}", fromGuUserId);
                string tempToGuUser = String.Format("ToGuUserId={0}", toGuUserId);

                string tempFromUser = String.Format("FromUser={0}", callerCalleeInfo.FromUser);
                string tempFromNumber = String.Format("FromNumber={0}", callerCalleeInfo.FromNumber);
                string tempToNumber = String.Format("nolocal:ToNumber={0}", callerCalleeInfo.ToNumber);
                string tempToUser = String.Format("nolocal:ToUser={0}", callerCalleeInfo.ToUser);
                string opType = String.Format("OperationType={0}", oprType);

                XElement xlem = null;

                if (!isVoicemailEnabled)
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", context),
                               new XElement("extension",
                                   new XAttribute("name", extention),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", destinationPattern),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", opType)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromGuUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToGuUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToUser)),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", ignoreEarlyM)),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", bypassMedia)),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                        //new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s valet_park::valet_lot auto in 8001 8999")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s execute_extension::att_xfer_conference XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", calling)),
                                       new XElement("action", new XAttribute("application", "hangup")))))));
                }
                else
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                       new XElement("section", new XAttribute("name", "dialplan"),
                          new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                          new XElement("context",
                              new XAttribute("name", context),
                              new XElement("extension",
                                  new XAttribute("name", extention),
                                  new XElement("condition",
                                      new XAttribute("field", "destination_number"),
                                      new XAttribute("expression", destinationPattern),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", opType)),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromGuUser)),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToGuUser)),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromUser)),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromNumber)),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToNumber)),
                                      new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToUser)),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=true")),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", ignoreEarlyM)),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", bypassMedia)),
                                      new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                                      new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                        //new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s valet_park::valet_lot auto in 8001 8999")),
                                      new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                      new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s execute_extension::att_xfer_conference XML PBXFeatures")),
                                      new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", calling)),
                        //new XElement("action", new XAttribute("application", "voicemail"), new XAttribute("data", string.Format("default $${{domain}} {1}", profile, extention))),
                                      new XElement("action", new XAttribute("application", "answer")),
                                      new XElement("action", new XAttribute("application", "voicemail"), new XAttribute("data", string.Format("default {0} {1}", endpoint.Domain, extention)))
                                      )))));

                    //TODO: pass the domain inside and put default domain

                }

                return xlem;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            
        }

        public static XElement RouteFax(string uuid, string extention, string context, string profile, string destinationPattern, Endpoint endpoint, string custVarString, string fromGuUserId, string toGuUserId, CallerCalleeInfo callerCalleeInfo, bool needRecord = false, bool isVoicemailEnabled = false, string group = "", string faxType = "")
        {
            XElement faxEnable = new XElement("Error");

            XElement faxSetProperty = new XElement("Error");

            logCSReq.Info(String.Format("Creating dialplan PBX - User fax - Type : {0}", faxType));

            try
            {
                if (faxType.Equals("AUDIO_T38"))
                {
                    faxEnable = new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "fax_enable_t38=true"));
                    faxSetProperty = new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "sip_execute_on_image=t38_gateway self nocng"));
                }
                else if (faxType.Equals("T38_AUDIO"))
                {
                    faxEnable = new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "sip_execute_on_image=t38_gateway self nocng"));
                    faxSetProperty = new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "refuse_t38=true"));
                }
                else if (faxType.Equals("T38_T30AUDIO"))
                {
                    faxEnable = new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "fax_enable_t38=true"));
                    faxSetProperty = new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "sip_execute_on_image=t38_gateway peer nocng"));
                }
                else if (faxType.Equals("T38_T38"))
                {
                    faxEnable = new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "t38_passthru=true"));
                    faxSetProperty = new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "refuse_t38=true"));
                }
                else if (faxType.Equals("T38PassThru"))
                {
                    faxEnable = new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "fax_enable_t38=true"));
                    faxSetProperty = new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "sip_execute_on_image=t38_gateway self nocng"));
                }
                else
                {
                    logCSReq.Debug("Default Fax Export");
                    faxEnable = new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "fax_enable_t38=true"));
                    faxSetProperty = new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "sip_execute_on_image=t38_gateway self nocng"));
                }

                logCSReq.Debug(String.Format("{0},{1}", faxEnable, faxSetProperty));

                using (RedisClient redisClient = new RedisClient(ConfigurationManager.AppSettings.Get("redisip")))
                {
                    //string key = string.Format("{0}:{1}:{2}", boardID, skill, section);
                    //Console.WriteLine(key);
                    if (redisClient.ContainsKey(String.Format("{0}_{1}", custVarString, extention)))
                    {
                        redisClient.Remove(String.Format("{0}_{1}", custVarString, extention));
                    }
                    redisClient.Add<string>(String.Format("{0}_{1}", custVarString, extention), uuid, new TimeSpan(0, 55, 0));
                }
            }
            catch (Exception ex)
            {
                logCSReq.Error("Error", ex);
            }

            try
            {
                logCSReq.Info("Creating dialplan PBX - Fax");

                string tempCustCompStr = String.Format("CustomCompanyStr={0}", custVarString);

                //<action application="bridge" data="user/1000@mydomain.com,[leg_delay_start=5]user/1001@mydomain.com,[leg_delay_start=15,leg_timeout=25]sofia/gateway/flowroute/12345678901" />
                string bridge = string.Empty;
                string bypassMedia = "false";

                string destinationGroup = "external";
                switch (endpoint.Type)
                {
                    case DnisType.Gateway:
                        destinationGroup = string.Format("gateway/{0}", endpoint.Profile);

                        break;

                    case DnisType.User:
                        destinationGroup = "user";
                        bypassMedia = "false";
                        break;

                    case DnisType.Group:
                        destinationGroup = "group";
                        bypassMedia = "false";
                        break;

                    case DnisType.loopback:
                        destinationGroup = "loopback";
                        break;

                    default:
                        destinationGroup = string.Format("{0}", endpoint.Profile);
                        break;
                }

                string option = string.Empty;

                endpoint.LegTimeout = 60;

                if (endpoint.LegStartDelay > 0)
                    option = string.Format("[leg_delay_start={0},leg_timeout={1},origination_caller_id_number={3}]", endpoint.LegStartDelay, endpoint.LegTimeout, bypassMedia, endpoint.Origination);
                else
                    option = string.Format("[leg_timeout={0},origination_caller_id_number={2}]", endpoint.LegTimeout, bypassMedia, endpoint.Origination);


                string protocoal = "sofia";
                string calling = string.Empty;
                string dnis = endpoint.Destination;


                //context.GetExtension<IActions>().GetSystemVariables(SystemVariables.Domain);
                if (!string.IsNullOrEmpty(endpoint.Domain))
                {
                    dnis = string.Format("{0}@{1}", dnis, endpoint.Domain);
                }

                switch (endpoint.Protocol)
                {
                    case Protocol.SIP:
                        calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    case Protocol.XMPP:
                        protocoal = "dingaling";
                        calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    case Protocol.PSTN:
                        protocoal = "freetdm";
                        calling = string.Format("{0}{1}/{2}/a/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    default:
                        calling = string.Format("{0}{1}/{2}", option, destinationGroup, dnis);
                        break;

                }

                

                string tempFromGuUser = String.Format("FromGuUserId={0}", fromGuUserId);
                string tempToGuUser = String.Format("ToGuUserId={0}", toGuUserId);

                string tempFromUser = String.Format("FromUser={0}", callerCalleeInfo.FromUser);
                string tempFromNumber = String.Format("FromNumber={0}", callerCalleeInfo.FromNumber);
                string tempToNumber = String.Format("nolocal:ToNumber={0}", callerCalleeInfo.ToNumber);
                string tempToUser = String.Format("nolocal:ToUser={0}", callerCalleeInfo.ToUser);

                XElement xlem = null;

                xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", context),
                               new XElement("extension",
                                   new XAttribute("name", extention),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", destinationPattern),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=Fax")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromGuUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToGuUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromUser)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempFromNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToNumber)),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempToUser)),
                                       faxEnable,
                                       faxSetProperty,
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ignore_early_media=true")),
                                       //new XElement("action", new XAttribute("application", "set"), new XAttribute("data", bypassMedia)),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                    //new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s valet_park::valet_lot auto in 8001 8999")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s execute_extension::att_xfer_conference XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", calling)),
                                       new XElement("action", new XAttribute("application", "hangup")))))));

                

                return xlem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static XElement RouteOperator(string uuid, string extention, string context, string profile, string destinationPattern, Endpoint endpoint, string custVarString, bool needRecord = false, bool isVoicemailEnabled = false, string group = "", string parkinit = "8000", string parkend = "8999")
        {

            try
            {
                using (RedisClient redisClient = new RedisClient(ConfigurationManager.AppSettings.Get("redisip")))
                {
                    //string key = string.Format("{0}:{1}:{2}", boardID, skill, section);
                    //Console.WriteLine(key);
                    if (redisClient.ContainsKey(String.Format("{0}_{1}", custVarString, extention)))
                    {
                        redisClient.Remove(String.Format("{0}_{1}", custVarString, extention));
                    }
                    redisClient.Add<string>(String.Format("{0}_{1}", custVarString, extention), uuid, new TimeSpan(0, 55, 0));
                }
            }
            catch (Exception ex)
            { }

            try
            {
                logCSReq.Info("Creating dialplan PBX - gateway");

                //<action application="bridge" data="user/1000@mydomain.com,[leg_delay_start=5]user/1001@mydomain.com,[leg_delay_start=15,leg_timeout=25]sofia/gateway/flowroute/12345678901" />
                string bridge = string.Empty;
                string bypassMedia = "false";

                string destinationGroup = "external";
                switch (endpoint.Type)
                {
                    case DnisType.Gateway:
                        destinationGroup = string.Format("gateway/{0}", endpoint.Profile);

                        break;

                    case DnisType.User:
                        destinationGroup = "user";
                        bypassMedia = "true";
                        break;

                    case DnisType.Group:
                        destinationGroup = "group";
                        bypassMedia = "true";
                        break;

                    case DnisType.loopback:
                        destinationGroup = "loopback";
                        break;

                    default:
                        destinationGroup = string.Format("{0}", endpoint.Profile);
                        break;
                }

                string option = string.Empty;


                if (endpoint.LegStartDelay > 0)
                    option = string.Format("[leg_delay_start={0},leg_timeout={1},bypass_media={2},origination_caller_id_number={3}]", endpoint.LegStartDelay, endpoint.LegTimeout, bypassMedia, endpoint.Origination);
                else
                    option = string.Format("[leg_timeout={0},bypass_media={1},origination_caller_id_number={2}]", endpoint.LegTimeout, bypassMedia, endpoint.Origination);


                string protocoal = "sofia";
                string calling = string.Empty;
                string dnis = endpoint.Destination;


                //context.GetExtension<IActions>().GetSystemVariables(SystemVariables.Domain);
                if (!string.IsNullOrEmpty(endpoint.Domain))
                {
                    dnis = string.Format("{0}@{1}", dnis, endpoint.Domain);
                }

                switch (endpoint.Protocol)
                {
                    case Protocol.SIP:
                        calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    case Protocol.XMPP:
                        protocoal = "dingaling";
                        calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    case Protocol.PSTN:
                        protocoal = "freetdm";
                        calling = string.Format("{0}{1}/{2}/a/{3}", option, protocoal, destinationGroup, dnis);
                        break;

                    default:
                        calling = string.Format("{0}{1}/{2}", option, destinationGroup, dnis);
                        break;

                }

                if (!string.IsNullOrEmpty(group))
                {
                    calling = string.Format("{0},pickup/{1}", calling, group);
                    //pickup/mygroup
                }


                XElement xlem = null;

                if (!isVoicemailEnabled)
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", context),
                               new XElement("extension",
                                   new XAttribute("name", extention),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", destinationPattern),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "OperationType=User")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ignore_early_media=true")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "2 b s execute_extension::fifo XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 b s execute_extension::att_xfer XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 b s execute_extension::att_xfer_group XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", string.Format("5 b s valet_park::{0} auto in {1} {2}", context, parkinit, parkend))),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 b s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", calling)),
                                       new XElement("action", new XAttribute("application", "hangup")))))));
                }
                else
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                       new XElement("section", new XAttribute("name", "dialplan"),
                          new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                          new XElement("context",
                              new XAttribute("name", context),
                              new XElement("extension",
                                  new XAttribute("name", extention),
                                  new XElement("condition",
                                      new XAttribute("field", "destination_number"),
                                      new XAttribute("expression", destinationPattern),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "OperationType=User")),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=true")),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
                                      new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ignore_early_media=true")),
                                      new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "2 b s execute_extension::fifo XML PBXFeatures")),
                                      new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 b s execute_extension::att_xfer XML PBXFeatures")),
                                      new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 b s execute_extension::att_xfer_group XML PBXFeatures")),
                                      new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", string.Format("5 b s valet_park::{0} auto in {1} {2}", context, parkinit, parkend))),
                                      new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 b s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                      new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", calling)),
                                      new XElement("action", new XAttribute("application", "voicemail"), new XAttribute("data", string.Format("{0} $${{domain}} {1}", profile, extention))),
                                      new XElement("action", new XAttribute("application", "hangup")))))));

                }

                return xlem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static XElement RouteUser(string extention, string context, string profile, string destinationPattern, bool isVoicemailEnabled, int timeout = 60, bool needRecord = false)
        //{

        //    string bypassMedia = "true";
        //    if (needRecord)
        //    {
        //        bypassMedia = "false";
        //    }

        //    try
        //    {
        //        logCSReq.Info("Creating dialplan PBX - user");

        //        //<action application="bridge" data="user/1000@mydomain.com,[leg_delay_start=5]user/1001@mydomain.com,[leg_delay_start=15,leg_timeout=25]sofia/gateway/flowroute/12345678901" />
        //        string bridge = string.Format("[leg_timeout={0},bypass_media={1}]user/{2}@$${{domain}}", timeout, bypassMedia, extention);
        //        List<string> legs = new List<string>();


        //        XElement xlem = null;
        //        if (isVoicemailEnabled)
        //        {
        //            xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
        //                                   new XElement("section", new XAttribute("name", "dialplan"),
        //                      new XAttribute("description", "RE Dial Plan For FreeSwitch"),
        //                      new XElement("context",
        //                          new XAttribute("name", context),
        //                          new XElement("extension",
        //                              new XAttribute("name", extention),
        //                              new XElement("condition",
        //                                  new XAttribute("field", "destination_number"),
        //                                  new XAttribute("expression", destinationPattern),
        //                                  new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
        //                                  new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=false")),
        //                                  new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
        //                                  new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ignore_early_media=true")),
        //                                  new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
        //                                  new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
        //                                  new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "5 ab s valet_park::valet_lot auto in 8001 8999")),
        //                                  new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
        //                                  new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", bridge)),
        //                                  new XElement("action", new XAttribute("application", "voicemail"), new XAttribute("data", string.Format("{0} $${{domain}} {1}", profile, extention))))))));
        //        }
        //        else
        //        {

        //            xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
        //                                new XElement("section", new XAttribute("name", "dialplan"),
        //                   new XAttribute("description", "RE Dial Plan For FreeSwitch"),
        //                   new XElement("context",
        //                       new XAttribute("name", context),
        //                       new XElement("extension",
        //                           new XAttribute("name", extention),
        //                           new XElement("condition",
        //                               new XAttribute("field", "destination_number"),
        //                               new XAttribute("expression", destinationPattern),
        //                               new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "ringback=${us-ring}")),
        //                               new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=false")),
        //                               new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
        //                               new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ignore_early_media=true")),
        //                               new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
        //                               new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
        //                               new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "5 ab s valet_park::valet_lot auto in 8001 8999")),
        //                               new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
        //                               new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", bridge)))))));

        //        }




        //        /*

        //         * new XElement("action",new XAttribute("application", "ring_ready"))

        //         * new XElement("action",new XAttribute("application", "set"), new XAttribute("data", "transfer_ringback=${us-ring}"))

        //         * new XElement("action",new XAttribute("application", "set"), new XAttribute("data", "uuid_bridge_continue_on_cancel=true")),

        //         * new XElement("action",new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=true")),

        //         * new XElement("action",new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),

        //         * new XElement("action",new XAttribute("application", "set"),  new XAttribute("data", "ignore_early_media=true")),

        //         * new XElement("action",new XAttribute("application", "set"),  new XAttribute("data", "bypass_media=true")),

        //         */

        //        return xlem;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public struct FmLeg
        {
            public string leg;
            public string bypassM;
        }

        public static XElement FollowME(string extention, string domain, string context, string profile, string destinationPattern, List<Endpoint> endpoints, int companyId, int tenantId, bool isVoicemailEnabled = false, bool isBindExtentionEnabled = true, int timeout = 60, bool isparallel = true, bool bypassMed = true, bool ignoreEarlyMedia = false)
        {
            try
            {
                logCSReq.Info("Creating dialplan PBX - followme");

                string tempCustCompStr = String.Format("CustomCompanyStr={0}_{1}", companyId, tenantId);


                //<action application="bridge" data="user/1000@mydomain.com,[leg_delay_start=5]user/1001@mydomain.com,[leg_delay_start=15,leg_timeout=25]sofia/gateway/flowroute/12345678901" />
                string bridge = string.Empty;
                List<FmLeg> legs = new List<FmLeg>();

                

                var ignoreEarlyM = "ignore_early_media=false";
                if (ignoreEarlyMedia)
                {
                    ignoreEarlyM = "ignore_early_media=true";
                }

                var bypassMedia = "bypass_media=true";

                if (isBindExtentionEnabled)
                {
                    //string bridgeExt = string.Format("[leg_timeout={0}]user/{2}@{3}", timeout, "true", extention, domain);
                    
                    if (!bypassMed)
                    {
                        bypassMedia = "bypass_media=false";
                    }
                    else
                    {
                        bypassMedia = "bypass_media=true";
                    }

                    var fmLegs = new FmLeg
                        {
                            leg = string.Format("[leg_timeout={0}]user/{2}@{3}", timeout, "true", extention, domain),
                            bypassM = bypassMedia
                        };
                    legs.Add(fmLegs);
                }

                foreach (var endpoint in endpoints)
                {
                    if (!bypassMed)
                    {
                        bypassMedia = "bypass_media=false";
                    }
                    else
                    {
                        bypassMedia = "bypass_media=true";
                    }

                    string destinationGroup = "external";
                    switch (endpoint.Type)
                    {
                        case DnisType.Gateway:
                            destinationGroup = string.Format("gateway/{0}", endpoint.Profile);
                            bypassMedia = "bypass_media=false";
                            break;

                        case DnisType.User:
                            destinationGroup = "user";
                            break;

                        case DnisType.Group:
                            destinationGroup = "group";
                            break;

                        case DnisType.loopback:
                            destinationGroup = "loopback";
                            break;

                        default:
                            destinationGroup = string.Format("{0}", endpoint.Profile);
                            break;
                    }

                    string option = string.Empty;


                    if (endpoint.LegStartDelay > 0)
                        option = string.Format("[leg_delay_start={0},leg_timeout={1},origination_caller_id_number={3},sip_h_X-Gateway={4}]", endpoint.LegStartDelay, endpoint.LegTimeout, bypassMedia, endpoint.Origination, endpoint.IpUrl);
                    else
                        option = string.Format("[leg_timeout={0},origination_caller_id_number={2},sip_h_X-Gateway={3}]", endpoint.LegTimeout, bypassMedia, endpoint.Origination, endpoint.IpUrl);


                    string protocoal = "sofia";
                    string calling = string.Empty;
                    string dnis = endpoint.Destination;


                    //context.GetExtension<IActions>().GetSystemVariables(SystemVariables.Domain);
                    if (!string.IsNullOrEmpty(endpoint.Domain))
                    {
                        dnis = string.Format("{0}@{1}", dnis, endpoint.Domain);
                    }

                    switch (endpoint.Protocol)
                    {
                        case Protocol.SIP:
                            calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                            break;

                        case Protocol.XMPP:
                            protocoal = "dingaling";
                            calling = string.Format("{0}{1}/{2}/{3}", option, protocoal, destinationGroup, dnis);
                            break;

                        case Protocol.PSTN:
                            protocoal = "freetdm";
                            calling = string.Format("{0}{1}/{2}/a/{3}", option, protocoal, destinationGroup, dnis);
                            break;

                        default:
                            calling = string.Format("{0}{1}/{2}", option, destinationGroup, dnis);
                            break;

                    }

                    var fmLeg = new FmLeg
                    {
                        leg = calling,
                        bypassM = bypassMedia
                    };
                    legs.Add(fmLeg);
                    //legs.Add(calling);

                }
                List<XElement> elements = new List<XElement>();
                if (isparallel)
                {
                    bridge = string.Join(",", legs.Select(i => i.leg));
                    XElement dialElement = new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", bridge));
                    elements.Add(dialElement);
                }
                else
                {

                    foreach (FmLeg leg in legs)
                    {
                        XElement dialElement2 = new XElement("action", new XAttribute("application", "set"), new XAttribute("data", leg.bypassM));
                        XElement dialElement = new XElement("action", new XAttribute("application", "bridge"), new XAttribute("data", leg.leg));

                        elements.Add(dialElement2);
                        elements.Add(dialElement);
                    }

                }

                XElement xlem = null;
                if (isVoicemailEnabled)
                {
                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                           new XElement("section", new XAttribute("name", "dialplan"),
                              new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                              new XElement("context",
                                  new XAttribute("name", context),
                                  new XElement("extension",
                                      new XAttribute("name", extention),
                                      new XElement("condition",
                                          new XAttribute("field", "destination_number"),
                                          new XAttribute("expression", destinationPattern),
                                          new XElement("action", new XAttribute("application", "ring_ready")),
                                          new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=FollowMe")),
                                          new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                          new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                          new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "transfer_ringback=${us-ring}")),
                                          new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=true")),
                                          new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
                                          new XElement("action", new XAttribute("application", "set"), new XAttribute("data", ignoreEarlyM)),
                                          new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "uuid_bridge_continue_on_cancel=true")),
                                          new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "dtmf_type=info")),
                                          new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                                          new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                        //new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s valet_park::valet_lot auto in 8001 8999")),
                                          new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                          new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s execute_extension::att_xfer_conference XML PBXFeatures")),
                                          elements,
                                          new XElement("action", new XAttribute("application", "voicemail"), new XAttribute("data", string.Format("{0} $${{domain}} {1}", profile, extention))),
                                          new XElement("action", new XAttribute("application", "hangup")))))));
                }
                else
                {

                    xlem = new XElement("document", new XAttribute("type", "freeswitch/xml"),
                                        new XElement("section", new XAttribute("name", "dialplan"),
                           new XAttribute("description", "RE Dial Plan For FreeSwitch"),
                           new XElement("context",
                               new XAttribute("name", context),
                               new XElement("extension",
                                   new XAttribute("name", extention),
                                   new XElement("condition",
                                       new XAttribute("field", "destination_number"),
                                       new XAttribute("expression", destinationPattern),
                                       new XElement("action", new XAttribute("application", "ring_ready")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "ringback=${us-ring}")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", "OperationType=FollowMe")),
                                       new XElement("action", new XAttribute("application", "export"), new XAttribute("data", tempCustCompStr)),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "transfer_ringback=${us-ring}")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "dtmf_type=info")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", ignoreEarlyM)),
                                       new XElement("action", new XAttribute("application", "set"), new XAttribute("data", "uuid_bridge_continue_on_cancel=true")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "3 ab s execute_extension::att_xfer XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "4 ab s execute_extension::att_xfer_group XML PBXFeatures")),
                        //new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s valet_park::valet_lot auto in 8001 8999")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "5 ab s execute_extension::att_xfer_conference XML PBXFeatures")),
                                       new XElement("action", new XAttribute("application", "bind_meta_app"), new XAttribute("data", "6 ab s execute_extension::att_xfer_outbound XML PBXFeatures")),
                                       elements,
                                       new XElement("action", new XAttribute("application", "hangup")))))));

                }

                /*
                  
                 * new XElement("action",new XAttribute("application", "ring_ready"))
                 
                 * new XElement("action",new XAttribute("application", "set"), new XAttribute("data", "transfer_ringback=${us-ring}"))

                 * new XElement("action",new XAttribute("application", "set"), new XAttribute("data", "uuid_bridge_continue_on_cancel=true")),
                 
                 * new XElement("action",new XAttribute("application", "set"), new XAttribute("data", "continue_on_fail=true")),

                 * new XElement("action",new XAttribute("application", "set"), new XAttribute("data", "hangup_after_bridge=true")),

                 * new XElement("action",new XAttribute("application", "set"),  new XAttribute("data", "ignore_early_media=true")),

                 * new XElement("action",new XAttribute("application", "set"),  new XAttribute("data", "bypass_media=true")),
                 
                 */

                return xlem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

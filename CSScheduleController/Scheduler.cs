﻿using System;
using System.Collections.Generic;
using System.Linq;
using DuoSoftware.DC.CallServerDBModelsOS;

namespace DuoSoftware.CSScheduleController
{
    public class Scheduler
    {
        log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public CSDB_CSSchedule PickSchedule(List<CSDB_CSSchedule> appointments, DateTime currentTime, int companyId, int tenantId)
        {
            try
            {
                logCSReq.Debug(String.Format("Picking Schedule - NoOFAppointments : {0}, currentTime : {1}, Company : {2}, Tenant : {3}", appointments.Count, currentTime, companyId, tenantId));
                int currTimeHrs = currentTime.Hour;
                int currTimeMinutes = currentTime.Minute;

                logCSReq.Debug(String.Format("Hour : {0}, Min : {1}", currTimeHrs, currTimeMinutes));

                var sortedList = from i in appointments
                                 where i.CompanyId.Equals(companyId) && i.TenantId.Equals(tenantId)
                                 orderby i.Priority ascending
                                 select i;

                foreach (var appointment in sortedList)
                {
                    string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(appointment);
                    logCSReq.Debug(String.Format("Appointment : {0}", jsonString));
                    if (appointment.DateRangeEnabled || appointment.DaysOfWeekEnabled || appointment.SingleDayEnabled || appointment.TimeRangeEnabled)
                    {
                        if (appointment.DateRangeEnabled)
                        {
                            if (!(currentTime >= appointment.RangeStartDate && currentTime <= appointment.RangeEndDate))
                            {
                                continue;
                            }
                        }

                        if (appointment.SingleDayEnabled)
                        {
                            if (!currentTime.Date.Equals(appointment.SingleDay.Date))
                            {
                                continue;
                            }
                        }

                        if (appointment.DaysOfWeekEnabled)
                        {
                            var days = appointment.DaysOfWeek.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                            var day = currentTime.DayOfWeek.ToString();

                            var isday = days.FirstOrDefault(i => i.Equals(day));

                            if (isday == null)
                            {
                                continue;
                            }

                        }

                        if (appointment.TimeRangeEnabled)
                        {
                            var startTimeArr = appointment.StartTime.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                            var endTimeArr = appointment.EndTime.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);

                            if (startTimeArr.Length == 2 && endTimeArr.Length == 2)
                            {
                                int startHr;
                                int startMin;
                                int endHr;
                                int endMin;

                                if (int.TryParse(startTimeArr[0], out startHr) && int.TryParse(startTimeArr[1], out startMin) && int.TryParse(endTimeArr[0], out endHr) && int.TryParse(endTimeArr[1], out endMin))
                                {
                                    var curTime = new TimeSpan(currTimeHrs, currTimeMinutes, 0);
                                    var startTime = new TimeSpan(startHr, startMin, 0);
                                    var endTime = new TimeSpan(endHr, endMin, 0);

                                    logCSReq.Debug(String.Format("Current Time : {0}, Start Time : {1}, End Time : {2}", curTime.ToString(), startTime.ToString(), endTime.ToString()));

                                    if (!(curTime >= startTime && curTime <= endTime))
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    continue;
                                }
                            }
                            else
                            {
                                logCSReq.Warn("Invalid time format");
                            }

                        }

                        return appointment;
                    }
                    else
                    {
                        logCSReq.Debug("Nothing enabled");
                        continue;
                    }

                }

                return null;

            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}

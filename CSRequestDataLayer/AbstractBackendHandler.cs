﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;
using DuoSoftware.DC.CallServerDBModelsOS;
using GenerateReportDataObjStore.Classes;
using ServiceStack.Messaging.Rcon;

namespace DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer
{
    public class AbstractBackendHandler
    {
        protected log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");
        protected string ObjectStoreIdCall = "";
        protected string ObjectStoreIdCDR = "";

        private static AbstractBackendHandler _instance;

        static string backEndTy = ConfigurationManager.AppSettings.Get("BackendType").ToUpper();

        protected AbstractBackendHandler()
        {
            
        }

        public bool ResetObjectStoreId()
        {
            try
            {
                var storeidCall = ConfigurationManager.AppSettings.Get("ObjectStoreId_Call");

                if (!String.IsNullOrEmpty(storeidCall))
                {
                    _instance.ObjectStoreIdCall = storeidCall;
                }
            }
            catch (Exception)
            {
                return false;
            }

            try
            {
                var storeidCDR = ConfigurationManager.AppSettings.Get("ObjectStoreId_CDR");

                if (!String.IsNullOrEmpty(storeidCDR))
                {
                    _instance.ObjectStoreIdCDR = storeidCDR;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public static AbstractBackendHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    if (backEndTy.Equals("DB"))
                    {
                        _instance = new DbAccessor();
                    }
                    else
                    {
                        _instance = new OSAccessor();

                        try
                        {
                            var storeidCall = ConfigurationManager.AppSettings.Get("ObjectStoreId_Call");

                            if (!String.IsNullOrEmpty(storeidCall))
                            {
                                _instance.ObjectStoreIdCall = storeidCall;
                            }
                        }
                        catch (Exception)
                        {
                            
                        }

                        try
                        {
                            var storeidCDR = ConfigurationManager.AppSettings.Get("ObjectStoreId_CDR");

                            if (!String.IsNullOrEmpty(storeidCDR))
                            {
                                _instance.ObjectStoreIdCDR = storeidCDR;
                            }
                        }
                        catch (Exception)
                        {

                        }

                        
                    }
                }

                return _instance;
            }
        }

        public virtual List<CPProfiles> GetCpProfileForCallServer(int cpId)
        {
            throw new NotImplementedException();
        }

        public virtual List<ConferenceUser> GetConfUserInfo(string roomName, int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        public virtual ConferenceInfo GetActiveConferenceRoomInfo(string roomName, int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        public virtual ConferenceInfo GetConferenceRoomInfoByGuid(string roomGuid)
        {
            throw new NotImplementedException();
        }

        public virtual List<HoldusicInfo> GetHoldMusicConfig(int callServerId)
        {
            throw new NotImplementedException();
        }

        public virtual GroupInfo GetGroupDetails(int callServerId, string groupName, string domain)
        {
            throw new NotImplementedException();
        }

        public virtual ExtDetails GetExtnsionDetailsForDid(string didNum, int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        public virtual ExtDetails GetExtensionDetails(string extension)
        {
            throw new NotImplementedException();
        }
        
        public virtual CSDB_subscriber GetSubDetailsForDomainCompany(string user, string domain, int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        public virtual List<ExtDetails> GetExtensionDetailsList(string extension)
        {
            throw new NotImplementedException();
        }

        public virtual ExtDetails GetExtensionDetails(string extension, int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        public virtual List<ExtDetails> GetExtensionDetailsForName(string extName, int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        public virtual GroupDetails GetGroupDetailsForCompany(string groupName, int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        public virtual GroupDetails GetGroupDetailsForUser(string username, int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        public virtual XElement GetTranslationsFromDB(int transId, int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        public virtual void GetGwFromRule(int companyId, int tenantId, string numToMatch, string aniNumToMatch, out string trunkNum, out string trunkCode, out string trunkDomain, out TrunkClass trClass, out int timeout, out int transId, out int aniTransId, out string ipUrl, out int trunkId, out string targetScript)
        {
            throw new NotImplementedException();
        }
        
        public virtual void GetCompanyForDomain(string domain, out int companyId, out int tenantId)
        {
            throw new NotImplementedException();
        }

        public virtual List<CSDB_subscriber> GetCompanyForDomain(string domain)
        {
            throw new NotImplementedException();
        }

        public virtual void GetCompanyForUserDomain(string username, string domain, out int companyId, out int tenantId)
        {
            throw new NotImplementedException();
        }

        public virtual UserProfileInfo GetDirectoryProfileDetails(int callServerId, string username, string domain)
        {
            throw new NotImplementedException();
        }

        public virtual List<TrunkInfo> GetSipGatewayDetailsForCallServer(int callServerId, string profile = "")
        {
            throw new NotImplementedException();
        }

        public virtual CSDB_CallServerRegMaster GetCallServerInfo(int callServerId)
        {
            throw new NotImplementedException();
        }

        public virtual CallControllerInfo GetSpecificCallController(int ccId)
        {
            throw new NotImplementedException();
        }

        public virtual List<CallControllerInfo> GetAvailableCallControllers(int callServerId)
        {
            throw new NotImplementedException();
        }

        public virtual void UpdateCcReqTimeStamp(int callControllerId)
        {
            throw new NotImplementedException();
        }

        public virtual ContextInfo GetContextCategory(string context)
        {
            throw new NotImplementedException();
        }

        public virtual bool UpdateConferenceRoom(string objGuid, Dictionary<string, object> updateData)
        {
            throw new NotImplementedException();
        }

        public virtual bool UpdateConfPressence(string roomName, string userName, int companyId, int tenantId, Dictionary<string, object> updateData)
        {
            throw new NotImplementedException();
        }

        public virtual bool AddConferenceHistory(int companyId, int tenantId, string confStartTime, string confEndTime, string roomName, string confUuid, string connectedUsrs, string notConnUsers, int viewObjId)
        {
            throw new NotImplementedException();
        }

        public virtual bool DeleteConferenceRoom(string roomName)
        {
            throw new NotImplementedException();
        }

        public virtual bool DeleteConferenceUsers(List<string> objGuid)
        {
            throw new NotImplementedException();
        }

        public virtual PbxConfigurations GetPbxConfigurations(string guUserId, int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        public virtual List<FollowMeConf> GetFollowMeConf(string guUserId, int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        //public virtual EndpointsConf GetEndpointConf(string epGuid, int companyId, int tenantId)
        //{
        //    throw new NotImplementedException();
        //}

        public virtual SubscriberDetails GetSipSubDetails(string username, int companyId, int tenantId, string guUserid = "")
        {
            throw new NotImplementedException();
        }

        public virtual List<PbxFeatureCodes> GetPbxFeatureCodes(int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        #region Cdr Operations

        public virtual bool SaveRawCallCdrInfo(CallCdrInfo cdrRaw)
        {
            throw new NotImplementedException();
        }

        public virtual bool SaveProcessedCdrInfo(CallCdrInfoProcessed cdrProcessed)
        {
            throw new NotImplementedException();
        }

        public virtual bool SaveReportCdrInfo(CallRelatedInfo cdrRep)
        {
            throw new NotImplementedException();
        }
        
        #endregion

        #region CallRule

        public virtual List<CSDB_CallRule> GetActiveCallRuleDetailsByAniDnisTrunk(string ani, string dnis, int direction, List<int> trunkIds = null)
        {
            throw new NotImplementedException();
        }

        public virtual CSDB_CallRule GetActiveCallRuleDetailsByAniDnisTrunkCompany(string ani, string dnis, int direction, int trunkId, int companyId , int tenantId)
        {
            throw new NotImplementedException();
        }

        #endregion


        #region Trunk

        public virtual List<CSDB_TrunkServerRegistration> GetTrunkListForServer(int callServerId)
        {
            throw new NotImplementedException();
        }

        public virtual CSDB_TrunkServerRegistration GetServerForTrunk(int trunkId)
        {
            throw new NotImplementedException();
        }

        public virtual CSDB_TrunkServerRegistration GetServerForTrunk(int trunkId, int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        public virtual bool CheckTrunkServerConnection(int callServerId, int trunkId)
        {
            throw new NotImplementedException();
        }

        #endregion

        public virtual List<CSDB_TrunkRegMaster> GetActiveTrunkList(List<int> trunkIds = null)
        {
            throw new NotImplementedException();
        }

        public virtual CSDB_TrunkRegMaster GetActiveTrunkByTrId(int trunkId)
        {
            throw new NotImplementedException();
        }

        public virtual List<CSDB_TrunkPhoneNumbers> GetActiveTrunkPhoneNumbers(string phoneNum)
        {
            throw new NotImplementedException();
        }

        public virtual CSDB_subscriber GetSubscriberByUserAndDomain(string username, string domain, bool checkDomain)
        {
            throw new NotImplementedException();
        }

        public virtual CSDB_UsrExtensions GetExtensionByGuUserIdAndCompanyAndExt(int companyId, int tenantId, string guUserId, string extension)
        {
            throw new NotImplementedException();
        }


        public virtual List<CSDB_TrunkPhoneNumbers> GetTrunkPhoneNumbersForCompany(int companyId, int tenantId, string trunkNum)
        {
            throw new NotImplementedException();
        }

        public virtual CSDB_TrunkRegMaster GetActiveTrunkByCode(string trunkCode, List<int> trunkIds)
        {
            throw new NotImplementedException();
        }

        public virtual CSDB_subscriber GetSubscriberForGuUserId(string guUserId, int company, int tenant)
        {
            throw new NotImplementedException();
        }

        public virtual CSDB_UsrExtensions GetExtensionByGuUserIdAndCompany(int companyId, int tenantId, string guUserId)
        {
            throw new NotImplementedException();
        }

        #region Context

        public virtual List<CSDB_Context> GetContextDetailsForCompany(int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        #endregion


        #region Network Profile

        public virtual List<CSDB_CSProfiles> GetNetworkProfilesForCompany(int companyId, int tenantId)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region CSSchedule

        public virtual List<CSDB_CSSchedule> GetAppointmentsForSchedule(string scheduleId)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region PABX Conf

        public virtual CSDB_PBXConfiguration GetPbxConfigurations(string guUserId)
        {
            throw new NotImplementedException();
        }

        public virtual bool UpdatePbxConfigurations(CSDB_PBXConfiguration pbx)
        {
            throw new NotImplementedException();
        }

        public virtual CSDB_Forwarding GetFwdRule(string reason, string guUserId)
        {
            throw new NotImplementedException();
        }

        public virtual CSDB_Forwarding GetFwdRule(DisconnectReason reason, string guUserId)
        {
            throw new NotImplementedException();
        }

        public virtual bool UpdateFwdRule(CSDB_Forwarding fwdRule)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

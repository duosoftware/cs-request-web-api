﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer
{
    public static class FileHandler
    {
        static log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public static string ReadFileText(string filePath)
        {
            try
            {
                logCSReq.Debug("Accessing file at : " + filePath);

                if (File.Exists(filePath))
                {
                    using (var sr = new StreamReader(filePath))
                    {
                        var fileContent = sr.ReadToEnd();

                        return fileContent;
                    }
                }
                else
                {
                    logCSReq.Warn("File Not Found");
                    return "";
                }
                
            }
            catch (Exception ex)
            {
                logCSReq.Error("Exception occurred while reading file : ", ex);
                return "";
            }
            
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Redis;

namespace DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer
{
    public class RedisHandler
    {
        private static RedisHandler _instance;

        private static ServiceStack.Redis.PooledRedisClientManager _cliManager;

        private RedisHandler()
        {
            var redisEp = ConfigurationManager.AppSettings.Get("redisip");
            _cliManager = new PooledRedisClientManager(redisEp);
        }

        public static RedisHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RedisHandler();
                    
                }
                return _instance;
            }
        }

        public Dictionary<string, string> GetHashValues(string hashId)
        {
            try
            {
                using (var client = _cliManager.GetClient())
                {
                    var hash = client.GetAllEntriesFromHash(hashId);

                    if (hash != null)
                    {
                        return hash;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool AddToHash(string hashId, string key, string value)
        {
            try
            {
                
                using (var client = _cliManager.GetClient())
                {
                    var result = client.SetEntryInHash(hashId, key, value);

                    return result;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool SetObjOnRedisCache(string key, string jsonString)
        {
            try
            {
                using (var client = _cliManager.GetClient())
                {
                    var result = client.Set(key, jsonString);

                    return result;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool AddToRedisCache(string key, string jsonString)
        {
            try
            {
                using (var client = _cliManager.GetClient())
                {
                    var result = client.Add(key, jsonString);
                    
                    return result;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool AddToRedisCache<T>(string key, T obj, TimeSpan timeout)
        {
            try
            {
                using (var client = _cliManager.GetClient())
                {
                    var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(obj);

                    var result = client.Add<T>(key, obj, timeout);

                    return result;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}

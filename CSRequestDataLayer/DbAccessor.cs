﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;
using DuoSoftware.DC.CallServerDBModels;

namespace DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer
{
    internal class DbAccessor : AbstractBackendHandler
    {
        public override List<HoldusicInfo> GetHoldMusicConfig(int callServerId)
        {
            try
            {
                using (var csdbe = new CSDBEntities())
                {
                    var cs = csdbe.CSDB_CallServerRegMasters.FirstOrDefault(i => i.CSKey == callServerId && i.RecordStatus == 1 && i.OperationalStatus.Equals("Active"));

                    if (cs != null)
                    {
                        var hmInfo = (from j in csdbe.CSDB_CallServerHoldMusics
                                     where j.ClusterID == cs.Zone
                                     select new HoldusicInfo
                                         {
                                             HoldMusicName = j.HMName,
                                             HoldMusicPath = j.StreamPath
                                         }).ToList();

                        return hmInfo;
                    }
                    else
                    {
                        throw new Exception("Invalid call server");
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public override GroupDetails GetGroupDetailsForCompany(string groupName, int companyId, int tenantId)
        {
            try
            {
                using (var csdbe = new CSDBEntities())
                {
                    var grp = csdbe.CSDB_Groups.FirstOrDefault(i => i.GroupName == groupName && i.CompanyID == companyId && i.TenantID == tenantId);

                    if (grp != null)
                    {
                        return new GroupDetails
                        {
                            CompanyID = grp.CompanyID,
                            Domain = grp.Domain,
                            GroupDescription = grp.GroupDescription,
                            GroupName = grp.GroupName,
                            TenantID = grp.TenantID
                        };
                    }
                    else
                    {
                        logCSReq.Warn("Extension not found");
                        return null;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public override GroupInfo GetGroupDetails(int callServerId, string groupName, string domain)
        {
            try
            {
                using (var oe = new GenericDBModelOpensips())
                {
                    using (var csdbe = new CSDBEntities())
                    {
                        var dbCheckCs = (from i in csdbe.CSDB_CallServerRegMasters
                                         where i.CSKey == callServerId && (CSStatus) i.CSStatus == CSStatus.Activated && i.RecordStatus == 1 && i.OperationalStatus.ToUpper() == "ACTIVE"
                                         select new {CompanyId = i.AccountNo, TenantId = i.Tenant}).FirstOrDefault();

                        if (dbCheckCs != null)
                        {
                            //TODO: Check CS Belongs to Zone - if yes only username matching has to be done - no Comp, tenant matching
                            logCSReq.Info(String.Format("Call server is a valid call server - CallServerId : {0}, CompanyID : {1}, TenantID : {2}", callServerId, dbCheckCs.CompanyId, dbCheckCs.TenantId));


                            var grps = from i in csdbe.CSDB_Groups
                                       where i.GroupName.Equals(groupName)
                                       select i;

                            if (bool.Parse(ConfigurationManager.AppSettings.Get("UseDomain")))
                            {
                                grps = grps.Where(j => j.Domain == domain);
                            }

                            var grpRec = grps.FirstOrDefault();

                            if (grpRec != null)
                            {

                                var dbUsrs = (from j in csdbe.CSDB_UsrGroups
                                              where j.GrpCompanyID == dbCheckCs.CompanyId && j.GrpTenantID == dbCheckCs.TenantId && j.GroupName.Equals(groupName)
                                              select j.Username).ToList();

                                var usrList = new List<UserProfileInfo>();

                                foreach (var usr in dbUsrs)
                                {
                                    var usr1 = usr;
                                    var tempDbUsrDetails = from i in oe.Subscribers
                                                           where i.Username == usr1
                                                           select new { Username = i.Username, Password = i.Password, Domain = i.Domain, CompanyID = i.CompanyID, TenantID = i.TenantID };

                                    if (bool.Parse(ConfigurationManager.AppSettings.Get("UseDomain")))
                                    {
                                        tempDbUsrDetails = tempDbUsrDetails.Where(j => j.Domain == domain);
                                    }

                                    var dbUsrDetails = tempDbUsrDetails.FirstOrDefault();


                                    if (dbUsrDetails != null)
                                    {
                                        var ext = csdbe.CSDB_UsrExtensions.FirstOrDefault(j => j.Name == usr && j.TenantID == dbUsrDetails.TenantID && j.CompanyID == dbUsrDetails.CompanyID && (ExtType)j.Type == ExtType.User && j.Active);

                                        if (ext != null)
                                        {
                                            logCSReq.Info("User profile found from db");
                                            var usrProfile = new UserProfileInfo
                                            {
                                                Username = dbUsrDetails.Username,
                                                Password = dbUsrDetails.Password,
                                                Domain = domain,
                                                Context = ext.Context,
                                                Extension = ext.Extension
                                            };

                                            usrList.Add(usrProfile);
                                        }
                                        else
                                        {
                                            throw new Exception("Extension data not found for user");
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception("No user found on db");
                                    }
                                }

                                if (dbUsrs.Any())
                                {
                                    logCSReq.Info(String.Format("Group : {0} found with users", groupName));
                                    var grp = new GroupInfo
                                                  {
                                                      GroupName = groupName,
                                                      Domain = grpRec.Domain,
                                                      Users = usrList
                                                  };

                                    return grp;
                                }
                                else
                                {
                                    throw new Exception("No user found on db for group or group does not exist");
                                }
                            }
                            else
                            {
                                throw new Exception("No group found with the given group name");
                            }
                        }
                        else
                        {
                            throw new Exception("Invalid call server or callserver is not private or call server is not activated");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override List<ConferenceUser> GetConfUserInfo(string roomName, int companyId, int tenantId)
        {
            try
            {
                using (var csdbe = new CSDBEntities())
                {
                    var conf = (from i in csdbe.CSDB_ConferencePresences
                               where i.RoomName == roomName && i.CompanyID == companyId && i.TenantID == tenantId
                               select new ConferenceUser
                                   {
                                       CompanyId = i.CompanyID,
                                       RoomName = i.RoomName,
                                       TenantId = i.TenantID,
                                       Endpoint = i.Destination,
                                       Crn = i.CRN,
                                       CurrentDeafFlag = i.CurrentDeafFlag,
                                       CurrentModFlag = i.CurrentModFlag,
                                       CurrentMuteFlag = i.CurrentMuteFlag,
                                       UserPin = i.UserPinID,
                                       UserName = i.UserName,
                                       Status = (ConferenceUserStatus)i.Status,
                                       JoinType = (ConferenceJoinType)i.JoinType,
                                       Protocol = (ConferenceEndpointProtocol)i.Protocol,
                                   }).ToList();

                    return conf;

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override ExtDetails GetExtensionDetails(string extension)
        {
            try
            {
                using (var csdbe = new CSDBEntities())
                {
                    var ext = csdbe.CSDB_UsrExtensions.FirstOrDefault(i => i.Extension.Equals(extension) && i.Active);

                    if (ext != null)
                    {
                        return new ExtDetails
                            {
                                Ext = ext.Extension,
                                ExtName = ext.Name,
                                ExtType = (ExtType)ext.Type,
                                CompanyId = ext.CompanyID,
                                TenantId = ext.TenantID,
                                Context = ext.Context
                            };
                    }
                    else
                    {
                        logCSReq.Warn("Extension not found");
                        return null;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public override ConferenceInfo GetActiveConferenceRoomInfo(string roomName, int companyId, int tenantId)
        {
            try
            {
                using (var csdbe = new CSDBEntities())
                {
                    var conf = csdbe.CSDB_ConferenceMasters.FirstOrDefault(i => i.ConfRoomName == roomName && i.CompanyID == companyId && i.TenantID == tenantId && i.Active == true && i.StartTime < DateTime.Now && i.EndTime > DateTime.Now);

                    if (conf != null)
                    {
                        return new ConferenceInfo
                        {
                            OperationalStatus = "Active",
                            CompanyId = conf.CompanyID,
                            TenantId = conf.TenantID,
                            RoomName = conf.ConfRoomName,
                            Pin = conf.Pin,
                            ConferenceType = conf.ConfType
                        };
                    }
                    else
                    {
                        logCSReq.Warn("Conference room not found");
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SubscriberDetails GetSipSubDetails(string username, int companyId, int tenantId, string guUserId = "")
        {
            try
            {
                using (var osdbe = new GenericDBModelOpensips())
                {
                    var sub = osdbe.Subscribers.FirstOrDefault(i => i.Username == username && i.CompanyID == companyId && i.TenantID == tenantId);

                    if (sub != null)
                    {
                        return new SubscriberDetails
                        {
                            Username = sub.Username,
                            Domain = sub.Domain,
                            Password = sub.Password,
                            TenantId = sub.TenantID.GetValueOrDefault(),
                            CompanyId = sub.CompanyID.GetValueOrDefault(),
                            VoicemailActive = sub.VoicemailActive,
                            GuUserId = sub.GuUserId
                        };
                    }
                    else
                    {
                        logCSReq.Warn("Subscriber not found");
                        return null;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public override List<ExtDetails> GetExtensionDetailsForName(string extName, int companyId, int tenantId)
        {
            try
            {
                using (var csdbe = new CSDBEntities())
                {
                    var ext = (from i in csdbe.CSDB_UsrExtensions
                              where i.Name == extName && i.CompanyID == companyId && i.TenantID == tenantId
                              select new ExtDetails
                                  {
                                      Ext = i.Extension,
                                      ExtName = i.Name,
                                      ExtType = (ExtType) i.Type,
                                      CompanyId = i.CompanyID,
                                      TenantId = i.TenantID,
                                      Context = i.Context
                                  }).ToList();

                    return ext;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public override ExtDetails GetExtensionDetails(string extension, int companyId, int tenantId)
        {
            try
            {
                using (var csdbe = new CSDBEntities())
                {
                    var ext = csdbe.CSDB_UsrExtensions.FirstOrDefault(i => i.Extension.Equals(extension) && i.CompanyID == companyId && i.TenantID == tenantId && i.Active);

                    if (ext != null)
                    {
                        return new ExtDetails
                        {
                            Ext = ext.Extension,
                            ExtName = ext.Name,
                            ExtType = (ExtType)ext.Type,
                            CompanyId = ext.CompanyID,
                            TenantId = ext.TenantID,
                            Context = ext.Context
                        };
                    }
                    else
                    {
                        logCSReq.Warn("Extension not found");
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public override void GetCompanyForUserDomain(string username, string domain, out int companyId, out int tenantId)
        {
            try
            {
                using (var oe = new GenericDBModelOpensips())
                {
                    var compDetails = oe.Subscribers.FirstOrDefault(i => i.Domain.Equals(domain) && i.Username.Equals(username));

                    if (compDetails != null)
                    {
                        if (compDetails.CompanyID != null && compDetails.TenantID != null)
                        {
                            companyId = (int)compDetails.CompanyID;
                            tenantId = (int)compDetails.TenantID;
                        }
                        else
                        {
                            throw new Exception("Cannot find a valid company or tenant - null");
                        }
                    }
                    else
                    {
                        throw new Exception("Cannot find a company for the given domain and user");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override void GetCompanyForDomain(string domain, out int companyId, out int tenantId)
        {
            try
            {
                using (var oe = new GenericDBModelOpensips())
                {
                    var compDetails = oe.Subscribers.FirstOrDefault(i => i.Domain.Equals(domain));

                    if (compDetails != null)
                    {
                        if (compDetails.CompanyID != null && compDetails.TenantID != null)
                        {
                            companyId = (int)compDetails.CompanyID;
                            tenantId = (int) compDetails.TenantID;
                        }
                        else
                        {
                            throw new Exception("Cannot find a valid company or tenant - null");
                        }
                    }
                    else
                    {
                        throw new Exception("Cannot find a company for the given domain");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override List<CPProfiles> GetCpProfileForCallServer(int cpId)
        {
            try
            {
                using (var csdbe = new CSDBEntities())
                {
                    var profList = (from i in csdbe.CSDB_CSProfiles
                                    where i.CPID == cpId
                                    select new CPProfiles
                                    {
                                       CPID = i.CPID,
                                       TenantID = i.TenantID,
                                       CompanyID = i.CompanyID,
                                       ClusterID = i.ClusterID,
                                       Port = i.Port,
                                       ProfileExtIp = i.ProfileExtIp,
                                       ProfileIntIp = i.ProfileIntIp,
                                       ProfileName = i.ProfileName,
                                       ProfileType = (ProfileType) i.ProfileType
                                    }).ToList();

                    return profList;

                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public override UserProfileInfo GetDirectoryProfileDetails(int callServerId, string username, string domain)
        {
            try
            {
                using (var csdbe = new CSDBEntities())
                {
                    using (var oe = new GenericDBModelOpensips())
                    {
                        
                        var dbCheckCs = (from i in csdbe.CSDB_CallServerRegMasters
                                         where i.CSKey == callServerId && (CSStatus) i.CSStatus == CSStatus.Activated && i.RecordStatus == 1 && i.OperationalStatus.ToUpper() == "ACTIVE"
                                         select new {CompanyId = i.AccountNo, TenantId = i.Tenant}).FirstOrDefault();

                        if (dbCheckCs != null)
                        {
                            
                            logCSReq.Info(String.Format("Call server is a valid call server - CallServerId : {0}, CompanyID : {1}, TenantID : {2}", callServerId, dbCheckCs.CompanyId, dbCheckCs.TenantId));

                            //var dbUsrDetails = (from i in oe.Subscribers
                            //                    where i.Domain == domain && i.Username == username
                            //                    select new {Username = i.Username, Password = i.Password, Domain = i.Domain, CompanyID = i.CompanyID, TenantID = i.TenantID}).FirstOrDefault();

                            //var dbUsrDetails = (from i in oe.Subscribers
                            //                    where i.Username == username
                            //                    select new { Username = i.Username, Password = i.Password, Domain = i.Domain, CompanyID = i.CompanyID, TenantID = i.TenantID }).FirstOrDefault();


                            var tempDbUsrDetails = from i in oe.Subscribers
                                                   where i.Username == username
                                                   select new {Username = i.Username, Password = i.Password, Domain = i.Domain, CompanyID = i.CompanyID, TenantID = i.TenantID, Email = i.Email_address};

                            if (bool.Parse(ConfigurationManager.AppSettings.Get("UseDomain")))
                            {
                                tempDbUsrDetails = tempDbUsrDetails.Where(j => j.Domain == domain);
                            }

                            var dbUsrDetails = tempDbUsrDetails.FirstOrDefault();


                            if (dbUsrDetails != null)
                            {
                                var ext = csdbe.CSDB_UsrExtensions.FirstOrDefault(j => j.Name == username && j.TenantID == dbUsrDetails.TenantID && j.CompanyID == dbUsrDetails.CompanyID && (ExtType)j.Type == ExtType.User && j.Active);

                                if (ext != null)
                                {
                                    logCSReq.Info("User profile found from db");
                                    var usrProfile = new UserProfileInfo
                                                         {
                                                             Username = dbUsrDetails.Username,
                                                             Password = dbUsrDetails.Password,
                                                             Domain = domain,
                                                             Context = ext.Context,
                                                             Extension = ext.Extension,
                                                             EmailAddr = dbUsrDetails.Email
                                                         };

                                    return usrProfile;
                                }
                                else
                                {
                                    throw new Exception("Extension data not found for user");
                                }
                            }
                            else
                            {
                                throw new Exception("No user found on db");
                            }
                        }
                        else
                        {
                            throw new Exception("Invalid call server or callserver is not private or call server is not activated");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override List<TrunkInfo> GetSipGatewayDetailsForCallServer(int callServerId, string profile = "")
        {
            try
            {
                using (var csdbe = new CSDBEntities())
                {
                    var dbCheckCs = (from i in csdbe.CSDB_CallServerRegMasters
                                    where i.CSKey == callServerId && (CSStatus)i.CSStatus == CSStatus.Activated && i.RecordStatus == 1 && i.OperationalStatus.ToUpper() == "ACTIVE"
                                    select new {CompanyId = i.AccountNo, TenantId = i.Tenant}).FirstOrDefault();

                    if (dbCheckCs != null)
                    {
                        //var result = from k in csdbe.CSDB_TrunkServerRegistration.AsEnumerable()
                        //             where k.ZoneOrCSID == callServerId && (TrunkConnection) k.Type == TrunkConnection.Private && ((RegistrationDirection) k.CSDB_TrunkRegMaster.RegDirection == RegistrationDirection.None || (RegistrationDirection) k.CSDB_TrunkRegMaster.RegDirection == RegistrationDirection.RegisterOut) && k.CSDB_TrunkRegMaster.RecordStatus == 1
                        //             select k.CSDB_TrunkRegMaster;

                        var result = from k in csdbe.CSDB_TrunkServerRegistrations
                                     join i in csdbe.CSDB_CallServerRegMasters on k.ZoneOrCSID equals i.CSKey
                                     where k.CSDB_TrunkRegMaster.Profile.Equals(profile) && k.ZoneOrCSID == callServerId && (TrunkConnection)k.Type == TrunkConnection.Private && ((RegistrationDirection)k.CSDB_TrunkRegMaster.RegDirection == RegistrationDirection.None || (RegistrationDirection)k.CSDB_TrunkRegMaster.RegDirection == RegistrationDirection.RegisterOut) && k.CSDB_TrunkRegMaster.RecordStatus == 1 && k.CSDB_TrunkRegMaster.OperationalStatus.ToUpper() == "ACTIVE" && (TrunkClass)k.CSDB_TrunkRegMaster.TrunkClass == TrunkClass.SIP
                                     select k.CSDB_TrunkRegMaster;


                        return result.Select(rec => new TrunkInfo
                                                        {
                                                            Domain = rec.Domain,
                                                            IpUrl = rec.IPUrl,
                                                            Password = rec.Password,
                                                            RegDir = (RegistrationDirection) rec.RegDirection,
                                                            TrunkCode = rec.TrunkCode,
                                                            Username = rec.Username,
                                                            ExpireSeconds = rec.ExpireSeconds,
                                                            RetrySeconds = rec.RetrySeconds,
                                                            PingSeconds = rec.PingSeconds
                                                        }).ToList();
                    }
                    else
                    {
                        throw new Exception("Invalid call server or call server not activated");
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override CallControllerInfo GetSpecificCallController(int ccId)
        {
            try
            {
                using(var csdbe = new CSDBEntities())
                {
                    var cc = (from i in csdbe.CSDB_CallControllerMasters
                              join j in csdbe.CSDB_CCPresences on i.CCID equals j.CCID
                              where i.CCID == ccId && i.Active && j.Availability
                              select new CallControllerInfo
                                         {
                                             CallControllerId = i.CCID,
                                             TcpIp = i.CCTcpIp,
                                             TcpPort = i.CCTcpPort
                                         }).FirstOrDefault();

                    return cc;

                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public override List<CallControllerInfo> GetAvailableCallControllers(int callServerId)
        {
            try
            {
                using (var csdbe = new CSDBEntities())
                {

                    var callCtrls = from i in csdbe.CSDB_CallServerRegMasters
                                    join k in csdbe.CSDB_CallControllerMasters on i.Zone equals k.ClusterID
                                    join l in csdbe.CSDB_CCPresences on k.CCID equals l.CCID
                                    where k.MaxCapacity > l.CurrentCapacity && l.Availability && l.State == 1 && i.CSKey == callServerId && i.RecordStatus == 1 && i.OperationalStatus.ToUpper() == "ACTIVE" && (CSStatus)i.CSStatus == CSStatus.Activated && k.Active
                                    select new
                                               {
                                                   CCID = k.CCID,
                                                   TcpIp = k.CCTcpIp,
                                                   TcpPort = k.CCTcpPort,
                                                   MaxCapacity = k.MaxCapacity,
                                                   CurrentCapacity = l.CurrentCapacity,
                                                   CurrentState = l.State,
                                                   LastReqTimeStamp = l.RequestTimeStamp
                                               };


                    return callCtrls.Select(rec => new CallControllerInfo
                                                       {
                                                           CallControllerId = rec.CCID,
                                                           CurrentCapacity = rec.CurrentCapacity,
                                                           CurrentState = rec.CurrentState,
                                                           TcpIp = rec.TcpIp,
                                                           MaxCapacity = rec.MaxCapacity,
                                                           TcpPort = rec.TcpPort,
                                                           LastReqTimeStamp = rec.LastReqTimeStamp
                                                       }).ToList();

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override void UpdateCcReqTimeStamp(int callControllerId)
        {
            try
            {
                using(var csdbe = new CSDBEntities())
                {
                    var ccPresence = (from i in csdbe.CSDB_CCPresences
                                     join j in csdbe.CSDB_CallControllerMasters on i.CCID equals j.CCID
                                     where i.CCID == callControllerId
                                     select i).FirstOrDefault();

                    if(ccPresence != null)
                    {
                        ccPresence.RequestTimeStamp = DateTime.Now;

                        csdbe.SaveChanges();
                    }
                    else
                    {
                        logCSReq.Warn(String.Format("Unable to find a call controller : {0} with given id to update time stamp", callControllerId));
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override void GetGwFromRule(int companyId, int tenantId, string numToMatch, string aniNumToMatch, out string trunkNum, out string trunkCode, out string trunkDomain, out TrunkClass trClass, out int timeout, out int transId, out int aniTransId, out string ipUrl, out int trunkId, out string targetScript)
        {
            try
            {
                using (var csdbe = new CSDBEntities())
                {
                    var outrulelist = (from i in csdbe.CSDB_CallRules
                                       where i.AccountNo == companyId && i.Tenant == tenantId && i.RoutingDirection == 2 && i.CallType == 2
                                       select i).ToList();

                    var orderedList = from j in outrulelist
                                      orderby j.Priority
                                      select j;


                    var rule = orderedList.FirstOrDefault(j => (Regex.IsMatch(numToMatch, j.DNISRegEx)));

                    if (rule != null)
                    {
                        logCSReq.Debug(String.Format("Call rule picked (ID) : {0}", rule.ID));
                        var trunk = csdbe.CSDB_TrunkRegMasters.FirstOrDefault(i => i.TrunkKey == rule.TrunkID);

                        if (trunk != null)
                        {
                            trunkCode = trunk.TrunkCode;
                            trunkNum = rule.TrunkNumber;
                            trunkDomain = trunk.Domain;
                            trClass = (TrunkClass) trunk.TrunkClass;
                            timeout = rule.Timeout;
                            transId = rule.TranslationID;
                            aniTransId = rule.ANITranslationID;
                            ipUrl = trunk.IPUrl;
                            trunkId = trunk.TrunkKey;
                            targetScript = rule.TargetScript;

                            logCSReq.Debug(String.Format("Gateway picked - TrunkCode : {0}, TrunkNumber : {1}, TrunkDomain : {2}, TrunkClass : {3}, Timeout : {4}", trunkCode, trunkNum, trunkDomain, trClass, timeout));
                        }
                        else
                        {
                            throw new Exception("trunk not found");
                        }
                    }
                    else
                    {
                        throw  new Exception("No attemdant gw transfer rule found");
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override GroupDetails GetGroupDetailsForUser(string username, int companyId, int tenantId)
        {
            try
            {
                using (var csdbe = new CSDBEntities())
                {
                    var usrgrp = csdbe.CSDB_UsrGroups.FirstOrDefault(i => i.Username == username && i.GrpCompanyID == companyId && i.GrpTenantID == tenantId);

                    if (usrgrp != null)
                    {
                        var grp = csdbe.CSDB_Groups.FirstOrDefault(i => i.GroupName == usrgrp.GroupName && i.CompanyID == companyId && i.TenantID == tenantId);

                        if (grp != null)
                        {
                            return new GroupDetails
                                {
                                    CompanyID = grp.CompanyID,
                                    Domain = grp.Domain,
                                    GroupDescription = grp.GroupDescription,
                                    GroupName = grp.GroupName,
                                    TenantID = grp.TenantID
                                };
                        }
                        else
                        {
                            logCSReq.Info("Group doesnt exist");
                            return null;
                        }
                    }
                    else
                    {
                        logCSReq.Info("User doesnt belong to a group");
                        return null;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public override ContextInfo GetContextCategory(string context)
        {
            try
            {
                logCSReq.Info("Getting context category");
                using (var csdbe = new CSDBEntities())
                {
                    var ctxt = csdbe.CSDB_Contexts.FirstOrDefault(i => i.ContextName == context);

                    if (ctxt != null)
                    {
                        logCSReq.Info(String.Format("Context found - category {0}", (ContextCat)ctxt.ContextCat));
                        return new ContextInfo
                        {
                            CompanyId = ctxt.CompanyID.GetValueOrDefault(),
                            TenantId = ctxt.TenantID.GetValueOrDefault(),
                            ContextCat = (ContextCat)ctxt.ContextCat,
                            ContextName = ctxt.ContextName,
                            ViewObjId = ctxt.ViewObjectID.GetValueOrDefault()
                        };
                    }
                    else
                    {
                        logCSReq.Info("No context found - returning null");
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                logCSReq.Error("Exception occurred while getting context category - context returned as Public", ex);
                return null;
            }
        }

        public override PbxConfigurations GetPbxConfigurations(string username, int companyId, int tenantId)
        {
            try
            {
                logCSReq.Info("Getting pbx configurations....");
                using (var csdbe = new CSDBEntities())
                {
                    var pbxConf = csdbe.CSDB_PBXConfigurations.FirstOrDefault(i => i.UserID == username && i.CompanyID == companyId && i.TenantID == tenantId);

                    if (pbxConf != null)
                    {
                        return new PbxConfigurations
                            {
                                ActiveApp = (ActApp) pbxConf.ActiveApp,
                                CompanyId = pbxConf.CompanyID,
                                TenantId = pbxConf.TenantID,
                                UserId = pbxConf.UserID,
                                UsrStatus = (PBXUsrStatus) pbxConf.Status,
                                DefaultFwdEp = pbxConf.DefaultFwdEndpoint,
                                UOMVal = pbxConf.UOMValue
                            };
                    }
                    else
                    {
                        logCSReq.Info("No context found - returning null");
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                logCSReq.Error("Exception occurred while getting context category - context returned as Public", ex);
                return null;
            }
        }

        public override List<FollowMeConf> GetFollowMeConf(string guUserId, int companyId, int tenantId)
        {
            try
            {
                logCSReq.Info("Getting follow me configurations....");
                using (var csdbe = new CSDBEntities())
                {
                    var followMeConf = (from i in csdbe.CSDB_FollowMeConfs
                                       where i.Extension == guUserId && i.CompanyID == companyId && i.TenantID == tenantId
                                       orderby i.Priority ascending
                                       select new FollowMeConf
                                           {
                                               CompanyId = i.CompanyID,
                                               TenantId = i.TenantID,
                                               EpType = i.FmEpType,
                                               UsrProfile = i.Endpoints,
                                               Priority = i.Priority,
                                               UomVal = i.UOMValue,
                                               UsrExtension = i.Extension
                                           }).ToList();

                    return followMeConf;
                }
            }
            catch (Exception ex)
            {
                logCSReq.Error("Exception occurred while getting context category - context returned as Public", ex);
                return null;
            }
        }

        //public override EndpointsConf GetEndpointConf(string epGuid, int companyId, int tenantId)
        //{
        //    try
        //    {
        //        logCSReq.Info("Getting endpoint configurations....");
        //        using (var csdbe = new CSDBEntities())
        //        {
        //            var epConf = csdbe.CSDB_EndpointsConfs.FirstOrDefault(i => i.ObjGuid == epGuid && i.CompanyID == companyId && i.TenantID == tenantId);

        //            if (epConf != null)
        //            {
        //                return new EndpointsConf
        //                    {
        //                        Destination = epConf.Destination,
        //                        Domain = epConf.Domain,
        //                        EpProtocol = (Protocol) Enum.Parse(typeof (Protocol), epConf.Protocol),
        //                        ObjGuid = epConf.ObjGuid,
        //                        Profile = epConf.Profile,
        //                        EpType = (DnisType)Enum.Parse(typeof(DnisType), epConf.EndpointType),
        //                        CompanyId = epConf.CompanyID,
        //                        TenantId = epConf.TenantID
        //                    };
        //            }

        //            return null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        logCSReq.Error("Exception occurred while getting endpoint configuration", ex);
        //        return null;
        //    }
        //}

        public override List<PbxFeatureCodes> GetPbxFeatureCodes(int companyId, int tenantId)
        {
            try
            {
                logCSReq.Info("Getting pbx feature codes....");

                using (var csdbe = new CSDBEntities())
                {
                    var featureConf = (from i in csdbe.CSDB_PbxFeatureCodes
                                      where i.CompanyID == companyId && i.TenantID == tenantId
                                      orderby i.Priority ascending
                                      select new PbxFeatureCodes
                                        {
                                            CompanyID = i.CompanyID,
                                            TenantID = i.TenantID,
                                            ObjGuid = i.ObjGuid,
                                            FeatureCode = i.FeatureCode,
                                            FeatureType = (PbxFeatureCodeType)Enum.Parse(typeof(PbxFeatureCodeType), i.FeatureType)
                                        }).ToList();

                    return featureConf;
                }
            }
            catch (Exception ex)
            {
                logCSReq.Error("Exception occurred while getting pbx feature code configuration", ex);
                return null;
            }
        }

        #region Cdr Operations

        public override bool SaveRawCallCdrInfo(CallCdrInfo cdrRaw)
        {
            throw new NotImplementedException();
        }

        public override bool SaveProcessedCdrInfo(CallCdrInfoProcessed cdrProcessed)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

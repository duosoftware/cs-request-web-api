﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;
using DuoSoftware.DC.CallServerDBModelsOS;

namespace DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer
{
    public static class CSReqWebApiDataOperator
    {
        static log4net.ILog logCSReq = log4net.LogManager.GetLogger("CSReqServiceLogger");

        public static int GetTrunkNumberLimit(string context, string trunkCode, string trunkNumber, out int companyId)
        {
            try
            {
                var ctxt = AbstractBackendHandler.Instance.GetContextCategory(context);

                if (ctxt != null)
                {
                    var trunkNumList = AbstractBackendHandler.Instance.GetTrunkPhoneNumbersForCompany(ctxt.CompanyId, ctxt.TenantId, trunkNumber);

                    if (trunkNumList.Count > 0)
                    {
                        var trunkIds = trunkNumList.Select(i => i.TrunkID).ToList();

                        var trunk = AbstractBackendHandler.Instance.GetActiveTrunkByCode(trunkCode, trunkIds);

                        if (trunk != null)
                        {
                            var concurrency = trunkNumList.FirstOrDefault(i => i.TrunkID == trunk.TrunkKey);

                            if (concurrency != null)
                            {
                                companyId = ctxt.CompanyId;
                                return concurrency.NoConcDials;
                            }
                            else
                            {
                                logCSReq.Warn("No trunk number concurrency data found");
                            }
                        }
                        else
                        {
                            logCSReq.Warn("No trunk found with given code");
                        }

                    }
                    else
                    {
                        logCSReq.Warn("No trunk numbers found for given company and number");
                    }
                    
                }
                else
                {
                    throw new Exception("Context not found");
                }

                companyId = -1;
                return -1;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static ExtDetails GetExtensionDetailsForCompanyGuUserId(string guUserId, int companyId, int tenantId)
        {
            try
            {
                var result = AbstractBackendHandler.Instance.GetExtensionByGuUserIdAndCompany(companyId, tenantId, guUserId);

                if (result != null)
                {
                    return new ExtDetails
                    {
                        CompanyId = result.CompanyID,
                        GuUserId = result.GuUserId,
                        TenantId = result.TenantID,
                        Context = result.Context,
                        Ext = result.Extension,
                        ExtName = result.ExtName,
                        ExtType = (ExtType)result.ExtType,
                        DidNumber = result.DidNumber ?? "",
                        DidEnabled = result.DidActive,
                        DodNumber = result.DodNumber ?? "",
                        DodEnabled = result.DodActive,
                        ExtParams = result.ExtraData
                    };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static bool SetForwardOptions(string fwdType, string number, bool activate, string guUserId, int companyId, int tenantId)
        {
            try
            {
                if (fwdType.Equals("Default"))
                {
                    var pbx = AbstractBackendHandler.Instance.GetPbxConfigurations(guUserId);

                    if (pbx != null)
                    {
                        if (activate)
                        {
                            pbx.Status = PBXUsrStatus.Forward.ToString();
                        }
                        else
                        {
                            pbx.Status = PBXUsrStatus.Available.ToString();
                        }

                        if (!String.IsNullOrEmpty(number))
                        {
                            pbx.DefaultFwdEndpoint = number;
                        }
                        

                        AbstractBackendHandler.Instance.UpdatePbxConfigurations(pbx);

                    }
                }
                else if (fwdType.Equals("Busy"))
                {
                    var fwdRule = AbstractBackendHandler.Instance.GetFwdRule(DisconnectReason.Busy, guUserId);

                    if (fwdRule != null)
                    {
                        if (activate)
                        {
                            fwdRule.IsActive = "True";
                        }
                        else
                        {
                            fwdRule.IsActive = "False";
                        }

                        if (!String.IsNullOrEmpty(number))
                        {
                            fwdRule.UserProfile = number;
                        }
                        
                        AbstractBackendHandler.Instance.UpdateFwdRule(fwdRule);
                        
                    }
                }
                else if (fwdType.Equals("NoAnswer"))
                {
                    var fwdRule = AbstractBackendHandler.Instance.GetFwdRule(DisconnectReason.NoAnswer, guUserId);

                    if (fwdRule != null)
                    {
                        if (activate)
                        {
                            fwdRule.IsActive = "True";
                        }
                        else
                        {
                            fwdRule.IsActive = "False";
                        }

                        if (!String.IsNullOrEmpty(number))
                        {
                            fwdRule.UserProfile = number;
                        }
                        
                        AbstractBackendHandler.Instance.UpdateFwdRule(fwdRule);

                    }
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static ExtDetails GetExtensionDetailsForExtCompanyGuUserId(string guUserId, int companyId, int tenantId, string extension)
        {
            try
            {
                var result = AbstractBackendHandler.Instance.GetExtensionByGuUserIdAndCompanyAndExt(companyId, tenantId, guUserId, extension);

                if (result != null)
                {
                    return new ExtDetails
                    {
                        CompanyId = result.CompanyID,
                        GuUserId = result.GuUserId,
                        TenantId = result.TenantID,
                        Context = result.Context,
                        Ext = result.Extension,
                        ExtName = result.ExtName,
                        ExtType = (ExtType)result.ExtType
                    };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static SubscriberDetails GetSubscriberDetailsForGuUserId(string guUserId, int companyId, int tenantId)
        {
            try
            {
                var result = AbstractBackendHandler.Instance.GetSubscriberForGuUserId(guUserId, companyId, tenantId);

                if (result != null)
                {
                    return new SubscriberDetails
                    {
                        CompanyId = result.CompanyID.GetValueOrDefault(),
                        GuUserId = result.GuUserId,
                        Domain = result.Domain,
                        Password = result.Password,
                        TenantId = result.TenantID.GetValueOrDefault(),
                        Username = result.Username,
                        VoicemailActive = result.VoicemailActive
                    };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static SubscriberDetails GetSubscriberDetailsForDomain(string username, string domain, bool checkDomain)
        {
            try
            {
                var result = AbstractBackendHandler.Instance.GetSubscriberByUserAndDomain(username, domain, checkDomain);

                if (result != null)
                {
                    return new SubscriberDetails
                        {
                            CompanyId = result.CompanyID.GetValueOrDefault(),
                            GuUserId = result.GuUserId,
                            Domain = result.Domain,
                            Password = result.Password,
                            TenantId = result.TenantID.GetValueOrDefault(),
                            Username = result.Username,
                            VoicemailActive = result.VoicemailActive
                        };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static int FindIncomingTrunkId(int csId, string dnisNum, string domain, out int concCalls, out int companyId, out int tenantId, out CallType cType, out string ScheduleId)
        {
            try
            {
                var trNumList = AbstractBackendHandler.Instance.GetActiveTrunkPhoneNumbers(dnisNum);

                foreach (var trNum in trNumList)
                {
                    var tr = AbstractBackendHandler.Instance.GetActiveTrunkByTrId(trNum.TrunkID);

                    if (tr != null && tr.IPUrl == domain)
                    {
                        concCalls = trNum.NoConcDials;
                        companyId = trNum.CompanyID;
                        tenantId = trNum.TenantID;
                        cType = (CallType) trNum.CallType;
                        ScheduleId = trNum.ScheduleId;

                        return tr.TrunkKey;
                    }
                }

                throw new Exception("Trunk not found");


            }
            catch (Exception)
            {

                throw;
            }
        }

        public static DevCallRuleData GetInboundRule(string dnisNum, string aniNum, int trunkId, int companyId, int tenantId)
        {
            try
            {
                var callRule = AbstractBackendHandler.Instance.GetActiveCallRuleDetailsByAniDnisTrunkCompany(aniNum, dnisNum, 1, trunkId, companyId, tenantId);

                //var cr = (from i in callRule
                //          orderby i.Priority
                //          select i).FirstOrDefault();

                if (callRule != null)
                {

                    return new DevCallRuleData
                        {
                            CompanyId = callRule.CompanyID,
                            TenantId = callRule.TenantID,
                            Url = callRule.TargetScript,
                            CallType = callRule.CallType,
                            RuleParameter = callRule.Parameter
                        };
                }
                else
                {
                    logCSReq.Error(String.Format("Inbound rule not found : dnisNum : {0}, aniNum : {1}, trunkId : {2}", dnisNum, aniNum, trunkId));
                    return null;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        //public static DevCallRuleData CheckAndGetInboundRule(int csId, string ani, string dnis)
        //{
        //    try
        //    {
        //        logCSReq.Info(String.Format("Getting Inbound rule - csId : {0}, ani : {1}, dnis : {2}", csId, ani, dnis));

        //        var splitArrANI = ani.Split(new string[] { "@" }, 2, StringSplitOptions.None);

        //        string domain = "";
        //        string aniNum = "";

        //        if (splitArrANI.Length == 2)
        //        {
        //            domain = splitArrANI[1];
        //            aniNum = splitArrANI[0];
        //        }

        //        var splitArrDNIS = dnis.Split(new string[] { "@" }, 2, StringSplitOptions.None);

        //        string number = splitArrDNIS[0];

        //        var tempNum = number.Split(new char[] { '-' });

        //        number = tempNum[0];


        //        var trunkList = AbstractBackendHandler.Instance.GetTrunkListForServer(csId);

        //        var trunkIdList = trunkList.Select(i => i.TrunkID).ToList();

        //        if (trunkIdList.Count > 0)
        //        {
        //            var trunkRegDetails = AbstractBackendHandler.Instance.GetActiveTrunkList(trunkIdList);

        //            var matchingTrunkList = trunkRegDetails.Where(k => k.IPUrl == domain);

        //            var modifiedTrIdList = matchingTrunkList.Select(i => i.TrunkKey).ToList();

        //            var trPhoneNumbers = AbstractBackendHandler.Instance.GetActiveTrunkPhoneNumbers(number, modifiedTrIdList);

        //            if (trPhoneNumbers.Count > 0)
        //            {
        //                var trunkIdsListPhn = trPhoneNumbers.Select(i => i.TrunkID).ToList();

        //                var callRule = AbstractBackendHandler.Instance.GetActiveCallRuleDetailsByAniDnisTrunk(aniNum, number, 1, trunkIdsListPhn);

        //                var cr = (from i in callRule
        //                         orderby i.Priority
        //                         select i).FirstOrDefault();

        //                if (cr != null)
        //                {
        //                    var trPhnNumberPicked = trPhoneNumbers.FirstOrDefault(i => i.PhoneNumber == number && i.TrunkID == cr.TrunkID);

        //                    if (trPhnNumberPicked != null)
        //                    {
        //                        return new DevCallRuleData
        //                            {
        //                                CompanyId = cr.CompanyID,
        //                                TenantId = cr.TenantID,
        //                                Url = cr.TargetScript,
        //                                CallType = cr.CallType,
        //                                MaxCalls = trPhnNumberPicked.NoConcDials,
        //                                PhoneNumber = number,
        //                                RuleParameter = cr.Parameter
        //                            };
        //                    }
        //                    else
        //                    {
        //                        logCSReq.Info("Unable to pick a call limit for trunk number");
        //                    }
        //                }
        //                else
        //                {
        //                    logCSReq.Info("No call rule found with matching trunk id");
        //                }

        //            }
        //            else
        //            {
        //                logCSReq.Info("No trunk numbers found for trunks and given number");
        //            }
        //        }
        //        else
        //        {
        //            logCSReq.Info("No trunk found mapped for given call server call server");
        //        }

        //        return null;

        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public static List<ProfileInfo> GetMyProfileDetailsDb(int companyId, int tenantId)
        {
            try
            {
                var clsProfList = AbstractBackendHandler.Instance.GetNetworkProfilesForCompany(companyId, tenantId);

                var profileDetails = (from i in clsProfList
                                      select new ProfileInfo
                                      {
                                          ClusterId = i.ClusterID,
                                          InternalIp = i.ProfileIntIp,
                                          Port = i.Port,
                                          ProfileName = i.ProfileName,
                                          ProfileType = (ProfileType)i.ProfileType,
                                          CpId = i.CPID,
                                          ExternalIp = i.ProfileExtIp
                                      }).ToList();

                return profileDetails;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public static ContextInfo GetInternalPbxContextConf(int companyId, int tenantId)
        {
            try
            {
                var ctxtList = AbstractBackendHandler.Instance.GetContextDetailsForCompany(companyId, tenantId);

                var ctxt = ctxtList.FirstOrDefault(i => (ContextCat) i.ContextCat == ContextCat.InternalPbx);

                if (ctxt != null)
                {
                    return new ContextInfo
                        {
                            TenantId = ctxt.TenantID.GetValueOrDefault(),
                            CompanyId = ctxt.CompanyID.GetValueOrDefault(),
                            ContextCat = (ContextCat) ctxt.ContextCat,
                            ContextName = ctxt.ContextName,
                            MediaByPass = ctxt.BypassMedia,
                            ParkEnabled = ctxt.ParkEnabled,
                            QueueEnabled = ctxt.QueueEnabled,
                            ViewObjId = ctxt.ViewObjectID.GetValueOrDefault(),
                            VoicemailEnabled = ctxt.VoiceMailActive
                        };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static string GetInboundTrDetailsForOpensips(string number, string ipUrl, CallType numType)
        {
            try
            {
                var trNumList = AbstractBackendHandler.Instance.GetActiveTrunkPhoneNumbers(number);
                
                var tempNumbers = new List<CSDB_TrunkPhoneNumbers>();

                if (numType == CallType.Incoming)
                {
                    tempNumbers = trNumList.Where(i => (CallType)i.CallType == CallType.Incoming || (CallType)i.CallType == CallType.Both || (CallType)i.CallType == CallType.Fax).ToList();
                }
                else if(numType == CallType.Outgoing)
                {
                    tempNumbers = trNumList.Where(i => (CallType)i.CallType == CallType.Outgoing || (CallType)i.CallType == CallType.Both || (CallType)i.CallType == CallType.Fax).ToList();
                }

                foreach (var num in tempNumbers)
                {
                    var trunk = AbstractBackendHandler.Instance.GetActiveTrunkByTrId(num.TrunkID);

                    if (trunk != null && trunk.IPUrl == ipUrl)
                    {
                        if (numType == CallType.Incoming)
                        {
                            return String.Format("{0},{1}", num.NoConcDials, trunk.Domain);
                        }
                        else if (numType == CallType.Outgoing)
                        {
                            return String.Format("{0},{1}", num.NoConcDials, trunk.Domain);
                        }
                    }
                }

                return ",";

            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static CallServerInfo GetCallServerForTrunk(int trunkId)
        {
            try
            {
                var result = AbstractBackendHandler.Instance.GetServerForTrunk(trunkId);

                if (result != null)
                {
                    var csInfo = AbstractBackendHandler.Instance.GetCallServerInfo(result.ZoneOrCSID);

                    if (csInfo != null)
                    {
                        return new CallServerInfo
                            {
                                CsId = csInfo.CSKey,
                                CsName = csInfo.CSName
                            };
                    }
                    else
                    {
                        throw new Exception("No callserver found with given id");
                    }
                }
                else
                {
                    
                }

                return new CallServerInfo();
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}

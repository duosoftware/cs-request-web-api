﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using DuoSoftware.CommonTools.Security;
using DuoSoftware.DC.CSRequestWebApi.CSRequestDataStructures;
using DuoSoftware.DC.CallServerDBModels;
using DuoSoftware.DC.CallServerDBModelsOS;
using DuoSoftware.ObjectStore.Client.Extensions;
using DuoSoftware.ObjectStore.Common;
using GenerateReportDataObjStore.Classes;
using ObjectStoreOperators;
using CSDB_CCPresence = DuoSoftware.DC.CallServerDBModelsOS.CSDB_CCPresence;
using CSDB_CSProfiles = DuoSoftware.DC.CallServerDBModelsOS.CSDB_CSProfiles;
using CSDB_CallControllerMaster = DuoSoftware.DC.CallServerDBModelsOS.CSDB_CallControllerMaster;
using CSDB_CallRule = DuoSoftware.DC.CallServerDBModelsOS.CSDB_CallRule;
using CSDB_CallServerHoldMusic = DuoSoftware.DC.CallServerDBModelsOS.CSDB_CallServerHoldMusic;
using CSDB_CallServerRegMaster = DuoSoftware.DC.CallServerDBModelsOS.CSDB_CallServerRegMaster;
using CSDB_ConferenceMaster = DuoSoftware.DC.CallServerDBModelsOS.CSDB_ConferenceMaster;
using CSDB_ConferencePresence = DuoSoftware.DC.CallServerDBModelsOS.CSDB_ConferencePresence;
using CSDB_Context = DuoSoftware.DC.CallServerDBModelsOS.CSDB_Context;
using CSDB_EndpointsConf = DuoSoftware.DC.CallServerDBModelsOS.CSDB_EndpointsConf;
using CSDB_FollowMeConf = DuoSoftware.DC.CallServerDBModelsOS.CSDB_FollowMeConf;
using CSDB_Groups = DuoSoftware.DC.CallServerDBModelsOS.CSDB_Groups;
using CSDB_PBXConfiguration = DuoSoftware.DC.CallServerDBModelsOS.CSDB_PBXConfiguration;
using CSDB_PbxFeatureCodes = DuoSoftware.DC.CallServerDBModelsOS.CSDB_PbxFeatureCodes;
using CSDB_TrunkPhoneNumbers = DuoSoftware.DC.CallServerDBModelsOS.CSDB_TrunkPhoneNumbers;
using CSDB_TrunkRegMaster = DuoSoftware.DC.CallServerDBModelsOS.CSDB_TrunkRegMaster;
using CSDB_TrunkServerRegistration = DuoSoftware.DC.CallServerDBModelsOS.CSDB_TrunkServerRegistration;
using CSDB_UsrExtensions = DuoSoftware.DC.CallServerDBModelsOS.CSDB_UsrExtensions;
using CSDB_UsrGroup = DuoSoftware.DC.CallServerDBModelsOS.CSDB_UsrGroup;
using CSDB_subscriber = DuoSoftware.DC.CallServerDBModelsOS.CSDB_subscriber;
using GenericDBModelOpensips = DuoSoftware.DC.CallServerDBModelsOS.GenericDBModelOpensips;

namespace DuoSoftware.DC.CSRequestWebApi.CSRequestDataLayer
{
    internal class OSAccessor : AbstractBackendHandler
    {
        public override GroupInfo GetGroupDetails(int callServerId, string groupName, string domain)
        {
            try
            {

                var objStoreCliGrp = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_Groups>(ObjectStoreIdCall, new ConcteteStoreAuth());
                var objStoreCliUsrGrp = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_UsrGroup>(ObjectStoreIdCall, new ConcteteStoreAuth());
                var objStoreCliUsrExt = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_UsrExtensions>(ObjectStoreIdCall, new ConcteteStoreAuth());
                var objStoreCliSub = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_subscriber>(ObjectStoreIdCall, new ConcteteStoreAuth());

                //TODO: Check CS Belongs to Zone - if yes only username matching has to be done - no Comp, tenant matching


                CSDB_Groups grpRec;

                int companyId = -1;
                int tenantId = -1;

                if (bool.Parse(ConfigurationManager.AppSettings.Get("UseDomain")))
                {
                    grpRec = objStoreCliGrp.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.GroupName == groupName && i.Domain == domain));
                }
                else
                {
                    grpRec = objStoreCliGrp.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.GroupName == groupName));
                }

                if (grpRec != null)
                {
                    companyId = grpRec.CompanyID;
                    tenantId = grpRec.TenantID;

                    var dbUsrsList = objStoreCliUsrGrp.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.CompanyID == companyId && i.TenantID == tenantId && i.GroupName == groupName));

                    var dbUsrs = (from j in dbUsrsList
                                  select j.GuUserId).ToList();

                    var usrList = new List<UserProfileInfo>();

                    foreach (var usr in dbUsrs)
                    {
                        var usr1 = usr;

                        var dbUsrDetails = objStoreCliSub.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.GuUserId == usr1 && i.CompanyID == companyId && i.TenantID == tenantId));

                        if (dbUsrDetails != null)
                        {
                            var ext = objStoreCliUsrExt.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.GuUserId == usr1 && i.TenantID == tenantId && i.CompanyID == companyId && i.ExtType == 1 && i.Active == "True"));

                            if (ext != null)
                            {
                                logCSReq.Info("User profile found from db");
                                var usrProfile = new UserProfileInfo
                                    {
                                        Username = dbUsrDetails.Username,
                                        Password = dbUsrDetails.Password,
                                        Domain = domain,
                                        Context = ext.Context,
                                        Extension = ext.Extension
                                    };

                                usrList.Add(usrProfile);
                            }
                            else
                            {
                                throw new Exception("Extension data not found for user");
                            }
                        }
                        else
                        {
                            throw new Exception("No user found on db");
                        }
                    }

                    if (dbUsrs.Any())
                    {
                        logCSReq.Info(String.Format("Group : {0} found with users", groupName));
                        var grp = new GroupInfo
                            {
                                GroupName = groupName,
                                Domain = grpRec.Domain,
                                Users = usrList
                            };

                        return grp;
                    }
                    else
                    {
                        throw new Exception("No user found on db for group or group does not exist");
                    }
                }
                else
                {
                    throw new Exception("No group found with the given group name");
                }

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override ExtDetails GetExtensionDetails(string extension)
        {
            try
            {
                var objStoreCliUsrExt = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_UsrExtensions>(ObjectStoreIdCall, new ConcteteStoreAuth());
                
                var ext = objStoreCliUsrExt.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.Extension == extension && i.Active == "True"));

                if (ext != null)
                {
                    return new ExtDetails
                        {
                            Ext = ext.Extension,
                            ExtName = ext.ExtName,
                            ExtType = (ExtType)ext.ExtType,
                            CompanyId = ext.CompanyID,
                            TenantId = ext.TenantID,
                            Context = ext.Context,
                            GuUserId = ext.GuUserId,
                            DidNumber = ext.DidNumber,
                            DidEnabled = ext.DidActive
                        };
                }
                else
                {
                    logCSReq.Warn("Extension not found");
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override CSDB_subscriber GetSubDetailsForDomainCompany(string user, string domain, int companyId, int tenantId)
        {
            try
            {
                var objStoreCliSub = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_subscriber>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var sub = objStoreCliSub.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.Username == user && i.Domain == domain && i.CompanyID == companyId && i.TenantID == tenantId));

                return sub;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override List<ExtDetails> GetExtensionDetailsList(string extension)
        {
            try
            {
                var objStoreCliUsrExt = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_UsrExtensions>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var ext = objStoreCliUsrExt.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.Extension == extension && i.Active == "True")).Select(k => new ExtDetails
                    {
                        Ext = k.Extension,
                        ExtName = k.ExtName,
                        ExtType = (ExtType) k.ExtType,
                        CompanyId = k.CompanyID,
                        TenantId = k.TenantID,
                        Context = k.Context,
                        GuUserId = k.GuUserId,
                        DidNumber = k.DidNumber,
                        DidEnabled = k.DidActive
                    }).ToList();

                return ext;


            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override ExtDetails GetExtensionDetails(string extension, int companyId, int tenantId)
        {
            try
            {
                var objStoreCliUsrExt = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_UsrExtensions>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var ext = objStoreCliUsrExt.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.Extension == extension && i.CompanyID == companyId && i.TenantID == tenantId && i.Active == "True"));
                
                if (ext != null)
                {
                    return new ExtDetails
                        {
                            Ext = ext.Extension,
                            ExtName = ext.ExtName,
                            ExtType = (ExtType)ext.ExtType,
                            CompanyId = ext.CompanyID,
                            TenantId = ext.TenantID,
                            Context = ext.Context,
                            GuUserId = ext.GuUserId,
                            DidNumber = ext.DidNumber,
                            DidEnabled = ext.DidActive,
                            ExtParams = ext.ExtraData
                        };
                }
                else
                {
                    logCSReq.Warn("Extension not found");
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override ConferenceInfo GetConferenceRoomInfoByGuid(string roomGuid)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_ConferenceMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var conf = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByUniqueKey(roomGuid));

                if (conf != null)
                {
                    return new ConferenceInfo
                    {
                        OperationalStatus = conf.OperationalStatus,
                        CompanyId = conf.CompanyID,
                        TenantId = conf.TenantID,
                        RoomName = conf.ConfRoomName,
                        Pin = conf.Pin,
                        ConferenceType = conf.ConfType,
                        UUId = conf.UUID,
                        ViewObjId = conf.ViewObjectID
                    };
                }
                else
                {
                    logCSReq.Warn("Conference room not found");
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override ConferenceInfo GetActiveConferenceRoomInfo(string roomName, int companyId, int tenantId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_ConferenceMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());

                
                var confList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.ConfRoomName == roomName && i.CompanyID == companyId && i.TenantID == tenantId));

                var conf = confList.FirstOrDefault(i => i.OperationalStatus == "Active" && DateTime.Parse(i.StartTime) < DateTime.Now && DateTime.Parse(i.EndTime) > DateTime.Now);

                    if (conf != null)
                    {
                        return new ConferenceInfo
                        {
                            OperationalStatus = conf.OperationalStatus,
                            CompanyId = conf.CompanyID,
                            TenantId = conf.TenantID,
                            RoomName = conf.ConfRoomName,
                            Pin = conf.Pin,
                            ConferenceType = conf.ConfType,
                            Domain = conf.Domain,
                            AllowAnonymousUser = conf.AllowAnonymousUser,
                            ConferenceNumber = conf.ConferenceNumber,
                            EndTime = DateTime.Parse(conf.EndTime),
                            LockedStatus = conf.LockedStatus,
                            MaxUsers = conf.MaxUsers,
                            ObjGuid = conf.ObjGuid,
                            StartTime = DateTime.Parse(conf.StartTime),
                            UUId = conf.UUID
                        };
                    }
                    else
                    {
                        logCSReq.Warn("Conference room not found");
                        return null;
                    }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override List<ConferenceUser> GetConfUserInfo(string roomName, int companyId, int tenantId)
        {
            try
            {

                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_ConferencePresence>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var confList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.RoomName == roomName && i.CompanyID == companyId && i.TenantID == tenantId));

                var conf = (from i in confList
                            select new ConferenceUser
                            {
                                CompanyId = i.CompanyID,
                                RoomName = i.RoomName,
                                TenantId = i.TenantID,
                                Endpoint = i.Destination,
                                Crn = i.CRN,
                                CurrentDeafFlag = i.CurrentDeafFlag,
                                CurrentModFlag = i.CurrentModFlag,
                                CurrentMuteFlag = i.CurrentMuteFlag,
                                UserPin = i.UserPinID,
                                UserName = i.UserName,
                                Status = (ConferenceUserStatus)i.Status,
                                JoinType = (ConferenceJoinType)i.JoinType,
                                Protocol = (ConferenceEndpointProtocol)Enum.Parse(typeof(ConferenceEndpointProtocol), i.EndpointType),
                                IsConnected = i.IsConnected,
                                GuUserId = i.GuUserId
                            }).ToList();

                    return conf;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override void GetCompanyForUserDomain(string username, string domain, out int companyId, out int tenantId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_subscriber>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var compDetails = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.Domain == domain && i.Username == username));

                if (compDetails != null)
                {
                    if (compDetails.CompanyID != null && compDetails.TenantID != null)
                    {
                        companyId = (int)compDetails.CompanyID;
                        tenantId = (int)compDetails.TenantID;
                    }
                    else
                    {
                        throw new Exception("Cannot find a valid company or tenant - null");
                    }
                }
                else
                {
                    throw new Exception("Cannot find a company for the given domain and user");
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override GroupDetails GetGroupDetailsForCompany(string groupName, int companyId, int tenantId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_Groups>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var grp = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.GroupName == groupName && i.CompanyID == companyId && i.TenantID == tenantId));

                if (grp != null)
                {
                    return new GroupDetails
                        {
                            CompanyID = grp.CompanyID,
                            Domain = grp.Domain,
                            GroupDescription = grp.GroupDescription,
                            GroupName = grp.GroupName,
                            TenantID = grp.TenantID
                        };
                }
                else
                {
                    logCSReq.Warn("Group not found");
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override List<PbxFeatureCodes> GetPbxFeatureCodes(int companyId, int tenantId)
        {
            try
            {
                logCSReq.Info("Getting pbx feature codes....");

                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_PbxFeatureCodes>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var fclist = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.CompanyID == companyId && i.TenantID == tenantId));

                var featureConf = (from i in fclist
                                   orderby i.Priority ascending
                                   select new PbxFeatureCodes
                                     {
                                         CompanyID = i.CompanyID,
                                         TenantID = i.TenantID,
                                         ObjGuid = i.ObjGuid,
                                         FeatureCode = i.FeatureCode,
                                         FeatureType = (PbxFeatureCodeType)Enum.Parse(typeof(PbxFeatureCodeType), i.FeatureType)
                                     }).ToList();

                return featureConf;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override GroupDetails GetGroupDetailsForUser(string username, int companyId, int tenantId)
        {
            try
            {
                var objStoreCliUsrGrp = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_UsrGroup>(ObjectStoreIdCall, new ConcteteStoreAuth());
                var objStoreCliGrp = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_Groups>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var usrgrp = objStoreCliUsrGrp.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.Username == username && i.CompanyID == companyId && i.TenantID == tenantId));

                string grpName = "";
                if (usrgrp != null)
                {
                    grpName = usrgrp.GroupName;
                    var grp = objStoreCliGrp.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.GroupName == grpName && i.CompanyID == companyId && i.TenantID == tenantId));
                    
                    if (grp != null)
                    {
                        return new GroupDetails
                            {
                                CompanyID = grp.CompanyID,
                                Domain = grp.Domain,
                                GroupDescription = grp.GroupDescription,
                                GroupName = grp.GroupName,
                                TenantID = grp.TenantID
                            };
                    }
                    else
                    {
                        logCSReq.Info("Group doesnt exist");
                        return null;
                    }
                }
                else
                {
                    logCSReq.Info("User doesnt belong to a group");
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override SubscriberDetails GetSipSubDetails(string username, int companyId, int tenantId, string guUserid = "")
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_subscriber>(ObjectStoreIdCall, new ConcteteStoreAuth());

                CSDB_subscriber sub;

                if (string.IsNullOrEmpty(guUserid))
                {
                    sub = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.Username == username && i.CompanyID == companyId && i.TenantID == tenantId));
                }
                else
                {
                    sub = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.GuUserId == guUserid && i.CompanyID == companyId && i.TenantID == tenantId));
                }


                if (sub != null)
                    {
                        return new SubscriberDetails
                        {
                            Username = sub.Username,
                            Domain = sub.Domain,
                            Password = sub.Password,
                            TenantId = sub.TenantID.GetValueOrDefault(),
                            CompanyId = sub.CompanyID.GetValueOrDefault(),
                            VoicemailActive = sub.VoicemailActive,
                            GuUserId = sub.GuUserId
                        };
                    }
                    else
                    {
                        logCSReq.Warn("Subscriber not found");
                        return null;
                    }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override List<ExtDetails> GetExtensionDetailsForName(string extName, int companyId, int tenantId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_UsrExtensions>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var extList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.ExtName == extName && i.CompanyID == companyId && i.TenantID == tenantId));

                var ext = (from i in extList
                           select new ExtDetails
                               {
                                   Ext = i.Extension,
                                   ExtName = i.ExtName,
                                   ExtType = (ExtType) i.ExtType,
                                   CompanyId = i.CompanyID,
                                   TenantId = i.TenantID,
                                   Context = i.Context,
                                   DidEnabled = i.DidActive,
                                   DidNumber = i.DidNumber,
                                   GuUserId = i.GuUserId
                               }).ToList();

                return ext;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override XElement GetTranslationsFromDB(int transId, int companyId, int tenantId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<DuoSoftware.DC.CallServerDBModelsOS.CSDB_Translations>(ObjectStoreIdCall, new ConcteteStoreAuth());


                var transList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.TransID == transId && i.TenantID == tenantId && i.CompanyID == companyId));

                var transInfo = new XElement("Translations",
                                             (from trans in transList
                                              select new XElement("Translation",
                                                                  new XElement("ID", trans.TransID),
                                                                  new XElement("TransName", trans.TranslationName),
                                                                  new XElement("LAdd", trans.LAdd),
                                                                  new XElement("RAdd", trans.RAdd),
                                                                  new XElement("LRemove", trans.LRemove),
                                                                  new XElement("RRemove", trans.RRemove),
                                                                  new XElement("Replace", trans.Replace))).FirstOrDefault());

                return transInfo;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override CSDB_CallServerRegMaster GetCallServerInfo(int callServerId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CallServerRegMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var csRec = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.CSKey == callServerId));

                if (csRec != null)
                {
                    return csRec;
                }
                else
                {
                    throw new Exception(String.Format("Unable to find call server info for given call server id : {0}", callServerId));
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override void GetGwFromRule(int companyId, int tenantId, string numToMatch, string aniNumToMatch, out string trunkNum, out string trunkCode, out string trunkDomain, out TrunkClass trClass, out int timeout, out int transId, out int aniTransId, out string ipUrl, out int trunkId, out string targetScript)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CallRule>(ObjectStoreIdCall, new ConcteteStoreAuth());
                var objStoreCliTr = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_TrunkRegMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var outrulelist = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.CompanyID == companyId && i.TenantID == tenantId && i.RoutingDirection == 2 && i.CallType == 2));

                var orderedList = from j in outrulelist
                                  orderby j.Priority
                                  select j;

                CSDB_CallRule rule;
                if(!String.IsNullOrWhiteSpace(aniNumToMatch))
                {
                    rule = orderedList.FirstOrDefault(j => (Regex.IsMatch(numToMatch, j.DNISRegEx) && (Regex.IsMatch(aniNumToMatch, j.ANIRegEx))));
                }
                else
                {
                    rule = orderedList.FirstOrDefault(j => (Regex.IsMatch(numToMatch, j.DNISRegEx)));
                }

                if (rule != null)
                {
                    logCSReq.Debug(String.Format("Call rule picked (ObjGuid) : {0}", rule.ObjGuid));

                    var trunk = objStoreCliTr.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.TrunkKey == rule.TrunkID));

                    if (trunk != null)
                    {
                        trunkCode = trunk.TrunkCode;
                        trunkNum = rule.TrunkNumber;
                        trunkDomain = trunk.Domain;
                        trClass = (TrunkClass)trunk.TrunkClass;
                        timeout = rule.Timeout;
                        transId = rule.TranslationID;
                        aniTransId = rule.ANITranslationID;
                        ipUrl = trunk.IPUrl;
                        trunkId = trunk.TrunkKey;
                        targetScript = rule.TargetScript;

                        logCSReq.Debug(String.Format("Gateway picked - TrunkCode : {0}, TrunkNumber : {1}, TrunkDomain : {2}, TrunkClass : {3}, Timeout : {4}, IpUrl : {5}", trunkCode, trunkNum, trunkDomain, trClass, timeout, ipUrl));
                    }
                    else
                    {
                        throw new Exception("trunk not found");
                    }
                }
                else
                {
                    throw new Exception("No attemdant gw transfer rule found");
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override ExtDetails GetExtnsionDetailsForDid(string didNum, int companyId, int tenantId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_UsrExtensions>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var ext = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.DidNumber == didNum && i.Active == "True" && i.DidActive == "True" && i.CompanyID == companyId && i.TenantID == tenantId));

                if (ext != null)
                {
                    logCSReq.Info(String.Format("DID Number Mapping Found : DID Active : {0}, DID Number : {1}", ext.DidActive, ext.DidNumber));
                    
                    return new ExtDetails
                        {
                            Ext = ext.Extension,
                            ExtName = ext.ExtName,
                            ExtType = (ExtType)ext.ExtType,
                            CompanyId = ext.CompanyID,
                            TenantId = ext.TenantID,
                            Context = ext.Context,
                            GuUserId = ext.GuUserId,
                            DidEnabled = ext.DidActive,
                            DidNumber = ext.DidNumber,
                            DodEnabled = ext.DodActive,
                            DodNumber = ext.DodNumber,
                            ExtParams = ext.ExtraData
                        };
                }
                else
                {
                    logCSReq.Debug("DID Not Found");
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override List<CSDB_subscriber> GetCompanyForDomain(string domain)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_subscriber>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var subDetails = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.Domain == domain)).ToList();

                return subDetails;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override void GetCompanyForDomain(string domain, out int companyId, out int tenantId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_subscriber>(ObjectStoreIdCall, new ConcteteStoreAuth());
                
                var compDetails = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.Domain == domain));

                if (compDetails != null)
                {
                    if (compDetails.CompanyID != null && compDetails.TenantID != null)
                    {
                        companyId = (int) compDetails.CompanyID;
                        tenantId = (int) compDetails.TenantID;
                    }
                    else
                    {
                        throw new Exception("Cannot find a valid company or tenant - null");
                    }
                }
                else
                {
                    throw new Exception("Cannot find a company for the given domain");
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override List<CPProfiles> GetCpProfileForCallServer(int cpId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CSProfiles>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var profLst = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.CPID == cpId));


                var profList = (from i in profLst
                                select new CPProfiles
                                    {
                                        CPID = i.CPID,
                                        TenantID = i.TenantID,
                                        CompanyID = i.CompanyID,
                                        ClusterID = i.ClusterID,
                                        Port = i.Port,
                                        ProfileExtIp = i.ProfileExtIp,
                                        ProfileIntIp = i.ProfileIntIp,
                                        ProfileName = i.ProfileName,
                                        ProfileType = (ProfileType) i.ProfileType
                                    }).ToList();

                return profList;
            }
            catch (Exception ex)
            {

                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override UserProfileInfo GetDirectoryProfileDetails(int callServerId, string username, string domain)
        {
            try
            {
                var objStoreCliCsReg = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CallServerRegMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());
                var objStoreCliSub = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_subscriber>(ObjectStoreIdCall, new ConcteteStoreAuth());
                var objStoreCliExt = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_UsrExtensions>(ObjectStoreIdCall, new ConcteteStoreAuth());


                var csList = objStoreCliCsReg.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.CSKey == callServerId && i.CSStatus == 2 && i.RecordStatus == 1 && i.OperationalStatus == "Active"));

                var dbCheckCs = (from i in csList
                                 select new {CompanyId = i.CompanyID, TenantId = i.TenantID}).FirstOrDefault();

                if (dbCheckCs != null)
                {

                    logCSReq.Info(String.Format("Call server is a valid call server - CallServerId : {0}, CompanyID : {1}, TenantID : {2}", callServerId, dbCheckCs.CompanyId, dbCheckCs.TenantId));

                    //var dbUsrDetails = (from i in oe.Subscribers
                    //                    where i.Domain == domain && i.Username == username
                    //                    select new {Username = i.Username, Password = i.Password, Domain = i.Domain, CompanyID = i.CompanyID, TenantID = i.TenantID}).FirstOrDefault();

                    //var dbUsrDetails = (from i in oe.Subscribers
                    //                    where i.Username == username
                    //                    select new { Username = i.Username, Password = i.Password, Domain = i.Domain, CompanyID = i.CompanyID, TenantID = i.TenantID }).FirstOrDefault();

                    var subList = objStoreCliSub.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.Username == username));

                    var tempDbUsrDetails = from i in subList
                                           select new {Username = i.Username, Password = i.Password, Domain = i.Domain, CompanyID = i.CompanyID, TenantID = i.TenantID, Email = i.Email_address};

                    if (bool.Parse(ConfigurationManager.AppSettings.Get("UseDomain")))
                    {
                        tempDbUsrDetails = tempDbUsrDetails.Where(j => j.Domain == domain);
                    }

                    var dbUsrDetails = tempDbUsrDetails.FirstOrDefault();


                    if (dbUsrDetails != null)
                    {
                        int tempTenant = dbUsrDetails.TenantID.Value;
                        int tempCompany = dbUsrDetails.CompanyID.Value;

                        var ext = objStoreCliExt.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.ExtName == username && i.TenantID == tempTenant && i.CompanyID == tempCompany && i.ExtType == 1 && i.Active == "True"));

                        if (ext != null)
                        {
                            logCSReq.Info("User profile found from db");
                            var usrProfile = new UserProfileInfo
                                {
                                    Username = dbUsrDetails.Username,
                                    Password = dbUsrDetails.Password,
                                    Domain = domain,
                                    Context = ext.Context,
                                    Extension = ext.Extension,
                                    EmailAddr = dbUsrDetails.Email
                                };

                            return usrProfile;
                        }
                        else
                        {
                            throw new Exception("Extension data not found for user");
                        }
                    }
                    else
                    {
                        throw new Exception("No user found on db");
                    }
                }
                else
                {
                    throw new Exception("Invalid call server or callserver is not private or call server is not activated");
                }

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override List<TrunkInfo> GetSipGatewayDetailsForCallServer(int callServerId, string profile = "")
        {
            try
            {
                var objStoreCliCsReg = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CallServerRegMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());
                var objStoreCliTsr = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_TrunkServerRegistration>(ObjectStoreIdCall, new ConcteteStoreAuth());
                var objStoreCliTrunk = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_TrunkRegMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());
                

                var csList = objStoreCliCsReg.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.CSKey == callServerId && i.CSStatus == 2 && i.RecordStatus == 1 && i.OperationalStatus == "Active"));

                var dbCheckCs = (from i in csList
                                 select new {CompanyId = i.CompanyID, TenantId = i.TenantID, SipServer = i.SipServer}).FirstOrDefault();

                if (dbCheckCs != null)
                {
                    var tsrList = objStoreCliTsr.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.ZoneOrCSID == callServerId && i.CloudType == 2));
                    var trList = objStoreCliTrunk.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.Profile == profile && (i.RegDirection == 0 || i.RegDirection == 2) && i.RecordStatus == 1 && i.OperationalStatus == "Active"));
                    //var csLst = objStoreCliCsReg.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByGettingAll());

                    var result = from k in tsrList
                                 //join i in csLst on k.ZoneOrCSID equals i.CSKey
                                 join l in trList on k.TrunkID equals l.TrunkKey
                                 select l;


                    return result.Select(rec => new TrunkInfo
                        {
                            Domain = rec.Domain,
                            IpUrl = rec.IPUrl,
                            Password = rec.Password,
                            RegDir = (RegistrationDirection) rec.RegDirection,
                            TrunkCode = rec.TrunkCode,
                            Username = rec.Username,
                            ExpireSeconds = rec.ExpireSeconds,
                            RetrySeconds = rec.RetrySeconds,
                            PingSeconds = rec.PingSeconds,
                            SipServer = dbCheckCs.SipServer
                        }).ToList();
                }
                else
                {
                    throw new Exception("Invalid call server or call server not activated");
                }

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }
                throw;
            }
        }

        public override CallControllerInfo GetSpecificCallController(int ccId)
        {
            try
            {
                var objStoreCliCc = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CallControllerMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());
                var objStoreCliCcPres = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CCPresence>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var ccList = objStoreCliCc.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.CCID == ccId && i.Active == "True"));
                var ccPresList = objStoreCliCcPres.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.CCID == ccId && i.Availability == "True"));

                var cc = (from i in ccList
                          join j in ccPresList on i.CCID equals j.CCID
                          select new CallControllerInfo
                              {
                                  CallControllerId = i.CCID,
                                  TcpIp = i.CCTcpIp,
                                  TcpPort = i.CCTcpPort
                              }).FirstOrDefault();

                return cc;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override List<CallControllerInfo> GetAvailableCallControllers(int callServerId)
        {
            try
            {
                var objStoreCliCc = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CallControllerMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());
                var objStoreCliCcPres = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CCPresence>(ObjectStoreIdCall, new ConcteteStoreAuth());
                var objStoreCliCsReg = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CallServerRegMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());
                
                var cs = objStoreCliCsReg.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.CSKey == callServerId && i.RecordStatus == 1 && i.OperationalStatus == "Active" && i.CSStatus == 2));

                if (cs != null)
                {
                    var ccList = objStoreCliCc.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.ClusterID == cs.Zone && i.Active == "True"));
                    var ccPresList = objStoreCliCcPres.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.Availability == "True" && i.State == 1));

                    var callCtrls = from k in ccList
                                    join l in ccPresList on k.CCID equals l.CCID
                                    where k.MaxCapacity > l.CurrentCapacity
                                    select new
                                        {
                                            CCID = k.CCID,
                                            TcpIp = k.CCTcpIp,
                                            TcpPort = k.CCTcpPort,
                                            MaxCapacity = k.MaxCapacity,
                                            CurrentCapacity = l.CurrentCapacity,
                                            CurrentState = l.State,
                                            LastReqTimeStamp = l.RequestTimeStamp,
                                            EnableSpecificComp = k.EnableSpecificCompany,
                                            SpecificCompany = k.SpecificCompanyId,
                                            SpecificTenant = k.SpecificTenantId
                                        };


                    return callCtrls.Select(rec => new CallControllerInfo
                        {
                            CallControllerId = rec.CCID,
                            CurrentCapacity = rec.CurrentCapacity,
                            CurrentState = rec.CurrentState,
                            TcpIp = rec.TcpIp,
                            MaxCapacity = rec.MaxCapacity,
                            TcpPort = rec.TcpPort,
                            LastReqTimeStamp = rec.LastReqTimeStamp,
                            EnableSpecificCompany = rec.EnableSpecificComp,
                            SpecificCompanyId = rec.SpecificCompany,
                            SpecificTenantId = rec.SpecificTenant
                        }).ToList();


                }

                return new List<CallControllerInfo>();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override void UpdateCcReqTimeStamp(int callControllerId)
        {
            try
            {
                var objStoreCliCcPres = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CCPresence>(ObjectStoreIdCall, new ConcteteStoreAuth());
                
                var ccPresence = objStoreCliCcPres.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.CCID == callControllerId));

                if (ccPresence != null)
                {
                    ccPresence.RequestTimeStamp = DateTime.Now;

                    objStoreCliCcPres.Store().ThisWay(i => i.SkipCompanyValidation().UsingPrimaryKeyProperty(j => j.ObjGuid).AndUpdate(ccPresence));
                }
                else
                {
                    logCSReq.Warn(String.Format("Unable to find a call controller : {0} with given id to update time stamp", callControllerId));
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override PbxConfigurations GetPbxConfigurations(string guUserId, int companyId, int tenantId)
        {
            try
            {
                logCSReq.Info("Getting pbx configurations....");
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_PBXConfiguration>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var pbxConf = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.GuUserId == guUserId && i.CompanyID == companyId && i.TenantID == tenantId));
                
                if (pbxConf != null)
                {
                    return new PbxConfigurations
                        {
                            ActiveApp = (ActApp) pbxConf.ActiveApp,
                            CompanyId = pbxConf.CompanyID,
                            TenantId = pbxConf.TenantID,
                            UserId = pbxConf.UserID,
                            UsrStatus = (PBXUsrStatus) Enum.Parse(typeof(PBXUsrStatus), pbxConf.Status),
                            DefaultFwdEp = pbxConf.DefaultFwdEndpoint,
                            UOMVal = pbxConf.UOMValue,
                            FollowMeRingType = pbxConf.FollowMeRingType,
                            ParkEnabled = pbxConf.ParkEnable,
                            QueueEnabled = pbxConf.QueueEnable,
                            ScheduleId = pbxConf.ScheduleId
                        };
                }
                else
                {
                    logCSReq.Info("No context found - returning null");
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                return null;
            }
        }


        public override bool UpdateConferenceRoom(string objGuid, Dictionary<string, object> updateData)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_ConferenceMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var roomInfo = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByUniqueKey(objGuid));

                if (roomInfo != null)
                {
                    object uuid;
                    roomInfo.RecUpdateTime = DateTime.Now.ToString();
                    roomInfo.RecUpdateUser = "CSRequestWebApi";

                    if (updateData.TryGetValue("UUID", out uuid))
                    {
                        roomInfo.UUID = uuid.ToString();
                    }

                    objStoreCli.Store().ThisWay(i => i.SkipCompanyValidation().UsingPrimaryKeyProperty(j => j.ObjGuid).AndUpdate(roomInfo));

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override bool UpdateConfPressence(string roomName, string userName, int companyId, int tenantId, Dictionary<string, object> updateData)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_ConferencePresence>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var confUsrInfo = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.RoomName == roomName && i.UserName == userName && i.CompanyID == companyId && i.TenantID == tenantId));

                if (confUsrInfo != null)
                {
                    object uuid;
                    object status;
                    object isConn;
                    object actTalker;
                    confUsrInfo.RecUpdateTime = DateTime.Now.ToString();
                    confUsrInfo.RecUpdateUser = "CSRequestWebApi";

                    if (updateData.TryGetValue("UUID", out uuid))
                    {
                        confUsrInfo.UUID = uuid.ToString();
                    }

                    if (updateData.TryGetValue("Status", out status))
                    {
                        confUsrInfo.Status = ((ConferenceUserStatus) status).GetHashCode();
                    }

                    if (updateData.TryGetValue("IsConnected", out isConn))
                    {
                        confUsrInfo.IsConnected = (int)isConn;
                    }

                    if (updateData.TryGetValue("ActiveTalker", out actTalker))
                    {
                        confUsrInfo.ActiveTalker = bool.Parse(actTalker.ToString());
                    }

                    confUsrInfo.Status = ConferenceUserStatus.Validated.GetHashCode();

                    objStoreCli.Store().ThisWay(i => i.SkipCompanyValidation().UsingPrimaryKeyProperty(j => j.ObjGuid).AndUpdate(confUsrInfo));

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                

                logCSReq.Error("Exception occurred while updating conference pressence", ex);
                throw;
            }
        }

        public override bool AddConferenceHistory(int companyId, int tenantId, string confStartTime, string confEndTime, string roomName, string confUuid, string connectedUsrs, string notConnUsers, int viewObjId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_ConferenceHistoryData>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var confHistory = new CSDB_ConferenceHistoryData
                    {
                        CompanyID = companyId,
                        TenantID = tenantId,
                        ConferenceEndTime = confEndTime,
                        ConferenceStartTime = confStartTime,
                        RecAddUser = "CSRequestWebApi",
                        RecAddTime = DateTime.Now.ToString(),
                        ObjClass = "CallServerInfo",
                        ObjType = "Conference",
                        ObjCategory = "ConferenceHistory",
                        ConferenceRoomName = roomName,
                        ConferenceUuid = confUuid,
                        ConnectedUsers = connectedUsrs,
                        ObjGuid = CommonTools.Common.NewGUID(),
                        NotConnectedUsers = notConnUsers,
                        GuVersionId = CommonTools.Common.NewGUID(),
                        ViewObjectID = viewObjId
                    };

                objStoreCli.Store().ThisWay(i => i.SkipCompanyValidation().UsingPrimaryKeyProperty(j => j.ObjGuid).AndInsert(confHistory));

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                logCSReq.Error("Exception occurred while adding conference history", ex);
                throw;
            }
        }

        public override bool DeleteConferenceRoom(string objGuid)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_ConferenceMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());

                objStoreCli.Delete().ThisWay(i => i.Deleting(objGuid));

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                logCSReq.Error("Exception occurred while deleting conference room", ex);
                throw;
            }
        }

        public override bool DeleteConferenceUsers(List<string> objGuidList)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_ConferencePresence>(ObjectStoreIdCall, new ConcteteStoreAuth());

                string tempObj = "";
                foreach (var obj in objGuidList)
                {
                    tempObj = obj;
                    objStoreCli.Delete().ThisWay(i => i.Deleting(tempObj));
                }

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                logCSReq.Error("Exception occurred while updating conference pressence", ex);
                throw;
            }
        }

        public override List<FollowMeConf> GetFollowMeConf(string guUserId, int companyId, int tenantId)
        {
            try
            {
                logCSReq.Info("Getting follow me configurations....");

                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_FollowMeConf>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var fmConf = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.GuUserId == guUserId && i.CompanyID == companyId && i.TenantID == tenantId));

                var followMeConfList = (from i in fmConf
                                        orderby i.Priority ascending
                                        select new FollowMeConf
                                        {
                                            CompanyId = i.CompanyID,
                                            TenantId = i.TenantID,
                                            EpType = i.FmEpType,
                                            UsrProfile = i.UserProfile,
                                            Priority = i.Priority,
                                            UomVal = i.UOMValue,
                                            UsrExtension = i.Extension,
                                            
                                        }).ToList();

                return followMeConfList;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                logCSReq.Error("Exception occurred while getting context category - context returned as Public", ex);
                return null;
            }
        }

        public override ContextInfo GetContextCategory(string context)
        {
            try
            {
                logCSReq.Info("Getting context category");
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_Context>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var ctxt = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.ContextName == context));

                if (ctxt != null)
                {
                    logCSReq.Info(String.Format("Context found - category {0}", (ContextCat)ctxt.ContextCat));
                    return new ContextInfo
                        {
                            CompanyId = ctxt.CompanyID.GetValueOrDefault(),
                            TenantId = ctxt.TenantID.GetValueOrDefault(),
                            ContextCat = (ContextCat)ctxt.ContextCat,
                            ContextName = ctxt.ContextName,
                            ViewObjId = ctxt.ViewObjectID.GetValueOrDefault(),
                            VoicemailEnabled = ctxt.VoiceMailActive,
                            MediaByPass = ctxt.BypassMedia,
                            ParkEnabled = ctxt.ParkEnabled,
                            QueueEnabled = ctxt.QueueEnabled,
                            IgnoreEarlyMedia = ctxt.IgnoreEarlyMedia
                        };
                }
                else
                {
                    logCSReq.Info("No context found - returning null");
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                logCSReq.Error("Exception occurred while getting context category - context returned as Public", ex);
                return null;
            }

        }

        //public override EndpointsConf GetEndpointConf(string epGuid, int companyId, int tenantId)
        //{
        //    try
        //    {
        //        logCSReq.Info("Getting endpoint configurations....");

        //        var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_EndpointsConf>(ObjectStoreIdCall, new ConcteteStoreAuth());

        //        var epConf = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.ObjGuid == epGuid && i.CompanyID == companyId && i.TenantID == tenantId));


        //        if (epConf != null)
        //        {
        //            return new EndpointsConf
        //                {
        //                    Destination = epConf.Destination,
        //                    Domain = epConf.Domain,
        //                    EpProtocol = (Protocol)Enum.Parse(typeof(Protocol), epConf.Protocol),
        //                    ObjGuid = epConf.ObjGuid,
        //                    Profile = epConf.Profile,
        //                    EpType = (DnisType)Enum.Parse(typeof(DnisType), epConf.EndpointType),
        //                    CompanyId = epConf.CompanyID,
        //                    TenantId = epConf.TenantID
        //                };
        //        }

        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.InnerException != null)
        //        {
        //            logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
        //        }
        //        else
        //        {
        //            logCSReq.Error("Backend Error Occurred : ", ex);
        //        }

        //        logCSReq.Error("Exception occurred while getting endpoint configurations - ", ex);
        //        return null;
        //    }
        //}

        #region Context

        public override List<CSDB_Context> GetContextDetailsForCompany(int companyId, int tenantId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_Context>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var ctxtList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.CompanyID == companyId && i.TenantID == tenantId));

                return ctxtList.ToList();

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        #endregion

        #region Cdr Operations

        public override bool SaveRawCallCdrInfo(CallCdrInfo cdrRaw)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CallCdrInfo>(ObjectStoreIdCDR, new ConcteteStoreAuth());

                objStoreCli.Store().ThisWay(i => i.SkipCompanyValidation().UsingPrimaryKeyProperty(j => j.CallId).AndInsert(cdrRaw));

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                logCSReq.Error("Exception occurred while saving call raw cdr info - ", ex);
                throw;
            }
            
        }

        public override bool SaveReportCdrInfo(CallRelatedInfo cdrRep)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CallRelatedInfo>(ObjectStoreIdCDR, new ConcteteStoreAuth());

                objStoreCli.Store().ThisWay(i => i.SkipCompanyValidation().UsingPrimaryKeyProperty(j => j.SessionId).AndInsert(cdrRep));

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                logCSReq.Error("Exception occurred while saving call raw cdr info - ", ex);
                throw;
            }
        }

        public override bool SaveProcessedCdrInfo(CallCdrInfoProcessed cdrProcessed)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CallCdrInfoProcessed>(ObjectStoreIdCDR, new ConcteteStoreAuth());

                objStoreCli.Store().ThisWay(i => i.SkipCompanyValidation().UsingPrimaryKeyProperty(j => j.CallId).AndInsert(cdrProcessed));

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                logCSReq.Error("Exception occurred while saving call raw cdr info - ", ex);
                throw;
            }
        }

        #endregion

        #region CallRule

        public override CSDB_CallRule GetActiveCallRuleDetailsByAniDnisTrunkCompany(string ani, string dnis, int direction, int trunkId, int companyId, int tenantId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CallRule>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var crList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.Enable == "True" && i.CompanyID == companyId && i.TenantID == tenantId && i.RoutingDirection == direction && i.TrunkID == trunkId));

                var cr = (from i in crList
                          where (Regex.IsMatch(dnis, i.DNISRegEx)) && (Regex.IsMatch(ani, i.ANIRegEx))
                          orderby i.Priority
                          select i).FirstOrDefault();

                return cr;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        //public override List<CSDB_CallRule> GetActiveCallRuleDetailsByAniDnisTrunk(string ani, string dnis, int direction, List<int> trunkIds = null)
        //{
        //    try
        //    {
        //        var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CallRule>(ObjectStoreIdCall, new ConcteteStoreAuth());

        //        IEnumerable<CSDB_CallRule> crList;

        //        if (trunkIds == null)
        //        {
        //            crList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.Enable == "True" && i.RoutingDirection == direction));
        //        }
        //        else
        //        {
        //            crList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.Enable == "True" && i.RoutingDirection == direction).FilterContains(k => k.TrunkID, BooleanOperator.AND, trunkIds));
        //        }

        //        return crList.Where(i => (Regex.IsMatch(dnis, i.DNISRegEx)) && (Regex.IsMatch(ani, i.ANIRegEx))).ToList();

        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.InnerException != null)
        //        {
        //            logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
        //        }
        //        else
        //        {
        //            logCSReq.Error("Backend Error Occurred : ", ex);
        //        }

        //        throw;
        //    }
        //}

        #endregion

        #region Trunk

        public override List<CSDB_TrunkServerRegistration> GetTrunkListForServer(int callServerId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_TrunkServerRegistration>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var trunkServList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.ZoneOrCSID == callServerId)).ToList();

                return trunkServList;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override CSDB_TrunkServerRegistration GetServerForTrunk(int trunkId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_TrunkServerRegistration>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var trunkServ = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.TrunkID == trunkId));

                return trunkServ;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override CSDB_TrunkServerRegistration GetServerForTrunk(int trunkId, int companyId, int tenantId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_TrunkServerRegistration>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var trunkServ = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.TrunkID == trunkId && i.CompanyID == companyId && i.TenantID == tenantId));

                return trunkServ;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override CSDB_TrunkRegMaster GetActiveTrunkByTrId(int trunkId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_TrunkRegMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var tr = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.RecordStatus == 1 && i.OperationalStatus == "Active" && i.TrunkKey == trunkId));

                return tr;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        //public override List<CSDB_TrunkRegMaster> GetActiveTrunkList(List<int> trunkIds = null)
        //{
        //    try
        //    {
        //        var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_TrunkRegMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());

        //        IEnumerable<CSDB_TrunkRegMaster> trList;

        //        if (trunkIds == null)
        //        {
        //            trList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.RecordStatus == 1 && i.OperationalStatus == "Active"));
        //        }
        //        else
        //        {
        //            trList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.RecordStatus == 1 && i.OperationalStatus == "Active").FilterContains(k => k.TrunkKey, BooleanOperator.AND, trunkIds));
        //        }

        //        return trList.ToList();

        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.InnerException != null)
        //        {
        //            logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
        //        }
        //        else
        //        {
        //            logCSReq.Error("Backend Error Occurred : ", ex);
        //        }

        //        throw;
        //    }
        //}

        public override CSDB_TrunkRegMaster GetActiveTrunkByCode(string trunkCode, List<int> trunkIds)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_TrunkRegMaster>(ObjectStoreIdCall, new ConcteteStoreAuth());

                if (trunkIds.Count > 0)
                {
                    var trunk = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.TrunkCode == trunkCode && i.RecordStatus == 1 && i.OperationalStatus == "Active").FilterContains(k => k.TrunkKey, BooleanOperator.AND, trunkIds));

                    return trunk;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override bool CheckTrunkServerConnection(int callServerId, int trunkId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_TrunkServerRegistration>(ObjectStoreIdCall, new ConcteteStoreAuth());

                
                var trSrvCon = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.TrunkID == trunkId && i.ZoneOrCSID == callServerId));

                if (trSrvCon != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override List<CSDB_TrunkPhoneNumbers> GetActiveTrunkPhoneNumbers(string phoneNum)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_TrunkPhoneNumbers>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var trList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.IsActive == "Active" && i.PhoneNumber == phoneNum));
                
                return trList.ToList();

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}, Stack Trace : {1}", ex.InnerException.Message, ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override List<CSDB_TrunkPhoneNumbers> GetTrunkPhoneNumbersForCompany(int companyId, int tenantId, string trunkNum)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_TrunkPhoneNumbers>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var trPhList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.IsActive == "Active" && i.PhoneNumber == trunkNum && i.TenantID == tenantId && i.CompanyID == companyId));
                
                return trPhList.ToList();

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        #endregion

        #region Subscriber

        public override CSDB_subscriber GetSubscriberByUserAndDomain(string username, string domain, bool checkDomain)
        {
            try
            {
                //string tempUsername = "agent5";
                //string tempDomain = "192.168.2.89";
                //tempUsername = username;
                //tempDomain = domain;

                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_subscriber>(ObjectStoreIdCall, new ConcteteStoreAuth());

                CSDB_subscriber sub;
                if (checkDomain)
                {
                    sub = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.Username == username && i.Domain == domain));
                }
                else
                {
                    sub = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().CorrectTokanization().ByFiltering(i => i.Username == username));
                }
                

                //var subList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.Username == tempUsername));

                //var sub = subList.FirstOrDefault(i => i.Domain == domain);
                
                return sub;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override CSDB_subscriber GetSubscriberForGuUserId(string guUserId, int company, int tenant)
        {
            try
            {
                //string tempUsername = "agent5";
                //string tempDomain = "192.168.2.89";
                //tempUsername = username;
                //tempDomain = domain;

                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_subscriber>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var sub = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.GuUserId == guUserId && i.CompanyID == company && i.TenantID == tenant));

                //var subList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.Username == tempUsername));

                //var sub = subList.FirstOrDefault(i => i.Domain == domain);

                return sub;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        #endregion

        #region UsrExtension

        public override CSDB_UsrExtensions GetExtensionByGuUserIdAndCompanyAndExt(int companyId, int tenantId, string guUserId, string extension)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_UsrExtensions>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var ext = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.GuUserId == guUserId && i.CompanyID == companyId && i.TenantID == tenantId && i.Extension == extension && i.Active == "True"));

                return ext;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        public override CSDB_UsrExtensions GetExtensionByGuUserIdAndCompany(int companyId, int tenantId, string guUserId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_UsrExtensions>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var ext = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.GuUserId == guUserId && i.CompanyID == companyId && i.TenantID == tenantId && i.Active == "True"));

                return ext;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        #endregion

        #region Network Profile

        public override List<CSDB_CSProfiles> GetNetworkProfilesForCompany(int companyId, int tenantId)
        {
            try
            {
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CSProfiles>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var clsProfList = objStoreCli.Get().Many(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.CompanyID == companyId && i.TenantID == tenantId));

                return clsProfList.ToList();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                throw;
            }
        }

        

        #endregion

        #region CSSchedule

        public override List<CSDB_CSSchedule> GetAppointmentsForSchedule(string scheduleId)
        {
            try
            {
                var objStoreCliSch = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CSScheduleAppointmentMap>(ObjectStoreIdCall, new ConcteteStoreAuth());

                var schedule = objStoreCliSch.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByUniqueKey(scheduleId));

                var schList = new List<CSDB_CSSchedule>();

                if (schedule != null && schedule.AppointmentIds != null)
                {
                    var appointments = schedule.AppointmentIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                    if (appointments.Length > 0)
                    {
                        var objStoreCliApp = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_CSSchedule>(ObjectStoreIdCall, new ConcteteStoreAuth());

                        foreach (var appointment in appointments)
                        {
                            var appntmnt = objStoreCliApp.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByUniqueKey(appointment));

                            if (appntmnt != null)
                            {
                                schList.Add(appntmnt);
                            }
                        }
                    }
                    


                }

                return schList;

            }
            catch (Exception)
            {
                
                throw;
            }
        }

        #endregion

        #region PBX Config

        public override CSDB_PBXConfiguration GetPbxConfigurations(string guUserId)
        {
            try
            {
                logCSReq.Info("Getting pbx configurations....");
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_PBXConfiguration>(ObjectStoreIdCall, new ConcteteStoreAuth());

                CSDB_PBXConfiguration pbxConf = null;
                pbxConf = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByUniqueKey(guUserId));

                if (pbxConf == null)
                {
                    pbxConf = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.GuUserId == guUserId));
                }

                if (pbxConf != null)
                {
                    return pbxConf;
                }
                else
                {
                    logCSReq.Info("No pbx configuration found - returning null");
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                logCSReq.Error("Exception occurred while getting context category - context returned as Public", ex);
                return null;
            }
        }

        public override bool UpdatePbxConfigurations(CSDB_PBXConfiguration pbx)
        {
            try
            {
                logCSReq.Info("Updating pbx configurations....");
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CSDB_PBXConfiguration>(ObjectStoreIdCall, new ConcteteStoreAuth());

                objStoreCli.Store().ThisWay(i => i.SkipCompanyValidation().UsingPrimaryKeyProperty(j => j.ObjGuid).AndUpdate(pbx));

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.StackTrace), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                return false;
            }
        }

        public override CallServerDBModelsOS.CSDB_Forwarding GetFwdRule(DisconnectReason reason, string guUserId)
        {
            try
            {
                logCSReq.Info("Get forwarding configurations....");
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CallServerDBModelsOS.CSDB_Forwarding>(ObjectStoreIdCall, new ConcteteStoreAuth());

                int fwdReason = reason.GetHashCode();

                var fwd = objStoreCli.Get().One(j => j.FromObjectServer().SkipCompanyValidation().ByFiltering(i => i.GuUserId == guUserId && i.Reason == fwdReason));

                return fwd;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                return null;
            }
        }

        public override bool UpdateFwdRule(CallServerDBModelsOS.CSDB_Forwarding fwd)
        {
            try
            {
                logCSReq.Info("Updating Fwd configurations....");
                var objStoreCli = new DuoSoftware.ObjectStore.Client.ObjectStoreClient<CallServerDBModelsOS.CSDB_Forwarding>(ObjectStoreIdCall, new ConcteteStoreAuth());

                objStoreCli.Store().ThisWay(i => i.SkipCompanyValidation().UsingPrimaryKeyProperty(j => j.ObjGuid).AndUpdate(fwd));

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    logCSReq.Error(String.Format("Backend Error Occurred : {0}", ex.InnerException.Message), ex.InnerException);
                }
                else
                {
                    logCSReq.Error("Backend Error Occurred : ", ex);
                }

                return false;
            }
        }

        #endregion
    }
}
